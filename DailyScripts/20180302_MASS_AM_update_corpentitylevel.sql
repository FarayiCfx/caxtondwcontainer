--mass update to corp entities 
--backup the corp entities table 
select		* 
into		CorporateEntities_backup_02032018
from		CorporateEntities

--use wingfield as an examle 
select AccountContactRef
		,*	
from	CorporateEntities 
where	CompanyName like 'wingf%'
 
 --OK so its the accountcontactref field
 update CorporateEntities
 set	AccountContactRef = 'Ben Blacker'
 where	CompanyName like 'wingf%'
 
select * from UserCorporateAssociation

--imported table 
select * from newsalesam --331 rows 

---update existing records 
drop	table if exists #temp1 
;
select	aa.*
		, bb.CorporateEntityId
		, bb.CorporateRole
		, cc.Id
		, cc.AccountContactRef	
		, cc.CompanyName
into	#temp1 
from	newsalesam aa
join	UserCorporateAssociation  bb
on		aa.[Client Email] = bb.Email
join	CorporateEntities cc
on		bb.CorporateEntityId = cc.Id
where	bb.Email is not null

select	* from #temp1

--do the update for the 320 rows 
update	aa
set		aa.accountcontactref = bb.[new am]
--select bb.* ,aa.accountcontactref ,aa.* 
from	CorporateEntities aa
join	#temp1 bb
on		aa.Id = bb.id

--> now for the 11 we need to insert to corp association table

select	aa.*
		, bb.CorporateEntityId
		, bb.CorporateRole

from	newsalesam aa
left	outer join	UserCorporateAssociation  bb
on		aa.[Client Email] = bb.Email
--join	CorporateEntities cc
--on	bb.CorporateEntityId = cc.Id
where	bb.Email is null




select *	from CurrencyBank.Trade 
where		FB_DateUpdated >= '2018-02-26'
and			TradeReason <> 'card load'

select *	from UserCorporateAssociation
where		[Email] like '%wingfield%'