drop table #temp1
;
with	cteThisYear as (
select	tradeid
		,datepart(year,TradeDate) as [Year]
		,datepart(MONTH,TradeDate) as [Month]
		,datepart(QQ,TradeDate) as [Quarter]
		,DATENAME(MONTH,TradeDate) as [MonthName]
		,TradeDate
		,TradeProfit 
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,UserId
		,GBPTurnover = case SellCcyCode when 'gbp' then sellamount else [dw].[UdfGetGBPValueXIGNITE](SellAmount,SellCcyCode) end
from	NonRetailTradesSpotFwdFlex_FB
where	tradedatekey >= 20160101)  --so this section would have to change to be current month rolling 
,
cte2	as (
select	SUM(TradeProfit) as RevenueMTD
		,[YEAR]
		,[MONTH]
		,[Quarter]
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,[MonthName]
		,userid
		,COUNT(TradeId) as TradeCount
		,sum(GBPTurnover) GBPTurnover
		,MAX(TradeDate) as LastTradeDate
from	cteThisYear aa
JOIN	[AmPortfolioTarget]  bb
on		aa.Quarter = bb.PortfolioTargetRowId
group	by [YEAR]
		,[MONTH]
		,[Quarter]
		,FB_AccountContactRef
		,FB_SalesContactRef
		,[MonthName]
		,FB_CompanyName
		,UserId) -->let cte2 be the first round of aggregations you perform for which you have extracted the data 

,cte3 as 	
(select	 TradeCount 
		,RevenueMTD
		,GBPTurnover

		,SUM(RevenueMTD) 
		over (partition by fb_Companyname,[userid],[year],[month] order by [YEAR],[MONTH] asc 
		rows between unbounded preceding and current row)	as MTDRevenue

		,SUM(GBPTurnover) 
		over (partition by fb_Companyname,[userid],[year],[month] order by [YEAR],[MONTH] asc 
		rows between unbounded preceding and current row)	as MTDTurnOver

		,[YEAR]
		,[MONTH]
		,[Quarter]
		,cte2.FB_AccountContactRef
		,cte2.FB_SalesContactRef
		,cte2.FB_Companyname
		,cte2.UserId
		,[MonthName]
		, cast(replicate('0',2-len([month])) as varchar(2)) + cast([Month] as varchar(2)) + '-' +[MonthName] as MonthNumber
		--,MonthlyTarget
		--,QuarterlyTarget
		--,YearlyTarget
--into	#temp1
from	cte2 
join	AmPortfolioTarget bb
on		cte2.Quarter = bb.PortfolioTargetRowId)

--then pivot the result set 
select [year],fb_Accountcontactref,fb_Companyname,userid,[01-January],[02-February],[03-March],[04-April],[05-May],[06-June],[07-July],[08-August],[09-September],[10-October],[11-November],[12-December] 
from 
(select revenuemtd,[year],MonthNumber,fb_Accountcontactref,fb_Companyname,userid from cte3) as sourcetable
pivot 
(
sum(revenuemtd) for MonthNumber in ([01-January],[02-February],[03-March],[04-April],[05-May],[06-June],[07-July],[08-August],[09-September],[10-October],[11-November],[12-December])
) as pivottable;



select distinct monthnumber from #temp1 order by monthnumber 


--then do it as a pivot example 
select [year],fb_Accountcontactref,fb_Companyname,userid,[01-January],[02-February],[03-March],[04-April],[05-May],[06-June],[07-July],[08-August],[09-September],[10-October],[11-November],[12-December] into #temp3
from 
(select tradecount,[year],[monthnumber],fb_Accountcontactref,fb_Companyname,userid from #temp1) as sourcetable
pivot 
(
sum(tradecount) for monthnumber in ([01-January],[02-February],[03-March],[04-April],[05-May],[06-June],[07-July],[08-August],[09-September],[10-October],[11-November],[12-December])
) as pivottable;



select * from #temp1 where userid = 'intlltdofevangelicalshop@caxtonfx.com' order by [year] , [month] asc

select * from #temp2 where userid = 'intlltdofevangelicalshop@caxtonfx.com'
select * from #temp3 where userid = 'intlltdofevangelicalshop@caxtonfx.com'


