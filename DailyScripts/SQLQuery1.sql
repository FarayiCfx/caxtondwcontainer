exec sp_who2 active

select * from dw.DimUserCorporateAssociation
where Email = 'jlaporte@argans.co.uk'

select * from finance44output
-------------------------------------------------------------------------------------------------------------------------------

with	cte1 as (
select	bb.corporateentityid
		, bb.corporaterole
		, aa.* 
from	finance44output aa
left	outer join	dw.dimusercorporateassociation bb
on		aa.userid = bb.email),
ctecorpowner as (
select	CorporateEntityId,Email,CorporateRole
from	dw.DimUserCorporateAssociation
where	CorporateRole = 3
)
select	cc.Email as CorpOwner
		, cte1.*	
from	cte1
left	outer join	 ctecorpowner cc
on		cte1.CorporateEntityId = cc.CorporateEntityId
where	cte1.CorporateEntityId = 766
------------------------------------------------------------------------------------------------------------------------
;
drop table if exists #temp
;
--use this to get all other rows that have a corp entity 
with	cte1 as (
select	bb.corporateentityid
		, bb.corporaterole as currentuserrole
		, aa.* 
from	finance44output aa
left	outer join	dw.dimusercorporateassociation bb
on		aa.userid = bb.email),
		ctecorpowner as (
select	CorporateEntityId,Email,CorporateRole
from	dw.DimUserCorporateAssociation
where	CorporateRole = 3
--order by 1 asc
)
select	cc.Email as CorpOwner
		, cte1.*	
		into #temp
from	cte1
left	outer join	 ctecorpowner cc
on		cte1.CorporateEntityId = cc.CorporateEntityId
--where	cte1.currentuserrole <> 3

order by cte1.CorporateEntityId asc

-------------------------------------------------------------------------------------------------------------------------------
select * from #temp
where USERID like '%wardl%' or USERID like 'graham@show%'


select * from FINANCE44Output 
where USERID like '%wardlaw%' or USERID like 'graham@show%'


---------------------------------------------------------------------------------------------------------------------------------
;
with	cte1 as (
select	bb.corporateentityid
		, bb.corporaterole
		, aa.* 
from	finance44output aa
left	outer join	dw.dimusercorporateassociation bb
on		aa.userid = bb.email
),
		ctecorpowner as (
select	CorporateEntityId,Email,CorporateRole
from	dw.DimUserCorporateAssociation
where	CorporateRole = 3
)
select	cc.Email as CorpOwner
		, cte1.*	
into	#temp1 
from	cte1
left	outer join	 ctecorpowner cc
on		cte1.CorporateEntityId = cc.CorporateEntityId
where	cte1.USERID in ('wardlawltdtashowstars@caxtonfx.com'
					,'david.shortland@proactive-pr.com'
					,'jlaporte@argans.co.uk'
					,'maciej.czakon@gmail.com'
					,'petersommer@caxtonfx.com'
					,'independentshellfish@btconnect.com'
					,'sm1@msluk.net'
					,'dreamwebs@caxtonfx.com'
					,'jane5292@sky.com'
					,'altfilimited@caxtonfx.com'
					,'maistracc@gmail.com'
					,'neringa@biliuk.com')
order	by CorporateEntityId asc


select	* from #temp1

select	* 
from	finance44output
where	userid = 'jlaporte@argans.co.uk'

--so now you need to get the corp owners transactions 



