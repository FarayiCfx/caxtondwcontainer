select * from TalksheetReportOutput

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF		OBJECT_ID('dbo.TalksheetReportOutput', 'U') IS NOT NULL 
DROP	TABLE dbo.TalksheetReportOutput;
go
select	 identity(BIGINT,1,1) as Id, 
		cast(ContactEmailAddress as varchar(255)) ContactEmailAddress,
		account_full_name as [accountname],
		cast(localdate as date) as [Date],
		lastname  
		+ ','
		+FIRSTNAME 
		+ ' ****' 
		+ right(pan,4) as [Name],
		CurrencyCode, 
		termlocation, 
		TERMCITY,  
		termcountry,
		cast(isnull(amttxn,0) as decimal(28,2)) as amttxn,
		cast(isnull(amtfee,0) as decimal(28,2)) as amtfee, 
		cast(isnull(billamt,0) as decimal(28,2)) as billamt
into	TalksheetReportOutput
from	talksheettrans (nolock)
go
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
alter table  TalksheetReportOutput add CONSTRAINT [PK_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
GO
CREATE NONCLUSTERED INDEX [ncc_accountname_includes] ON [dbo].[TalksheetReportOutput]
(
	[accountname] ASC
)
INCLUDE ( 	[ContactEmailAddress],
	[date],
	[Name],
	[CurrencyCode],
	[termlocation],
	[TERMCITY],
	[termcountry],
	[amttxn],
	[amtfee],
	[billamt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE NONCLUSTERED INDEX [ncc_contactemailaddress_includes] ON [dbo].[TalksheetReportOutput]
(
	[ContactEmailAddress] ASC
)
INCLUDE ( 	[accountname],
	[date],
	[NAME],
	[CurrencyCode],
	[termlocation],
	[TERMCITY],
	[termcountry],
	[amttxn],
	[amtfee],
	[billamt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO






SELECT * FROM TalksheetReportOutput
WHERE ACCOUNTNAME = 'Open Development Services (uk) Ltd'

SELECT	* 
FROM	[dbo].[TalksheetReportOutput]
where	accountname = 'Fideres Advisors Llp'
