select top 5 * from [altapay].[Fact_AltaPayFundingFiles] 

select top 5 * from [FireBird].[CurrencyBankAccountTransaction]
--refunds 
exec sp_help '[FireBird].[CurrencyBankAccountTransaction]'

select	fl.[date] as AltapayDate,
		fl.[Type],
		fl.[ID] as altapay_id,
		fl.[Reconciliation Identifier],
		fl.[payment],
		fl.[order],
		fl.terminal,
		fl.shop,
		fl.[transaction currency],
		fl.[transaction amount],
		fl.[exchange rate],
		fl.[settlement currency],
		fl.[settlement amount],
		fl.[fixed fee],
		fl.[fixed fee vat],
		fl.[rate based fee],
		fl.[rate based fee vat],
		[filename],
		tr.TransactionDateTime as TransactionDateTimeInFirebird,
		tr.amount as AmountInFireBird, 
		tr.balance as BalanceInFireBird,
		tr.[Status]  as StatusInFireBird,
		tr.[ClearedAmount],
		tr.ExternalTransactionId,
		tr.ExternalTransRef
from	[altapay].[Fact_AltaPayFundingFiles] fl
left outer join	[FireBird].[CurrencyBankAccountTransaction] tr
on		fl.payment = tr.[externaltransactionid]
where	(fl.[date] between '2015-10-01 00:00:00.000' and '2015-11-01 00:00:00.000')
and		fl.[type] = 'payment'
and  tr.[externaltransactionid]  is null
order	by altapay_id,[filename]
--


