select top 10 * from talksheettrans

select  * from TalkSheetTransPerCompany


create view TalkSheetTransPerCompany 
as
select	ContactEmailAddress,
		account_full_name as [account name],
		cast(localdate as date) as [Date],
		lastname  
		+ ','
		+FIRSTNAME 
		+ ' ****' 
		+ right(pan,4) as [Name],
		CurrencyCode, 
		termlocation, 
		TERMCITY,  
		termcountry,
		amttxn,
		amtfee, 
		billamt
from	talksheettrans (nolock)

create	nonclustered index ncc_accountname_includes on talksheettrans (account_full_name)
include (ContactEmailAddress,localdate,lastname,firstname,currencycode,termlocation,termcity,termcountry,amttxn,amtfee,billamt)


create	nonclustered index ncc_contactemailaddress_includes on talksheettrans (ContactEmailAddress)
include (account_full_name,localdate,lastname,firstname,currencycode,termlocation,termcity,termcountry,amttxn,amtfee,billamt)

