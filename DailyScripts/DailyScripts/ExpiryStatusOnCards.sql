use datwarehouse 
go

select * from [dbo].ExpiryCardStatuses
where idiwallet = '1013'

SELECT * FROM [dbo].[ExpiryStatusesFileFromAlana] where [wallet id] = '24967'

select * from ExpiryCardStatuses where idiwallet = '24967'

-->first create an id value 

SELECT	IDENTITY(int, 1,1) AS ID_Num ,
		*
INTO	ExpCardsWithRowId  
FROM	ExpiryCardStatuses
-->
select	* from ExpCardsWithRowId

-->visually sample all the records with more than one occurence 
with	cte1 as (
select	count(idipan) as idipan_count,
		idipan as idipan_
from	ExpCardsWithRowId
group	by IDIpan
having	count(IDIpan) > 1)

select	top 100 * 
from	ExpCardsWithRowId aa
join	cte1
on		aa.IDIpan = cte1.idipan_
order	by aa.IDIpan

/*
select *	from ExpiryCardStatuses 
where		[�stand kort heiti en] like '%card closed%'
order		by IDIpan
*/
--select all the distinct expiry statuses 
select		distinct [�stand kort heiti en] from ExpiryCardStatuses

-----------------------------------------------------------------------
create		view ExpiryStatusOnCards
as
select		IDIpan,
			[Kortn�mer_6_4],
			[Card Awaiting Production],
		[Card Blocked temporarily],
		[17-Mar,Card Open],
		[Card Closed],
		[18-Jan,Card Awaiting Activation],
		[Card Open],
		[18-Feb,Card Open],
		[16-Dec,Card Awaiting Activation],
		[Card Awaiting Activation],
		[18-May,Card Closed],
		[18-Jul,Card Awaiting Activation] 
FROM
(select id_num,
		IDIpan,
		[Kortn�mer_6_4],
		[�stand kort heiti en]
from	ExpCardsWithRowId) p

PIVOT	

(
count	(id_num)
FOR		p.[�stand kort heiti en] 
in 
(								[Card Awaiting Production],
								[Card Blocked temporarily],
								[17-Mar,Card Open],
								[Card Closed],
								[18-Jan,Card Awaiting Activation],
								[Card Open],
								[18-Feb,Card Open],
								[16-Dec,Card Awaiting Activation],
								[Card Awaiting Activation],
								[18-May,Card Closed],
								[18-Jul,Card Awaiting Activation])
) as pvt
--order by 1 

-->then confirm the number with the below distinct statement 
select distinct idipan from ExpiryStatusOnCards

select * from expcardswithrowid where idipan = 11328663

select top 10 * from ExpiryStatusOnCards

IF		OBJECT_ID('dbo.2017Output', 'U') IS NOT NULL 
DROP	TABLE dbo.[2017Output]; 
go
select	aa.Kortn�mer_6_4,
		aa.[Card Awaiting Production]
		+ aa.[Card Blocked temporarily] 
		+ aa.[17-Mar,Card Open]
		+ aa.[Card Closed]
		+ aa.[18-Jan,Card Awaiting Activation]
		+ aa.[Card Open]
		+ aa.[18-Feb,Card Open]
		+ aa.[16-Dec,Card Awaiting Activation]
		+ aa.[card awaiting activation]
		+ aa.[18-May,Card Closed]
		+ aa.[18-Jul,Card Awaiting Activation]
		as [CheckSum]
		,
		CardState = case 
					WHEN aa.[Card Awaiting Production] > 0 THEN 'U'
					when aa.[Card Blocked temporarily]  > 0 THEN 'A'
					when aa.[17-Mar,Card Open] > 0 then 'V'
					WHEN aa.[Card Closed] > 0 THEN 'L'
					WHEN aa.[18-Jan,Card Awaiting Activation] > 0 THEN 'P'
					WHEN aa.[Card Open] > 0 THEN 'V'
					WHEN aa.[18-Feb,Card Open] > 0 THEN 'V'
					WHEN aa.[16-Dec,Card Awaiting Activation] > 0 THEN 'P'
					WHEN aa.[Card Awaiting Activation] > 0 THEN 'P'
					WHEN aa.[18-May,Card Closed] > 0 THEN 'L'
					WHEN aa.[18-Jul,Card Awaiting Activation] > 0 THEN 'P'
					end,
		bb.*
into	dbo.[2017Output]
from	ExpiryStatusOnCards aa
join	dw.[BI109_EXPIRY_2017] bb
on		aa.IDIpan = bb.IDIpan
--where	aa.[card closed] = 0
--order	by [CheckSum]
------------------------------------------------------------------
select * from [2017Output]

select top 10 * from dbo.ExpiryStatusOnCards
select top 10 * from dw.[BI109_EXPIRY_2017]






















----------------------------------------------------------------------------------------------------------------------

IF		OBJECT_ID('dbo.2018Output', 'U') IS NOT NULL 
DROP	TABLE dbo.[2018Output]; 
go
select	aa.[Card Awaiting Production]
		+ aa.[Card Blocked temporarily] 
		+ aa.[17-Mar,Card Open]
		+ aa.[Card Closed]
		+ aa.[18-Jan,Card Awaiting Activation]
		+ aa.[Card Open]
		+ aa.[18-Feb,Card Open]
		+ aa.[16-Dec,Card Awaiting Activation]
		+ aa.[card awaiting activation]
		+ aa.[18-May,Card Closed]
		+ aa.[18-Jul,Card Awaiting Activation]
		as [CheckSum]
		,
		CardState = case 
					WHEN aa.[Card Awaiting Production] > 0 THEN 'U'
					when aa.[Card Blocked temporarily]  > 0 THEN 'A'
					when aa.[17-Mar,Card Open] > 0 then 'V'
					WHEN aa.[Card Closed] > 0 THEN 'L'
					WHEN aa.[18-Jan,Card Awaiting Activation] > 0 THEN 'P'
					WHEN aa.[Card Open] > 0 THEN 'V'
					WHEN aa.[18-Feb,Card Open] > 0 THEN 'V'
					WHEN aa.[16-Dec,Card Awaiting Activation] > 0 THEN 'P'
					WHEN aa.[Card Awaiting Activation] > 0 THEN 'P'
					WHEN aa.[18-May,Card Closed] > 0 THEN 'L'
					WHEN aa.[18-Jul,Card Awaiting Activation] > 0 THEN 'P'
					end,
		bb.*	
into	dbo.[2018Output]
from	ExpiryStatusOnCards aa
join	dw.[BI109_EXPIRY_2018] bb
on		aa.IDIpan = bb.PANID
---where	[card closed] = 0
order	by [CheckSum]



select * from dbo.[2017Output]

select * from dbo.[2018Output]

select top 10 * from dw.[BI109_EXPIRY_2018]



SELECT TOP 10 * FROM ValitorExpiry2017
SELECT TOP 10 * FROM ExpiryStatusOnCards


SELECT TOP 10 * from dw.[BI109_EXPIRY_2017]
