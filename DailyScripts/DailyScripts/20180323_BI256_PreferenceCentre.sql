create	VIEW [BI-256-PreferenceCentre]
as
/*

This is to show the communication preferences by userid
The result set is intially several rows per user 


*/
with	ctePrefCentre as (
select	username
		,Email
		,ContactEmailAddress
		,UserId
		,cast([Channel | All | Email] as bit) as [Channel | All | Email]
		,cast([Channel | All | Phone] as bit) as [Channel | All | Phone]
		,cast([Channel | All | Post] as bit) as [Channel | All | Post]
,cast([Preference | PremierIp | Analyst market updates and daily currency reports] as bit) as [Preference | PremierIp | Analyst market updates and daily currency reports]
,cast([Preference | PremierIp | Annual account review] as bit) as [Preference | PremierIp | Annual account review]
,cast([Preference | PremierIp | Monthly market and currency updates] as bit) as [Preference | PremierIp | Monthly market and currency updates]
,cast([Preference | PremierIp | Monthly market updates] as bit) as [Preference | PremierIp | Monthly market updates]
,cast([Preference | PremierIp | Opt-in to all communications] as bit) as [Preference | PremierIp | Opt-in to all communications]
,cast([Preference | PremierIp | Rates / promotions / currency updates and offers] as bit) as [Preference | PremierIp | Rates / promotions / currency updates and offers]
,cast([Preference | RetailCards | Daily euro currency updates] as bit) as [Preference | RetailCards | Daily euro currency updates]
,cast([Preference | RetailCards | Daily US dollar currency updates] as bit) as [Preference | RetailCards | Daily US dollar currency updates]
,cast([Preference | RetailCards | Opt-in to all marketing and currency updates] as bit) as [Preference | RetailCards | Opt-in to all marketing and currency updates]
,cast([Preference | RetailCards | Rate updates / promotions and offers] as bit)as [Preference | RetailCards | Rate updates / promotions and offers]
,cast([Preference | RetailCards | Weekly euro and US dollar currency updates] as bit) as [Preference | RetailCards | Weekly euro and US dollar currency updates]
,cast([Preference | RetailIp | Analyst market updates and daily currency reports] as bit) as [Preference | RetailIp | Analyst market updates and daily currency reports]
,cast([Preference | RetailIp | Annual account review] as bit) as [Preference | RetailIp | Annual account review]
,cast([Preference | RetailIp | Market driven notifications] as bit) as [Preference | RetailIp | Market driven notifications] 
,cast([Preference | RetailIp | Opt-in to all marketing and currency updates] as bit) as [Preference | RetailIp | Opt-in to all marketing and currency updates]
,cast([Preference | RetailIp | Rate updates / promotions / currency updates and offers] as bit) as [Preference | RetailIp | Rate updates / promotions / currency updates and offers] 
from 
(select	cc.username
		,cc.email
		,cc.ContactEmailAddress
		--,aa.customerkey
		,aa.UserId
		, cast (aa.optedin as int) as optedin
		--, BB.preferencevalue
		--, BB.accounttype
		--,bb.preferencetype
		,preferencetype + ' | ' + accounttype+ ' | ' +preferencevalue  as FullPreferenceValue
from	[Firebird].[FactUserCommunicationPreference] aa
join	FireBird.DimCommunicationPreference bb
on		aa.communicationpreferenceid = BB.id
join	dw.DimCustomerReference cc
on		aa.customerkey = cc.CustomerKey) as sourcetable 
pivot
(
sum		(optedin)
for		FullPreferenceValue 
in		([Channel | All | Email]
		,[Channel | All | Phone] 
		,[Channel | All | Post] 
,[Preference | PremierIp | Analyst market updates and daily currency reports]
,[Preference | PremierIp | Annual account review]
,[Preference | PremierIp | Monthly market and currency updates]
		,[Preference | PremierIp | Monthly market updates]
		,[Preference | PremierIp | Opt-in to all communications]
		,[Preference | PremierIp | Rates / promotions / currency updates and offers]
		,[Preference | RetailCards | Daily euro currency updates]
		,[Preference | RetailCards | Daily US dollar currency updates]
		,[Preference | RetailCards | Opt-in to all marketing and currency updates]
		,[Preference | RetailCards | Rate updates / promotions and offers]
		,[Preference | RetailCards | Weekly euro and US dollar currency updates]
		,[Preference | RetailIp | Analyst market updates and daily currency reports]
		,[Preference | RetailIp | Annual account review]
		,[Preference | RetailIp | Market driven notifications]
		,[Preference | RetailIp | Opt-in to all marketing and currency updates]
		,[Preference | RetailIp | Rate updates / promotions / currency updates and offers])
)	as pivottable)
select	username
		,Email
		,contactemailaddress
		,UserID
		,isnull([Channel | All | Email],0) as[Channel | All | Email]
		,isnull([Channel | All | Phone],0)as [Channel | All | Phone]  
		,isnull([Channel | All | Post],0)as [Channel | All | Post]
,isnull([Preference | PremierIp | Analyst market updates and daily currency reports],0)as [Preference | PremierIp | Analyst market updates and daily currency reports]
,isnull([Preference | PremierIp | Annual account review],0) as[Preference | PremierIp | Annual account review]
,isnull([Preference | PremierIp | Monthly market and currency updates],0)[Preference | PremierIp | Monthly market and currency updates]
		,isnull([Preference | PremierIp | Monthly market updates],0)as [Preference | PremierIp | Monthly market updates]
		,isnull([Preference | PremierIp | Opt-in to all communications],0)as [Preference | PremierIp | Opt-in to all communications]
,isnull([Preference | PremierIp | Rates / promotions / currency updates and offers],0)as[Preference | PremierIp | Rates / promotions / currency updates and offers]
,isnull([Preference | RetailCards | Daily euro currency updates],0)as[Preference | RetailCards | Daily euro currency updates]
,isnull([Preference | RetailCards | Daily US dollar currency updates],0)as[Preference | RetailCards | Daily US dollar currency updates]
		,isnull([Preference | RetailCards | Opt-in to all marketing and currency updates],0)as[Preference | RetailCards | Opt-in to all marketing and currency updates]
,isnull([Preference | RetailCards | Rate updates / promotions and offers],0)as[Preference | RetailCards | Rate updates / promotions and offers]
,isnull([Preference | RetailCards | Weekly euro and US dollar currency updates],0)as[Preference | RetailCards | Weekly euro and US dollar currency updates]
,isnull([Preference | RetailIp | Analyst market updates and daily currency reports],0) as [Preference | RetailIp | Analyst market updates and daily currency reports]
,isnull([Preference | RetailIp | Annual account review],0)as [Preference | RetailIp | Annual account review]
,isnull([Preference | RetailIp | Market driven notifications],0) as [Preference | RetailIp | Market driven notifications]
,isnull([Preference | RetailIp | Opt-in to all marketing and currency updates],0)as[Preference | RetailIp | Opt-in to all marketing and currency updates]
,isnull([Preference | RetailIp | Rate updates / promotions / currency updates and offers],0)as[Preference | RetailIp | Rate updates / promotions / currency updates and offers]
from	ctePrefCentre
 ;

