-->#temp1 just try and match records from the file to any rows in firebird 
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go

with		cteCountRows as (
select		count(customerkey) as count_firebird_transactions,
			customerkey
from		firebird.factcurrencybankaccounttransaction (nolock)
group by	customerkey),

cteCountRowsFis as (
select		count(customerkey) as count_firebird_transactions,
			customerkey
from		firebird.factcurrencybankaccounttransaction (nolock)
WHERE		EXTERNALTRANSREF = 'FIS'
group by	customerkey)


select		cast(avlbal as decimal(18,2)) as avlbalDecimal
			,email_address1 as email
			,cteCountRows.count_firebird_transactions count_firebird_transactions_anytype
			,cteCountRowsFis.count_firebird_transactions as count_firebird_transactions_fis
			,bb.customerkey
			,aa.*  
			
		-----	into #temp1
from		FIS_Output_Raphaels aa
join		[dw].[DimTalksheetCardsAndContacts] bb
on			aa.cardid = bb.cardid
left		outer join cteCountRows 
on			bb.customerkey = cteCountRows.customerkey 
left		outer join cteCountRowsFis
on			bb.customerkey = cteCountRowsFis.customerkey 

------then get everything line by line 
select	email_address1 as emailaddress
			,aa.RowNumber
			,cast(avlbal as decimal(18,2)) as avlbalDecimal
			,dd.id as FirebirdTransactionId
			,bb.customerkey
			,dd.transactiondatetime
			,dd.amount
			,dd.balance
			,dd.cleared
			,dd.status
			,dd.externaltransactionid
			,dd.externaltransref
		-----	into #temp1
from		FIS_Output_Raphaels aa
join		[dw].[DimTalksheetCardsAndContacts] bb
on			aa.cardid = bb.cardid
join		firebird.factcurrencybankaccounttransaction dd (nolock)
on			bb.customerkey = dd.customerkey 


select * from firebird.factcurrencybankaccounttransaction  where customerkey = 261353


---------------------------------------------------------------------------------------------------------------
DECLARE @EMAIL NVARCHAR(255)

SELECT	@EMAIL = 'sarah.tory@tesco.net'

SELECT	* FROM #TEMP1 WHERE EMAIL = @EMAIL

SELECT	* FROM #TEMP2 WHERE Email_address1 = @EMAIL

---------------------------------------------------------------------------------------------------------------
--128501 rows in total

select *	from #temp1 where count_firebird_transactions is null order by avlbal
--109295 without firebird transactions

select *	from #temp1 where count_firebird_transactions is not null order by avlbal
--19 206 with firebird entries 
 
select * from dw.dimtalksheetcardsandcontacts where contact_id = 242114 rsmith@brookes.ac.uk
---------------------------------------------------------------------------------------------------------------
with	cte1 as (
select	count(customerkey) as count_,
		customerkey
from	firebird.factcurrencybankaccounttransaction (nolock) 
where	externaltransref = 'fis'
group	by customerkey 
)
select	* 
from	#temp1 aa join cte1 bb
on		aa.customerkey = bb.customerkey 
where	aa.count_firebird_transactions is not null


select * from firebird.factcurrencybankaccounttransaction (nolock) where customerkey = 159352

select	aa.* 
		,bb.transactiondatetime
		,bb.amount
		,bb.[status]
		,bb.externaltransref
from	#temp1 aa (NOLOCK)
join	firebird.factcurrencybankaccounttransaction bb (NOLOCK)
on		aa.customerkey = bb.customerkey 
where	bb.externaltransref = 'FIS'
order	by customerkey 