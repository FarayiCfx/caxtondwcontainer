
select @@servername 

select	customerkey, CAST ([username] AS NVARCHAR(50)) username
from	dw.dimcustomerreference (nolock)
where	username is not null

select	*
from	dw.dimvalitorwallet
where	accountid 
in ('Walsh.kathryn97@yahoo.co.uk','ianrobins@live.co.uk')

select customerkey,valitorwalletid
from dw.dimvalitorwallet (nolock)

exec sp_help 'dw.dimvalitorwallet'



select  username,* 
from	dw.dimcustomerreference 
where	username in ('Walsh.kathryn97@yahoo.co.uk','ianrobins@live.co.uk')

select * from dw.[DimValitorWalletNonMatching]


truncate table dw.dimvalitorwallet
truncate table dw.dimvalitorwalletnonmatching

with	cte1 as (
select	customerkey, CAST ([username] AS NVARCHAR(50)) username
from	dw.dimcustomerreference (nolock)
where	username is not null)
select * from cte1 where username in ('Walsh.kathryn97@yahoo.co.uk')

truncate table [valitor].[fact_WalletTransactions]
truncate table [valitor].[fact_WalletTransactionsNoMatch]

select * from [valitor].[fact_WalletTransactionsNoMatch]

select top 100 * from [valitor].[fact_WalletTransactions]

SELECT * FROM DW.DIMMCC
WHERE MCC_ID = 4111


IF		OBJECT_ID('tempdb..#temp1', 'U') IS NOT NULL
DROP	TABLE #Temp1; 
go  
select	aa.[type] as [Type],
		sum(transactionamount) as  SumOfTransactionAmount,
		aa.transactioncurrency as TransactionCurrency,
		aa.MerchantCountryCode,
		dc.country_name,
		datename(year,transactiondatetime) as [Year],
		datepart(month,transactiondatetime) as [Month]
into	#temp1 
from	[valitor].[fact_WalletTransactions] aa (nolock)   
join	dw.dimcountry dc (nolock)
on		aa.merchantcountrycode = dc.country_iso  
where	transactiondatetime > '2015-04-30 23:59:59.000'
and		transactionkeycategory = 'Sale'
group	by	aa.[type],
		aa.transactioncurrency,
		aa.MerchantCountryCode,
		dc.country_name,
		datename(year,transactiondatetime),
		datepart(month,transactiondatetime);

		select * from #temp1

IF		OBJECT_ID('tempdb..#temp2', 'U') IS NOT NULL
DROP	TABLE #Temp2;
go
with	cte1 as (
select  *,
		dw.GetCurrencyName(transactioncurrency) TransactionCurrencyName,
		dw.GetCurrencyAlphaCode(transactioncurrency) CurrencyCode,
		dw.GetGBPValue(isnull(SumOfTransactionAmount,0),isnull((dw.GetCurrencyAlphaCode(transactioncurrency)),0)) as GBPValue
from	#temp1
	)
select	*,
		rank() over (partition by [year],[month],gbpvalue order by gbpvalue desc ) as spending_rank
		into #temp2
from	cte1 
order by [year],[month]

select * from #temp2 order by [year],[month]


select * from #temp1 order by [year],[month],merchantname

select * from dw.dimcountry

	
		select top 5 * from [dw].[DimXERates]

select top 5 * from	#temp1

select top 10 * from  dw.dimcurrencycodesiso4217
where alphabeticcode = 'fkp'



select @@servername
