--These are the dupe ref id's that appear in the valitor extract 
select	count(referenceid) refid_ , referenceid
from	[valitor].[fact_WalletTransactions_extract] (nolock) aa
where	referenceid not like '%:%' AND transactiondatetime > '2015-04-09 08:26:11.437'
group	by referenceid
having	count(referenceid) > 1

exec	sp_spaceused '[valitor].[fact_WalletTransactions_extract]' --2 070 573 --296 527      

select  count(*)
from	[valitor].[fact_WalletTransactions_extract] aa (nolock)
join	[FireBird].[CurrencyBankAccountTransaction] bb (nolock)
on		aa.referenceid = bb.EXTERNALTRANSACTIONID 
--296 527 transactions line up 
-----------------------------------------------------------------------------------------------------------
 IF OBJECT_ID('tempdb.dbo.#ANALYSIS', 'U') IS NOT NULL 
 DROP TABLE #ANALYSIS; 

--COMPARE THE DUPES IN VALITOR AGAINST FIREBIRD 
with	cteDuplicateReferenceIdsValitor as (
select	count(referenceid) count_in_valitor , referenceid
from	[valitor].[fact_WalletTransactions_extract] (nolock) aa
where	referenceid not like '%:%' AND transactiondatetime > '2015-04-09 08:26:11.437'
group	by referenceid
having	count(referenceid) > 1)

select	count_in_valitor,aa.* 
INTO	#ANALYSIS
from	[FireBird].[CurrencyBankAccountTransaction] aa
join	cteDuplicateReferenceIdsValitor bb
on		(aa.externaltransactionid = bb.referenceid)
where	aa.externaltransref = 'Valitor'
ORDER	BY aa.externaltransactionid 

select * from #analysis where externaltransactionid = '6358387806530142'

select	count(externaltransactionid) as count_,externaltransactionid
from	#analysis
group	by externaltransactionid
ORDER	BY COUNT_ ASC



----------------------------------------------------------------------------------------------------------------------------------------------------------------

declare @referenceid nvarchar(100)

select	@referenceid = '6357017237456665'

select	[id] as IdInValitorFile,
		ExtractDateTime,
		TransactionDatetime,
		[TYPE], 
		BillingAmount,
		BillingCurrency,
		TimeStampId,
		AccountId,
		WalletId,
		[Description],
		[Label],
		''''+ReferenceId AS REFERENCEID,
		DownloadFilename
from	[valitor].[fact_WalletTransactions_extract] (nolock)
where	referenceid = @referenceid
order	by id asc 

select	ID,TRANSACTIONDATETIME,AMOUNT,CLEARED AMOUNT,BALANCE,ACCOUNTSUMMARYID,[STATUS],CLEAREDAMOUNT,
		''''+EXTERNALTRANSACTIONID AS EXTERNALTRANSACTIONID,EXTERNALTRANSREF
from	[FireBird].[CurrencyBankAccountTransaction] (nolock)
where	externaltransactionid = @referenceid
and		externaltransref = 'valitor'

select	min(transactiondatetime) as min_,max(transactiondatetime) as max_ 
from	[FireBird].[CurrencyBankAccountTransaction]
where	externaltransref = 'valitor'


select top 10 * from [FireBird].[CurrencyBankAccountTransaction]
where	externaltransref = 'valitor'