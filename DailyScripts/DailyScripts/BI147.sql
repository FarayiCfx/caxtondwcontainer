

WITH	CTE1 AS (
select	aa.ClaimValueForAccountType,bb.Title,
		bb.FirstName, bb.LastName, bb.AddressLine1, bb.AddressLine2, bb.City, bb.County, bb.Postcode,
		--asso.*,
		ca.*
from	dw.FactAspNetUserClaims aa (nolock) 
join	dw.DimCustomerReference bb (nolock)
on		AA.CustomerKey = BB.CustomerKey
JOIN	DW.DimUserCorporateAssociation asso
on		bb.UserName = asso.Email
join	CorpsAndIndustries ca
on		asso.CorporateEntityId = ca.Id
where	aa.ClaimValueForAccountType = 'Corporate:PrimaryAccount:PortalCards')
select	Text as IndustryTypeInFireBird,companyname,
		claimvalueforaccounttype,title,firstname,lastname,addressline1,
		addressline2,city,county,postcode,
		currencypairstraded,tradingas
from	cte1 



 
