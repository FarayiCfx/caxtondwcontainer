--some people were not migrated they were simply created brand new, and so their trades were not transferred from talksheet 
--to get round this I will create two identical results and use union as a way to dedupe them
--first section is an example

--link the firebird ones first 
drop table if exists #temp1;

with	cte1 as (
SELECT	bb.Id,BB.USERNAME,aa.* 
from	DBFX_Affiliates aa
join	dw.dimcustomerreference bb
on		aa.email_address = bb.UserName)

SELECT	tr.Id as TradeId,
		TR.UserId,
		TR.TradeDate,
		TR.TradeProfit,
		tr.TradeReason, 
		tr.SellCcyCode, 
		tr.BuyCcyCode,
		tr.SellAmount, 
		tr.BuyAmount,
		0.1*TR.TradeProfit AS AffiliateCommission,
		CTE1.[AFFILIATE NAME],
		CTE1.BRANCH_ADDRESS1 as Address1,
		CTE1.BRANCH_ADDRESS2 as Address2,
		CITYNAME,
		[CLIENT NAME]
		into #temp1
FROM	CTE1 JOIN firebird.factCurrencyBankTrade TR 
ON		CTE1.USERNAME = TR.UserId

------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
drop table if exists #temp2 
go

with	cte1 as (
SELECT	bb.Id,aa.* ,
		BB.CUSTOMERKEY 
from	DBFX_Affiliates aa
join	dw.dimcustomerreference bb
on		aa._contact_id = bb.dbfxcontact_id
and		aa.email_address = bb.contactemailaddress	)

select	dl.dealno as tradeid,
		cte1.email_address as userid,
		dl.dealdate as tradedate,
		dl.revenue as [tradeprofit],
		'' as TradeReason,
		cursold as Sellccycode,
		curbought as Buyccycode,
		AmtSold as SellAmount,
		AmtBought as BuyAmount,
		0.1*dl.revenue as affiliatecommission,
		cte1.[affiliate name],
		cte1.branch_address1 as address1,
		cte1.branch_address2 as address2,
		cte1.cityname,
		cte1.[client name]
into	#temp2
from	cte1 
join	dbfx.deallist dl
on		cte1.customerkey = dl.customerkey 

--then use a union to ring togeteher data and deduping trades that correctly exist in both systems
with cte1 as (
select	cast(tradedate as date) as TradeDate,
		userid,
		Tradeprofit,
		Sellccycode,
		buyccycode,
		sellamount,
		buyamount,
		affiliatecommission,
		[affiliate name] ,
		address1,
		address2,
		cityname,
		[client name]
		from #temp1

		union 
		
select	cast(tradedate as date) as TradeDate,
		userid,
		Tradeprofit,
		Sellccycode,
		buyccycode,
		sellamount,
		buyamount,
		affiliatecommission,
		[affiliate name] ,
		address1,
		address2,
		cityname,
		[client name]
from	#temp2)

select	* 
from	cte1 
where userid in 
('bccmtaylor@sky.com','carlos.bateman@hotmail.com','sam.johnson1978@icloud.com','grecsys@aol.com','paulinekeith4@gmail.com','Stevekelseyathome@me.com','ecn.obrien@gmail.com','gbosher@gmail.com','oxymoronracing@hotmail.com','rubysailing@hotmail.co.uk','billgallaghersingapore@hotmail.com','peterrees@aol.com','gcny007@yahoo.com','steverogers5@hotmail.com','Peter.rosenfeld@fortiusclinic.com','Nicholas.Goodman@caxtonfx.com','bazenger@gmail.com','philipbritton@btinternet.com','petersellers1960@gmail.com','Paul_davies1970@yahoo.co.uk','etrozzo@me.com','mcinally@blueyonder.co.uk','bernieandrob@ntlworld.com','mrk_rax@yahoo.co.uk','nigeldunlop@hotmail.com','neil@ralphson.org.uk','andrew.loose@kbr.com','richard.alty@gmail.com','leecleasby@googlemail.com','claire9@me.com','stevehoward55@yahoo.co.uk','bethgibs@gmail.com','martintmhill@gmail.com','listonwebber@hotmail.co.uk','mathew.boylan@MATADORasia.com','evitts@sky.com','malcolmnewton@onetel.com','Bwyat@hotmail.com','derekmiddlemas@yahoo.com','ctmorris@questverus.com','juliette@johnpenny.co.uk','stellaj54@hotmail.co.uk','susan.mullerworth@maindec.com','margaret.blakeborough@sfr.fr','christopher@lasttix.com.au','bfenton@fdgroup.com.au','lynn.mayfield@btinternet.com','lgian@sas.upenn.edu','andrea@hamiltonassociates.org.uk','jennisongrey@btinternet.com','1nmexburns@comcast.net','nicms@hotmail.com','wattsie10965@gmail.com','please see below','Jonmoodyuk@gmail.com','stuart.nice@hotmail.co.uk','x_cindi_x@yahoo.com','paul.farrer@enerflex.com','clivemitchell@btinternet.com','elenabain@mail.ru','ejm104@hotmail.com','roger.martin9@gmail.com','neil.morgan29@btinternet.com','thesaundersgirl@gmail.com','Lisa_barton@sky.com','eddiecarter@tiscali.co.uk','stesuk@ntlworld.com','robert.macaloney@skanska.co.uk','amje.britton@hotmail.com','carpentorian@aol.com','benbowrm@hotmail.com','warmday@talktalk.net','no e-mail','rward@wandw.co.uk','Vnk1@kitek.co.uk','nik.turner1@gmail.com','josieandgraham@hotmail.co.uk','nthnwillwriters@aol.com','karen.mccabe@talktalk.net','edentrace@europe.com','jenny.morrison@morrisonspowart.com','song.stanislav@jpmorgan.com','rhiharrington@hotmail.co.uk','aholyday@yahoo.co.uk','racthornton@tiscali.co.uk','beckacarley@hotmail.com','stevetimperleymac@me.com','gillian.grech@wanadoo.fr','info@aubergetilleuls.com','roger@droversway.co.uk','hazzer@hotmail.com','stevewrenafc@msn.com','hicks-france@wanadoo.fr','ginawong.gw@googlemail.com','yamfaz@btinternet.com','paulhinder@ymail.com','brian-molloy@hotmail.co.uk','dforbes99@gmail.com','wozza_hutch@yahoo.co.uk','id451@hotmail.com','Jason@reynoldsgroup.co.uk')

