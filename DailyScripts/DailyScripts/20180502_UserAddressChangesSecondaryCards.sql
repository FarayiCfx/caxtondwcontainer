--use production connection

----->step 1 look at user address last modified and link it back to aspnetusers
declare @yesterday date

select	@yesterday = cast ( GETDATE()-1 as date)

--print	@yesterday

drop			table if exists #temp1 
;
select			aa.Id as NumericId
				,aa.UserId
				,aa.LastModified
				,aa.Approved
				,aa.Address1
				,aa.Address2
				,aa.City
				,aa.County
				,aa.Postcode
--,aa.PostcodeId
--,aa.Country
--,aa.FB_DateCreated
--,aa.FB_DateUpdated
--,aa.FB_DateDeleted
				,bb.id as userid_aspnet	
into			#temp1 
from			[user].[useraddresslastmodified] aa 
join			aspnetusers bb
on				aa.userid = bb.email
where			aa.LastModified >= @yesterday
;

select * from #temp1 order by LastModified desc


-----> step 2 go and get the original address from the applicants table 
drop	table if exists #temp2
;
select	cc.Address1 as OldAddress1
		,cc.Address2 as OldAddress2
		,cc.Postcode as OldPostcode 
		,cc.FirstName as ApplicantFirstname
		, cc.LastName as applicantlastname 
		,aa.*
into	#temp2		
from	#temp1 aa
join	Applicants cc
on		aa.UserId = cc.Email
;

select * from #temp2 order by LastModified

-----> step 3 show me the address change records that now have access to secondary accounts 
drop	table if exists #temp3
;
with	cte1 as (
select	* 
from	aspnetuserclaims(nolock)
where	claimtype = 'resourceaccess'
and		ClaimValue like '%secondary%'
)
select	aa.* , bb.claimtype,bb.claimvalue, bb.reversedsecondaryaccountid 
into	#temp3
from	#temp2 aa join cte1 bb
on		aa.userid_aspnet = bb.userid 

select * from #temp3


select * from AspNetUsers where id = 'C00252752'

-------------------------------------------------------------------
--2120 rows in #temp3 then follow through to the valitor.pan table 
select	aa.* 
		, vp.*
from	#temp3 aa
join	AspNetUsers bb
on		aa.REVERSEDsecondaryaccountid = bb.Id
join	Valitor.Wallet ww
on		bb.Email = ww.AccountId
join	Valitor.Pan vp 
on		ww.Id = vp.WalletId
order	by LastModified desc 

--------George Walker , just look at his individual data points 
select * from #temp3 where userid = 'george.walker@allsop.co.uk'
select * from aspnetusers where id = 'c00318138'
select * from aspnetuserclaims where userid = 'c00318138'
select * from valitor.wallet where accountid = 'george.walker@allsop.co.uk'
select * from valitor.pan where walletid = 386526 order by fb_datecreated
select * from aspnetusers where id in('c00318154','c00597873')

----29 988 rows 
select distinct claimtype from AspNetUserClaims (nolock)

select	top 10 * 
from	Valitor.Pan
where	WalletId = 10869

select	top 10 * 
from	Valitor.Wallet
where	AccountId = 'bobjarvisbob@googlemail.com'

SELECT TOP 10 * FROM AspNetUserClaims
