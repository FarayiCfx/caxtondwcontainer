USE [datawarehouse]
GO

/****** Object:  View [dw].[DailyApplicationsToAccounts]    Script Date: 7/4/2016 2:54:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


alter 	VIEW [dw].[DailyApplicationsToAccountsToLoadTotal]
AS
/*
This view looks at FACTASPNETUSERSCLAIMS (in the function)
in order to make a decision AS to what type will be assigned

It's a known fact the [Type] reolution slows down the query 
but this report has a time limit and time sensitivity so needs
to be produced
*/

SELECT	[Product] = CASE app.WantToSendMoney WHEN 1 THEN 'Main business' ELSE 'Cards' END,

		[Type] = CASE	WHEN app.customerkey = -1 THEN 'Rejected Applicant'
						WHEN app.customerkey = -2	THEN 'Duplicate Application'
						WHEN app.wanttosendmoney = 0 
						then [dw].[UdfGetCardClaimValue](cust.CustomerKEY)
						ELSE 'Money Transfer'

						END 

		,cust.Id AS FireBirdID
		,cust.CustomerKey

		,app.status
		,app.EKYCStatus
		,app.PostcodeStatus
		,app.PreAuthStatus
		,app.WantToSendMoney
		,app.RegistrationDateKey
		,app.CreationDateKey
		,app.Email
		--,isnull(app.AffiliateCode,'') AffiliateCode
		,app.ApplicationKey
		,cast (app.CreationDate as date) CreationDate
		,cast (app.RegistrationDate as date) RegistrationDate
		,agg.VALITOR_MASTERCARD_LoadTotalGBP
		,agg.VALITOR_MASTERCARD_CardLoadCount
		,LoadRegistered = case when agg.VALITOR_MASTERCARD_CardLoadCount >0 then 'Loaded' ELSE 'Not Loaded' end

FROM	dw.FactApplicants app (NOLOCK)
LEFT	OUTER JOIN dw.DimCustomerReference cust (NOLOCK)
ON		app.CustomerKEY = cust.CustomerKey
left	outer JOIN dw.CustomerRFMAggregations agg
on		app.CustomerKEY = agg.CustomerKey
where	app.CustomerKEY > 1 
and app.CreationDateKey <> 19000101 --this filter means that secondary accounts will be ignored , because they do not have a creation date
-------------------------------------------------------------------------------------------------------------------------

select * from [dw].[DailyApplicationsToAccountsToLoadTotal]



with cte1 as (

select  count(customerkey) as [count_of_customers], 
		sum(valitor_mastercard_loadtotalgbp) as valitor_mastercard_loadtotalgbp,
		sum(valitor_mastercard_cardloadcount) as valitor_mastercard_cardloadcount,
		product,
		[type],
		creationdate,
		registrationdate,
		loadregistered
from	[dw].[DailyApplicationsToAccountsToLoadTotal]
group	by product,
		[type],
		creationdate,
		registrationdate,
		loadregistered
		
		)-->
		select	sum([count_of_customers]) [count_of_customers],
				loadregistered,
				datepart(yyyy,creationdate) as year
				from cte1
				group by loadregistered,datepart(yyyy,creationdate) 
				order by loadregistered,datepart(yyyy,creationdate) 




select top 10 * from  [dw].[DailyApplicationsToAccounts]

select top 10 * from dw.CustomerRFMAggregations

select top 1000 * from dw.FactApplicants where CustomerKEY > 1

