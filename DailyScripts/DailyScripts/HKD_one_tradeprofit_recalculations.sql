
select	* 
from	currencybank.trade (nolock)
where	cast(tradedate as date) = '2017-04-26'
and		tradeprofit = 37.40

--here is how I am getting to the market table through the client book and the book open date 
with cte1 as (
select	[Name]
		+' - '
		+convert(varchar(24) ,bb.BookOpenDate,103)
		+' '
		+convert(varchar(24) ,bb.BookOpenDate,108) as NetBookName,
		aa.id as TradeId,
		aa.UserId, 
		aa.SellCcyCode, 
		aa.BuyCcyCode, 
		aa.BuyCcyClientTradingBookId as TradeTableBuyCcyClientTradingBookId,
		aa.BuyAmount,
		aa.SellAmount,
		aa.TradeProfit,
		aa.TradeDate,
		aa.TradeEndDate,
		datediff(dd,aa.TradeDate,aa.TradeEndDate) AS DaysToSettlement,
		aa.TradeReason,
		aa.isreversed,
		aa.isunwound,
		aa.BuyCcyClientTradingBookId,
		bb.id as ClientTradeBookId ,
		bb.name,
		bb.ccycode,
		mk.id as MarketTradeId,
		mk.bookname,
		mk.counterpartyrefno,
		mk.sellccy as MarcketTradeSellCCY,
		mk.sellccyamount as MarketTradeSellCCYAmount,
		mk.buyccy as MarketTradeBuyCCY,
		mk.buyccyamount  as MarketTradeBuyCCYAmount,
		mk.bookopendate
from	currencybank.Trade aa (nolock)
join	currencybank.ClientTradingBook bb (nolock)
on		aa.BuyCcyClientTradingBookId = bb.Id
--and		aa.SellCCyClientTradingBookId = bb.id
join	CurrencyBank.MarketTrade mk (nolock)
on		bb.Name = mk.BookName
and		convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
and		bb.CcyCode = mk.BuyCcy)
select	* from cte1 
where	tradeid = 1730007

select	* 
from	currencybank.trade (nolock)
where	cast(tradedate as date) = '2017-04-26'
and		tradeprofit = 37.40

select * from currencybank.markettrade where id in (44849,44850)  