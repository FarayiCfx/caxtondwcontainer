exec sp_who2 active

select	* 
from	dw.dimdatefinancialperiods
order	by id asc 
;
-->This is the main book section :
drop table if exists #temp
drop table if exists #cyly
;
with	cteRowset as 
(
select	tradeid
		,datepart(year,TradeDate) as [Year]
		,datepart(MONTH,TradeDate) as [Month]
		,datepart(QQ,TradeDate) as [Quarter]
		,DATENAME(MONTH,TradeDate) as [MonthName]
		,TradeDate
		,TradeProfit 
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,UserId
		,GBPTurnover = case SellCcyCode when 'gbp' then sellamount else [dw].[UdfGetGBPValueXIGNITE](SellAmount,SellCcyCode) end
		,bb.FiscalYear
		,bb.FiscalQuarter
		,bb.FiscalYearName
		,bb.fiscalmonthrank
		,bb.FinancialMonth
--,cast(replicate('0',2-len([FiscalMonthRank])) as varchar(2)) + CAST([FiscalMonthRank] AS varchar(2)) +'-'+[MonthName] AS FinancialMonth
from	NonRetailTradesSpotFwdFlex_FB aa
join	dw.dimdateFinancialPeriods bb
on		aa.tradedatekey = bb.ID
where	tradedatekey >= 20170101--start at 2016 --starting at 2016 is not right - it take 3:45 minutes just to get the data 01:59 from 2017 onwards
)									
,
cte2	as (
select	sum(tradeprofit) as revenuemtd
		,count(tradeid) as tradecount
		,sum(gbpturnover) gbpturnover
		,max(tradedate) as lasttradedate
		,[year]
		,[month]
		,[quarter]
		,fb_companyname
		,fb_accountcontactref
		,fb_salescontactref
		,[monthname]
		,userid
		,fiscalyear
		,fiscalquarter
		,fiscalyearname
		,fiscalmonthrank
		,FinancialMonth	
from	cteRowset aa
JOIN	[AmPortfolioTarget]  bb
on		aa.[Quarter] = bb.PortfolioTargetRowId
group	by	[year]
			,[month]
			,[quarter]
			,fb_companyname
		,fb_accountcontactref
		,fb_salescontactref
		,[monthname]
		,userid
		,fiscalyear
		,fiscalquarter
		,fiscalyearname
		,fiscalmonthrank
		,FinancialMonth) -->let cte2 be the first round of aggregations you perform for which you have extracted the data 
	,
cteCYLYComparison as(
select	ty.*
		,ly.GBPTurnover AS LYGBPTurnover
		,ly.RevenueMTD as LYRevenueMTD
		,ly.[Year] as LYyear
		,ly.FiscalYear as LYFiscalYear
		,ly.[Month] as LYmonth
		,ly.[Quarter] as LYquarter
		,ly.FB_Companyname as LYFB_Companyname 
--into	#CYLY
from	cte2 ty
LEFT	OUTER join	cte2 as ly 
on		ty.[FB_Companyname] = ly.FB_Companyname
and		ty.UserId = LY.UserId
and		ty.[MonthName] = ly.[MonthName]
and		ty.[FiscalYear]-1 = LY.[FiscalYear])
	
select	[YEAR]
		,[Month]
		,FiscalMonthRank
		,[FiscalYear]
		,[FiscalQuarter]
		,[fiscalyearname]
		,[FinancialMonth]
		,isnull(fb_companyname,'') as ClientName
		,isnull(userid,'') as Username
		,isnull(fb_accountcontactref,'') as AccountManager
		,LastTradeDate
		,revenuemtd
		,gbpturnover as MTDTurnOver
		,(gbpturnover/nullif(revenuemtd,0))/100 as MTDMargin
		,isnull(tradecount,0)as MTDTradeCount

		,sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)/FiscalMonthRank as AverageMonthRev
		,sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)/FiscalMonthRank as AverageMonthTurnover

		,((sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)
		/
		nullif(sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank),0)) / FiscalMonthRank) / 100 as AverageMonthMargin 

		,SUM(revenuemtd) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDRevenue 
		,SUM(gbpturnover) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDTurnover

		,(SUM(gbpturnover) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) 
		/
		nullif(SUM(revenuemtd) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) ,0) ) /100 as QTDMargin 

		,SUM(tradecount) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDTradeCount 

		,sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDRevenue 
		,sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDTurnover 
		,sum(tradecount) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDTradeCount
		
		,(sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) 
		/
		nullif(sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank),0)
		) / 100 as YTDMargin 

		,sum(lyrevenuemtd) over (partition by fb_companyname,userid)   as lyrevenue
		,sum(lygbpturnover) over (partition by fb_companyname,userid)   as lyturnover

		,isnull(lygbpturnover,0) as lastyearturnoveronecolumn 

		,(sum(lygbpturnover) over (partition by fb_companyname,userid)
		/
		nullif(sum(lyrevenuemtd) over (partition by fb_companyname,userid),0))
		/100 as lastyearmargin 
		
from	cteCYLYComparison
WHERE	FB_AccountContactRef = 'Adam Stevens'

;
select	* from #temp where fb_Companyname like 'interagro%'

exec sp_who2 active

;
