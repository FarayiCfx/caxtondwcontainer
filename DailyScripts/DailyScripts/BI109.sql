select count(*) from valitorexpiry2017
--240899
go
select count(*) from valitorexpiry2018
--119872
go

---------------------------------------------------------------------->
create view dw.BI109_EXPIRY_2017
as
select  ex17.* , 
		wl.ValitorWalletId,
		--cust.UserName, 
		cust.FirstName,
		cust.LastName,
		cust.ContactEmailAddress,
		cust.AddressLine1, 
		cust.AddressLine2,
		cust.Postcode,
		--cust.Id,
		IsVisaMigration = case left(cust.ID,3) WHEN 'mig' THEN 'Migrated' else 'NotMigrated' end,
		RFM.VALITOR_MASTERCARD_CardLoadCount,
		rfm.VALITOR_MASTERCARD_LastLoadDate,
		rfm.VALITOR_MASTERCARD_LoadTotalGBP,
		dw.UdfGetCardClaimValue(cust.customerkey) as PrimaryOrSecondary,
		cust.SourceCreationDate as CreationDate
from	ValitorExpiry2017 ex17 (nolock)
join	dw.DimValitorWallet wl (nolock)
on		ex17.IDIwallet = wl.ValitorWalletId 
join	dw.DimCustomerReference cust (nolock)
on		wl.AccountId = cust.UserName
left outer join	dw.CustomerRFMAggregations rfm
on		cust.CustomerKey = rfm.CustomerKey
where	ex17.IDIwallet <> 0

---------------------------------------------------------------------->
create view dw.BI109_EXPIRY_2018
as
select  ex18.* , 
		wl.ValitorWalletId,
		--cust.UserName, 
		cust.FirstName,
		cust.LastName,
		cust.ContactEmailAddress,
		cust.AddressLine1, 
		cust.AddressLine2,
		cust.Postcode,
		--cust.Id,
		IsVisaMigration = case left(cust.ID,3) WHEN 'mig' THEN 'Migrated' else 'NotMigrated' end,
		RFM.VALITOR_MASTERCARD_CardLoadCount,
		rfm.VALITOR_MASTERCARD_LastLoadDate,
		rfm.VALITOR_MASTERCARD_LoadTotalGBP,
		dw.UdfGetCardClaimValue(cust.customerkey) as PrimaryOrSecondary,
		cust.SourceCreationDate as CreationDate
from	ValitorExpiry2018 ex18 (nolock)
join	dw.DimValitorWallet wl (nolock)
on		ex18.WalletID = wl.ValitorWalletId 
join	dw.DimCustomerReference cust (nolock)
on		wl.AccountId = cust.UserName
left outer join	dw.CustomerRFMAggregations rfm
on		cust.CustomerKey = rfm.CustomerKey
where	ex18.WalletID <> 0



grant select on dw.BI109_EXPIRY_2017 to cfx_datareader
GO
grant select on dw.BI109_EXPIRY_2018 to cfx_datareader
GO
-------------------------------------------------only use above to create data 
select * from dw.BI109_EXPIRY_2017

select * from dw.BI109_EXPIRY_2018

--

select * from dw.CustomerRFMAggregations order by DWDateUpdated DESC


select top 5 * from ValitorExpiry2017 (nolock)
select top 5 * from [dw].[DimValitorWallet]

exec sp_spaceused 'ValitorExpiry2017'
---240 899              

select * from ValitorExpiry2017 where IDIwallet = 0


select  ex17.* , wl.*
from	ValitorExpiry2017 ex17 (nolock)
join	dw.DimValitorWallet wl (nolock)
on		ex17.IDIwallet = wl.ValitorWalletId 

select top 5 * from ValitorExpiry2018 (nolock)

select top 5 * from dw.DimCustomerReference

select * from [dw].[DimValitorWalletNonMatching]

exec sp_spaceused 'ValitorExpiry2018'
------119872              
select * from valitorexpiry2018 where walletid = 0


select  top 1000 * from valitor.FactWalletTransactions

drop table #temp1


select  * 
INTO	#TEMP1
from	dw.FactAspNetUserClaims (nolock)
WHERE	ClaimType = 'ACCOUNTTYPE'
order	by CustomerKey,ClaimValue

select * from #temp1

select	count(userid) as count_ , 
		userid
from	#temp1
group	by userid
order by count_ desc 

select	* from #temp1 where customerkey in (100957)

select *,dw.UdfGetCardClaimValue(customerkey) from #temp1
where customerkey in (100957)

exec SP_HELP 'dw.factaspnetuserclaims'


exec sp_who2 active
