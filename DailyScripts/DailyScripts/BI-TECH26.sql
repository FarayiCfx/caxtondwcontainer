alter view ClientDebtorAndCreditor
as
with	cte1 as (
select	aa.id as TradeId
		,aa.Userid
		,aa.Sellccycode
		,aa.Buyccycode
		,aa.Sellamount
		,aa.Buyamount
		,cast (aa.Tradedate as Date) as TradeDate
		,cast (aa.TradeEndDate as Date) as SettlementDate
		,aa.Tradeprofit
		,aa.Tradereason,
		bb.id as InwardPaymentId,
		--bb.tradeid,
		bb.amount as AmountPaidIn,
		bb.PaidDate as PaidInDate
from	currencybank.trade aa (nolock)
join	[CurrencyBank].[TradeDetail] bb (nolock)
on		aa.id = bb.tradeid 
where	aa.tradedate > '2017-03-31' 
and		aa.tradereason <> 'card load'),
cteDebtor as (
select	
		cast (transactiondatetime as date) as CreditDate
		,[amount] as CreditAmount
		,[Status] AS [CreditStatus]
		,externaltransactionid
		--,externaltransref
from	currencybank.accounttransaction (nolock) 
where	externaltransref   = 'trade'
and		amount > 0
and		transactiondatetime > '2017-03-31'
and [Status] like 'credit for trade%'
)

select	AA.* 
		, CreditDate
		,isnull(CreditAmount,0) as CreditAmount
		,isnull(CreditStatus,'') as CreditStatus
		,isnull(externaltransactionid,0) as externaltransactionid
from	cte1 aa
left	outer join cteDebtor bb 
on		aa.tradeid = bb.externaltransactionid
---where	aa.Tradeid = 1759955

---------------------------------------------------------------------------------
select top 10 * from currencybank.trade (nolock) where tradedate > '2017-03-31'

select top 10 * from currencybank.trade  where id = 1675902
---------------------------------------------------------------------------------


select	* 
from	currencybank.accounttransaction
where	externaltransactionid = '1675902'
order	by 1 asc 

select	* from [CurrencyBank].[TradeDetail] where tradeid = 1675902
order by 1 asc 

select  * from ClientDebtorAndCreditor where tradeid = 880126


select	count(externaltransactionid),
		externaltransactionid 
from	currencybank.accounttransaction (nolock)
where	[Status] like 'credit for trade%'
group	by externaltransactionid
having	count(externaltransactionid) > 1

--RIDICULOUSE EXAMPLE
select * from currencybank.trade where id =  880126

select * from currencybank.tradedetail where tradeid = 880126

SELECT * FROM currencybank.accounttransaction  WHERE EXTERNALTRANSACTIONID = '880126'


select * from currencybank.trade (nolock) where userid = 'krichards@samsoncontrols.co.uk'
select * from currencybank.accountsummary where userid = 'krichards@samsoncontrols.co.uk'



select * from currencybank.accounttransaction 
where accountsummaryid in (630540,630541) and	[Status] like 'credit for%'

SELECT TOP 10 * FROM ClientDebtorAndCreditor
