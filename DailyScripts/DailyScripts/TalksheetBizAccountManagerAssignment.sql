select * from [Firebird].[AccountManager] 

select * from _accountmanager_updates

select	* 
into	[Firebird].[UserAccountManagerAssociation_backup20_07_2017]
from	[Firebird].[UserAccountManagerAssociation]
;

IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go

with	cte1 as (select * from [firebird].[accountmanager] where accounttyperef = 'retail')
select	aa.* , bb.*
into	#temp1
from	_accountmanager_updates aa
left	outer join	cte1 bb
on		aa.[Account Manager] = bb.UserName
-----------------------------------------------------------------------------------------------
select	aa.Id
		,aa.AccountManagerId
		,cast (aa.UserId as nvarchar(512)) as UserId 
		,aa.DateCreated
		,aa.IsDeleted
		,aa.FB_DateCreated
		,aa.FB_DateUpdated
		,aa.FB_DateDeleted
		,bb.username
		,bb.fullname
from	[Firebird].[UserAccountManagerAssociation] aa (nolock)
join	[Firebird].[AccountManager] bb (nolock)
on		aa.accountmanagerid = bb.id


exec sp_help '[Firebird].[UserAccountManagerAssociation]'


/*
insert  into [Firebird].[UserAccountManagerAssociation] (AccountManagerId,UserId,DateCreated,IsDeleted,FB_DateCreated)
select	isnull([id],21),userid,getdate(),0,getdate()
from	#temp1
------------------------------------------------------------------------------------------------------
insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('Matthew Downward','Matthew Downward',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('Adam Stevens','Adam Stevens',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('Rehan Ansari','Rehan Ansari',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('Jack Lane-Matthews','Jack Lane-Matthews',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('Anthony Creese','Anthony Creese',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('James Tinsley','James Tinsley',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('Matthew Downward','Matthew Downward',getdate(),'Retail')

insert into [Firebird].[AccountManager] (username,FullName,FB_DateCreated,AccountTypeRef)
values ('System','System',getdate(),'Retail')
*/