--anything that matches on registrationnumber 
create view Firebird_To_Sage_Matches
as
WITH	cte1 AS (
SELECT	aa.username as FB_Username,
		aa.fastpaycontactref as FB_FastpayContactref, 
		ua.corporateentityid as FB_CorporateEntityid,
		ua.corporaterole as FB_CorporateRole,
		cc.companyname as FB_Companyname,
		cc.registrationnumber AS FB_RegistrationNumber, 
		cc.SalesContactRef as FB_SalesContactRef,
		cc.AccountContactRef as FB_AccountContactRef
from	dw.dimcustomerreference aa (NOLOCK)
join	dw.dimusercorporateassociation ua (NOLOCK)
on		aa.username = ua.email
join	dw.dimcorporateentities cc (NOLOCK)
on		ua.corporateentityid = cc.id
)

--insert into #resultSet

select	cte1.*,	
		sg.comp_mtnumber, 
		sg.comp_sales_person, 
		sg.comp_accountmanagermt,
		sg.comp_name, 
		sg.[account manager],
		sg.comp_registration_alphanumeric ,
		sg.[Activated Date],
		sg.[Reactivated Date],
		'Matches on registrationnumber'  as MatchType 
from	cte1 
join	dbfx.clientlistcrm sg
on		cte1.FB_RegistrationNumber = comp_registration_alphanumeric
union 
select	cte1.*,	
		sg.comp_mtnumber, 
		sg.comp_sales_person, 
		sg.comp_accountmanagermt,
		sg.comp_name, 
		sg.[account manager],
		sg.comp_registration_alphanumeric ,
		sg.[Activated Date],
		sg.[Reactivated Date],
		'Matches on fastpay contactref'  as MatchType 
from	cte1 
join	dbfx.clientlistcrm sg
on		cte1.FB_FastpayContactref = cast(sg.comp_mtnumber as varchar(50))



select * from UnMatchedFirbirdToSageCompanies

--not worried about the sage side, focus on the Firebird side 
--here are your unmatched companies from firebird to sage 
create	view UnMatchedFirbirdToSageCompanies
as
select	id,companyname,registrationnumber,SalesContactRef,AccountContactRef,TalksheetMigrated,fb_datecreated
from	dw.dimcorporateentities aa (nolock)
where	id not in (select  distinct fb_Corporateentityid from Firebird_To_Sage_Matches)
order	by fb_datecreated desc 

select  * from firebird_to_sage_matches where fb_username = 'jess.sturkey@bulkpowders.com'


select top 10 * from unmatchedfirbirdtosagecompanies
order by fb_datecreated desc
