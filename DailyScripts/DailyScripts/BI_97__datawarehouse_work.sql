IF		OBJECT_ID('TEMPDB..#CorpFastPay') IS NOT NULL
DROP	TABLE #CorpFastPay
go
create	table adhoc.CorpFastPay(
idrow	int identity (1,1),
email_	varchar (255),
entity  varchar	(10) default 'Corporate')

-----------------------------------------------------------------------
insert into adhoc.CorpFastPay(email_) values('beth@bethwightman.com')
insert into adhoc.CorpFastPay(email_) values('laurence@whitebrandagency.com')
insert into adhoc.CorpFastPay(email_) values('elizabeth@flooredgenius.com')
insert into adhoc.CorpFastPay(email_) values('Mark.Brewer@bioss.ac.uk')
insert into adhoc.CorpFastPay(email_) values('carsandparts@middlebartongarage.com')
insert into adhoc.CorpFastPay(email_) values('joachim.thuex@yahoo.co.uk')
insert into adhoc.CorpFastPay(email_) values('andrew@triptease.com')
insert into adhoc.CorpFastPay(email_) values('robin@rmscm.co.uk')
insert into adhoc.CorpFastPay(email_) values('support@cookiebite.net')
insert into adhoc.CorpFastPay(email_) values('Bayo@plenishcleanse.com')
insert into adhoc.CorpFastPay(email_) values('tim@firefly-collection.com')
insert into adhoc.CorpFastPay(email_) values('accounts@fluidity-f2.com')
insert into adhoc.CorpFastPay(email_) values('rob.cooksey@pearlanddean.com')
insert into adhoc.CorpFastPay(email_) values('rebecca@boho-betty.com')
insert into adhoc.CorpFastPay(email_) values('ab@thefishsociety.co.uk')
insert into adhoc.CorpFastPay(email_) values('vintage@susancaplan.co.uk')
insert into adhoc.CorpFastPay(email_) values('steve@admerchandise.co.uk')
insert into adhoc.CorpFastPay(email_) values('info@fimark.com')
insert into adhoc.CorpFastPay(email_) values('carlos@structuredrevolution.com')
insert into adhoc.CorpFastPay(email_) values('info@merxconcierge.co.uk')
insert into adhoc.CorpFastPay(email_) values('paul@jjvaillant.co.uk')
insert into adhoc.CorpFastPay(email_) values('accounts@sterlingip.co.uk')
insert into adhoc.CorpFastPay(email_) values('steve@jumbocruiser.com')
insert into adhoc.CorpFastPay(email_) values('lalage@lalagebeaumont.com')
insert into adhoc.CorpFastPay(email_) values('anna@plumboutique.co.uk')
insert into adhoc.CorpFastPay(email_) values('accounts@autocraftequipment.co.uk')
insert into adhoc.CorpFastPay(email_) values('billing@leadinglocally.com')
insert into adhoc.CorpFastPay(email_) values('shohreh@tmgcl.com')
insert into adhoc.CorpFastPay(email_) values('ahamer@amhresearch.com')
insert into adhoc.CorpFastPay(email_) values('sales@bsgtractorsandmachinery.co.uk')
insert into adhoc.CorpFastPay(email_) values('gilles.wafflart@btinternet.com')
insert into adhoc.CorpFastPay(email_) values('Bursar@greenes.org.uk')
insert into adhoc.CorpFastPay(email_) values('office@getcycling.org.uk')
insert into adhoc.CorpFastPay(email_) values('rathbone@me.com')
insert into adhoc.CorpFastPay(email_) values('finance@situ.co.uk')
insert into adhoc.CorpFastPay(email_) values('abell@chartered.org')
insert into adhoc.CorpFastPay(email_) values('carole.harvey@pandect.co.uk')
insert into adhoc.CorpFastPay(email_) values('tansy@fmtv.london')
insert into adhoc.CorpFastPay(email_) values('trisha.miller@sunamp.co.uk')
insert into adhoc.CorpFastPay(email_) values('pingrulo@phmr.com')

-----
select * from adhoc.CorpFastPay
-----

select		count(id) as count_, 
			sum(tradeprofit) as sumtradeprofit,
			cast (TradeDate as date ) as Date_,
			isnull(entity,'Retail') as entity,
			aa.TradeReason
from		FireBird.FactCurrencyBankTrade aa (nolock)
left		outer join #corpfastpay cp
on			aa.userid = cp.email_
where		TradeDate	between '2016-09-12 17:30:00.000' 
						and		'2016-09-13 17:30:00.000'
						and		aa.TradeReason <> 'card load'
group	by	cast(TradeDate as date), 
			TradeReason ,
			isnull(entity,'Retail') 
order	by	cast(TradeDate as date) 

exec sp_who2

select top 10 * from dw.FactApplicants order by 1 desc 

select top 100 * 
from dbfx.DealList
where dealno > 388963

select * from dw.CustomDatesForRevReport


--------------------------------------------------------------------------------------------------------------------------------------------------------------
----work out the date and time frames using a recursive cte 
with	dates as (
select	[date] =  cast('2016-01-01 17:30:00.000' as datetime)
union	all select [date] = DATEADD(dd,1,[date])
FROM	DATES
where	[date] < '2020-01-01 17:30:00.000'
)
select	IDENTITY(int, 1,1) AS Period_Number, 
		[date] as Start_Period ,
		dateadd(dd,1,[date]) as End_Period,
		CAST(dateadd(dd,1,[date]) AS DATE) as Report_Date
----into	dw.CustomDatesForRevReport
from	dates 
option	(MAXRECURSION 2000)

exec sp_help 'dw.CustomDatesForRevReport'

------------------------------------------------------------------------------------------------------------------------------------------------------------

declare		@start datetime,
			@end datetime,
			@Report_date date

select		@start = start_period ,
			@end = end_period ,
			@Report_date = Report_Date
			from dw.CustomDatesForRevReport 
			where report_date = cast(getdate()-4 as date);

--print		@start
--print		@end
--print		@Report_date;

-------------------------------------
WITH CTE1	AS( 
select		 tradeprofitGBP = case when	aa.SellCcyCode = 'GBP' or aa.BuyCcyCode = 'GBP' then aa.TradeProfit	
								when	aa.SellCcyCode = 'EUR' or aa.BuyCcyCode = 'EUR' then [dw].[UdfGetGBPValue](aa.TradeProfit,'EUR')
								when	aa.SellCcyCode = 'USD' or aa.BuyCcyCode = 'USD' then [dw].[UdfGetGBPValue](aa.TradeProfit,'USD')
								else	[dw].[UdfGetGBPValue](aa.TradeProfit,aa.SellCcyCode)
								end
								,*
from		FireBird.FactCurrencyBankTrade aa (nolock) 
where		TradeDate	between @start
						and		@end
						and		aa.TradeReason <> 'card load')
						
select		count(id) as count_, 
			sum(tradeprofitGBP) as sumtradeprofitGBP,
			cast (TradeDate as date ) as Source_Date,
			cast (isnull(entity,'Retail') as nvarchar(20)) as entity,
			cast(aa.TradeReason as nvarchar(20)) as TradeReason,
			'FireBird' as SourceSystem,
			@start as Start_period,
			@end as End_Period ,
			@Report_date as Report_Date
from		CTE1 aa (nolock)
left		outer join adhoc.corpfastpay cp
on			aa.userid = cp.email_
where		AA.TradeDate	between @start
						and		@end
						and		aa.TradeReason <> 'card load'
group	by	cast(TradeDate as date), 
			TradeReason ,
			isnull(entity,'Retail') 
go


---->

declare		@start datetime,
			@end datetime,
			@Report_Date date

select		@start = start_period ,
			@end = end_period ,
			@Report_Date = Report_date
from		dw.CustomDatesForRevReport 
where		report_date = cast(getdate()-4 as date)

--print		@start
--print		@end
--print		@Report_Date


/*select *	from dbfx.DealList where dealdate between @start and @end and 
CompanyName not like '%Visa Card%'
order		by dealdate asc */

select		cast(DealDate as date) as Source_Date,
			count (dealno) as count_,
			--sum(revenue) as RevSum,
			[dw].[UdfGetGBPValue](sum(revenue),revenuecurrency) as RevSumGBP,
			cast (accounttype as nvarchar(50)) as AccountType ,
			Entity = case ISNULL(accounttype,'NONE') 
						when 'None' then cast('Retail' as NVARCHAR(20))
						when 'Referer' then cast('Referer' as nvarchar(20))
						else cast('Corporate' as nvarchar(20))	END  ,
			'TalkSheet' as SourceSystem ,
			@start as Start_Period,
			@end as End_Period ,
			@Report_Date as Report_Date
from		dbfx.DealList where dealdate between @start and @end
and			CompanyName not like '%Visa Card%'
group		by	cast(DealDate as date),
				RevenueCurrency,
				accounttype 

---truncate table dbfx.deallist


select *	from dbfx.DealList
where		CompanyName like '%visa card%'
order		by dealdate 

select * from tab1
select * from tab2

drop table tab1
drop table tab2


select * from dw.CustomDatesForRevReport 





declare		@start datetime,
			@end datetime,
			@Report_Date date

select		@start = start_period ,
			@end = end_period ,
			@Report_Date = Report_date
from		dw.CustomDatesForRevReport 
where		report_date = cast(getdate()-4 as date)

--print		@start
--print		@end
--print		@Report_Date


/*select *	from dbfx.DealList where dealdate between @start and @end and 
CompanyName not like '%Visa Card%'
order		by dealdate asc */

select		cast(DealDate as date) as Source_Date,
			count (dealno) as count_,
			--sum(revenue) as RevSum,
			[dw].[UdfGetGBPValue](sum(revenue),revenuecurrency) as RevSumGBP,
			cast (accounttype as nvarchar(50)) as AccountType ,
			Entity = case ISNULL(accounttype,'NONE') 
						when 'None' then cast('Retail' as NVARCHAR(20))
						when 'Referer' then cast('Referer' as nvarchar(20))
						else cast('Corporate' as nvarchar(20))	END  ,
			'TalkSheet' as SourceSystem ,
			@start as Start_Period,
			@end as End_Period ,
			@Report_Date as Report_Date
from		dbfx.DealList where dealdate between @start and @end
and			CompanyName not like '%Visa Card%'
group		by	cast(DealDate as date),
				RevenueCurrency,
				accounttype 





				select	* ,[month]
				from	dw.CustomDatesForRevReport


				exec sp_help 'dw.CustomDatesForRevReport'

-------------------------------------------------------------------------------------------------------------------------------------------------------
				
create table	dw.CustomDatesForRevReport (
				[Period_Number] [int] IDENTITY(1,1) NOT NULL,
				[Start_Period] [datetime] NULL,
				[End_Period] [datetime] NULL,
				[Report_Date] [date] NULL,
				[MonthName] varchar(100),
				[Corporate FX monthly total budget] numeric(28,2) null,
				[Corporate FX daily total budget] numeric (28,2) null,
				[Retail FX monthly total budget] numeric (28,2) null,
				[Retail FX daily total budget] numeric (28,2))
				
				
with	dates as (
select	[date] =  cast('2016-01-01 17:30:00.000' as datetime)
union	all select [date] = DATEADD(dd,1,[date])
FROM	DATES
where	[date] < '2020-01-01 17:30:00.000'
)
insert	into dw.CustomDatesForRevReport ([Start_Period],[End_Period],[Report_Date],[MonthName])
		select [date] /*as Start_Period*/ ,
		dateadd(dd,1,[date]) /*as End_Period*/,
		CAST(dateadd(dd,1,[date]) AS DATE) /*as Report_Date*/,
		datename ([month],dateadd(dd,1,[date]))
from	dates 
option	(MAXRECURSION 2000)
------------------------------------------------------------------------------------------------------------


SELECT	datepart (year,report_date),
		* 
from	dw.CustomDatesForRevReport
where	datepart (year,report_date) = '2016'



update	dw.CustomDatesForRevReports
set		[Corporate FX monthly total budget] = 143733.00 ,
		[Corporate FX daily total budget] = 6844.00 ,
		[Retail FX monthly total budget] = 259257.00,
		[Retail FX daily total budget] = 12345.57
		WHERE	MonthName = 'March'
		AND		datepart (year,report_date) = '2016'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



select * from dw.CustomDatesForRevReport 

declare		@start datetime,
			@end datetime,
			@Report_Date date,
			@corporate_fx_monthly_total_budget numeric (28,2),
			@corporate_fx_Daily_total_budget numeric (28,2),
			@retail_fx_monthly_total_budget numeric (28,2),
			@retail_fx_Daily_total_budget numeric (28,2)

select		@start = start_period ,
			@end = end_period ,
			@Report_Date = Report_date,
			@corporate_fx_monthly_total_budget = [Corporate FX monthly total budget],
			@corporate_fx_daily_total_budget = [Corporate FX daily total budget],
			@retail_fx_monthly_total_budget = [Retail FX monthly total budget],
			@retail_fx_Daily_total_budget = [Retail FX daily total budget]
from		dw.CustomDatesForRevReport 
where		report_date = cast(getdate() as date)

--print		@start
--print		@end
--print		@Report_Date
--print		@corporate_fx_monthly_total_budget


/*select *	from dbfx.DealList where dealdate between @start and @end and 
CompanyName not like '%Visa Card%'
order		by dealdate asc */

select		cast(DealDate as date) as Source_Date,
			count (dealno) as count_,
			--sum(revenue) as RevSum,
			[dw].[UdfGetGBPValue](sum(revenue),revenuecurrency) as RevSumGBP,
			cast (accounttype as nvarchar(50)) as AccountType ,
			Entity = case ISNULL(accounttype,'NONE') 
						when 'None' then cast('Retail' as NVARCHAR(20))
						when 'Referer' then cast('Referer' as nvarchar(20))
						else cast('Corporate' as nvarchar(20))	END  ,
			'TalkSheet' as SourceSystem ,
			@start as Start_Period,
			@end as End_Period ,
			@Report_Date as Report_Date,
			@corporate_fx_monthly_total_budget as [Corporate FX monthly total budget],
			@corporate_fx_daily_total_budget as [Corporate FX daily total budget],
			@retail_fx_monthly_total_budget as [Retail FX monthly total budget],
			@retail_fx_Daily_total_budget AS [Retail FX daily total budget]
from		dbfx.DealList 
where		dealdate between @start and @end
and			CompanyName not like '%Visa Card%'
group		by	cast(DealDate as date),
				RevenueCurrency,
				accounttype 

------->


declare		@start datetime,
			@end datetime,
			@Report_date date,
			@corporate_fx_monthly_total_budget numeric (28,2),
			@corporate_fx_Daily_total_budget numeric (28,2),
			@retail_fx_monthly_total_budget numeric (28,2),
			@retail_fx_Daily_total_budget numeric (28,2)

select		@start = start_period ,
			@end = end_period ,
			@Report_date = Report_Date,
			@corporate_fx_monthly_total_budget = [Corporate FX monthly total budget],
			@corporate_fx_daily_total_budget = [Corporate FX daily total budget],
			@retail_fx_monthly_total_budget = [Retail FX monthly total budget],
			@retail_fx_Daily_total_budget = [Retail FX daily total budget]
			from dw.CustomDatesForRevReport 
			where report_date = cast(getdate() as date);

--print		@start
--print		@end
--print		@Report_date;

-------------------------------------
WITH CTE1	AS( 
select		 tradeprofitGBP = case when	aa.SellCcyCode = 'GBP' or aa.BuyCcyCode = 'GBP' then aa.TradeProfit	
								when	aa.SellCcyCode = 'EUR' or aa.BuyCcyCode = 'EUR' then [dw].[UdfGetGBPValue](aa.TradeProfit,'EUR')
								when	aa.SellCcyCode = 'USD' or aa.BuyCcyCode = 'USD' then [dw].[UdfGetGBPValue](aa.TradeProfit,'USD')
								else	[dw].[UdfGetGBPValue](aa.TradeProfit,aa.SellCcyCode)
								end
								,*
from		FireBird.FactCurrencyBankTrade aa (nolock) 
where		TradeDate	between @start
						and		@end
						and		aa.TradeReason <> 'card load')
						
select		count(id) as count_, 
			sum(tradeprofitGBP) as sumtradeprofitGBP,
			cast (TradeDate as date ) as Source_Date,
			cast (isnull(entity,'Retail') as nvarchar(20)) as entity,
			cast(aa.TradeReason as nvarchar(20)) as TradeReason,
			'FireBird' as SourceSystem,
			@start as Start_period,
			@end as End_Period ,
			@Report_date as Report_Date,
			@corporate_fx_monthly_total_budget as [Corporate FX monthly total budget],
			@corporate_fx_daily_total_budget as [Corporate FX daily total budget],
			@retail_fx_monthly_total_budget as [Retail FX monthly total budget],
			@retail_fx_Daily_total_budget AS [Retail FX daily total budget]
from		CTE1 aa (nolock)
left		outer join adhoc.corpfastpay cp
on			aa.userid = cp.email_
where		AA.TradeDate	between @start
						and		@end
						and		aa.TradeReason <> 'card load'
group	by	cast(TradeDate as date), 
			TradeReason ,
			isnull(entity,'Retail')