exec sp_help 'staging.FactPaymentTransfer'

select * from staging.FactPaymentTransfer
select * from firebird.factpaymenttransfer

alter procedure dw.mergefactpaymenttransfer
/*
This stored procedure will merge the payment.transfer data
it has to be a mergebecause some of the edata 
*/
as

set NOCOUNT on


merge	firebird.factpaymenttransfer as target
using	(select customerkey,
				[dw].[udfgetdateinteger](cast(executeddate as date)) as ExecutedDateKey, 
				[dw].[udfgetdateinteger](PayAwayDate) as PayAwayDateKey,
				[dw].[udfgetdateinteger](cast (CreatedDate as date)) as CreatedDateKey,
				[dw].[udfgetdateinteger](cast (PaymentProcessingDate as date)) as PaymentProcessingDateKey,
				isnull(bb.ISOCurrency_KEY,0) as BuyCCYCodeISOCurrencyKey,
				Id,BeneficiaryId,UserId,BuyCcyCode,BuyAmount,Complete,[Automatic],ExecutedDate,PayAwayDate,
				PayStatus,WorkflowInstanceId,ResubmitCount,AccountTransactionId,Authorisation,AuthorisationStatus,
				FileAccepted,PaymentFile,FileSent,PaymentAccepted,CreatedDate,PaymentProcessing,PaymentProcessingDate,
				UrgentIntl,RejectionReason,DWDateInserted,DWDateUpdated
		from staging.factpaymenttransfer aa
		left outer join dw.DimISOCurrencyName bb
		on aa.BuyCcyCode = bb.NameCode ) as source

on		(target.id = source.id)
when	matched then 
update	set executeddate = source.executeddate, --this is the only thing that will actually change 
			dwdateupdated = source.dwdateupdated --so we also want to remember the date of the change 
			when not matched then

insert		(CustomerKey,
			ExecutedDateKey	,
			PayAwayDateKey,
			createddatekey,
			PaymentProcesingDateKey,
			BuyCCyCodeISOCurrencyKey,
			Id,
			BeneficiaryId,
			UserId,
			BuyCcyCode,
			BuyAmount,
			Complete,
			[Automatic],
			ExecutedDate,
			PayAwayDate,
			PayStatus,
			WorkflowInstanceId,
			ResubmitCount,
			AccountTransactionId,
			Authorisation,
			AuthorisationStatus,
			FileAccepted,
			PaymentFile,
			FileSent,
			PaymentAccepted,
			CreatedDate,
			PaymentProcessing,
			PaymentProcessingDate,
			UrgentIntl,
			RejectionReason,
			DWDateInserted,
			DWDateUpdated)
values	(
			source.CustomerKey,
			source.ExecutedDateKey,
			source.PayAwayDateKey,
			source.CreatedDateKey,
			source.PaymentProcessingDateKey,
			source.BuyCCyCodeISOCurrencyKey,
			source.Id,
			source.BeneficiaryId,
			source.UserId,
			source.BuyCcyCode,
			source.BuyAmount,
			source.Complete,
			source.Automatic,
			source.ExecutedDate,
			source.PayAwayDate,
			source.PayStatus,
			source.WorkflowInstanceId,
			source.ResubmitCount,
			source.AccountTransactionId,
			source.Authorisation,
			source.AuthorisationStatus,
			source.FileAccepted,
			source.PaymentFile,
			source.FileSent,
			source.PaymentAccepted,
			source.CreatedDate,
			source.PaymentProcessing,
			source.PaymentProcessingDate,
			source.UrgentIntl,
			source.RejectionReason,
			source.DWDateInserted,
			source.DWDateUpdated
);
GO

----->
drop table [firebird].[factpaymenttransfer]
go

create table [firebird].[factpaymenttransfer](
	[customerkey] bigint,
	[executeddatekey] int,
	[payawaydatekey] int,
	[createddatekey] int,
	[paymentprocesingdatekey] int,
	[buyccycodeisocurrencykey]int, 
	
	[id] [bigint] not null, --this should still be the primary key for the table 
	[beneficiaryid] [bigint] not null,
	[userid] [nvarchar](max) null,
	[buyccycode] [nvarchar](max) null,
	[buyamount] [decimal](18, 2) not null,
	[complete] [bit] not null,
	[automatic] [bit] not null default ((0)),
	[executeddate] [datetime] null,
	[payawaydate] [datetime] null,
	[paystatus] [nvarchar](max) null,
	[workflowinstanceid] [uniqueidentifier] null,
	[resubmitcount] [bigint] not null default ((0)),
	[accounttransactionid] [bigint] not null default ((0)),
	[authorisation] [nvarchar](max) null,
	[authorisationstatus] [tinyint] null,
	[fileaccepted] [tinyint] null,
	[paymentfile] [nvarchar](max) null,
	[filesent] [datetime] null,
	[paymentaccepted] [tinyint] null,
	[createddate] [datetime] null,
	[paymentprocessing] [tinyint] null,
	[paymentprocessingdate] [datetime] null,
	[urgentintl] [tinyint] null,
	[rejectionreason] [nvarchar](255) null,
	[dwdateinserted] datetime,
	[dwdateupdated] datetime 
 constraint [pk_payment.transfer] primary key clustered 
(
	[id] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on)
)



select * from firebird.factpaymenttransfer

select isnull(max(id),0) from firebird.factpaymenttransfer

select isnull(max(DWDateInserted),getdate()) from  firebird.factpaymenttransfer


DECLARE @datetimeoffset datetimeoffset(4) = '12-10-25 12:32:10 +01:00';  
DECLARE @date date= @datetimeoffset;  
  
SELECT	@datetimeoffset AS '@datetimeoffset ', @date AS 'date';  


select datetimeoffset(4) = getdate()


select * from dw.ContactEmailChangeAuditLog
