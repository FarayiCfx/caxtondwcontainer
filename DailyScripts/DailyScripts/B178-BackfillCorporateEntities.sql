-->do a backup of the table first :
select	* 
into	[CorporateEntities_backup_20_06_2017]
from	[dbo].[CorporateEntities]


-->So this tells me the FIREBIRD entities 
IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1
;
with	cte1 as (select distinct fb_companyname ,[account manager],comp_sales_person,salesperson
				from [Firebird_To_Sage_Matches])
SELECT	aa.id ,aa.companyname,aa.registrationnumber as registrationnumber,
		aa.salescontactref as fb_salecontactref
		,aa.accountcontactref as fb_accountcontactref
		,bb.*
		,aa.fb_datecreated,
		aa.fb_Dateupdated
		,'' as Done
INTO	#TEMP1
FROM	[CorporateEntities]  aa (nolock)
left	outer join cte1 bb
on		aa.companyname = bb.fb_companyname
order	by fb_datecreated desc 

select * from #temp1 order by fb_datecreated desc


----------------------------------------------------------------------------------------------
-->select and view the sales contactref where the data is null
SELECT * FROM #TEMP1 WHERE SALESCONTACTREF IS NULL AND SALESPERSON IS NOT NULL

-->update the sales contactref where the data is null
update	cc
set		cc.salescontactref  =  tmp.salesperson
--select cc.salescontactref,cc.*
from	corporateentities cc
join	#temp1 tmp on cc.id = tmp.[Id]
where	cc.salescontactref is null
--4912 rows updated 
---------------------------------------------------------------------------------------------
-->select and view the sales contactref where the data is null
SELECT * FROM #TEMP1 WHERE accountcontactref IS NULL AND [account manager] IS NOT NULL

-->update the sales contactref where the data is null
update	cc
set		cc.accountcontactref  =  tmp.[account manager] 
--select cc.accountcontactref,tmp.[account manager] ,cc.*
from	corporateentities cc
join	#temp1 tmp on cc.id = tmp.[Id]
where	cc.accountcontactref is null
AND		[account manager] IS NOT NULL

--68 rows updated 

select * from #temp1 where salescontactref is null order by fb_datecreated desc 
--1485 rows 

select * from #temp1 where accountcontactref is null order by fb_datecreated desc 
--914 rows 

select	fb_datecreated,
		id as fb_companyid,
		companyname as fb_companyname,
		registrationnumber as fb_registrationnumber,
		salescontactref as fb_salescontactref,
		accountcontactref as fb_accountcontactref,
		[account manager] as [account manager(sage)],
		comp_Sales_person as [comp_sales_person(sage)],
		salesperson as [salesperson(sage)],
		fb_dateupdated,
		'' as Done
from	#temp1 
where	accountcontactref is null
or		salescontactref is null
order	by fb_datecreated desc 

----------------------------------------------------------------------------------------------
--these are the accounts without corporate entities 
with	cte1 as (
select	aa.id,aa.fastpaycontactref,bb.companyname,bb.registrationnumber, bb.accountcontactref
from	aspnetusers aa (nolock)
left	outer join	corporateentities bb
on		aa.fastpaycontactref = bb.accountcontactref
where	accounttyperef  = 'talksheetbiz')
select	distinct fastpaycontactref from cte1 where companyname is null







