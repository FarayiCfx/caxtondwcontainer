exec sp_help '[staging].[FireBirdFactCurrencyBankTrade_V2]'

CREATE TABLE [staging].[FireBirdFactCurrencyBankTrade_V2](
	[CustomerKey] [bigint] NULL,
	[TradeDateKey] [int] NULL,
	[Id] [bigint] NOT NULL,
	[UserId] [nvarchar](256) NULL,
	[SellCcyCode] [nvarchar](100) NULL,
	[BuyCcyCode] [nvarchar](100) NULL,
	[SellAmount] [decimal](18, 2) NOT NULL,
	[BuyAmount] [decimal](18, 2) NOT NULL,
	[Settled] [bit] NOT NULL,
	[BuyCcyClientTradingBookId] [bigint] NOT NULL,
	[SellCcyClientTradingBookId] [bigint] NOT NULL,
	[WorkflowInstanceId] [uniqueidentifier] NULL,
	[TradeDate] [datetime] NULL,
	[IsAutomatic] [bit] NOT NULL,
	[TradeEndDate] [datetime] NOT NULL,
	[TradeProfit] [decimal](18, 2) NOT NULL,
	[TreasuryProfit] [decimal](18, 2) NOT NULL,
	[Reversal] [bit] NOT NULL,
	[IsReversed] [bit] NOT NULL,
	[IsUnWound] [bit] NOT NULL,
	[TradeReason] [nvarchar](50) NULL,
	[HelpdeskUserId] [nvarchar](256) NULL,
	[TransferId] [bigint] NOT NULL,
	[TradeNotes] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
	[DWDateInserted] [datetime] NULL,
	[DWDateUpdated] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.StagingFactCurrencyBankTradeV2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT (getdate()) FOR [TradeDate]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [IsAutomatic]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [TradeEndDate]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [TradeProfit]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [TreasuryProfit]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [Reversal]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [IsReversed]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [IsUnWound]
GO

ALTER TABLE [staging].[FireBirdFactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [TransferId]
GO


