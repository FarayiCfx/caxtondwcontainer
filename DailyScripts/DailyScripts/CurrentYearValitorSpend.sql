create view CurrentYearValitorSpend
as
/*
View to read the valitor data and sum up the spend for the current year
Then pivot the data so that it goes across and not down per month 
*/
select	customerkey as Customerkey
		,datename(year,getdate()) as [Year]
		,isnull(January,0) as January
		,isnull(February,0) as February
		,isnull(March,0) as March
		,isnull(April,0) as April 
		,isnull(May,0) as May
		,isnull(June,0) as June
		,isnull(July,0) as July 
		,isnull(August,0) as August
		,isnull(September,0) as September
		,isnull(October,0) as October 
		,isnull(November,0) as November
		,isnull(December,0) as December 
from 
(select	sum(transactionamountgbp) as SumGBPTransactionAmount
		,customerkey 
		,datename(month,transactiondatetime) as  MonthOfCurrentYear
from	valitor.factwallettransactions (nolock)
where	transactiondatetime >= DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)
group	by customerkey ,datename(month,transactiondatetime)
) as SourceTable
pivot
(
sum	(SumGBPTransactionAmount) 
for MonthOfCurrentYear in (January,February,March,April,May,June,July,August,September,October,November,December
)) As PivotTable 



exec sp_who2 active


select * from CurrentYearValitorSpend order by 1

