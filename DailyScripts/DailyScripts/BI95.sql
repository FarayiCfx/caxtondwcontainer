IF		OBJECT_ID('tempdb..#temptable') IS NOT NULL
DROP	TABLE #temptable
go
create	table #temptable (idrow int identity (1,1),
						company_name varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS)
GO


INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Company Name') 
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Thurrock Christian Fellowship')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Adept RF Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Luck Vision Partnership Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Sturge Industries Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Building Technology Systems Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('MPI Brokers Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('International Fellowship Of Evangelical Students ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Gap Medics Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Whitworth Media Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Stita (S.T International Tourist Agency Llp)')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Home Hardware ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Culbone Marine Finance Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Design Blue Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('British Energy Saving Technology')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Roller Coaster Club Of Great Britian')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Green Bear UK Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Helen Amy Murray Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Swim Trek Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Dean Close School ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Centre For Local Economic Strategies')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Belfast Royal Academy')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Ivybridge Academy Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Globesource Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('2Serve Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Woodlands School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Mercanta Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The School Travel Company Ltd.')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('THMK49 Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Chiltern Hills Academy')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Nomadic Expeditions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Donkey Sanctuary Trustee Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Profimation Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Meridien Apparel Solutions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Pierce Recovery And Breakdown')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Kingdom Creative Studios Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Hurstpierpoint College Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Premier Fashions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Inclusion Barnet')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Stratford Girls Grammar School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Wellboring')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Tackle Africa ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Bridgwater College')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Max Travel Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Barrington Hill Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Little Pickers Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Hotelrez Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Shephard Press Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Haybridge High School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Chameleon Lean Solutions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Licensed Trade Charity')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Idoui Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Engineering Contractor Services Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Target Umbrella Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Dreamwebs Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Boohoo.com')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Modus Furniture ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Metals Focus Data Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Travelfab Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Moorgate Communications')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('TFT (Ilkley) Ltd (testing for textiles)')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Seltek Consultants')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Rendcomb College')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Durbin Plc')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Lawscript ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Skill Centre Ltd.')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Williams And Hill Forwarding Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Apollo Chemicals Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Thasi Ltd Ta The British Shop')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Cognitive Applications Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('ARU Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Prospect Health')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Saddle Skedaddle ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Travellers Secrets Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Osprey Europe Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Grammar School At Leeds ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Specialist Tours Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Floyd Technologies Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Dwr Property Llp')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Theobald barber Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Beauworth Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Adsensa Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('2 Build Exhibitions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Ossianix')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Marketforce Business Media Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('International Netball Federation')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Discover The World Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Substation Design Applications Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Shiplake College ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Cossack Labs Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('AC Insight Partners Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Halbro Sportswear Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Chr Travel Llp')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('London School Of Business And Finance ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Afrika Kapital Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Bello Visto')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('4bit Consulting Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Botteau Offshore Site Services Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Delancey Business Management ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Perspektiv Optics Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Kenyan Adventure Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Find A Future')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Solihull School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Cafe Azzurro Coffee Company Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Vetstream Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Association of British Orchestras')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Univeg Katope UK Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Fendahl Technology Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Open Development Services (UK) Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('One Traveller Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Rodney Tolley Walks')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Curbralan Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Heritage And Archaeological Research')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Turing Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Belfast Visitor And Convention Bureau ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Leisure And Outdoor Furniture Assocation ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Backwater Classic Car Tours')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Avalon Sciences Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Tettenhall College ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Zenprop UK Management Services Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Portsmouth High School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Photovoice')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Leventhorpe Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Ian Russel')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Solstor UK Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Creditsafe Business Solutions')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Peterborough School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Sirva Relocation (No.1) Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('M-Piric Audio Visual Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Sovereign Tourism Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Man Consultancy Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Oxford Learning Solutions')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Orchestrate Media Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Impulse Racing Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('DCK Concessions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Moondance Sailing Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('York College')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Gresham''s School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('CTC Cycling Holidays And Touring')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Alpine Guides Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('African Childrens Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('World Association Of Girl Guides And Girl Scouts')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Atlantic Marine & Aviation LLP')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Daniel Johns Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Volt Europe ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Interagro UK Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('NC Squared Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Mafana Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Stephen Perse Foundation ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Tour Constructor Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Appleton School Academy Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('AEI Group Ltd / AEI Live Ltd (AEI Media)')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Argans Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Topcashback Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Powder Byrne International Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('South Hampstead High School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Mike Richards Recruitment ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Holt School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Park Hall Academy')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Zetechtics Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Prompt Maternity Foundation')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Officetwelve Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Kettering Buccleuch Academy Ulst')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('United Church Schools Trust')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Kirintec Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Haal Media Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Phoenix Financial Public Relations Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Market Intelligence Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Anglo European School ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Easy Avenues Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Lancing College ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('LGO E-Commerce UK Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Fettes College ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Montana Treks Ltd ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Ace Cultural Tours Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Eit International')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Solmar Villas Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Karnac Books')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('OM Safety Solutions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Privacy International')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('South Gloucestershire & Stroud College')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('WDSI Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Emea Recruitment')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Clgr Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Pridelord Groundworks Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Sun Textile UK Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Peoples Voice Media ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Bellavistatrogir Llp')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Mo-Sys Engineering')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Playdale Playgrounds Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('World Aviation Communications ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('World Aviation Events Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Futuresense Foundation')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Grey Juice Lab Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Childaid To Russia & The Republics')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Inalytics Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('The Lady Eleanor Holles School')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Miris International Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Unite The Union')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Clifton College ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Global Radio Data Communications Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Northcott Global Solutions Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Kinapse Limited')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Unimaq Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Arts University Bournemouth ')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Spektrix Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Ethicap')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Countrywise Communication Ltd')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Derek Mills')
INSERT INTO #TEMPTABLE(COMPANY_NAME) VALUES('Cloudscape It Ltd')
GO

--this is the company name 
--rather this is where to get the company name from 
select top 10 * from  Account where account_short_name like '%Mountain Tracks %'

select top 10 * from  Account where account_short_name like '%AC Insight%'


select top 10 * from [dbo].[Branch] where account_id = 163426

select top 10 * from Contact
where account_id = 163426


exec sp_help 'account'
exec sp_help 'branch'
exec sp_help 'contact'

select top 10 * from country 



IF		OBJECT_ID('tempdb..##finaloutput_bi95') IS NOT NULL
DROP	TABLE ##finaloutput_bi95
go
with	cte1 as (
select	rank() over (partition by aa.Account_short_name order by bb.branch_id desc) as rank,
		aa.Account_id,
		bb.branch_id,
		aa.Account_short_name as [Company Name], 
		aa.Account_main_contact, 
		bb.branch_address1 as Registered_Address1, 
		bb.branch_address2 as Registered_Address2, 
		cc.CityName as Registered_city, 
		ss.State as Registered_County, 
		bb.branch_zip as Registered_Postcode,
		cr.Country as Registered_country ,
		aa.Account_full_name as  [Company Full name]
from	account aa
left	outer JOIN	branch bb
on		aa.Account_id = bb.account_id
join	CityName cc
on		bb.branch_city = cc.CityName_ID
join	dbo.[state] ss
on		bb.branch_state = ss.State_ID
join	country cr
on		bb.branch_country = cr.Country_ID
where	aa.Account_short_name in(select company_name from #temptable))
select	* 
into	##finaloutput_bi95
from	#TEMPTABLE AA LEFT OUTER JOIN CTE1 BB ON 
		AA.company_name = BB.[Company Name]
and		bb.[rank] = 1
--------
--the following is the list to send to Beata 
--select * from ##finaloutput_bi95 

select	company_name, registered_address1,registered_address2,registered_city,registered_county,
		registered_postcode,registered_country,isnull([company full name],'not found in talksheet') as [TradingAs]
from	##finaloutput_bi95


select  contact_id 
from	Contact
where	account_id in (select account_id from ##finaloutput_bi95)

----then the next step is a continuation of the previous ticket 

IF		OBJECT_ID('tempdb..#emailwithranks') IS NOT NULL
DROP	TABLE #emailwithranks
go
;

with    cte1 as (
select  rank() over (partition by contact_id order by Contact_Email_id desc) as rank_,
        * 
from    contact_email (nolock))
select  *  into #emailwithranks 
from    cte1 where rank_ = 1 ;

--select * from #emailwithranks

--declare a cte for each type , 

with		cteEuroCorp as (
select		Contact_ID,
			isnull(CurrencyCardDetailID,'') as [EURCARDID],
			isnull(CardNumber,'') AS [EURCARDNUMBER],
			[EURCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459566')
,
cteGBPCorp as (
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [GBPCARDID],
        isnull(CardNumber,'') AS [GBPCardNumber],
		[GBPCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459567')
,
cteUSDCorp as(
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [USDCARDID],
        isnull(CardNumber,'') AS [USD Card Number],
		[USDCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459568')

select  aa.contact_id ,
        isnull(aa.parent_id,0) as parent_id,
        acc.Account_short_name as [Company Name (not for upload)],
        AA.contact_first_name,
        AA.contact_last_name,
        Gender = case    when contact_salutation like 'Ms%' then 'Female'
                        when contact_salutation like 'Mrs%' then 'Female'
                        when contact_salutation like 'Miss%' then 'Female' 
                        when contact_salutation like 'Mr%' then 'Male'
                        else 'not supplied'
                        end ,
                        dob ,
						case	when isnull(aa.parent_id,0) = 0 then 'Yes'
								when isnull(aa.parent_id,0) > 0 then 'No'
						end as Iscompanyadmin ,
        datediff(yy,isnull(dob,''),getdate()) as Age, 
        eml.email_address ,
        aa.account_id as [Card group name],
        isnull(usd.usdcardid,'') as usdcardid,
        isnull(usd.[usd card number],'') as [usd card number],
		[USDCardstatus],
        isnull(eur.[eurcardid],'') as [EURCARDID],
        isnull(eur.[EURCARDNUMBER],'') as [EURCARDNUMBER],
		[EURCardStatus],
        isnull(gbp.[GBPCARDID],'') as [GBPCARDID],
        isnull(gbp.[GBPCardNumber],'') as [GBPCardNumber],
		[GBPCARDSTATUS]
from    Contact aa
left    outer join cteEuroCorp eur
on        aa.contact_id = eur.contact_id
left    outer join cteGBPCorp gbp
on        aa.contact_id = gbp.contact_id 
left    outer join cteUSDCorp usd
on        aa.contact_id = usd.contact_id
left    outer join #emailwithranks eml
on        aa.contact_id = eml.contact_id
join    Account acc
on        aa.account_id = acc.Account_id
where    aa.contact_id in
(select    contact_id 
from    Contact
where    account_id in (select account_id from ##finaloutput_bi95)
)
order    by aa.account_id,aa.parent_id ASC