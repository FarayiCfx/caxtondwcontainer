IF		OBJECT_ID('dbo.[BankReconciliationResult]', 'U') IS NOT NULL 
DROP	TABLE dbo.[BankReconciliationResult];
GO
CREATE	TABLE [dbo].[BankReconciliationResult](
	[MatchType] [varchar](100) NOT NULL,
	[Voided] bit null,
	[TradeId] [bigint]  NULL,
	[AccountTransactionId_FireBird] BIGINT NULL,
	[userid] [nvarchar](256) NULL,
	[primarytelephone] [nvarchar](100) NULL,
	[customerrefno] [nvarchar](max) NULL,
	--[firstname] [nvarchar](255) NULL,
	--[lastname] [nvarchar](255) NULL,
	[Rownum] [int] NOT NULL,
	[fastpaycontactref] [varchar](8000) NULL,
	[DateKey] [int] NULL,
	[Sort Code] [nvarchar](255) NULL,
	[Account Number] [nvarchar](255) NULL,
	[Account Alias] [nvarchar](255) NULL,
	[Account Short Name] [nvarchar](255) NULL,
	[Currency] [nvarchar](255) NULL,
	[Account Type] [nvarchar](255) NULL,
	[BIC] [nvarchar](255) NULL,
	[Bank Name] [nvarchar](255) NULL,
	[Branch Name] [nvarchar](255) NULL,
	[Date] [datetime] NULL,
	[Narrative #1] [nvarchar](255) NULL,
	[Narrative #2] [nvarchar](255) NULL,
	[Narrative #3] [nvarchar](255) NULL,
	[Narrative #4] [nvarchar](255) NULL,
	[Narrative #5] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[Debit] [nvarchar](255) NULL,
	[Credit] [nvarchar](255) NULL,
	[Amount] [nvarchar](255) NULL,
	[Allocated] [nvarchar](255) NULL,
	[CtpyTrade] [nvarchar](255) NULL,
	[VALITOR] [nvarchar](255) NULL,
	[CREDORAX] [nvarchar](255) NULL,
	[Manual Payment] [nvarchar](255) NULL,
	[RBSmargin] [nvarchar](255) NULL,
	[Client Money] [nvarchar](255) NULL,
	[Contra] [nvarchar](255) NULL,
	[Client 1] [nvarchar](255) NULL,
	[Client 2] [nvarchar](255) NULL
)
GO
------------------------------------------------------------------------------------------------->

exec sp_help 'dbo.[BankReconciliationResult]'

exec sp_help 'settlement.rbsstatement'

select  * into dbo.[BankReconciliationResult_temp] 
from dbo.[BankReconciliationResult] order by rownum

select * from dbo.[BankReconciliationResult]

--this will show me who is duplicating 
with	cteShowDuplicates as (
select	count(rownum) as count_,rownum
from	dbo.[BankReconciliationResult]
group	by rownum
having	count(rownum) > 1 )
select	aa.* 
from	dbo.[BankReconciliationResult] aa
join	cteShowDuplicates bb
on		aa.rownum = bb.rownum
order	by rownum



select * from dbo.[BankReconciliationResult]
