select top 10 * from dw.dimcustomerreference 

select top 10 * from firebird.factcurrencybanktrade
where helpdeskuserid is not null order by id desc

IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1
go
select	bb.customerkey
		,bb.sourcecreationdate as CreationDateCustomer 
		,aa.id
		,TradeProfitGBP
		,TradeReason
		,TradeDate
		,helpdeskuserid
		,IsHelpdesk = case isnull(helpdeskuserid,'') when '' Then 0 else 1 end 
		into #temp1
from	firebird.factcurrencybanktrade aa (nolock)
left	outer join dw.dimcustomerreference bb (nolock)
on		aa.customerkey = bb.customerkey
where	bb.sourcecreationdate > '2015-02-28'
and		aa.tradedate > '2015-02-28'
-------------------------------------------------------------------------------
select * from #temp1 
where customerkey = 6614
order by 1 asc

select	datepart(yyyy,creationdatecustomer) as [CreationYear]
		,datepart(yyyy, tradedate) as TradeYear
		,IsHelpdesk
		,tradereason
		,count(id) as TradeCount_
		,sum(TradeProfitGBP) AS Revenue
from	#temp1 
--where customerkey = 6614
group	by  datepart(yyyy,creationdatecustomer) 
			,datepart(yyyy, tradedate)
			,IsHelpdesk
			,tradereason 
order by	1,2

			select * from #temp1 where datepart(yyyy,CreationDateCustomer) = 2017
			and datepart(yyyy,TradeDate)= 2014

			select * from dw.dimcustomerreference where customerkey = 598061
--------------------------------------------------------------------------------------------------------------

with cte1 as (
select	bb.customerkey
		,bb.sourcecreationdate as CreationDateCustomer 
		,aa.id
		,TradeProfitGBP
		,TradeReason
		,TradeDate
		,helpdeskuserid
		,IsHelpdesk = case isnull(helpdeskuserid,'') when '' Then 0 else 1 end 
		,bb.accounttyperef
from	firebird.factcurrencybanktrade aa (nolock)
left	outer join dw.dimcustomerreference bb (nolock)
on		aa.customerkey = bb.customerkey
where	bb.sourcecreationdate > '2015-02-28'
and		aa.tradedate > '2015-02-28')


select	datepart(yyyy,creationdatecustomer) as [CreationYear]
		,datepart(yyyy, tradedate) as TradeYear
		,IsHelpdesk
		,TradeReason
		,accounttyperef
		,count(id) as TradeCount_
		,sum(TradeProfitGBP) AS Revenue
from	cte1 
group	by  datepart(yyyy,creationdatecustomer) 
			,datepart(yyyy, tradedate)
			,IsHelpdesk
			,tradereason 
			,accounttyperef
order by	1,2