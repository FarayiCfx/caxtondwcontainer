select * from aspnetusers where username = 'daniel@onetraveller.co.uk' 

select * from aspnetusers WHERE id in ('CM00198518','C00414261')

select top 10 * from Payment.Transfer

select distinct paystatus from payment.transfer

select  * from CurrencyBank.AccountSummary

select  * from CurrencyBank.Currency

-----------------------------------------------------------------------------------------------------------------------------------------------------
alter VIEW dbo.TransfersBeneficiariesAndBalances 
as

with	cteAccSummCurr
as		(
select	accsum.*,
		curr.buyccycode 
from	CurrencyBank.AccountSummary accsum (NOLOCK)
join	CurrencyBank.Currency curr (NOLOCK)
on		accsum.CurrencyId = curr.Id)

select		aa.id as Transferid,	
			aa.buyamount as Amount,
			aa.BuyCcyCode, 
			cast(/*isnull(*/aa.createddate/*,'')*/ as date) as Bookeddate,
			cast(/*isnull(*/aa.payawaydate/*,'')*/ as date) as Payawaydate,
			cast(/*isnull(*/aa.executeddate/*,'')*/ as date) as Executeddate,
			aa.Userid,
			upper(aa.paystatus) as PayStatus,
			bb.name as Beneficiaryname,
			cteAccSummCurr.Balance, 

			PaymentAccepted = case aa.paymentaccepted	when 0 then 'Rejected'	
														when 1 then 'Accepted'
														else null end,

			aa.FileSent,

			FileAccepted = case aa.FileAccepted when 0 then 'Rejected'
										when 1 then 'Accepted'
										else null END, 


			coalesce(aa.authorisationstatus,'') as AuthorisationStatus

from		payment.transfer aa (nolock)
join		payment.beneficiary bb (nolock)
on			aa.beneficiaryid = bb.id
left		outer join		cteAccSummCurr
on			aa.userid		= cteAccSummCurr.userid
and			aa.buyccycode	= cteAccSummCurr.buyccycode 


order		by 1 

where		aa.id = 83457


exec sp_spaceused 'payment.transfer'


select * from dbo.TransfersBeneficiariesAndBalances 
order by 1 DESC

select * from dbo.TransfersBeneficiariesAndBalances 
where transferid = 1272669
order by 1 DESC


exec sp_help 'dbo.TransfersBeneficiariesAndBalances'




select top 10000 * from payment.transfer order by id desc 

select top 100 * from payment.beneficiary order by id desc 






select * from dbfx.deallist

select distinct AuthorisationStatus from Payment.Transfer



select * from CurrencyBank.AccountSummary where userid = 'teamatley@gmail.com'

with	cteAccSummCurr
as		(
select	accsum.*,
		curr.buyccycode 
from	CurrencyBank.AccountSummary accsum (NOLOCK)
join	CurrencyBank.Currency curr (NOLOCK)
on		accsum.CurrencyId = curr.Id)

select * from cteAccSummCurr where userid = 'teamatley@gmail.com'

