--example of intra day and edo file difference 
select top 10 * from [Settlement].[RbsStatement]
where [Narrative2] = 'RBSSE2I05976798'
-->so the intra day entry below has itl 
S||CAXTONCLUSDA||CAXTON FX LTD -|USD||RBOSGB2L|||29/12/2016|/CCT/IXAHIS6X00KP /PERSON|RBSSE2I05976798||||ITL||6185.00

-->the EOD file for the 13nth is actually dated for the next day 
select	* 
from	settlement.FileImport
where	cast(FileDateTime as date) = '2016-12-30'
and		[filename] like '%AE%'
-->END of example for missing narratives

-->THIS IS THE VOIDED TRANSACTION  WITH NO TRANSACTIONID 
select	* 
from	Firebird.factcurrencybankaccounttransaction (nolock)
where	ExternalTransactionId = '223946'

--here is an example of someone coming up 3 times but there is one bankline transaction 
select * from [BankLineFileImport_Dec2016] where [Narrative #4] = '26133525929720000N'

select	* 
from	Firebird.factcurrencybankaccounttransaction (nolock)
where	id in (6294604,6294613,6294617)

