select * from BankLineFileImport_Feb17

drop table BankLineFileImport_Feb17_withrowid
go
select	IDENTITY (int, 1,1) Rownum,
cast(CONVERT(varchar(20),[date],112) as INT) as DateKey,
		* ,
		Amount = case debit when  0 then credit 
		else debit end
into	BankLineFileImport_Feb17_withrowid
from	BankLineFileImport_Feb17
go


select * from BankLineFileImport_Feb17_withrowid

-->8788 records

truncate table dbo.[BankReconciliationResult]
go

-->pick out the transaction references and put the data into a temp table first and count through what you have available  
IF		OBJECT_ID('tempdb..#result_set') IS NOT NULL
DROP	TABLE #result_set
go
-->
with	cte1 as (
select	
		TRANREFID1 = case when [Narrative #1] like '%TRANREF%' then 
		REPLACE(replace([Narrative #1],'TRANREF',''),':','')	
					END,
					
					TRANREFID2 = case when [Narrative #2] like '%TRANREF%' then 
		REPLACE(replace([Narrative #2],'TRANREF',''),':','')	
					END,

					* 
from	BankLineFileImport_Feb17_withrowid)
		 
select	LTRIM(COALESCE(tranrefid1,tranrefid2)) as TranRefIdCoalesce,
		cte1.*
into	#result_set 
from	cte1 

-->?how many of these records have a usable transaction reference?
select * from #result_set where TranRefIdCoalesce is not null


select	count(tranrefidcoalesce) as count_,
		tranrefidcoalesce
from	#result_set group by tranrefidcoalesce
order	by 1 desc 

-->5397 rows 

with cte1 as (
	select replace(replace([status],'Debit for payaway|',''),'Debit for PayAway','') as derived_tranref,* 
	from FireBird.factcurrencybankaccounttransaction (nolock)
	where status like 'Debit for payaway%' and datekey >  20170101)

	
	insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],
[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],
[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],
[Narrative #3],[Narrative #4],[Narrative #5],[Type],
[Debit] ,
[Credit],
[Amount])

select		'Tranref - bankaccountsummaryid',
			NULL,
			null,
			cc.id,
			dw.username,
			null,
			null,
			rownum,
			null,
			null,
			[Sort Code],[Account Number],
[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],
[Narrative #3],[Narrative #4],[Narrative #5],[Type],debit,credit,aa.amount
from	#result_set aa
join	cte1 cc
on		aa.TranRefIdCoalesce = cc.derived_tranref
join	dw.dimcustomerreference dw
on		cc.customerkey = dw.customerkey 
where	aa.tranrefidcoalesce is not null

--tranrefs done:
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

--->
IF		OBJECT_ID('TEMPDB..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
select	--[dbo].[GetNumbers2]([Narrative #2])as fastpaycontactref,

		fastpaycontactref = case when len ([dbo].[GetNumbers2]([Narrative #2])) < 2 then [dbo].[GetNumbers2]([Narrative #1]) 
		else [dbo].[GetNumbers2]([Narrative #2]) end,

		--cast(CONVERT(varchar(20),[date],112) as INT) as DateKey,
		* 
		into #temp1
from	#NextUnAllocatedBlock
---where	type not in ('D/D','CHG','SCR','BLN')
order	by	[Narrative #1]
go
--3421 rows 

select * from #temp1


with	cteMultipleMatch as (
-->MATCHING ON FASTPAYCONTACTREF
select	'FastPayContactRef - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,
		#temp1.* 
from	#temp1 
join	 dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.fastpaycontactref
join	firebird.factcurrencybanktrade tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradedatekey 
and		#TEMP1.amount = tr.sellamount 
and		tr.isunwound = 0 
and		tr.isreversed = 0
and		#temp1.fastpaycontactref <> '' --order by rownum
union	
-->NOW ATTEMPT MATCH ON PHONE NUMBER
select	'PhoneNumber - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,
		#temp1.* 
from	#temp1 
JOIN	dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.PRIMARYTELEPHONE
join	firebird.factcurrencybanktrade tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradedatekey 
and		#temp1.amount = tr.sellamount
and		tr.isunwound = 0 
and		tr.isreversed = 0
WHERE	LEN(#temp1.fastpaycontactref) > 5 
union
-->NOW ATTEMPT MATCH ON REFERENCE NUMBER
select	'FireBirdCustomerRefNo - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,
		#temp1.* 
from	#temp1 
JOIN	dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.customerrefno
join	firebird.factcurrencybanktrade tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradedatekey 
and		#temp1.amount = tr.sellamount
and		tr.isunwound = 0 
and		tr.isreversed = 0
WHERE	LEN(#temp1.fastpaycontactref) > 5 ) 

insert into dbo.[BankReconciliationResult]([MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select		[MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount]
from		cteMultipleMatch
go


--
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
----------------------------------------------------------------->

with	cteMultipleMatchEndDateKey as (
-->MATCHING ON FASTPAYCONTACTREF
select	'FastPayContactRef - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,
		#temp1.* 
from	#temp1 
join	 dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.fastpaycontactref
join	FactCurrencyBankTradeEndDateKey tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradeenddatekey 
and		#TEMP1.amount = tr.sellamount 
and		tr.isunwound = 0 
and		tr.isreversed = 0
and		#temp1.fastpaycontactref <> '' --order by rownum
union	
-->NOW ATTEMPT MATCH ON PHONE NUMBER
select	'PhoneNumber - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,
		#temp1.* 
from	#temp1 
JOIN	dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.PRIMARYTELEPHONE
join	FactCurrencyBankTradeEndDateKey tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradeenddatekey 
and		#temp1.amount = tr.sellamount
and		tr.isunwound = 0 
and		tr.isreversed = 0
WHERE	LEN(#temp1.fastpaycontactref) > 5 
union
-->NOW ATTEMPT MATCH ON REFERENCE NUMBER
select	'FireBirdCustomerRefNo - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,
		#temp1.* 
from	#temp1 
JOIN	dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.customerrefno
join	FactCurrencyBankTradeEndDateKey tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradeenddatekey 
and		#temp1.amount = tr.sellamount
and		tr.isunwound = 0 
and		tr.isreversed = 0
WHERE	LEN(#temp1.fastpaycontactref) > 5 ) 

insert into dbo.[BankReconciliationResult]([MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select		[MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount]
from		cteMultipleMatchEndDateKey
go

-->

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

------------------------------------------------------------------------------------------------------------------>


with		cteNarrative1and2 as(
-->join to rbs settlement table on narrative1 
SELECT		'Narrative 1 to settlements table and credit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #1] = bb.Narrative1
and			aa.credit = bb.creditvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #1] like 'RBS%'
union		
SELECT		'Narrative 1 to settlements table and debit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #1] = bb.Narrative1
and			aa.debit = bb.debitvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #1] like 'RBS%'
union
-->now join the rbs statement table to Narrative2 
SELECT		'Narrative 2  to settlements and credit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #2] = bb.Narrative2
and			aa.credit = bb.creditvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				left outer JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #2] like 'RBS%'
union 
SELECT		'Narrative 2  to settlements and debit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #2] = bb.Narrative2
and			aa.debit = bb.debitvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				left outer JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #2] like 'RBS%'
)

insert	into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[userid],[primarytelephone],[customerrefno],
											[Rownum],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],
											[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],
											[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])

SELECT  [Matchtype],voided, null, username,null,null,
		[Rownum],[DateKey],[Sort Code],[Account Number],
		[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],
		[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],
		[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount]
FROM	cteNarrative1and2
go
--1485 rows 

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
--------------------------------------------------------------------------------->


--select * from #NextUnAllocatedBlock
------->Narrative 1 company name 
with	cteCompanyName as(
select	bb.email,aa.companyname,
		dw.username, 
		dw.fastpaycontactref,
		tr.*
from	dw.dimcorporateentities aa (nolock)
join	[dw].[DimUserCorporateAssociation] bb (nolock)
on		aa.id = bb.corporateentityid
join	dw.dimcustomerreference dw (nolock)
on		bb.email = dw.username
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		tr.externaltransactionid <> 0)

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[DateKey],[Sort Code],
[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],
[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])

select	'Narrative 1 & company name - currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #1] = bb.companyname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go


IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
------------------------------------------------------------>


with	cteCompanyName as(
select	bb.email,aa.companyname,
		dw.username, 
		dw.fastpaycontactref,
		tr.*
from	dw.dimcorporateentities aa (nolock)
join	[dw].[DimUserCorporateAssociation] bb (nolock)
on		aa.id = bb.corporateentityid
join	dw.dimcustomerreference dw (nolock)
on		bb.email = dw.username
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		tr.externaltransactionid <> 0)

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],
[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[Rownum],
[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],
[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
-------------------------------------->
select	'Narrative 2 & company name- currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #2] = bb.companyname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go

---------------------------------------------------------------------------------->

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
--------------------------------------------------------------------------------------->
with	cteCompanyName as(
select	dw.username,
		dw.Firstname+' '+LastName as Fullname  ,
		dw.fastpaycontactref,
		tr.*
from	dw.dimcustomerreference dw (nolock)
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		externaltransactionid <> 0 )

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[DateKey],
[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],
[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],
[Credit],[Amount])

-------------------------------------->
select	'Narrative 1 & full name - currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #1] = bb.fullname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go


select * from #NextUnAllocatedBlock

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

-----------------------------------------------------------------

with	cteCompanyName as(
select	dw.username,
		dw.Firstname+' '+LastName as Fullname  ,
		dw.fastpaycontactref,
		tr.*
from	dw.dimcustomerreference dw (nolock)
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		externaltransactionid <> 0 )

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[DateKey],
[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],
[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],
[Credit],[Amount])

-------------------------------------->
select	'Narrative 2 & full name - currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #2] = bb.fullname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go

--select * from #NextUnAllocatedBlock

-----------------------------------remove irreconcilable items  

insert	into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[Rownum],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select  aa.[Narrative #4]+' - cannot be reconciled internally',NULL,NULL,NULL,NULL,NULL,NULL,aa.*
from	#NextUnAllocatedBlock aa
where	[Narrative #4]  = 'CCY A/C TFR'
or		[Narrative #4] = 'INTER A/C TFR'


insert	into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[Rownum],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select  aa.[Narrative #5]+' - cannot be reconciled internally',NULL,NULL,NULL,NULL,NULL,NULL,aa.*
from	#NextUnAllocatedBlock aa
where	[Narrative #5]  = 'CCY A/C TFR'
or		[Narrative #5] = 'INTER A/C TFR'

--------------------------------------------------------------------------------------------------------------------------------
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

----------------------------->CURRENCBANKACCOUNTSUMMARY
with	cteMultipleMatch as (
-->MATCHING ON FASTPAYCONTACTREF
select	'FastPayContactRef - CurrencyBankAccountTransaction' as MatchType,
		dc.USERNAME AS UserName,cb.id as AccounTransactionid,
		dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.fastpaycontactref
join	firebird.[FactCurrencyBankAccountTransaction] cb
on		dc.customerkey = cb.customerkey
and		#NextUnAllocatedBlock.datekey = cb.datekey 
and		#NextUnAllocatedBlock.amount = cb.amount 
AND		 CB.EXTERNALTRANSACTIONID <> 0 --just added this for dupe removal
and		#NextUnAllocatedBlock.fastpaycontactref <> '' 
and		cb.externaltransref = 'rbs'
--order by rownum
union	
-->NOW ATTEMPT MATCH ON PHONE NUMBER

select	'PhoneNumber - CurrencyBankAccountTransaction' as MatchType,
		dc.USERNAME AS UserName,cb.id as AccounTransactionid,
		dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.fastpaycontactref
join	firebird.[FactCurrencyBankAccountTransaction] cb
on		dc.customerkey = cb.customerkey
and		#NextUnAllocatedBlock.datekey = cb.datekey 
and		#NextUnAllocatedBlock.amount = cb.amount 
WHERE	LEN(#NextUnAllocatedBlock.fastpaycontactref) > 5 
--order by rownum

union
-->NOW ATTEMPT MATCH ON REFERENCE NUMBER
select	'FireBirdCustomerRefNo - CurrencyBankAccountTransaction' as MatchType,
		dc.USERNAME AS UserName,cb.id as AccounTransactionid,
		dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.customerrefno
join	firebird.[FactCurrencyBankAccountTransaction] cb
on		dc.customerkey = cb.customerkey
and		#NextUnAllocatedBlock.datekey = cb.datekey 
and		#NextUnAllocatedBlock.amount = cb.amount 
WHERE	LEN(#NextUnAllocatedBlock.fastpaycontactref) > 5 
) 

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select		[MatchType],[username],AccounTransactionid,[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount]
from		cteMultipleMatch
go


--------------------------------------------------------------------------------------------------------------------------------
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

------------------------------------------------------------------------------------------------------------------------------>
select * from #NextUnAllocatedBlock where credit <> 0

with	cteuserandcompany as (
select	firstname+ ' '+lastname as fullname,
		dw.fastpaycontactref,
		username,
		cc.companyname,
		tr.id as accounttransactionid,
		tr.datekey,
		tr.amount,
		tr.status,
		tr.externaltransactionid,
		tr.externaltransref
from	dw.dimcustomerreference dw (nolock)
left	outer join dw.dimusercorporateassociation bb (nolock)
on		dw.username = bb.email
left	outer join dw.dimcorporateentities cc (nolock)
on		bb.corporateentityid = cc.id
join	firebird.factcurrencybankaccounttransaction tr
on		dw.customerkey = tr.customerkey 
where	dw.id <> '0' 
and		externaltransref = 'rbs')

insert into [BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],
[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],
[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])

select		'Narrative 1 fullname - currencybankaccounttransaction',
			null,
			null,
			accounttransactionid,
			username,
			null,
			null,
			aa.*
from		#NextUnAllocatedBlock aa join cteuserandcompany bb
on			[Narrative #1] = bb.fullname
and			aa.datekey = bb.datekey
and			aa.amount = bb.amount
order by	rownum

----------------------------------------------------------------------------
with	cteuserandcompany as (
select	firstname+ ' '+lastname as fullname,
		dw.fastpaycontactref,
		username,
		cc.companyname,
		tr.id as accounttransactionid,
		tr.datekey,
		tr.amount,
		tr.status,
		tr.externaltransactionid,
		tr.externaltransref
from	dw.dimcustomerreference dw (nolock)
left	outer join dw.dimusercorporateassociation bb (nolock)
on		dw.username = bb.email
left	outer join dw.dimcorporateentities cc (nolock)
on		bb.corporateentityid = cc.id
join	firebird.factcurrencybankaccounttransaction tr
on		dw.customerkey = tr.customerkey 
where	dw.id <> '0' 
and		externaltransref = 'rbs')

insert into [BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],
[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],
[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])

select		'Narrative 2 fullname - currencybankaccounttransaction',
			null,
			null,
			accounttransactionid,
			username,
			null,
			null,
			aa.*
from		#NextUnAllocatedBlock aa join cteuserandcompany bb
on			[Narrative #2] = bb.fullname
and			aa.datekey = bb.datekey
and			aa.amount = bb.amount
order by	rownum

------------------------------------------------------------------------------------------------------------------------------------>

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
---------------------------------------------------------------------------------------------------------------------------------------->

drop table deftable
go
with	cteuserandcompany as (
select	firstname+ ' '+lastname as fullname,
		dw.fastpaycontactref,
		username,
		cc.companyname,
		tr.id as accounttransactionid,
		tr.datekey,
		tr.amount,
		tr.status,
		tr.externaltransactionid,
		tr.externaltransref
from	dw.dimcustomerreference dw (nolock)
left	outer join dw.dimusercorporateassociation bb (nolock)
on		dw.username = bb.email
left	outer join dw.dimcorporateentities cc (nolock)
on		bb.corporateentityid = cc.id
join	firebird.factcurrencybankaccounttransaction tr
on		dw.customerkey = tr.customerkey 
where	dw.id <> '0' 
and		externaltransref = 'rbs')
/*
insert into [BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],
[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],
[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
*/
select		'Narrative 2 fullname - currencybankaccounttransaction (amount not included)' as matchtype,
			null as voided,
			null as tradeid,
			accounttransactionid,
			username,
			null as primarytelephone,
			null as customerrefno,
			aa.*
			into deftable 
from		#NextUnAllocatedBlock aa join cteuserandcompany bb
on			[Narrative #2] = bb.fullname
and			aa.datekey = bb.datekey
--and			aa.amount = bb.amount
order		by	rownum
go

insert into [BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],
[userid],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],
[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],
[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select * from deftable


drop table deftable 
GO



IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go


------------------------------------------------------------------------------------------------------------------------->

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select		'Outgoing funds from bank to outside 3rd party - not reconciled ' ,NULL,NULL,NULL,NULL,[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount]
from		#NextUnAllocatedBlock 
where		debit <> 0


IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount])
select		'Cannot be reconciled automatically - please search manually' ,NULL,NULL,NULL,NULL,[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount]
from		#NextUnAllocatedBlock 


IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	fastpaycontactref = case when len ([dbo].[GetNumbers2](aa.[Narrative #2])) < 2 then [dbo].[GetNumbers2](aa.[Narrative #1]) 
		else [dbo].[GetNumbers2](aa.[Narrative #2]) end,
		aa.* 
into	#NextUnAllocatedBlock
from	BankLineFileImport_Feb17_withrowid aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go



select * from #NextUnAllocatedBlock




