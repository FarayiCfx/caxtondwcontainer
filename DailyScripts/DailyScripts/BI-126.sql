--Make sure the RFM table is up to date 
exec [dw].[MergeBuyAndHoldRfmStats]
go
exec [dw].[MergeBuyAndSendrfmStats]
GO

-------------------------------------------------------------------------------
create view dw.[BI-126 Dormant Clients]
as
WITH CTE1 AS (select	replace(isnull(bb.ContactEmailAddress,bb.UserName),
						'unknown',
						bb.UserName)	as contactemailaddress,
			bb.CustomerKey,BB.AccountTypeRef, 
			--BB.AccountTypeRefDerived ,
		aa.BuyAndSend_TradeCount,
		aa.BuyAndSend_FirstDate, 
		aa.BuyAndSend_LastDate 
from	dw.CustomerRFMAggregations aa
join	dw.DimCustomerReference  bb
on		aa.CustomerKey = bb.CustomerKey
where	BuyAndSend_TradeCount > 0)

select	*	from cte1
WHERE		BuyAndSend_LastDate < DATEADD(MONTH,-14,GETDATE())
and			accounttyperef = 'retail'
--order by	BuyAndSend_LastDate

grant select on dw.[BI-126 Dormant Clients] to cfx_datareader 



select DATEADD(MONTH,-14,GETDATE())

select top 10 * from dbfx.DealList

