-->start with this 
select top 10 * from CardActivations

select min(expiry_asint) from CardActivations

select max(expiry_asint) from CardActivations


alter  view CardActivations
as
WITH	cteActivations as (
select	valitorpanid as valitorpanid_active
		,cast(expiry as int) expiry_active 
		,isdirty as isdirty_active
		,isarchived as isarchived_active
		,CardStatusCode as cardstatus_isactive
		,CardSequenceNumber as CardSequenceNumber_isactive
from	valitor.pan 
where	isdirty = 0
and		isarchived = 0
and		cardstatuscode = 'V')

select	id 
		,valitorpanid 
		,expiry
		,cast(expiry as int) expiry_asint 
		,datecreated 
		,isdirty
		,isarchived
		,panstatuscode
		,CardStatusCode
		,valitorpanid_active
		,expiry_active
		,cardstatus_isactive
		,ActivatedByCustomer = case when valitorpanid_active is not null then 1 else 0 end 
		,'20' + left(expiry,2) as YearExpiry
		,right(expiry,2) as MonthExpiry
		,CardStatus = case when cardstatuscode ='v' then 'active'
							when cardstatuscode in ('l','a') then 'terminated_or_blocked'
							else 'inactive' end 
		
from	valitor.pan  aa
left	outer join cteActivations bb
on		aa.valitorpanid = bb.valitorpanid_active
and		bb.expiry_active > cast(aa.expiry as int)
--where	aa.expiry  = '1705' 
and		aa.isdirty = 0



select valitorpanid from valitor.panreissue
