select top 1000 * from [valitor].[fact_WalletTransactions] (nolock) order by 1 asc

select top 100 * from [valitor].[fact_WalletTransactions_Loading] (nolock)

select	count(wallettransaction_key) as count_,transactionkeycategory 
from	[valitor].[fact_WalletTransactions] (nolock)
group	by transactionkeycategory

exec sp_Spaceused'[valitor].[fact_WalletTransactions_Loading]'

exec sp_Spaceused'[valitor].[fact_WalletTransactions]'

select min(WalletTransaction_KEY) from [valitor].[fact_WalletTransactions]

exec sp_help '[valitor].[fact_WalletTransactions_loading]'

truncate table [valitor].[fact_WalletTransactions_Loading]
truncate table [valitor].[fact_WalletTransactions]


exec sp_help '[valitor].[fact_WalletTransactions]'

select * from [valitor].[fact_WalletTransactions]

alter table		[valitor].[fact_WalletTransactions_Loading] 
alter column	arn DECIMAL (38,0) NULL

alter table		[valitor].[fact_WalletTransactions_Loading] 
alter column 	Timestampid nvarchar(255)

alter table		[valitor].[fact_WalletTransactions] 
alter column	arn DECIMAL (38,0) NULL

alter table		[valitor].[fact_WalletTransactions] 
alter column 	Timestampid nvarchar(255)




select * from valitor.dimmerchant 


exec sp_who2


				
insert into[valitor].[fact_wallettransactions](Merchant_Key,Wallet_Key,ISOCurrency_KEY,Date_Key,Country_Key,Processor_KEY,Id,PanId,AccountId,ReferenceId,
BillingAmount,BillingCurrency,RegistrationDate,SettlementId,TransactionDateTime,TransactionKey,TransactionKeyCategory,ClearingCycleId,CardTypeCode,Arn,AuthorizationNumber,
AuthorizationTransactionId,ForeignExchangeFee,MerchantId,MerchantCity,MerchantCountryCode,TransactionAmount,TransactionCurrency,ProcessingDate,Region,TimestampId,[Description],[Transaction Type])

Select 
						        COALESCE(m.[Merchant_KEY],0) as Merchant_KEY
							   ,COALESCE(iso.[ISOCurrency_KEY],0) as ISOCurrency_KEY
							   ,COALESCE(w.[Wallet_KEY],0) as Wallet_KEY	
							   ,d.[Date_KEY]	
							   ,COALESCE(c.[Country_KEY],0) as Country_KEY	
							   ,COALESCE(p.[Processor_KEY],0) as Processor_KEY						     
							   ,l.[Id]
							   ,[PanId]
							   ,l.[AccountId]
							   ,case when ([ReferenceId] is null) then ''
							    else [ReferenceId] 
								end as [ReferenceId]
							   ,[BillingAmount]
							   ,[BillingCurrency]
							   ,[RegistrationDate]
							   ,[SettlementId]
							   ,[TransactionDateTime]
							   ,[TransactionKey]
							   ,[TransactionKeyCategory]
							   ,[ClearingCycleId]
							   ,[CardTypeCode]
							   ,[Arn]
							   ,[AuthorizationNumber]
							   ,[AuthorizationTransactionId]
							   ,[ForeignExchangeFee]							 				 	
							   ,l.[MerchantId]	
							   ,l.[MerchantCity]
							   ,l.[MerchantCountryCode]	as MerchantCountryCode	   
							   ,l.[TransactionAmount]
							   ,iso.[NameCode] as [TransactionCurrency]
							   ,l.[ProcessingDate]
							   ,l.[Region]
							   ,[TimestampId]
							   ,[Description]
							   ,[Transaction Type]
						  from [valitor].[fact_WalletTransactions_Loading] l
						  left join [valitor].[DimMerchant] m on l.[Merchantmcc]=m.[Merchantmcc] 
						  left join [dw].[DimISOCurrency] iso on iso.[ISOCODE]=l.[TransactionCurrency]
						  left join [dw].[DimDate] d on left(d.[date],11)=left(l.[transactionDateTime],11)
						  left join [dw].[DimWallet] w on w.WalletId=l.walletid and w.CardOrder=1 --Looking for Primary
						  left join [dw].[DimCountry] c on c.Country_ISO=l.[MerchantCountryCode]
						  left join [dw].[DimProcessor] p on p.ProcessorName='Valitor'	