-->drop table fis_output_raphaels
go
------------------------------------------------------------------------
-->adelembolland@btinternet.com
select	* from fis_output_raphaels 
where contact_id = 70906
order by 1 asc

select	aa.* , 
		bb.*
from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid
where	aa.cardid = 2507146772279886

select	* from firebird.factcurrencybankaccounttransaction (nolock) where customerkey = 12933 order by id asc

-->fshaw@sky.comn
select	* from fis_output_raphaels where email_address1 = 'fshaw@sky.com'

select	aa.* , bb.*
from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid
where	aa.cardid = 2507146713727340

select	* from firebird.factcurrencybankaccounttransaction(nolock) where customerkey = 14568 order by id asc

-->burgespm@virginmedia.com
select	*	from fis_output_raphaels 
where		email_address1 = 'burgespm@virginmedia.com'

select	aa.* , bb.*
from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid
where	aa.cardid = 2507008740575862

select	* from firebird.factcurrencybankaccounttransaction(nolock) where customerkey = 55347 order by id asc
---------------------------------------------------------------------------------------------------------------------------------------------------------------
---okay go ahead and create an output

select count(*) from FIS_Output_Raphaels
--128508

----------------------------------------------------------------------------------start files here-----------------------------------------------------------
IF		OBJECT_ID('tempdb..#Deposits') IS NOT NULL
DROP	TABLE #Deposits
go
select	cast(abs(amount) as decimal(18,2)) as AmountAsPositive,* 
into	#Deposits
from	firebird.factcurrencybankaccounttransaction (nolock)
where	status = 'Clear funds from balance for card load'
;
with	cteRaphaelsData as (
select	cast(avlbal as decimal(18,2)) as avlbalDecimal,
		aa.* , 
		bb.customerkey

from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid)

select	aa.RowNumber
		,aa.Email_address1
		,aa.AvlbalDecimal
		,aa.[Card type]
		,aa.[Program description]
		,aa.[Cardid]
		,aa.Lastname
		,aa.code
		,aa.currency
		,bb.id as TransactionIdFirebird
		,bb.Transactiondatetime
		,bb.amount
		,AmountAsPositive
		,bb.balance
		,bb.accountsummaryid
		,bb.status
		,bb.externaltransactionid
		,bb.externaltransref
into	[Raphaels_matched_output]
from	cteRaphaelsData aa
join	#Deposits bb
on		aa.customerkey = bb.customerkey 
and		aa.avlbalDecimal = bb.AmountAsPositive 

------------------------------------------------------------------money from fis into currency balance (below)
IF		OBJECT_ID('tempdb..#Deposits') IS NOT NULL
DROP	TABLE #Deposits

select	cast(abs(amount) as decimal(18,2)) as AmountAsPositive,* 
into	#Deposits
from	firebird.factcurrencybankaccounttransaction (nolock)
where	status = 'Deposit for Migrated Visa Card Balance'
;
with	cteRaphaelsData as (
select	cast(avlbal as decimal(18,2)) as avlbalDecimal,
		aa.* , 
		bb.customerkey
from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid)

--insert	into [Raphaels_matched_output]
select	aa.RowNumber
		,aa.Email_address1
		,aa.AvlbalDecimal
		,aa.[Card type]
		,aa.[Program description]
		,aa.[Cardid]
		,aa.Lastname
		,aa.code
		,aa.currency
		,bb.id as TransactionIdFirebird
		,bb.Transactiondatetime
		,bb.amount
		,AmountAsPositive
		,bb.balance
		,bb.accountsummaryid
		,bb.status
		,bb.externaltransactionid
		,bb.externaltransref
from	cteRaphaelsData aa
join	#Deposits bb
on		aa.customerkey = bb.customerkey 
---and		aa.avlbalDecimal = bb.AmountAsPositive 

-----------------------------------------------------------<HD> Any text that looks like migration-------------------------------------------

IF		OBJECT_ID('tempdb..#Deposits') IS NOT NULL
DROP	TABLE #Deposits

select	cast(abs(amount) as decimal(18,2)) as AmountAsPositive,* 
into	#Deposits
from	firebird.factcurrencybankaccounttransaction (nolock)
where	status like  '%Migrat%' and status not like '%error%'
;
with	cteRaphaelsData as (
select	cast(avlbal as decimal(18,2)) as avlbalDecimal,
		aa.* , 
		bb.customerkey

from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid)

---insert	into [Raphaels_matched_output]
select	aa.RowNumber
		,aa.Email_address1
		,aa.AvlbalDecimal
		,aa.[Card type]
		,aa.[Program description]
		,aa.[Cardid]
		,aa.Lastname
		,aa.code
		,aa.currency
		,bb.id as TransactionIdFirebird
		,bb.Transactiondatetime
		,bb.amount
		,AmountAsPositive
		,bb.balance
		,bb.accountsummaryid
		,bb.status
		,bb.externaltransactionid
		,bb.externaltransref
from	cteRaphaelsData aa
join	#Deposits bb
on		aa.customerkey = bb.customerkey 
---and		aa.avlbalDecimal = bb.AmountAsPositive 
order	by rownumber asc

--------------------------------------------------------then all other (non matched) records 

with	cteRaphaelsData as (

select	cast(avlbal as decimal(18,2)) as avlbalDecimal,
		aa.* , 
		bb.customerkey
from	FIS_Output_Raphaels aa
join	[dw].[DimTalksheetCardsAndContacts] bb
on		aa.cardid = bb.cardid
)

insert	into [Raphaels_matched_output]
select	aa.RowNumber
		,aa.Email_address1
		,aa.AvlbalDecimal
		,aa.[Card type]
		,aa.[Program description]
		,aa.[Cardid]
		,aa.Lastname
		,aa.code
		,aa.currency
		,0 as TransactionIdFirebird
		,'' as Transactiondatetime
		,0 as amount
		,0 as AmountAsPositive
		,0 as balance
		,0 as accountsummaryid
		,0 as [status]
		,0 as externaltransactionid
		,0 as externaltransref
from	cteRaphaelsData aa
left	outer join	[Raphaels_matched_output] bb
on		aa.RowNumber = bb.Rownumber 
where	bb.rownumber is null
order	by bb.rownumber asc

------------------------------------------------->

select	Email_address1,
		AvlbalDecimal,
		[Card type],
		[Program description],
		[Cardid],
		Lastname,
		code,
		currency,
		TransactionIdFirebird,
		Transactiondatetime,
		amount,
		AmountAsPositive,
		balance,
		accountsummaryid,
		[status],
		externaltransactionid,
		externaltransref 
from	[Raphaels_matched_output]


select	* from [Raphaels_matched_output] where email_address1 = 'jay1@teknavo.com'


exec sp_who2 active
