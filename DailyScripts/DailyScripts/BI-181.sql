-->

CREATE	view [BI181-TopCustomers]
as
with	cteLoadSum12Months as (
select	sum(abs(billingamountgbp)) as sum_loads_12months,	
		customerkey 
from	valitor.factwallettransactions (nolock) 
where	description = 'load'
and		transactiondatekey >= 20160229
group	by customerkey )
,
cteMarch2016Loadsums as (
select	sum(abs(billingamountgbp)) as sum_loads_march2016_onwards,
		avg(abs(billingamountgbp)) as avg_load_amt_march2016_onwards,	
		max(transactiondatetime) as Last_load_date,
		customerkey 
from	valitor.factwallettransactions (nolock) 
where	description = 'load'
and		transactiondatekey >= 20160229
group	by customerkey )
,
cteSpend as (
select	customerkey,
		COUNT(wallettransaction_key) as TotalSpendCount,
		avg(transactionamountgbp) as Avg_account_Spend,
		sum(transactionamountgbp) as sum_account_Spend,
		cast(max(transactiondatetime) as date) last_spend_date
from	valitor.factwallettransactions (nolock) 
where	transactionkeycategory = 'sale'
and		transactiondatekey >20160229
group	by customerkey)
,
cteSecondaryCardAccess as (
select	count(customerkey) as secondary_card_access_count,
		customerkey
from	[dw].[FactAspNetUserClaims] (nolock)
where	claimtype ='resourceaccess' 
and		claimvalue like '%View:SecondaryAccount%'
group	by customerkey )

select	accounttyperef,
		aa.customerkey,
		id as FirebirdId,
		firstname,lastname,
		isnull(contactemailaddress,username) as email , 
		sourcecreationdate as DateJoined,
		DateOfBirth, 
		isnull(sum_loads_12months,0) sum_loads_12months,
		isnull(sum_loads_march2016_onwards,0) sum_loads_march2016_onwards,
		isnull(avg_load_amt_march2016_onwards,0) avg_load_amt_march2016_onwards,
		isnull(Last_load_date,0) Last_load_date,
		--isnull(TotalSpendCount,0) TotalSpendCount,
		isnull(Avg_account_Spend,0) Avg_account_Spend,
		isnull(sum_account_Spend,0) sum_account_Spend,
		last_spend_date,
		isnull(secondary_card_access_count,0) secondary_card_access_count
from	dw.dimcustomerreference aa (nolock)
left	outer join cteLoadSum12Months bb 
on		aa.customerkey = bb.customerkey
left	outer join cteMarch2016Loadsums cc
on		aa.customerkey = cc.customerkey 
left	outer join cteSpend dd
on		aa.customerkey = dd.customerkey 
left	outer join cteSecondaryCardAccess ee
on		aa.customerkey = ee.customerkey
where	aa.id <> '0'
and		aa.sourcecreationdate > '2016-02-29'

select top 10 * from valitor.factwallettransactions



select * from [BI181-TopCustomers] order by secondary_card_access_count desc