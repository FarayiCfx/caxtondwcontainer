select * from [dbo].[MarketingAndFinanceMaster] where retail_secondary_cardclaim = 'true'


exec sp_who2 active



ALTER view [dbo].[MarketingAndFinanceMaster] 
AS
  with cte1 as (
  select	customerkey,'True 'as Retail_Primary_CardClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Retail:PrimaryAccount:PortalCards'),
  cte2 as (
  select	customerkey,'True 'as Retail_Secondary_CardClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Retail:SecondaryAccount:PortalCards'),
  cte3 as (
  select	customerkey,'True 'as Retail_Primary_IPTransferClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Retail:PrimaryAccount:PortalTransfer'),
  cte4 as (
  select	customerkey,'True 'as Corporate_Primary_CardClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Corporate:PrimaryAccount:PortalCards'),
  cte5 as (
  select	customerkey,'True 'as Corporate_Primary_IPTransferClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Corporate:PrimaryAccount:PortalTransfer'),
  cte6 as (
  select	customerkey,'True 'as Corporate_Migration_IPTransferClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'TalksheetBiz:PrimaryAccount:PortalTransfer'),

   ctePreviousMonthSpend as (
  select	sum(TransactionAmountGbp) as PreviousCalendarMonthSpend
		,customerkey
from	valitor.factwallettransactions (nolock)
where	transactiondatetime between dateadd(month, datediff(month, -1, getdate()) - 2, 0) 
and		dateadd(ss, -1, dateadd(month, datediff(month, 0, getdate()), 0)) 
group	by customerkey 
  ),

cteOverAllBalanceAdjustment as (select	sum(bILLINGAmountGbp) as OverallBalanceAdjustment
		,customerkey
from	valitor.factwallettransactions (nolock)
WHERE	LABEL IN ('Align','S')
group	by customerkey )

select	aa.customerkey
		,id as FirebirdId
		,Username
		,Title
		,Firstname
		,Lastname
		,Gender
		,Dateofbirth
		,Postcode
		,aa.Sourcecreationdate
		,[VALITOR_MASTERCARD_CardLoadCount] as VALITOR_CardLoadCount
		,[VALITOR_MASTERCARD_LoadTotalGBP] as VALITOR_LoadTotalGBP
		,[VALITOR_MASTERCARD_SpendTotalGBP] as VALITOR_SpendTotalGBP
		,isnull (PreviousCalendarMonthSpend,0) as VALITOR_PrevMonthSpend
		,[VALITOR_MASTERCARD_AverageSpendGBP] as VALITOR_AverageSpendGBP
		,[VALITOR_MASTERCARD_FirstLoadDate] as VALITOR_FirstLoadDate
		,[VALITOR_MASTERCARD_LastLoadDate] as VALITOR_LastLoadDate
		,isnull(cteOverAllBalanceAdjustment.OverallBalanceAdjustment,0) as VALITOR_TotalBalanceAdjustment
		--,[CardLoad_FirstDate]
		--,[CardLoad_LastDate]
		--,[CardLoad_TradeCount]
		,[BuyAndSend_TradeCount]
		,[BuyAndSend_Tradeprofit_GBP_Total]
		,[BuyAndSend_FirstDate]
		,[BuyAndSend_LastDate]
		,isnull(cte1.Retail_Primary_CardClaim,'False') as Retail_Primary_CardClaim
		,isnull(cte2.Retail_Secondary_CardClaim,'False') as Retail_Secondary_CardClaim
		,isnull(cte3.Retail_Primary_IPTransferClaim,'False') as Retail_Primary_IPTransferClaim
		,isnull(cte4.Corporate_Primary_CardClaim,'False') as Corporate_Primary_CardClaim
		,isnull(cte5.Corporate_Primary_IPTransferClaim,'False') as Corporate_Primary_IPTransferClaim
		,isnull(cte6.Corporate_Migration_IPTransferClaim,'False') as Corporate_Migration_IPTransferClaim
from	dw.dimcustomerreference aa (nolock)
left	outer join [dw].[CustomerRFMAggregations]  bb (nolock)
on		aa.customerkey = bb.customerkey 
left	outer join cte1 
on		aa.customerkey = cte1.customerkey 
left	outer join cte2 
on		aa.customerkey = cte2.customerkey 
left	outer join cte3
on		aa.customerkey = cte3.customerkey
left	outer join cte4 
on		aa.customerkey = cte4.customerkey 
left	outer join cte5 
on		aa.customerkey = cte5.customerkey 
left	outer join cte6
on		aa.customerkey = cte6.customerkey 
left	outer join ctePreviousMonthSpend
on		aa.customerkey = ctePreviousMonthSpend.customerkey 
left	outer join cteOverAllBalanceAdjustment
on		aa.customerkey = cteOverAllBalanceAdjustment.customerkey 
where	aa.id <> '0'
GO



exec sp_help '[dw].[CustomerRFMAggregations]'


with	cte1 as (
SELECT   OBJECT_NAME(S.[OBJECT_ID]) AS [OBJECT NAME], 
         I.[NAME] AS [INDEX NAME], 
         USER_SEEKS, 
         USER_SCANS, 
         USER_LOOKUPS, 
         USER_UPDATES 
FROM     SYS.DM_DB_INDEX_USAGE_STATS AS S 
         INNER JOIN SYS.INDEXES AS I 
ON		I.[OBJECT_ID] = S.[OBJECT_ID] 
AND		I.INDEX_ID = S.INDEX_ID 
WHERE    OBJECTPROPERTY(S.[OBJECT_ID],'IsUserTable') = 1 )
select	* from cte1 where [OBJECT NAME] = 'factwallettransactions'
