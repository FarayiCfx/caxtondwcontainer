DROP PROCEDURE IF EXISTS [dw].[GetCorporateCardStatementWithDateMultipleValues_V2]
GO


create procedure [dw].[GetCorporateCardStatementWithDateMultipleValues_V2] @panid varchar(max) , @include_alignments bit ,@startdate DATE, @enddate DATE
as

SET NOCOUNT ON

-->create a temp table for the multiple valued lookups on walletid
drop	table if exists #temp1
create	table #temp1(PanID BIGINT )

  WHILE CHARINDEX(',',@PanId,-1)<>0
  BEGIN
    INSERT INTO #temp1 VALUES((SELECT LEFT(@PanId, CHARINDEX(',',@PanId)-1)))
    SET @PanId=(SELECT RIGHT(@PanId,LEN(@PanId)-CHARINDEX(',',@PanId)))
  END
 ---> then get the last value after the last comma 
  insert into #temp1 
  select right(@panid, LEN(@panid) - CHARINDEX('\',REVERSE(@panid)))

--select * from #temp1 

drop	table if exists #currencycode

select	distinct AlphabeticCode, NumericCode
into	#currencycode
from	[dw].[DimCurrencyCodesISO4217]

--> select * from #currencycode

if		(@include_alignments = 0)

select	TransactionKeyCategory as [Type]
		,isnull(fl.authorizationnumber,'') as authorizationnumber
		,cast(transactiondatetime as date) as TransactionDate
		,ISNULL(description,'') as [Description]
		,ISNULL(mc.MerchantDescription,'') as [Merchant Category]
		,MerchantName
		,MerchantCity
		,isnull(fl.BillingAmount,0) as BillingAmount
		,isnull(fl.TransactionAmount,0) as TransactionAmount
		,cast(ProcessingDate as date) as ProcessingDate
		,(select AlphabeticCode from #currencycode where NumericCode = fl.BillingCurrency) as BillingCurrency
		,(select AlphabeticCode from #currencycode where NumericCode = fl.TransactionCurrency) as TransactionCurrency
		,fl.WalletId
		,lk.MaskedPan
from	valitor.FactWalletTransactions fl
left	outer join dw.DimMCC mc
on		fl.MerchantMCC = mc.MCC_ID
join	CorpUserToPanLookup lk
on		fl.PanId = lk.ValitorPanId 
where	lk.ValitorPanId in (select panid from #temp1) 
and		fl.TransactionDateTime between @startdate and @enddate

else 

select	TransactionKeyCategory as [Type]
		,isnull(fl.authorizationnumber,'') as authorizationnumber
		,cast(transactiondatetime as date) as TransactionDate
		,ISNULL(description,'') as [Description]
		,ISNULL(mc.MerchantDescription,'') as [Merchant Category]
		,MerchantName
		,MerchantCity
		,isnull(fl.BillingAmount,0) as BillingAmount
		,isnull(fl.TransactionAmount,0) as TransactionAmount
		,cast(ProcessingDate as date) as ProcessingDate
		,(select AlphabeticCode from #currencycode where NumericCode = fl.BillingCurrency) as BillingCurrency
		,(select AlphabeticCode from #currencycode where NumericCode = fl.TransactionCurrency) as TransactionCurrency
		,fl.WalletId
		,lk.MaskedPan
from	valitor.FactWalletTransactions fl
left	outer join dw.DimMCC mc
on		fl.MerchantMCC = mc.MCC_ID
join	CorpUserToPanLookup lk
on		fl.WalletId = lk.ValitorWalletId 
where	lk.ValitorPanId in (select panid from #temp1) 
and		fl.Type = 'BalanceAdjustment'
and		fl.TransactionDateTime between @startdate and @enddate

UNION	ALL

select	TransactionKeyCategory as [Type]
		,isnull(fl.authorizationnumber,'') as authorizationnumber
		,cast(transactiondatetime as date) as TransactionDate
		,ISNULL(description,'') as [Description]
		,ISNULL(mc.MerchantDescription,'') as [Merchant Category]
		,MerchantName
		,MerchantCity
		,isnull(fl.BillingAmount,0) as BillingAmount
		,isnull(fl.TransactionAmount,0) as TransactionAmount
		,cast(ProcessingDate as date) as ProcessingDate
	,(select AlphabeticCode from #currencycode where NumericCode = fl.BillingCurrency) as BillingCurrency
		,(select AlphabeticCode from #currencycode where NumericCode = fl.TransactionCurrency) as TransactionCurrency
		,fl.WalletId
		,lk.MaskedPan
from	valitor.FactWalletTransactions fl
left	outer join dw.DimMCC mc
on		fl.MerchantMCC = mc.MCC_ID
join	CorpUserToPanLookup lk
on		fl.PanId = lk.ValitorPanId 
where	lk.ValitorPanId in (select panid from #temp1) 
--and		fl.Type <> 'BalanceAdjustment'
and fl.TransactionDateTime between @startdate and @enddate


DROP TABLE #temp1

GO


