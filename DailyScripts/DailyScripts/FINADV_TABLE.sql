CREATE TABLE FIS.FINADV_STAGING 
(
ROWID INT IDENTITY (1,1) NOT NULL,
MTID INT NULL, 
LOCALDATE DATETIME NULL,
LOCALTIME INT NULL,
TLOGID INT NULL,
ORGTLOGID INT NULL,
ITEMID INT NULL,
ORGITEMID INT NULL,
PAN BIGINT NULL,
CARDID BIGINT NULL,
CRDPRODUCT NVARCHAR (255) NULL,
PROGRAMID NVARCHAR (255) NULL,
BRNCODE NVARCHAR (255) NULL,
TXNCODE INT NULL,
TXNSUBCODE INT NULL,
CURTXN INT NULL,
AMTTXN DECIMAL (38,0) NULL,
AMTFEE DECIMAL (38,0) NULL,
AMTTXNCB DECIMAL (38,0) NULL,
CURSET INT NULL,
RATESET DECIMAL (38,0) NULL,
AMTSET DECIMAL (38,0) NULL,
BILLAMT DECIMAL (38,0) NULL,
ACCCUR INT NULL,
ACCNO INT NULL,
ACCTYPE INT,
BILLCONVRATE DECIMAL (38,0) NULL,
APPROVALCODE NVARCHAR (255) NULL,
CORTEXDATE DATETIME NULL,
STAN INT NULL,
RRN BIGINT NULL,
TERMCODE NVARCHAR (255) NULL,
CRDACPTID NVARCHAR (255) NULL,
TERMLOCATION NVARCHAR (255) NULL,
TERMSTREET NVARCHAR (255) NULL,
TERMCITY NVARCHAR (255) NULL,
TERMCOUNTRY NVARCHAR (255) NULL,
[SCHEMA] NVARCHAR (255) NULL,
ARN  DECIMAL (38,0) NULL,
FIID NVARCHAR(255) NULL,
RIID NVARCHAR (255) NULL,
REASONCODE INT NULL,
CHIC NVARCHAR (255) NULL,
CHAC INT NULL,
CHP INT NULL,
CP INT NULL,
CDIM NVARCHAR(255),
CHAM INT NULL,
CHA INT NULL,
TVR NVARCHAR (255) NULL,
MSGSRC INT NULL,
RCC INT NULL,
MCC INT NULL,
CBACKIND NVARCHAR (255),
TXNDATE DATETIME NULL,
TXNTIME INT NULL,
TERMTYPE NVARCHAR (255),
CTXDATELOCAL DATETIME NULL,
CTXTIMELOCAL INT NULL,
AIID INT NULL,
DLVCYCLE INT NULL,
ACTIONCODE INT NULL,
RSPCODE INT NULL
)