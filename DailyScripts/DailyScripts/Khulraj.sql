
where		Narrative1 like '%STUDIO GOBO DEVELO%'

exec sp_help 'Settlement.RbsStatement'

-->get the fileimportid from this table here 
select	top 10 * from settlement.FileImport where id = 11925

-->the EOD file for the 13nth is actually dated for the next day 
select	* 
from	settlement.FileImport
where	cast(FileDateTime as date) = '2017-01-14'
and		[filename] like '%AE%'
-->END of example for missing narratives

-->Look at all end of day files now:
select	* 
from	settlement.FileImport (nolock)
where	[filename] like '%AE%'


-->unallocated 
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
select	aa.id as StatementId,
		aa.AccountNumber,
		aa.AccountName,
		aa.AccountCurrency,
		aa.Narrative1,
		aa.Narrative2,
		aa.Narrative3,
		aa.Narrative4,
		aa.Narrative5,
		aa.CreditValue,
		aa.DebitValue, 
		aa.SettlementType,
		aa.UserName,
		aa.DateCreated as TransactionDate,
		aa.TradeFundingHintList,
		aa.accounttransactionid as accounttransactionid_in_statement,
		bb.id as AccountTransactionId,
		bb.TransactionDateTime,
		bb.Amount,
		bb.Cleared,
		bb.AccountSummaryId 
into	#temp1
from	Settlement.RbsStatement aa
left	outer join CurrencyBank.AccountTransaction bb
on		cast (aa.Id as nvarchar (512)) = bb.ExternalTransactionId
and		bb.ExternalTransRef = 'RBS'
where	aa.transactiondate between '2017-04-01 00:00:00.000' and '2017-04-30 00:00:00.000'


select * from #temp1  where narrative1 = 'Tranref:486890'

select * from Settlement.RbsStatement where narrative1 = 'Tranref:486890'

select * from CurrencyBank.AccountTransaction (nolock) where externaltransactionid = '6882804'

select top 20  * from CurrencyBank.AccountTransaction 


exec sp_help 'Settlement.RbsStatement'
exec sp_help 'CurrencyBank.AccountTransaction'









GO
SELECT	* 
FROM	#temp1 
WHERE	TransactionDate between '2017-01-01' and '2017-01-15'
and		Narrative1 like '%studio globo%'
GO


select * from #temp1 order by tradefundinghintlist desc 

select * from #temp1 where narrative1 like '%STUDIO GOBO DEVELO%'

---->allocated 
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
select	aa.id as StatementId,
		aa.AccountNumber,
		aa.AccountName,
		aa.AccountCurrency,
		aa.Narrative1,
		aa.Narrative2,
		aa.Narrative3,
		aa.Narrative4,
		aa.Narrative5,
		aa.CreditValue,
		aa.DebitValue, 
		aa.SettlementType,
		--aa.UserName,
		ACC.UserId,
		cast(aa.DateCreated as date) as TransactionDate,
		bb.id as AccountTransactionId,
		bb.TransactionDateTime,
		bb.Amount,
		bb.Cleared,
		bb.AccountSummaryId 
		into #temp1
from	Settlement.RbsStatement aa
left	outer join CurrencyBank.AccountTransaction bb
on		aa.Id = bb.ExternalTransactionId
JOIN	CurrencyBank.AccountSummary ACC
ON		BB.AccountSummaryId = ACC.Id
and		bb.ExternalTransRef = 'RBS'
where	bb.ExternalTransactionId is not null
go
select	* 
from	#temp1 
where	TransactionDate between '2017-01-01' and '2017-01-15'


exec sys.sp_help 'Settlement.RbsStatement'

exec sys.sp_spaceused 'Settlement.RbsStatement'

exec sys.sp_help 'Settlement.RbsStatement'

EXEC SP_HELP 'CurrencyBank.AccountTransaction'

exec sp_who2 active

select top 10 * from CurrencyBank.AccountTransaction (NOLOCK) where ExternalTransRef = 'rbs'

select top 10 * from CurrencyBank.AccountTransactionDetail

select * from CurrencyBank.MarketTrade 
where CounterpartyRefNo IN('L161222A0B4A700','L161222A2314900','ELBANKO40072208','L161228A0B7C400','ELBANKO40077432','ELBANKO40068612') 


select top 10 * from currencybank.trade order by 1 desc 

exec sp_who2 active
