select  count(*),cardstatus 
from	CardActivations
where	expiry = '1708' 
group	by cardstatus

;
with	cteActive as (
select	expiry as expiry, convert(decimal(8,2),[0]) as Active_NotActivatedByCustomer,convert (decimal(8,2),[1]) as Active_ActivatedByCustomer
from (
select id,expiry, cast(ActivatedByCustomer as varchar(3)) ActivatedByCustomer from CardActivations where cardstatus = 'active'
) as sourcetable
pivot (
count(id)
for ActivatedByCustomer in([0],[1])
) as pivottable )

,
 cteInActive as (
select expiry as expiry, convert (decimal(8,2),[0]) as NotActivated_from_inactive, convert (decimal(8,2),[1]) as Actived_from_inactive
from (
select id,expiry, cast(ActivatedByCustomer as varchar(3)) ActivatedByCustomer from CardActivations where cardstatus = 'inactive'
) as sourcetable
pivot (
count(id)
for ActivatedByCustomer in([0],[1])
) as pivottable )

, cteMailingFile as (
select expiry as expiry, convert(decimal(8,2),[0]) as [MailingFile_Notactivated], convert(decimal(8,2),[1]) as [MailingFile_Activated]
from (
select id,expiry, cast(ActivatedByCustomer as varchar(3)) ActivatedByCustomer from CardActivations where cardstatus <> 'terminated_or_blocked'
) as sourcetable
pivot (
count(id)
for ActivatedByCustomer in([0],[1])
) as pivottable )

 select aa.expiry
		,'20' + left(aa.expiry,2) as YearExpiry
		,right(aa.expiry,2) as MonthExpiry
		,cast(NotActivated_from_inactive as int) as NotActivated_from_inactive
		,cast(Actived_from_inactive as int) as Actived_from_inactive
		,cast(NotActivated_from_inactive + Actived_from_inactive as int) as [Inactive]
		,cast((Actived_from_inactive/(NotActivated_from_inactive + Actived_from_inactive))*100 as int)as [%Activated_from_inactive]
		,cast(Active_NotActivatedByCustomer as int) as Active_NotActivatedByCustomer
		,cast(Active_ActivatedByCustomer as int) as Active_ActivatedByCustomer
		,cast ((Active_ActivatedByCustomer/(Active_NotActivatedByCustomer+Active_ActivatedByCustomer))*100 as int) as [%Activated_by_customer]
		,cast ([MailingFile_Notactivated] as int) as [MailingFile_Notactivated]
		,cast ([MailingFile_Activated] as  int) as [MailingFile_Activated]
		,cast(([MailingFile_Activated]/([MailingFile_Activated]+[MailingFile_Notactivated]))*100 as int) as [%MailingFileActivated]
 from	cteMailingfile aa 
 left	outer join cteInActive bb 
 on		aa.expiry = bb.expiry 
 left	outer join cteActive  cc
 on		aa.expiry = cc.expiry
 order	by aa.expiry