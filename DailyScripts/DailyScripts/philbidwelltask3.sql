WITH		cte1 AS (
SELECT		*	
FROM		dw.factaspnetuserclaims (nolock)
where		claimtype = 'accounttype' and claimvalue like '%:PrimaryAccount:PortalCards%')
SELECT		COUNT(userid) AS count_,userid FROM cte1 GROUP BY userid ORDER BY count_ desc


DROP VIEW dw.RetailBaseCustomerProfiling
go

CREATE VIEW dw.RetailBaseCustomerProfiling
AS 
WITH	cteSecondaryCards AS (
SELECT	COUNT(customerkey) AS count_,customerkey
FROM	dw.FactAspNetUserClaims (nolock)
WHERE	claimtype = 'resourceaccess' 
AND		(claimvalue like '%:view:currencycards:%')
GROUP	BY CustomerKey),

cteMoneyTransferValue AS (
SELECT	customerkey,COUNT(CustomerKey) AS MoneyTransferCount,
		SUM([dw].[UdfGetGBPValue](SellAmount,SellCcyCode)) AS MoneyTransferValueGBP
FROM	FireBird.FactCurrencyBankTrade
WHERE	TradeReason IN ('buy and hold','buy and send')
GROUP	BY CustomerKey)

SELECT	AA.customerkey,id AS FireBirdId,
		postcode,
		DateOfBirth,
		AA.Gender,
		AA.Title,
		DATEDIFF (yy,dateofbirth,GETDATE()) AS [Age],
		MasterCardNew = CASE WHEN id LIKE 'MIG%' THEN 'NO' ELSE 'YES' END,
		MasterCardMigrated = CASE WHEN id LIKE 'MIG%' THEN 'YES' ELSE 'NO' END,
		RF.VALITOR_MASTERCARD_CardLoadCount,
		RF.VALITOR_MASTERCARD_LoadTotalGBP,
		rf.FIS_VISA_CardLoadCount,
		rf.FIS_VISA_CardLoadTotalGBP,
		ISNULL(sec.count_,0) AS SecondaryCardCount ,
		isnull(rf.VALITOR_MASTERCARD_LastLoadDate,0) AS VALITOR_MASTERCARD_LastLoadDate,
		ISNULL(mt.MoneyTransferValueGBP,0) AS MoneyTransferValue ,
		ISNULL(mt.MoneyTransferCount,0) AS MoneyTransferCount,
		aa.AccountTypeRef
FROM	dw.DimCustomerReference AA (nolock)
LEFT	OUTER JOIN	dw.CustomerRFMAggregations RF (nolock)
ON		AA.CustomerKey = RF.CustomerKey
LEFT	OUTER JOIN cteSecondaryCards sec
ON		aa.CustomerKey = sec.CustomerKey
LEFT	OUTER JOIN cteMoneyTransferValue mt
ON		aa.CustomerKey = mt.CustomerKey
WHERE	DateOfBirth <> '1905-01-01 00:00:00.000' --where the dateofbirth is valid 
AND		AA.AccountTypeRef IN ('Card & money transfer - Firebird',
						'Retail card - self migrated user',
						'Retail card - Firebird',
						'MoneyTransfer - Firebird',
						'Retail card - automated migration from TalkSheet')

SELECT	* FROM dw.RetailBasecustomerProfiling


SELECT	customerkey,SellCcyCode,SUM(sellamount) AS sellamount
FROM	FireBird.FactCurrencyBankTrade
GROUP	BY customerkey,SellCcyCode
ORDER	BY CustomerKey



SELECT	[dw].[UdfGetGBPValue](SellAmount,SellCcyCode) AS gbpvalue,* 
FROM	FireBird.FactCurrencyBankTrade
WHERE	TradeReason IN ('buy and hold','buy and send')
AND CustomerKey = 113786
ORDER	BY Id DESC


SELECT	customerkey,COUNT(CustomerKey) AS MoneyTransferCount,
		SUM([dw].[UdfGetGBPValue](SellAmount,SellCcyCode)) AS MoneyTransferValueGBP
FROM	FireBird.FactCurrencyBankTrade
WHERE	TradeReason IN ('buy and hold','buy and send')
GROUP	BY CustomerKey

SELECT TOP 100 * FROM FireBird.FactCurrencyBankTrade


select * from dw.factaspnetuserclaims 
WHERE claimtype = 'resourceaccess' 
AND (claimvalue like '%:view:currencycards:%')
ORDER BY UserId DESC

SELECT * FROM [dw].[CustomerRFMAggregations]



UPDATE  [dw].[CustomerRFMAggregations]
 SET valitor_mastercard_lastloaddate = '1900-01-01 00:00:00.000' 
 WHERE valitor_mastercard_lastloaddate = '1800-01-01 00:00:00.000'


SELECT  * 
FROM	valitor.FactWalletTransactions
where	customerkey = 117118

WHERE Type = 'presentment'


SELECT * FROM dw.DIMXERATES
