-->LOOKING FURTHER DOWN THE LIST MOST OF THEN ARE OKAY.

-->first get the trade id based on trade profit  amount and date 
select	tradeprofit,* 
from	currencybank.trade (nolock)
where	cast(tradedate as date) = '2017-05-19'
and		tradeprofit =0
and tradereason <> 'card load'
------------------------------>

--drop temp table 
IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1;

declare @tradeid bigint

select @tradeid = 1774347
;
--->then the cte below also picks up the profit line because it is not matching buy amount 
with cte1 as (
select [Name]
  +' - '
  +convert(varchar(24) ,bb.BookOpenDate,103)
  +' '
  +convert(varchar(24) ,bb.BookOpenDate,108) as NetBookName
  ,aa.id as TradeId
  ,mk.id as MarketTradeId
from currencybank.Trade aa (nolock)
join currencybank.ClientTradingBook bb (nolock)
on  aa.BuyCcyClientTradingBookId = bb.Id
--and  aa.SellCCyClientTradingBookId = bb.id
join CurrencyBank.MarketTrade mk (nolock)
on  bb.Name = mk.BookName
and  convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
/*and  aa.BuyCcyCode = mk.BuyCcy
and aa.sellccycode = mk.sellccy*/)
select	* 
into	#temp1 
from	cte1 
where	tradeid = @tradeid 

-->look at the individual trade
select tradeprofit,* from currencybank.trade where id = @tradeid 

--then look at the market trade book and decide 
select SellCcy,SellCcyAmount,BuyCcy,BuyCcyAmount,PartiallyClosed
,CounterpartyRefNo,BookOpenDate,Id,BookName,BankRate,Comment,DateCreated,LastModifiedTimeStamp
from currencybank.markettrade 
where id in (select MarketTradeId from #temp1)

