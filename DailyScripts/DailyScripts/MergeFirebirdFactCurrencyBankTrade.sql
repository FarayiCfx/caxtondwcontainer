drop	procedure if exists [dw].[mergefirebirdfactcurrencybanktrade];  
go
create	procedure [dw].[mergefirebirdfactcurrencybanktrade]
as
/*
This is to merge the data to the firebird.factcurrencybanktrade table  
instead of a straight insert 
*/
merge	firebird.factcurrencybanktrade as target

using
(
select	CurrencyPriority = case when SellCcyCode = 'GBP' or BuyCcyCode = 'GBP' then 'GBP'
		when SellCcyCode <> 'GBP' and BuyCcyCode <> 'GBP' and BuyCcyCode = 'EUR' or SellCcyCode ='EUR' then 'EUR'
		when SellCcyCode <> 'GBP' and BuyCcyCode <> 'GBP' and BuyCcyCode = 'USD' or SellCcyCode ='USD' then 'USD'
		when	SellCcyCode <> 'GBP'
				OR SellCcyCode <> 'EUR'
				OR SellCcyCode <> 'USD' 
				and BuyCcyCode <> 'GBP' 
				OR BuyCcyCode <> 'GBP' 
				or BuyCcyCode <> 'EUR' 
				OR BuyCcyCode <> 'USD'
		THEN SellCcyCode
		end, 
		datepart(mm,tradedate) as MonthInteger,
		datepart(yyyy,TradeDate) as YearInteger,
		*  
from	[Staging].[FireBirdFactCurrencyBankTrade]) as source

on		(target.id = source.id)
when	matched 
then	update 
set		CustomerKey =  source.CustomerKey  
		,TradeDateKey = source.TradeDateKey
		,UserId = source.UserId
		,SellCcyCode = source.SellCcyCode
		,BuyCcyCode = source.BuyCcyCode
		,SellAmount = source.SellAmount
		,BuyAmount = source.BuyAmount 
		,Settled = source.Settled
		,BuyCcyClientTradingBookId = source.BuyCcyClientTradingBookId
		,SellCcyClientTradingBookId = source.SellCcyClientTradingBookId
		,WorkflowInstanceId = source.WorkflowInstanceId
		,TradeDate = source.TradeDate
		,IsAutomatic = source.IsAutomatic
		,TradeEndDate = source.TradeEndDate
		,TradeProfit = source.TradeProfit

		,TradeProfitGBP = isnull(dw.[UdfGetGBPValueFromFinanceExchangeRates](source.TradeProfit,source.CurrencyPriority,source.MonthInteger, source.YearInteger) , 
			dw.[UdfGetGBPValueFromFinanceExchangeRatesNoDate](source.tradeprofit,source.CurrencyPriority))

		,TreasuryProfit = source.TreasuryProfit
		,Reversal = source.Reversal
		,IsReversed = source.IsReversed
		,IsUnWound = source.IsUnwound
		,TradeReason = source.TradeReason
		,HelpDeskUserId = source.HelpDeskUserId
		,TransferId = source.TransferId
		,TradeNotes = source.TradeNotes
		,FB_DateCreated = source.FB_DateCreated
		,FB_DateUpdated = source.FB_DateUpdated
		,DWDateUpdated = getdate()
when not matched 
then Insert	 (
			CustomerKey,
			TradeDateKey,
			Id,
			UserId,
			SellCcyCode,
			BuyCcyCode,
			SellAmount,
			BuyAmount,
			Settled,
			BuyCcyClientTradingBookId,
			SellCcyClientTradingBookId,
			WorkflowInstanceId,
			TradeDate,
			IsAutomatic,
			TradeEndDate,
			TradeProfit,
			TradeProfitGBP,
			TreasuryProfit,
			Reversal,
			IsReversed,
			IsUnWound,
			TradeReason,
			HelpDeskUserId,
			TransferId,
			TradeNotes,
			FB_DateCreated,
			FB_DateUpdated,
			FB_DateDeleted,
			DWDateInserted)

values		(source.customerKey,
			source.TradeDateKey,
			source.Id,
			source.UserId,
			source.SellCcyCode,
			source.BuyCcyCode,
			source.SellAmount,
			source.BuyAmount,
			source.Settled,
			source.BuyCcyClientTradingBookId,
			source.SellCcyClientTradingBookId,
			source.WorkflowInstanceId,
			source.TradeDate,
			source.IsAutomatic,
			source.TradeEndDate,
			source.TradeProfit,

			--MonthInteger,
			--YearInteger,
			
			isnull(dw.[UdfGetGBPValueFromFinanceExchangeRates](source.TradeProfit,source.CurrencyPriority,source.MonthInteger, source.YearInteger) , 
			dw.[UdfGetGBPValueFromFinanceExchangeRatesNoDate](source.tradeprofit,source.CurrencyPriority))/*as TradeProfitGBP*/, 

			source.TreasuryProfit,
			source.Reversal,
			source.IsReversed,
			source.IsUnWound,
			source.TradeReason,
			source.HelpDeskUserId,
			source.TransferId,
			source.TradeNotes,
			source.FB_DateCreated,
			source.FB_DateUpdated,
			source.FB_DateDeleted,
			source.DWDateInserted)
			;


