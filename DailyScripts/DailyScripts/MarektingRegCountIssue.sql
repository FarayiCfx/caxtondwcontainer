select * from applicationvalidationresults

select * from ApplicationAttempts 


select	count(id) as count_,
		cast(registrationdate as date) regdate
from	Applicants 
where cast(RegistrationDate as date) > '2016-07-31'
group	by cast(registrationdate as date) 
order	by regdate asc 

select top 10 * from applicants where email = 'chrissy5549@talktalk.net'



----------------------------------------------------------------------


SELECT COUNT(ID) AS COUNT_,
	cast(RegistrationDate as date) as regdate ,
	ApplicationState,
	EKYCStatus,
	PreAuthStatus,
	PostcodeStatus,
	WantToSendMoney
	FROM Applicants (nolock)
	where RegistrationDate > '2016-08-22 00:00:00.000'
	group by 
	cast(RegistrationDate as date) ,
	ApplicationState,
	EKYCStatus,
	PreAuthStatus,
	PostcodeStatus,
	WantToSendMoney
	order by regdate asc 
------------------------------------------------------------------------------------------------------	
-->old way everything before the release 
drop	table #temp1 
go
SELECT	COUNT(aa.id) AS [countofapplications] ,
		CAST(RegistrationDate AS DATE) AS  RegDate,
		aa.WantMultiCurrencyCard,
		[status] = CASE [status] WHEN 0 THEN 'None'
					WHEN 1 THEN 'Pending'
					WHEN 2 THEN 'passed'
					WHEN 3 THEN 'validated'
					WHEN 4 THEN 'rejected'
					WHEN 5 THEN 'archived'
					end,

					EKYCStatus = CASE EKYCStatus WHEN 0 THEN 'None'
							WHEN 1 THEN 'Passed'
							WHEN 2 THEN 'failed'
							WHEN 3 THEN 'approved'
							WHEN 4 THEN 'rejected'
							WHEN 5 THEN 'sanctionsfailed'
							when 6 THEN 'sanctionspassed'
							WHEN 10 THEN 'PassedKYC_AVSFailed'
							WHEN 999 THEN 'migrated'
							END,

							PostcodeStatus = CASE PostcodeStatus
							WHEN 0 THEN 'none'
							WHEN 1 THEN 'passed'
							WHEN 2 THEN 'failed'
							WHEN 3 THEN 'Approved'
							WHEN 4 THEN 'rejected'
							END ,

							PreAuthStatus = CASE PreAuthStatus
							WHEN 0 THEN 'none'
							WHEN 1 THEN 'passed'
							WHEN 2 THEN 'failed'
							WHEN 3 THEN 'approved'
							WHEN 4 THEN 'rejected'
							END
							
INTO		#TEMP1
FROM		dbo.Applicants aa (nolock)
join		dbo.AspNetUsers bb (nolock)
on			aa.Email = bb.UserName
WHERE		CAST(RegistrationDate AS DATE) > '2016-08-21' 
and			CAST(RegistrationDate AS DATE) < '2016-08-24'
GROUP	BY	CAST(RegistrationDate AS DATE),
			aa.WantMultiCurrencyCard,
			[status],
			EKYCStatus,
			PostcodeStatus,
			PreAuthStatus
ORDER	BY CAST(RegistrationDate AS DATE)

---------------------------------------------------------------------------
-->check secondaries 
select *	from #temp1 
where		wantmulticurrencycard = 1
and			ekycstatus <> 'sanctionspassed' 

-->check pendings -->before change 
select *	from #temp1 
where		wantmulticurrencycard = 1
and			status = 'pending' 


---->use the following code to actually sample a record  
with	cte1 as (
SELECT	aa.RegistrationDate,
		aa.Email, 
		aa.CreationDate,
		aa.PrimaryAccountId,
		aa.AccountTypeRef,
		aa.WantMultiCurrencyCard,
		CAST(RegistrationDate AS DATE) AS  RegDate,
		[status] = CASE [status] WHEN 0 THEN 'None'
					WHEN 1 THEN 'Pending'
					WHEN 2 THEN 'passed'
					WHEN 3 THEN 'validated'
					WHEN 4 THEN 'rejected'
					WHEN 5 THEN 'archived'
					end,

					EKYCStatus = CASE EKYCStatus WHEN 0 THEN 'None'
							WHEN 1 THEN 'Passed'
							WHEN 2 THEN 'failed'
							WHEN 3 THEN 'approved'
							WHEN 4 THEN 'rejected'
							WHEN 5 THEN 'sanctionsfailed'
							when 6 THEN 'sanctionspassed'
							WHEN 10 THEN 'PassedKYC_AVSFailed'
							WHEN 999 THEN 'migrated'
							END,

							PostcodeStatus = CASE PostcodeStatus
							WHEN 0 THEN 'none'
							WHEN 1 THEN 'passed'
							WHEN 2 THEN 'failed'
							WHEN 3 THEN 'Approved'
							WHEN 4 THEN 'rejected'
							END ,

							PreAuthStatus = CASE PreAuthStatus
							WHEN 0 THEN 'none'
							WHEN 1 THEN 'passed'
							WHEN 2 THEN 'failed'
							WHEN 3 THEN 'approved'
							WHEN 4 THEN 'rejected'
							END
FROM		dbo.Applicants aa (nolock)
join		dbo.AspNetUsers bb (nolock)
on			aa.Email = bb.UserName
WHERE		CAST(RegistrationDate AS DATE) > '2016-08-21' 
and			CAST(RegistrationDate AS DATE) < '2016-08-24')
select		* from cte1 where EKYCStatus =  'sanctionspassed'


-----------------------------------------------------------------------------------------------------
--the new way of counting registrants using application state 

drop	table #temp2
go

CREATE view [dbo].[PostRegChangeCounts]
as
SELECT	COUNT(aa.id) AS [countofapplications] ,
		
		CAST(RegistrationDate AS DATE) AS  RegDate,
		
		aa.WantMultiCurrencyCard,

		applicationstatetext = case aa.ApplicationState		
				when 0 then 'Unknown'
				when 1 then 'ApplicationStarted '
				when 2 then 'ApplicationAbandoned '
				when 3 then 'ApplicationSubmitted '
				when 4 then 'ApplicationRejected '
				when 5 then 'ComplianceVerification '
				when 6 then 'InternationalPaymentsEligibleUk '
				when 7 then 'InternationalPaymentsEligibleNonUk '
				when 8 then 'InternationalPaymentsEligibleUAE '
				when 9 then 'KycPassNoIpReason '
				when 10 then 'BeneficiaryEligible '
				when 11 then 'AwaitingWSEKYC '
				when 12 then 'ApplicationNotDuplicate '
				when 13 then 'ApplicationArchived '
				end,
				DATEPART(hh,aa.RegistrationDate) as hour_
--INTO		#TEMP2

FROM		dbo.Applicants aa (nolock)
join		dbo.AspNetUsers bb (nolock)
on			aa.Email = bb.UserName
WHERE		CAST(RegistrationDate AS DATE) > '2016-08-23' 
GROUP	BY	CAST(RegistrationDate AS DATE),
			aa.WantMultiCurrencyCard,
			aa.ApplicationState
ORDER	BY	CAST(RegistrationDate AS DATE)

--------------------------------------

SELECT * FROM [PostRegChangeCounts]



select *	from #temp2  
where		wantmulticurrencycard = 1
and			applicationstatetext = 'KycPassNoIpReason'


select	sum(countofapplications) as count_,regdate ,
status 
from	#temp1 
group	by regdate ,status
order	by regdate

select	* from ApplicationValidationResults

---status 0 is secondary cards
select	bb.*,aa.* 
from	Applicants aa 
join	ApplicationValidationResults bb
ON		AA.Id = BB.ApplicantId
where	AA.status = 0 
and		cast(registrationdate as date) = '2016-08-24'

select	* 
from	ApplicationValidationResults 
where	validationname in ('SanctionsCheck','FullUKKYC')
order	by Status

select	distinct validationname from ApplicationValidationResults



select	*	from #temp2  
where		wantmulticurrencycard = 1
and			applicationstatetext = 'KycPassNoIpReason  '
;

-->show me duplicates ?
with cte1 as (
select	count(email) as count_,
		email, 
		cast(registrationdate as date) as date_
from	Applicants (nolock)
where	cast(RegistrationDate as date) > '2016-08-23'
group	by	email, 
			cast(registrationdate as date)
			HAVING COUNT(EMAIL) > 1)
			select count(date_) as dupecount,date_
			from cte1 group by date_


---> 
select	top 10 * from applicants WHERE EMAIL = 'psm@psmthepsm.com'

select	* from ApplicationValidationResults where ApplicantId = 394842