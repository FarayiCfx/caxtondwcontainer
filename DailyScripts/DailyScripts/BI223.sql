/*
Run this in the production geo replica database ;

tjjsqsfozh.database.windows.net
*/
select	* from Firebird.UserBusinessAffiliateAssociation

select	* from firebird.BusinessAffiliate
;
exec	sp_help '[Firebird].[BusinessAffiliate]'

select  * 
from	Firebird.UserBusinessAffiliateAssociation
where	AccountTypeRef = 'corporate'

exec	sp_help 'Firebird.UserBusinessAffiliateAssociation'

select [name] as AffiliateName from firebird.businessaffiliate order by 1 asc

select * from firebird.userbusinessaffiliateassociation where AccountTypeEntityId ='151'

select [name]
		,* 
from	firebird.businessaffiliate
where	[Name] like 'our bumble%'

select * from dw.DimCorporateEntities order by 1 desc

select * from dw.DimUserCorporateAssociation order by 1 desc

select top 10 * from dw.DimCustomerReference

exec sp_who2 active

---------------------------------------------------------------------------------------------------------------
;

alter procedure GetTradesByAffiliate @AffiliateName varchar(255), @stardate datetime, @enddate datetime
as
/*
declare @affiliatename varchar (255)
		,@startdate datetime
		,@enddate datetime

select	@affiliatename = 'Our Bumble'
;
*/

with	cteAllClientsPerAffiliate
as
(
select	aa.AccountTypeRef
		,aa.AccountTypeEntityId
		,bb.Name as AffiliateName
		,bb.RevenueShare
		,ua.Email
		,au.FirstName +' '+ au.LastName as [Client Name]
		,cc.CompanyName
from	firebird.userbusinessaffiliateassociation aa
join	firebird.businessaffiliate bb
on		aa.businessaffiliateid = bb.id
join	dw.DimCorporateEntities cc
on		aa.AccountTypeEntityId = cc.Id
join	dw.DimUserCorporateAssociation ua
on		cc.Id = ua.CorporateEntityId
join	dw.DimCustomerReference au 
on		ua.Email = au.Email
where	aa.accounttyperef = 'corporate' --34 rows 

union

select	aa.AccountTypeRef
		,aa.AccountTypeEntityId
		,bb.Name as AffiliateName
		,bb.RevenueShare
		,au.Email
		,au.FirstName +' '+ au.LastName as [Client Name]
		,'' as Companyname
from	firebird.userbusinessaffiliateassociation aa
join	firebird.businessaffiliate bb
on		aa.businessaffiliateid = bb.id
join	dw.DimCustomerReference au
on		aa.AccountTypeEntityId = au.Id
where	aa.accounttyperef = 'retail'
)
-->spot trades
select	AffiliateName
		,ClientName = case when accounttyperef = 'corporate' then companyname + ' - Corporate' else [Client Name] + ' - Retail' end
		,RevenueShare
		,Email as ClientEmail
		,bb.TradeDate
		,Id as tradeid
		,SellCcyCode
		, SellAmount
		, BuyAmount
		, BuyCcyCode
		, TradeEndDate
		,TradeProfit
		,cast((RevenueShare/100)* TradeProfit as decimal(10,2))as [Commission Due]
		, TradeReason
		,'Spot trade' as TradeType

from	cteAllClientsPerAffiliate aa 
join	Firebird.FactCurrencyBankTrade bb
on		aa.Email = bb.UserId
where	AffiliateName = @affiliatename
and		TradeDate between @stardate and @enddate
and		bb.TradeReason <> 'card load'

union 

-->forward trades 
select	AffiliateName
		,ClientName = case when accounttyperef = 'corporate' then companyname + ' - Corporate' else [Client Name] + ' - Retail' end
		,RevenueShare
		,Email as ClientEmail
		,fw.TradeDate
		,fw.Id as tradeid
		,fw.SellCcyCode
		, fw.SellAmount
		, fw.BuyAmount
		, fw.BuyCcyCode
		, fw.TradeEndDate
		,fw.TradeProfit
		,cast((RevenueShare/100)* fw.TradeProfit as  decimal(10,2))as [Commission Due]
		, fw.TradeReason
		,'Forward' as TradeType 
from	cteAllClientsPerAffiliate aa 
join	Firebird.FactCurrencyBankForwardTrade fw
on		aa.Email = fw.UserId
where	AffiliateName = @affiliatename
and		TradeDate between @stardate and @enddate
union 
-->flex trades
select	AffiliateName
		,ClientName = case when accounttyperef = 'corporate' then companyname + ' - Corporate' else [Client Name] + ' - Retail' end
		,RevenueShare
		,Email as ClientEmail
		,fl.TradeDate
		,fl.Id as tradeid
		,fl.SellCcyCode
		, fl.SellAmount
		, fl.BuyAmount
		, fl.BuyCcyCode
		, fl.TradeEndDate
		,fl.TradeProfit
		,cast((RevenueShare/100)* fl.TradeProfit  as decimal (10,2))as [Commission Due]
		, fl.TradeReason
		,'Flex' as TradeType 
from	cteAllClientsPerAffiliate aa 
join	 Firebird.FactCurrencyBankFlexTrade fl
on		aa.Email = fl.UserId
where	AffiliateName = @affiliatename
and		TradeDate between @stardate and @enddate
;

exec GetTradesByAffiliate @affiliatename = 'our bumble', @stardate ='2017-02-20', @enddate = '2018-01-01'

select top 5 * from Firebird.FactCurrencyBankTrade

select top 5 * from Firebird.FactCurrencyBankFlexTrade

select top 5 * from Firebird.FactCurrencyBankForwardTrade

select * from firebird.userbusinessaffiliateassociation
select * from firebird.businessaffiliate


-->
exec sp_who2 active

-->corporate  entities 
/*;
with	Entities as (
select	aa.AccountTypeRef
		,aa.AccountTypeEntityId
		,bb.Name as AffiliateName
		,bb.RevenueShare
		,ua.Email
		,au.FirstName +' '+ au.LastName as [Client Name]
		,cc.CompanyName
		----,cc.*
from	firebird.userbusinessaffiliateassociation aa
join	firebird.businessaffiliate bb
on		aa.businessaffiliateid = bb.id
join	CorporateEntities cc
on		aa.AccountTypeEntityId = cc.Id
join	UserCorporateAssociation ua
on		cc.Id = ua.CorporateEntityId
join	AspNetUsers au 
on		ua.Email = au.Email
where	aa.accounttyperef = 'corporate' --34 rows 

union

select	aa.AccountTypeRef, aa.AccountTypeEntityId
		,bb.Name as AffiliateName
		,bb.RevenueShare
		,au.Email
		,au.FirstName +' '+ au.LastName as [Client Name]
		,'' as Companyname
from	firebird.userbusinessaffiliateassociation aa
join	firebird.businessaffiliate bb
on		aa.businessaffiliateid = bb.id
join	AspNetUsers au
on		aa.AccountTypeEntityId = au.Id
where	aa.accounttyperef = 'retail'
)
select	Entities.*
		,tr.Id
		,tr.UserId
		,tr.TradeDate
		,tr.BuyCcyCode
		,tr.BuyAmount
		,tr.SellCcyCode
		,tr.SellAmount
		,tr.TradeProfit
		,(RevenueShare/100)* TradeProfit as Commission
		,tr.TradeReason
from	Entities 
join	CurrencyBank.Trade tr
on		Entities.Email = tr.UserId
--and		AffiliateName like 'our bumble%'
order	by TradeDate desc 
*/

exec sp_who2 active
