DROP TABLE [dw].[CustomerRFMAggregations_2]
GO

CREATE TABLE [dw].[CustomerRFMAggregations_2](
	[CustomerKey] [bigint] NOT NULL default (0),
	[FIS_VISA_CardLoadCount] [int] NULL default (0),
	[FIS_VISA_CardLoadTotalGBP] [decimal](20, 2) NULL default (0),
	[FIS_VISA_TotalSpendGBP] [decimal](20,2) NULL default (0),
	[FIS_VISA_AverageSpendGBP] [decimal](20,2) NULL default (0),
	[FIS_VISA_TransactionCount] [bigint] NULL default (0),
	[FIS_VISA_LastSpendDate] [datetime] NULL default (0),
	[FIS_VISA_FirstLoadDate] [datetime] NULL default (0),
	[FIS_VISA_LastLoadDate] [datetime] NULL default (0),
	[MigrationActivationDate] [datetime] NULL default (0),
	[MigrationActivationFlag] [bit] NULL default (0),
	
	[VALITOR_MASTERCARD_CardLoadCount] [int] NULL default (0),
	[VALITOR_MASTERCARD_LoadTotalGBP] [decimal](20, 2) NULL default (0),
	[VALITOR_MASTERCARD_SpendTotalGBP] [decimal](20, 2) NULL default (0),
	[VALITOR_MASTERCARD_AverageSpendGBP] [decimal](20, 2) NULL default (0),
	[VALITOR_MASTERCARD_FirstLoadDate] [datetime] NULL default (0),
	[VALITOR_MASTERCARD_LastLoadDate] [datetime] NULL default (0),
	
	[CardLoad_TradeCount]	[int] NULL default(0),
	[CardLoad_Tradeprofit_GBP_Total]	decimal(20,2) default(0), 
	[CardLoad_FirstDate]	[datetime] NULL default (0),
	[CardLoad_LastDate]		[datetime] NULL default (0),

	[BuyAndHold_TradeCount]	[int] NULL default (0),
	[BuyAndHold_Tradeprofit_GBP_Total]	decimal(20,2) null default(0), 
	[BuyAndHold_FirstDate]	[datetime] NULL default (0),
	[BuyAndHold_LastDate]	[datetime] NULL default (0),

	[BuyAndSend_TradeCount] [int] NULL default (0),
	[BuyAndSend_Tradeprofit_GBP_Total]	decimal(20,2) null default(0), 
	[BuyAndSend_FirstDate]	[datetime] NULL default (0),
	[BuyAndSend_LastDate]	[datetime] NULL default (0),

	[BBGuarantee_TradeCount]	[int] NULL default (0),
	[BBGuarantee_Tradeprofit_GBP_Total]	decimal (20,2) default(0),
	[BBGuarantee_FirstDate]	[datetime] NULL default (0),
	[BBGuarantee_LastDate]		[datetime] NULL default (0),

	[MoneyTransfer_Talksheet_DealCount] [int] NULL default (0),
	[MoneyTransfer_Talksheet_First_Dealdate] [datetime] NULL default (0),
	[MoneyTransfer_Talksheet_Last_DealDate] [datetime] NULL default (0),
	[MoneyTransfer_Talksheet_SumOfRevenue] [decimal](20, 2) NULL default (0),
	
	[AccountTypeRef] [nvarchar](max) NULL,
	[SourceCreationDate] [datetime] NULL,
	[DWDateUpdated] [datetime] NULL,
CONSTRAINT [PK_CustomerRFMAggregations_2] PRIMARY KEY CLUSTERED 
(
	[CustomerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO


exec sp_help '[dw].[CustomerRFMAggregations_2]'
