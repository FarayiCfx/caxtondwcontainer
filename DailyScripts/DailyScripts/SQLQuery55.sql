select  top 10 * from firebird.[factCurrencyBankTradeDetail]
;
/*
so look at trades in month of January then create
another cte to see events that have taken place after  
*/
with cteTrades as(
select	ID,TradeDate,userid,sellccycode,buyccycode,sellamount,buyamount,TradeProfit
		,tradereason
from	FireBird.FactCurrencyBankTrade
where	tradedatekey between 20180101 and 20180131
and		TradeReason <> 'card load' ----order by SellAmount --> 6361 
),
cte2 as (
select	replace([status],'debit for trade id :','') as TradeIdDerived
		,* 
		, cast (ExternalTransactionId as int) as ExternalTransIdInt
	
from	firebird.factcurrencybankaccounttransaction
where	externaltransref = 'trade'
and		datekey > 20180131
and		[status] like 'debit for trade%') --18931

select	aa.*
		,bb.TransactionDateTime 
		,bb.Amount
		,bb.Balance
		,bb.Status
		,bb.ExternalTransactionId
		,bb.ExternalTransRef
		, AccountSummaryId
from	cteTrades aa
join	cte2 bb
on		aa.Id = bb.ExternalTransIdInt
where UserId = 'jennybeltre@hotmail.com'
order	by aa.Id asc


select	* 
from	FireBird.FactCurrencyBankAccountTransaction
where	AccountSummaryId =  803333

select * from FireBird.FactCurrencyBankTradeDetail where TradeId = 2325973



------------------------------------------------------------------------------------------------

with	cteTrades as(
select	ID,tradedatekey,TradeDate,userid,sellccycode,buyccycode,sellamount,buyamount,TradeProfit
		,tradereason
from	FireBird.FactCurrencyBankTrade
where	tradedatekey between 20180101 and 20180131
and		TradeReason <> 'card load' ----order by SellAmount --> 6361 
),
cte2 as (
select	replace([status],'credit for trade id :','') as TradeIdDerived
		,* 
		,cast (ExternalTransactionId as int) as ExternalTransIdInt
from	firebird.factcurrencybankaccounttransaction
where	externaltransref = 'trade'
and		datekey > 20180131
and		[status] like 'credit for trade%') --18931

select	aa.*
		,bb.TransactionDateTime
		,bb.Amount
		,bb.Balance
		,bb.Status
		,bb.ExternalTransactionId
		,bb.ExternalTransRef
from	cteTrades aa
join	cte2 bb
on		aa.Id = bb.ExternalTransIdInt
order	by aa.Id asc 
---
select	* 
from	FireBird.FactCurrencyBankTrade
where	Id = 2325973 

select	* 
from	FireBird.FactPaymentTransfer 
where	Id = 119243

select top 10 * from valitor.FactWalletTransactions
order by 1 desc

select	top 10 * 
from	FireBird.FactCurrencyBankAccountTransaction
where	Status like '%2.25%'

select	* 
from	firebird.factcurrencybankaccounttransaction
where	status like '%load fee%'
order	by id desc

select	* 
from	firebird.factcurrencybankaccounttransaction
where	status like '%load fee%'
and		status not like '%<HD>%'

select	* 
from	firebird.factcurrencybankaccounttransaction
where	status like '%2.25%'
order	by id desc

exec sp_who2 active


