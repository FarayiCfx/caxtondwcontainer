select	count(*) 
from	[dw].[CustomerRFMAggregations]
where	fis_visa_cardloadcount > 0
and		valitor_mastercard_cardloadcount = 0
and		fis_visa_lastloaddate > '2015-05-01 00:00:00.000'


select	aa.customerkey, dc.id as FirebirdId, aa.fis_visa_cardloadcount,aa.fis_visa_cardloadtotalgbp,
		aa.fis_visa_lastloaddate,valitor_mastercard_cardloadcount,aa.accounttyperef,
		bb.expdate as expdate1, cc.expdate as expdate2
from	[dw].[CustomerRFMAggregations] aa

left	outer join	dw.tscarddetail_expiry bb
on		aa.customerkey = bb.customerkey
and		bb.expiryrank = 1

left	outer join	dw.tscarddetail_expiry cc
on		aa.customerkey = cc.customerkey
and		cc.expiryrank = 2

join	dw.dimcustomerreference dc
on		aa.customerkey = dc.customerkey 

where	aa.fis_visa_cardloadcount > 0
and		aa.valitor_mastercard_cardloadcount = 0
and		fis_visa_lastloaddate > '2015-05-01 00:00:00.000'
and		aa.accounttyperef = 'Retail card - automated migration from TalkSheet'


-------------------------------------------

select	dc.firstname, dc.lastname,coalesce (dc.username,dc.contactemailaddress) as email,
		aa.customerkey, dc.id as FirebirdId, aa.fis_visa_cardloadcount,aa.fis_visa_cardloadtotalgbp,
		aa.fis_visa_lastloaddate,valitor_mastercard_cardloadcount,aa.accounttyperef
from	[dw].[CustomerRFMAggregations] aa

join	dw.dimcustomerreference dc
on		aa.customerkey = dc.customerkey 

where	aa.fis_visa_cardloadcount > 0
and		aa.valitor_mastercard_cardloadcount = 0
and		fis_visa_lastloaddate > '2015-05-01 00:00:00.000'
and		aa.accounttyperef = 'Retail card - automated migration from TalkSheet'


select	top 10 * 
from	dw.dimcustomerreference


select	top 10 * 
from	dw.factapplicants

exec	sp_who2 active
