select count(*) from CurrencyBank.trade where TradeDate BETWEEN '2016-12-01' and '2016-12-31'

CREATE view CounterPartyRefReport
as
with cte1 as (
select	aa.id as TradeId,
		aa.UserId, 
		aa.SellCcyCode, 
		aa.BuyCcyCode, 
		aa.BuyCcyClientTradingBookId as TradeTableBuyCcyClientTradingBookId,
		aa.BuyAmount,
		aa.TradeDate,
		aa.TradeReason,
		aa.BuyCcyClientTradingBookId,
		bb.id as ClientTradeBookId ,
		bb.name,
		bb.ccycode,
		mk.id as MarketTradeId,
		mk.bookname,
		mk.counterpartyrefno,
		mk.sellccy as MarcketTradeSellCCY,
		mk.sellccyamount as MarketTradeSellCCYAmount,
		mk.buyccy as MarketTradeBuyCCY,
		mk.buyccyamount  as MarketTradeBuyCCYAmount,
		mk.bookopendate
from	currencybank.Trade aa (nolock)
join	currencybank.ClientTradingBook bb (nolock)
on		aa.BuyCcyClientTradingBookId = bb.Id
join	CurrencyBank.MarketTrade mk (nolock)
on		bb.Name = mk.BookName
and		convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
and		bb.CcyCode = mk.BuyCcy)
select * from cte1 where counterpartyrefno is not NULL

select * from #result_set where counterpartyrefno is not NULL

select top 5 * from currencybank.Trade
select top 5 * from currencybank.ClientTradingBook
select top 5 * from currencybank.MarketTrade


exec sp_help 'currencybank.ClientTradingBook'

select top 3 * from CurrencyBank.Trade where UserId = 'jon.wesson@iconofficedesign.co.uk' order by 1 desc

select		*	,
			convert(varchar(24) ,BookOpenDate,120) as DateNoMillisecond
from		currencybank.ClientTradingBook 
where		name = 'FB_20/02/2017_Adam.Stevens@CAXTONFX.com_jon.wesson@iconofficedesign.co.uk_Normal'

select * ,convert(varchar(24) ,BookOpenDate,120) as DateNoMillisecond
from	CurrencyBank.MarketTrade
where	BookName = 'FB_20/02/2017_Adam.Stevens@CAXTONFX.com_jon.wesson@iconofficedesign.co.uk_Normal'
-----

select top 3 * from CurrencyBank.Trade where TradeDate >'2016-02-02' 
and TradeReason <> 'card load' 
order by 1 asc

select		*	,
			convert(varchar(24) ,BookOpenDate,120) as DateNoMillisecond
from		currencybank.ClientTradingBook where id = 100962

select * ,convert(varchar(24) ,BookOpenDate,120) as DateNoMillisecond
from	CurrencyBank.MarketTrade 
where	BookName = 'FB_03/02/2016_Normal' 



------------------------------------------------------------------------------------------------------->
select 
from currencybank.ClientTradingBook aa
join CurrencyBank.MarketTrade bb
on aa.Name = bb.BookName
and convert(varchar(24) ,BookOpenDate,120)  = convert(varchar(24) ,BookOpenDate,120) 