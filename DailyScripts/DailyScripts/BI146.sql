select top 10 * from CurrencyBank.AccountTransaction

select  * from CurrencyBank.Currency

exec sp_help 'CurrencyBank.Currency'
exec sp_help 'CurrencyBank.accountsummary'

--UP TO November 2016 from the begining of time 
IF		OBJECT_ID('tempdb..#November2016') IS NOT NULL
DROP	TABLE #November2016
GO
with	cteNovember as (
select	max(id) as Maxid,
		accountsummaryid
from	CurrencyBank.AccountTransaction (nolock)
where	TransactionDateTime < '2016-11-30 23:59:00.000'
group	by AccountSummaryId)

select	aa.Maxid,
		bb.balance as BalanceNov2016,
		aa.accountsummaryid,
		cc.UserId,
		dd.BuyCcyCode
into	#November2016
from	cteNovember aa
join	CurrencyBank.AccountTransaction (nolock) bb
on		aa.Maxid = bb.Id
join	currencybank.AccountSummary cc
ON		bb.AccountSummaryId = cc.Id
join	CurrencyBank.Currency dd
on		cc.CurrencyId = dd.Id
GO
--

--UP TO December 2016 from the begining of time 
IF		OBJECT_ID('tempdb..#December2016') IS NOT NULL
DROP	TABLE #December2016
GO
with	cteDecember as (
select	max(id) as Maxid,
		accountsummaryid
from	CurrencyBank.AccountTransaction (nolock)
where	TransactionDateTime < '2016-12-31 23:59:00.000'
group	by AccountSummaryId)

select	aa.Maxid as MaxIdDec2016,
		bb.balance as BalanceDec2016,
		aa.accountsummaryid ,
		cc.UserId as UserIdDec2016,
		dd.BuyCcyCode AS BuyCcyCodeDecember
into	#December2016
from	cteDecember aa
join	CurrencyBank.AccountTransaction (nolock) bb
on		aa.Maxid = bb.Id
join	currencybank.AccountSummary cc
ON		bb.AccountSummaryId = cc.Id
join	CurrencyBank.Currency dd
on		cc.CurrencyId = dd.Id
GO

select * from #November2016 order by BalanceNov2016

select * from #December2016 order by BalanceDec2016

select *	from CurrencyBank.AccountTransaction 
where		AccountSummaryId = 563375
go

with	cte1 as (
select	BalanceNov2016 ,BalanceDec2016,aa.accountsummaryid ,aa.Userid, aa.buyccycode
from	#November2016 aa
full	outer join #December2016 bb
on		aa.accountsummaryid = bb.accountsummaryid)
select	*
from	cte1 where BalanceNov2016 <> BalanceDec2016
AND		USERID = 'jfds@bryanston.co.uk'
GO

alter view ClientBalances_Nov_Dec_2016
as
SELECT aa.userid,aa.Balance,aa.ValueDate ,bb.BuyCcyCode
FROM [CurrencyBank].[ClientMoney] aa
join CurrencyBank.Currency bb
on aa.CurrencyId = bb.Id
where aa.ValueDate in ('2016-12-30 00:00:00.000','2016-11-30 00:00:00.000')

grant	select on  ClientBalances_Nov_Dec_2016 to cfx_datareader 

select	* from ClientBalances_Nov_Dec_2016 where	userid = 'jfds@bryanston.co.uk'

select	* from [CurrencyBank].[ClientMoney] where	valuedate in ('2016-12-31 00:00:00.000')







  exec sp_spaceused '[CurrencyBank].[ClientMoney]'

  -----714591              








