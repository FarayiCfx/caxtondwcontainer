DROP TABLE #TEMP 
GO

CREATE TABLE #TEMP (ReversedEmailAddress varchar(255))

insert into #temp (ReversedEmailAddress) values ('1504490@chester.ac.uk')
insert into #temp (ReversedEmailAddress) values ('Aaronmg95@aol.com')
insert into #temp (ReversedEmailAddress) values ('abimuir17@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('adaireileen4@gmail.com')
insert into #temp (ReversedEmailAddress) values ('Alexcroasdell16@gmail.com')
insert into #temp (ReversedEmailAddress) values ('beckydrury@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('Bella-pink-rose@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('billie.j-harris@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('carys2010@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('charliebarnes10@gmail.com')
insert into #temp (ReversedEmailAddress) values ('Charlottemorgan-nyarko@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('daniels2009@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('dionneappiah@gmail.com')
insert into #temp (ReversedEmailAddress) values ('dm.gaskin96@gmail.com')
insert into #temp (ReversedEmailAddress) values ('ed_cripps@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('edwarddixon96@gmail.com')
insert into #temp (ReversedEmailAddress) values ('eilishbrand@btinternet.com')
insert into #temp (ReversedEmailAddress) values ('ellenorrhodes@gmail.com')
insert into #temp (ReversedEmailAddress) values ('ellie_bates_07@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('ellieimpey@gmail.com')
insert into #temp (ReversedEmailAddress) values ('fihilton@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('georgiabratt16@yahoo.co.uk')
insert into #temp (ReversedEmailAddress) values ('hansanaz123@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('hanz692@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('hollyhanbury@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('info@helenesoderstrom.se')
insert into #temp (ReversedEmailAddress) values ('Islabaxter9@gmail.com')
insert into #temp (ReversedEmailAddress) values ('j.woodrow97@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('jamesp1994@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('jamie.hough@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('jennyparker96@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('jjmcgowan22@gmail.com')
insert into #temp (ReversedEmailAddress) values ('jordanboyd-x@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('joshluke007@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('Joshua_donaldson_12@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('Katee_pm@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('kbh10@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('keb41@kent.ac.uk')
insert into #temp (ReversedEmailAddress) values ('keenansportcoach@gmail.com')
insert into #temp (ReversedEmailAddress) values ('l.ambler97@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('laurenrowe.21@gmail.com')
insert into #temp (ReversedEmailAddress) values ('leahwhitehead@outlook.com')
insert into #temp (ReversedEmailAddress) values ('libbyaitcheson@talktalk.net')
insert into #temp (ReversedEmailAddress) values ('lindsey-leitch.x@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('livj96@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('Lollie.loulou@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('meganeastes@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('melissafret@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('NRSimmons001@mailinator.com')
insert into #temp (ReversedEmailAddress) values ('ori178@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('owenhwilliams21@gmail.com')
insert into #temp (ReversedEmailAddress) values ('philipmulcahy5@gmail.com')
insert into #temp (ReversedEmailAddress) values ('rashmi_sudarsan@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('rdownie35@gmail.com')
insert into #temp (ReversedEmailAddress) values ('rosienickels@yahoo.co.uk')
insert into #temp (ReversedEmailAddress) values ('rosiesmejka9@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('Sam-prince97@hotmail.com')
insert into #temp (ReversedEmailAddress) values ('Sarahkearns7@googlemail.com')
insert into #temp (ReversedEmailAddress) values ('sophi1996@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('Theaafish@gmail.com')
insert into #temp (ReversedEmailAddress) values ('tomrainnie@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('Wesleyconnor@hotmail.co.uk')
insert into #temp (ReversedEmailAddress) values ('yazminejones12@hotmail.co.uk')


select	tmp.*,aa.email as email_recorded_in_application,
		bb.UserName as Actual_Account_Username
		,aa.[affiliatecode],heardaboutus
		,status = case aa.Status when 0 then 'None'
								when 1 then 'Pending'
								when 2 then 'Passed'
								when 3 then 'Validated'
								when 4 then 'Rejected'
								when 5 then 'Archived'
								end
		,aa.RegistrationDate 
from #temp tmp
left outer join applicants aa
on tmp.ReversedEmailAddress = aa.Email
left outer join aspnetusers bb
on aa.Email = bb.UserName


---select top 10 * from Applicants 


