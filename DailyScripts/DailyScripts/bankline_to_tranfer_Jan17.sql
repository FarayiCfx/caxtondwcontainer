-->total count of the bankline file itself
select * from BankLineFileImport_Jan2017
-->13245 records

-->pick out the transaction references and put the data into a temp table first and count through what you have available  
IF		OBJECT_ID('#result_set') IS NOT NULL
DROP	TABLE #result_set;
-->
with	cte1 as (
select	TRANREFID1 = case when [Narrative #1] like '%TRANREF%' then 
		REPLACE(replace([Narrative #1],'TRANREF',''),':','')	
					END,
					
					TRANREFID2 = case when [Narrative #2] like '%TRANREF%' then 
		REPLACE(replace([Narrative #2],'TRANREF',''),':','')	
					END,
					* 
from	BankLineFileImport_Jan2017) -->make sure to change this line to reflect the new file !!!!!!!!!!!!!!!!!!!
select	COALESCE(tranrefid1,tranrefid2) as TranRefIdCoalesce,
		cte1.*
into	#result_set 
from	cte1 

-->How may have ctpytr as the "allocated" reference?
select * from #result_set where Allocated = 'CtpyTr'


-->show me everything else that you have to resolve that is not a trade?
select * from #result_set  where TranRefIdCoalesce  is null




-->now go and join this result set to the transfer table 
select  aa.debit,
		aa.credit,
		aa.currency,
		bb.BuyAmount,
		bb.BuyCcyCode,
		aa.*,
		bb.*
from	#result_set aa
join	FireBird.FactPaymentTransfer bb
on		aa.TranRefIdCoalesce = bb.Id
order	by bb.CreatedDateKey
-->5548
-->these are the transactions for which we 

select * from #result_set
select top 10 * from FireBird.FactPaymentTransfer
select top 10 * from FireBird.FactCurrencyBankTrade


-->go one step further and retrieve the trade id
IF		OBJECT_ID('#result_set2') IS NOT NULL
DROP	TABLE #result_set2;
go
select	TranRefIdCoalesce,
		aa.[sort code],
		aa.[account number],
		aa.[account alias],
		aa.[account short name],
		aa.currency,
		aa.[account type],
		aa.bic,
		aa.[bank name],
		aa.[branch name],
		aa.[date],
		aa.[narrative #1],
		aa.[narrative #2],
		aa.[narrative #3],
		aa.[narrative #4],
		aa.[narrative #5],
		aa.[type],
		aa.debit,
		aa.credit,
		bb.id as TransferId,
		bb.userid,
		trd.id as TradeId,
		trd.UserId as TradeUserId,
		trd.HelpDeskUserId,
		trd.TradeReason
into	#result_set2
from	#result_set aa
join	FireBird.FactPaymentTransfer bb
on		aa.TranRefIdCoalesce = bb.Id
left	outer join FireBird.FactCurrencyBankTrade trd --additional join to currencybanktrade
on		bb.Id = trd.TransferId
order	by bb.CreatedDateKey

SELECT	* FROM #RESULT_SET2 WHERE TRADEID IS NULL
----------------------------------------------------------------------------------------------------------------->

