drop procedure if exists GetDebtorsAndCreditors_AllPayments
go
create procedure GetDebtorsAndCreditors_AllPayments @startdate datetime, @enddate datetime 
as 
/*
Farayi Nzenza 

This will report on trades and money owed on the trades 

This has to be wrapped in a stored procedure 
because of the need to use temp tables, index 
them on the fly and do lookups to trade detail 
table

The need to work out when money comes in per trade

Slight change from the original version in that 
we want the money coming in over all time, not just 
the cutoff at monthend
*/
set		nocount on

IF		OBJECT_ID('tempdb..#MoneyPaidOut') IS NOT NULL
DROP	TABLE #MoneyPaidOut 

declare @startdateinteger int
		,@enddateinteger int 
		,@startofmonthinteger int
		,@endofperiodinteger int 

select	@startdateinteger = cast(convert(varchar(12),@startdate,112) as int)
select	@enddateinteger =  cast(convert(varchar(12),@enddate,112) as int)
select	@startofmonthinteger = cast(convert(varchar(12), DATEADD(month, DATEDIFF(month, 0, @startdate), 0),112) as int)
select	@endofperiodinteger = cast(convert(varchar(12),EOMONTH (@startdate,0),112) as int)
	
--debug section 
/*
print	@startdateinteger 

print	@enddateinteger 

print	@startofmonthinteger

print	@endofperiodinteger 
*/
select	sum(amount) as AmountPaidOut
		,externaltransactionid
		,cast(transactiondatetime as date) as DatePaidOut
into	#MoneyPaidOut
from	[firebird].factcurrencybankaccounttransaction (nolock)
where		externaltransref = 'trade'
and			amount > 0
and			datekey >= @startdateInteger  
and			([status] like 'credit for buy%' or [status] like 'credit for trade%')
group	by	externaltransactionid
			,cast(transactiondatetime as date) 

select		aa.id as TradeId,
			aa.buyccycode
			,aa.BuyAmount
			,AA.SellCcyCode
			,aa.SellAmount
			--,aa.Settled
			,cast(aa.TradeDate as Date) as TradeDate
			,cast(aa.TradeEnddate as Date) as [SettlementDate]
			--,isnull(aa.HelpdeskUserId,'') as HelpdeskUserId
			,aa.UserId
			,BB.tradeid_summarytable
			,isnull(bb.AmountPaidin,0) as AmountPaidIn
			,isnull(aa.TradeReason,'') as TradeReason
			,aa.TradeProfit
			,isnull(buyamount,0) - isnull(AmountPaidOut,0) as payable
			,isnull(sellamount,0) - isnull(AmountPaidIn,0) as receivable
			,isnull(AmountPaidIn,0) as AmountPaidIn
			--,cc.AmountPaidOut
			,cc.*
from	firebird.[factCurrencyBankTrade] AS aa (nolock)

OUTER	APPLY	(select tradeid as tradeid_summarytable , 
						sum(amount) as AmountPaidIn
				from	firebird.[factCurrencyBankTradeDetail] bb (nolock)
				where	bb.tradeid = AA.id
				and		bb.paiddatekey between @startdateinteger and @endofperiodinteger
				GROUP	BY BB.tradeid) 
				BB
left		outer join #MoneyPaidOut  cc
on			aa.id = cc.externaltransactionid
where 		aa.isunwound = 0
			and		aa.isreversed = 0
			and		aa.reversal = 0 
			and		aa.tradedatekey between @startdateinteger and @enddateinteger
			and		aa.tradereason <> 'card load'
			and		aa.userid <> 'val_monitor'


exec GetDebtorsAndCreditors_AllPayments @startdate = '2017-09-01' , @enddate = '2017-09-30'

select	getdate()

select	count(id) 
from	firebird.factcurrencybanktrade aa
where	aa.tradedate between '2015-01-21' and '2017-09-25' --1 493 990
and		aa.isunwound = 0 --1 492 307
and		aa.isreversed = 0 -- 1 490 607
and		aa.reversal = 0  -- 1 488 910
and		aa.tradereason <> 'card load'--179 878
and		aa.userid <> 'val_monitor' --179 056

exec sp_who2 active
