alter procedure [dw].[MergeBuyAndSendrfmStats]
as
/*
Merge the load totals, counts and min and max dates to the 
customer aggregate RFM table 
*/
with	cteBuyAndSendStats as(
select	sum(TradeProfit) GBPTradeProfit , 
		count(id) as Count,
		min(TradeDate) as MinTradeDate,
		max(TradeDate) as MaxTradeDate,
		customerkey 
from	FireBird.FactCurrencyBankTrade (nolock)
where	TradeReason =  'Buy and Send'
group	by customerkey)

merge	DW.CustomerRFMAggregations rfm 
using	(
select	ck.customerkey,
		isnull (cte.GBPTradeProfit,0) as [BuyAndSend_Tradeprofit_GBP_Total],
		isnull (cte.count,0) as [BuyAndSend_TradeCount],
		isnull (cte.MinTradeDate , '1900-01-01 00:00:00.000') as [BuyAndSend_FirstDate],
		isnull (cte.MaxTradeDate, '1900-01-01 00:00:00.000') as [BuyAndSend_LastDate]
from	dw.dimcustomerreference ck
left	outer join cteBuyAndSendStats cte
on		ck.customerkey = cte.customerkey ) as source
on		rfm.customerkey = source.customerkey 
when	matched 
then	update 
set		rfm.[BuyAndSend_Tradeprofit_GBP_Total] = source.[BuyAndSend_Tradeprofit_GBP_Total],
		rfm.[BuyAndSend_TradeCount] = source.[BuyAndSend_TradeCount],
		rfm.[BuyAndSend_FirstDate] = source.[BuyAndSend_FirstDate],
		rfm.[BuyAndSend_LastDate] = source.[BuyAndSend_LastDate],
		rfm.DWDateUpdated = getdate()
		
when	not matched
then	insert (CustomerKey,
				[BuyAndSend_Tradeprofit_GBP_Total],
				[BuyAndSend_TradeCount],
				[BuyAndSend_FirstDate],
				[BuyAndSend_LastDate],
				DWDateUpdated)
				values (
				source.CustomerKey,
				source.[BuyAndSend_Tradeprofit_GBP_Total],
				source.[BuyAndSend_TradeCount],
				source.[BuyAndSend_FirstDate],
				source.[BuyAndSend_LastDate],
				getdate())
		;

GO



exec [dw].[MergeBuyAndSendrfmStats]

