select top 10 * from [FireBird].[FactPaymentTransfer]  where complete = 1 

exec [dbo].[GetAccountSummaryCredits4]  @username = '' ,@fastpay = '25427'

exec [dbo].[GetAccountSummaryCredits4]  @username = 'fiderespartners@caxtonfx.com' ,@fastpay = ''

exec [dbo].[GetTransfersOut2]  @username = '' ,@fastpay = '25427'

exec [dbo].[GetTransfersOut2]  @username = 'fiderespartners@caxtonfx.com' ,@fastpay = ''

exec [dbo].[GetTradesByUserId3]  @username = '' ,@fastpay = '25427'

exec [dbo].[GetTradesByUserId3]  @username = 'fiderespartners@caxtonfx.com' ,@fastpay = ''

exec [dbo].[GetTradesByUserId2]  @username = 'fiderespartners@caxtonfx.com' 


select * from dw.dimcustomerreference where fastpaycontactref = '16449'

select	replace(isnull(Title,'') ,'unknown','')+ ' ' +isnull(Firstname,'') +' '+isnull(LastName,'')  +char(13)+char(10) +
		isnull(AddressLine1,'') 
		 +char(13)+char(10) +
		isnull(City,'') 
		 +char(13)+char(10) +
		isnull(Postcode,'') as NameAddress
from	dw.dimcustomerreference 
where	username =	@username 
or fastpaycontactref = @fastpay

---------------------------------------------
select *  
from #FireBirdResultSet
order by externaltransactionid asc

select *
from #TalkSheetResultSet
order by TransactionDateTime asc 

--using this as an example , here are some tests
INVQE-2015-07 DARBSSE2I05436800QUINN EMANUEL URQUHART  SULLIVANINWARD CCY PYMT


select * from tblcfxgljournalbatch
where description = 'INVQE-2015-07 DARBSSE2I05436800QUINN EMANUEL URQUHART  SULLIVANINWARD CCY PYMT'

select * 
from tblcfxgljournaldetail 
where journal_batch_id in(3538623,3540803,3540804)

select	*
from	tblContactAndContactRef 
where clientref = 25427

select	jb.journal_batch_id,deal_no, value_date, description, jd.currency, gl_code, jd.dr_amount, jd.cr_amount
from	dbo.tblcfxgljournalbatch jb inner join tblcfxgljournaldetail jd on jb.journal_batch_id = jd.journal_batch_id
where	deal_no in (select dealno from tbldeal d inner join contact_dbfx c on d.clientcode = c.contact_id 
where	c.contactref = 25427)
and		gl_code in ('9998', '9990')

select * from tbldeal 
where dealno in (352261,352263)

------------------------------------------------------------
create		procedure [dbo].[GetAccountSummaryCredits4] 
@username	nvarchar(1024) null,
@fastpay	varchar(50) null
as
SET NOCOUNT ON 
/*
This sp gets funds in for the client statements

Funds in from Talksheet were not consistently migrated 
to firebird currencybankccounttransaction table 

There is an issue with some credits not being brought over
from Talksheet

Approach taken is to get Firebird only stuff as one block

Then get the Talksheet stuff as one block then bring it together
using a union all, then there will be no overlap of records.
*/
IF OBJECT_ID('tempdb..#TalkSheetResultSet') IS NOT NULL
DROP TABLE #TalkSheetResultSet

declare @customerkey bigint , 
		@fastpaycontactref varchar(50),@fastpay varchar(50),
		@username	nvarchar(1024) 

		set @fastpay = '16449'

if		ISNULL(@fastpay,'') = ('')
		
		select	@customerkey = customerkey,
				@fastpaycontactref = fastpaycontactref
		from	dw.dimcustomerreference 
		where	username = @username 

else	
		select		@customerkey = customerkey ,
					@fastpaycontactref = fastpaycontactref
		from		dw.dimcustomerreference 
		where		fastpaycontactref = @fastpay


/*
first thing get data from Firebird that we know of , we will use this
to lookup against the Talksheet data set. 
*/
IF OBJECT_ID('tempdb..#FireBirdResultSet') IS NOT NULL
DROP TABLE #FireBirdResultSet

select	--aa.Id,
		convert (varchar(30), aa.TransactionDatetime,104) as TransactionDate,
		TransactionDateTime,
		--aa.id as AccountTransactionId,
		aa.Amount,
		aa.Cleared,
		aa.[Status],
		aa.ExternalTransactionId,
		aa.ExternalTransRef,
		curr.buyccycode as Currency,
		ISNULL(cast(bb.Narrative1 as varchar(255)),'') AS Narrative1
		--bb.Accounttransactionid,
		--bb.Settlementtype as RbsSettlementType 
into	#FireBirdResultSet
from	firebird.factcurrencybankaccounttransaction aa
left	outer join	settlement.rbsstatement bb 
on		aa.externaltransactionid = bb.id
and		aa.externaltransref = 'rbs'
join	dw.dimcustomerreference dw
on		dw.customerkey = aa.customerkey 
join	[FireBird].[DimCurrencyBankAccountSummary] summ
on		aa.accountsummaryid = summ.id 
join	currencybank.currency curr
on		summ.currencyid = curr.id
--left outer join tblcfxgljournalbatch jrnl
--on aa.externaltransactionid = jrnl.journal_batch_id
where	aa.customerkey = @customerkey 
and		amount > 0 and (aa.[status] in ('Currency deposited'/*,'Migrated funds in'*/) or aa.[status] like '%<HD>%') 
;
/*
Now go and pickup the Talksheet data which you will use to fill in the Talksheet 
side of things , so ignore migrated funds in as recorded by firebird because 
not everything was picked up, the below section is ripped from spCFXRptClientJournals
on dbfx caxton-db1 
*/
with	cteEntireTalksheetResultset as 
(
select	*
from	tblContactAndContactRef cc 
cross	apply
(
select	jb.journal_batch_id,deal_no, value_date, description, jd.currency, gl_code, jd.dr_amount, jd.cr_amount
from	dbo.tblcfxgljournalbatch jb inner join tblcfxgljournaldetail jd on jb.journal_batch_id = jd.journal_batch_id
where	deal_no in (select dealno from tbldeal d inner join contact_dbfx c on d.clientcode = c.contact_id 
where	c.contactref = cc.clientref)
and		gl_code in ('9998', '9990')
--and value_date >= @start_date and value_date <= @end_date

union 

select jb.journal_batch_id,deal_no, value_date, description, jd.currency, gl_code, jd.dr_amount, jd.cr_amount
from dbo.tblcfxgljournalbatch jb inner join tblcfxgljournaldetail jd on jb.journal_batch_id = jd.journal_batch_id
where IsNull(deal_no, 0) = 0
and
client_ref = cc.clientref
and gl_code = '9990'
--and value_date >= @start_date and value_date <= @end_date
and jb.journal_type = '013'

union 

select jb.journal_batch_id,deal_no, value_date, description, jd.currency, gl_code, jd.dr_amount, jd.cr_amount
from dbo.tblcfxgljournalbatch jb inner join tblcfxgljournaldetail jd on jb.journal_batch_id = jd.journal_batch_id
where IsNull(deal_no, 0) = 0
and
client_ref = cc.contact_id
and gl_code = '9990'
and jb.journal_type != '013') A
where cc.clientref = @fastpaycontactref)

select	convert (varchar(30), value_Date,104) as TransactionDate,
		value_date as TransactionDatetime,
		cr_amount as [Amount],
		1 as Cleared,
		'' as [Status],
		journal_batch_id as ExternalTransactionId,
		'FastPay' as ExternalTransRef,
		currency,
		[description] as Narrative1
into	#TalkSheetResultSet
from	cteEntireTalksheetResultset
where	description not like '%new deal%'
and		description not like '%transfer from cm%'
and		cr_amount > 0

-->then output the final result set
select row_number() over (order by TransactionDateTime asc) as RowNumber,
		a.* into deftable
from(
select	TransactionDateTime,TransactionDate,Amount,Cleared,[Status],ExternalTransactionId,ExternalTransRef,Currency,Narrative1
from	#FireBirdResultSet
union 
select	TransactionDateTime,TransactionDate,Amount,Cleared,[Status],ExternalTransactionId,ExternalTransRef,Currency,Narrative1
from	#TalkSheetResultSet) a


exec sp_help deftable



exec sp_who2 active
