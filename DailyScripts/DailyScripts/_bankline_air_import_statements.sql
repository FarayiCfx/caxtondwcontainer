select * from rbs.EOD_StagingTable

select	* from	RBS.EODFile_BalanceRecord

select	*	from	RBS.EODFile_StatementRecord
where		[Narrative Line 1] like '%gobo%'

select		*	
from		RBS.EODFile_StatementRecord
where		/*[Narrative Line 1] like '%tranref%'
or			[Narrative Line 2] like '%tranref%'
or			*/[Narrative Line 3] like '%tranref%'
or			[Narrative Line 4] like '%tranref%'


exec sp_help 'RBS.EODFile_BalanceRecord'
---------------------------------------------->
insert	into rbs.EODFile_BalanceRecord
		([Record Identifier],
		[Sort Code],
		[Account Number],
		[Account Alias],
		[Account Name],
		[Account Currency],
		[Date],
		[Last nights ledger],
		[Todays ledger],
		[Last nights cleared],
		[Todays cleared],
		[Start of day ledger],
		[Start of day cleared],
		[FileName],
		DWDateInserted)

select  dbo.UFN_SEPARATES_COLUMNS(rowdata,1,'|') AS [Record Identifier],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,2,'|') AS [Sort Code],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,3,'|') AS [Account Number],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,4,'|') AS [Account Alias],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,5,'|') AS [Account Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,6,'|') AS [Account Currency] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,7,'|') AS [Date] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,8,'|') AS [Last nights ledger] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,9,'|') AS [Todays ledger] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,10,'|') AS [Last nights cleared] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,11,'|') AS [Todays cleared] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,12,'|') AS [Start of day ledger] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,13,'|') AS [Start of day cleared],
		[Filename],
		[DWDateInserted]
 from	RBS.EOD_StagingTable
 where	left(rowdata,3) = 'BE|'

------------------------------------------------------------------------------------------------------------
exec sp_help 'RBS.EODFile_StatementRecord'

INSERT		RBS.EODFile_StatementRecord

			([Record Identifier],
			[Sort Code],
			[Account Number],
			[Account Alias],
			[Account Name],
			[Account Currency],
			[Account Type],
			[Bank Identifier Code],
			[Bank Name],
			[Bank Branch Name],
			[Transaction Date],
			[Narrative Line 1],
			[Narrative Line 2],
			[Narrative Line 3],
			[Narrative Line 4],
			[Narrative Line 5],
			[Transaction Type],
			[Debit Value],
			[Credit Value],
			[FileName],
			[DWDateInserted])

select  dbo.UFN_SEPARATES_COLUMNS(rowdata,1,'|') AS [Record Identifier],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,2,'|') AS [Sort Code],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,3,'|') AS [Account Number],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,4,'|') AS [Account Alias],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,5,'|') AS [Account Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,6,'|') AS	[Account Currency],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,7,'|') AS	[Account Type],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,8,'|') AS	[Bank Identifier Code],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,9,'|') AS	[Bank Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,10,'|') AS [Bank Branch Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,11,'|') AS [Transaction Date],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,12,'|') AS [Narrative Line 1],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,13,'|') AS [Narrative Line 2],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,14,'|') AS [Narrative Line 3],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,15,'|') AS [Narrative Line 4],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,16,'|') AS [Narrative Line 5],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,17,'|') AS [Transaction Type],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,18,'|') AS [Debit Value],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,19,'|') AS [Credit Value],
		[FileName],
		[DWDateInserted]
 from	RBS.EOD_StagingTable
 where	left(rowdata,2) = 'S|'
 -----


 select top 10 * from firebird.factcurrencybankaccounttransaction
 order by dwdateinserted desc

select * from BankLineFileImport

select * from RBS.EODFile_StatementRecord
