/****** Object:  View [dbo].[BBG_OperationsCurrentYear]    Script Date: 19/01/2018 15:35:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[BBG_OperationsCurrentYear]
as
with	cte1	as 
		(select * from  UserOfferRedemptionMetadata (nolock)
		where code = 'BUYBACK-BUY-CCY'),
		cte2 AS (
		select	* from  UserOfferRedemptionMetadata (nolock)
		where code = 'BUYBACK-BUY-VALUE'),

		cteRate AS (
		select	* from  UserOfferRedemptionMetadata (nolock)
		where code = 'BUYBACK-BUY-RATE'),

		CTE3 AS (
		SELECT	CTE1.*, 
				CAST (CTE2.VALUE AS NUMERIC(20,2)) AS MONETARYVALUE, 
				CTErate.value as ExchangeRate,
				CAST (CTE2.VALUE AS NUMERIC(20,2))/CAST (CTErate.VALUE AS float)  as GBPValue
		FROM	CTE1	
		JOIN	CTE2	
		ON		CTE1.UserOfferAssociationId = CTE2.UserOfferAssociationId
		join	cterate 
		on		cterate.UserOfferAssociationId = CTE2.UserOfferAssociationId)
		--select * from cte3 
		,
		cte4 as(
		select	cte3.* , 
				Redeemed = case  when ExecutionDate is not null then 1 else 0 end ,
				Expired = case when DateExpired is not null then 1 else 0 end, 
				uc.DateCreated
		from	cte3 
		join	UserOfferAssociation uc
		on		cte3.userofferassociationid = uc.id
		WHERE	YEAR(UC.DATECREATED) = YEAR(GETDATE()) 
		),

cteTotal	as (select 	datename(mm,datecreated) as MONTH,
			sum(monetaryvalue) as CCY,
			round(sum(GBPValue),2) as GBP,
			value as CCY_CODE
from		cte4 
group	by	datename(mm,datecreated),
			value ,
			CODE),

			---select * from ctetotal
		
cteRedeemed AS(select 	datename(mm,datecreated) as MONTH,
			sum(monetaryvalue) as CCY,
			round(sum(GBPValue),2) as GBP,
			value as CCY_CODE,
			Redeemed
from		cte4 
WHERE		Redeemed = 1 
group	by	datename(mm,datecreated),
			value ,
			CODE,
			Redeemed),

cteExpired	AS(select 	datename(mm,datecreated) as MONTH,
			sum(monetaryvalue) as CCY,
			round(sum(GBPValue),2) as GBP,
			value as CCY_CODE,
			Expired
from		cte4 
WHERE		Expired = 1 
group	by	datename(mm,datecreated),
			value ,
			CODE,
			Expired)

select		aa.[MONTH] ,
			AA.CCY_CODE,
			AA.CCY AS BBG_TOTAL,
			AA.GBP AS GBP_EQUIV_TOTAL,
			--bb.EXPIRED ,
			--isnull(bb.ccy_code,0) as ExpiredCCYCode,
			--cc.Redeemed,
			--isnull(cc.ccy_code,0) as RedeemedCCYCode ,
			isnull(CC.CCY,0) AS REDEEMED_TOTAL,
			isnull(cc.gbp,0) as GBP_EQUIV_REDEEMED,
			isnull(BB.CCY,0) AS EXPIRED_TOTAL,
			isnull(BB.GBP,0) AS GBP_EQUIV_EXPIRED
from		ctetotal aa
left		outer join cteExpired bb
on			aa.[month] = bb.[month]
and			aa.ccy_Code = bb.ccy_code		
left outer	join cteRedeemed cc
on			aa.[month] = cc.[month]
and			aa.ccy_Code = cc.ccy_code	
GO


