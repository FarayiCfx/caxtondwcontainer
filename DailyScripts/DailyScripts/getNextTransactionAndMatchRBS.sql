
drop	function if exists dw.getNextTransactionAndMatchRBS
go
create	function dw.getNextTransactionAndMatchRBS (@accounttransactionid bigint)
returns @NextTransactions table 
/*
Farayi	Nzenza

With no way of tying the RBS intra day bank statement data to 
retail trades but with the finance team requiring this to 
be done for the cash reconcliation figures this function has 
been created to try and help with that situation it looks at 
the next available transaction for that accountsummaryid after the credit 
for trade event assuming that in every case the next event will be "debit for payaway"
that this is how it will always flow through the underlying data 
*/

(
NextIdValue						bigint null
,TransactionDateTime	datetime null
,NextAmount					decimal (18,2) null
,NextAmountAbs			decimal(18,2) null
,Balance				decimal (18,2) null
,AccountSummaryId		bigint null
,[NextStatus]				varchar(255) null
,ExternalTransactionId	varchar(255) null
,ExternalTransRef		varchar(255) null
,RbsDate				varchar(255)
)
as
begin

declare		@AccountSummaryId bigint 

select		@AccountSummaryId = accountsummaryid 
from		firebird.factCurrencyBankAccountTransaction 
where		id = @accounttransactionid

insert		into @NextTransactions(NextIdValue,TransactionDateTime, NextAmount,NextAmountAbs, Balance, AccountSummaryId, NextStatus, ExternalTransactionId, ExternalTransRef, RbsDate)

select 		top 1 aa.Id		
		,aa.TransactionDateTime 
		,aa.Amount		
		,abs(Amount)
		,aa.Balance	
		,aa.AccountSummaryId
		,aa.[Status]	
		,aa.ExternalTransactionId 
		,aa.ExternalTransRef 
		,[dbo].[UFN_SEPARATES_COLUMNS](bb.[FileEntry],11,'|') as RbsDate
from	firebird.factCurrencyBankAccountTransaction aa 
left	outer join Settlement.RbsStatement bb
on		aa.externaltransactionid = bb.id
where	aa.accountsummaryid = @AccountSummaryId
and		aa.Id > @accounttransactionid
order	by aa.id asc

return ;
end;

----then create a stored procedure, that will use it

drop	procedure if exists dw.GetCRDRForPayaway
go
create	procedure dw.GetCRDRForPayaway @startdate datetime,@enddate datetime
as
/*
An attempt to try and track outgoing payments
all the way through to RBS 
*/
----select top 10 * from firebird.factcurrencybanktrade
drop table if exists #temp1
;
select	Id as TradeId
		, TradeDate
		,cast (id as varchar(255)) as TradeIdVarchar
		,UserId
		,TradeReason
into	#temp1
from	FireBird.FactCurrencyBankTrade
where	TradeDate between @startdate and @enddate
and		TradeReason <> 'card load'
;
with	cte1 as (
select	aa.*
		,bb.id
		,bb.TransactionDateTime
		,bb.Amount
		,bb.Balance
		,bb.AccountSummaryId 
		,bb.Status
		,bb.ExternalTransactionId
		,bb.ExternalTransRef
from	#temp1 aa
left	outer join firebird.factCurrencyBankAccountTransaction bb
on		aa.TradeIdVarchar = bb.ExternalTransactionId
and		bb.ExternalTransRef = 'Trade')

select	* from cte1 
cross	apply dw.getNextTransactionAndMatchRBS(id)
where	cte1.Status like 'credit for trade id :%'
-------------------------------------------------------------------------------
-->run it below 
exec	dw.GetCRDRForPayaway @startdate ='2017-11-01',@enddate ='2017-11-19'

select	*
from	firebird.factCurrencyBankAccountTransaction aa
cross	apply dw.getNextTransactionAndMatchRBS(id)
where	aa.status	like 'Credit for trade%'
and		aa.TransactionDateTime between '2017-11-01' and '2017-11-02'


select * from firebird.factCurrencyBankAccountTransaction where AccountSummaryId in (789964,789998) order by id asc

select * from [FireBird].[DimCurrencyBankAccountSummary] where id in (789964,789998) order by id asc

select * from [FireBird].[DimCurrencyBankAccountSummary] where userid = 'griesz_brisson@yahoo.com'

select	top 100 *
from	firebird.factCurrencyBankAccountTransaction where  ExternalTransRef = 'rbs' and amount <0
order by id desc



select top 10 * from [FireBird].[FactPaymentTransfer]
