select * from [BankLineFileImport_Dec2016]
go

truncate table [dbo].[BankReconciliationResult]
go
-->now put all unallocated rows into one place 
IF		OBJECT_ID('TEMPDB..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
select	IDENTITY (int, 1,1) Rownum, 
		--[dbo].[GetNumbers2]([Narrative #2])as fastpaycontactref,
		fastpaycontactref = case when len ([dbo].[GetNumbers2]([Narrative #2])) < 2 then [dbo].[GetNumbers2]([Narrative #1]) 
		else [dbo].[GetNumbers2]([Narrative #2]) end,

		cast(CONVERT(varchar(20),[date],112) as INT) as DateKey,
		* 
		into #temp1
from	[dbo].[BankLineFileImport_Dec2016]
where	allocated = ''
AND		type not in ('D/D','CHG','SCR','BLN')
order	by	[Narrative #1]
go
-->3578 rows
---select * from #temp1 where [Narrative #1] = 'BERKHAMSTED SCHOOL'
--GO
-->now extract the things you can match on fastpaycontactref, phonenumber, and firebirdcustomerrefnumber 

with	cteMultipleMatch as (
-->MATCHING ON FASTPAYCONTACTREF
select	'FastPayContactRef - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#temp1.* 
from	#temp1 
join	 dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.fastpaycontactref
join	firebird.factcurrencybanktrade tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradedatekey 
and		#TEMP1.amount = tr.sellamount 
and		tr.isunwound = 0 
and		tr.isreversed = 0
and		#temp1.fastpaycontactref <> '' --order by rownum
union	
-->NOW ATTEMPT MATCH ON PHONE NUMBER
select	'PhoneNumber - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#temp1.* 
from	#temp1 
JOIN	dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.PRIMARYTELEPHONE
join	firebird.factcurrencybanktrade tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradedatekey 
and		#temp1.amount = tr.sellamount
and		tr.isunwound = 0 
and		tr.isreversed = 0
WHERE	LEN(#temp1.fastpaycontactref) > 5 
union
-->NOW ATTEMPT MATCH ON REFERENCE NUMBER
select	'FireBirdCustomerRefNo - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#temp1.* 
from	#temp1 
JOIN	dw.dimcustomerreference dc
on		#temp1.fastpaycontactref = dc.customerrefno
join	firebird.factcurrencybanktrade tr
on		dc.customerkey = tr.customerkey
and		#temp1.datekey = tr.tradedatekey 
and		#temp1.amount = tr.sellamount
and		tr.isunwound = 0 
and		tr.isreversed = 0
WHERE	LEN(#temp1.fastpaycontactref) > 5 ) 

insert into dbo.[BankReconciliationResult]([MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select		[MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from		cteMultipleMatch
go


--> now show what is left over (unallocated)
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go

select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

--->now match with trade end date 

with	cteMultipleMatchTradeEndDate as (
-->MATCHING ON FASTPAYCONTACTREF
select	'FastPayContactRef & trade end date - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.fastpaycontactref
join	FactCurrencyBankTradeEndDateKey tr
on		dc.customerkey = tr.customerkey
and		#NextUnAllocatedBlock.datekey = tr.tradeenddatekey 
and		#NextUnAllocatedBlock.amount = tr.sellamount 
and		#NextUnAllocatedBlock.fastpaycontactref <> ''-- order by tradeid
union	
-->NOW ATTEMPT MATCH ON PHONE NUMBER
select	'PhoneNumber & trade end date - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
JOIN	dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.PRIMARYTELEPHONE
join	FactCurrencyBankTradeEndDateKey tr
on		dc.customerkey = tr.customerkey
and		#NextUnAllocatedBlock.datekey = tr.tradeenddatekey 
and		#NextUnAllocatedBlock.amount = tr.sellamount
WHERE	LEN(#NextUnAllocatedBlock.fastpaycontactref) > 5 
union
-->NOW ATTEMPT MATCH ON REFERENCE NUMBER
select	'FireBirdCustomerRefNo & trade end date - Trades' as MatchType,
		tr.id as TradeId,tr.userid,dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
JOIN	dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.customerrefno
join	FactCurrencyBankTradeEndDateKey tr
on		dc.customerkey = tr.customerkey
and		#NextUnAllocatedBlock.datekey = tr.tradeenddatekey 
and		#NextUnAllocatedBlock.amount = tr.sellamount
WHERE	LEN(#NextUnAllocatedBlock.fastpaycontactref) > 5 ) 

insert into dbo.[BankReconciliationResult]([MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select		[MatchType],[TradeId],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from		cteMultipleMatchTradeEndDate
go

--create the next unallocatedblock of rows 
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go


--select * from #NextUnAllocatedBlock order by 1 

-----------------------------------end of above section 

with		cteNarrative1and2 as(
-->join to rbs settlement table on narrative1 
SELECT		'Narrative 1 to settlements table and credit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #1] = bb.Narrative1
and			aa.credit = bb.creditvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #1] like 'RBS%'
union		
SELECT		'Narrative 1 to settlements table and debit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #1] = bb.Narrative1
and			aa.debit = bb.debitvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #1] like 'RBS%'
union
-->now join the rbs statement table to Narrative2 
SELECT		'Narrative 2  to settlements and credit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #2] = bb.Narrative2
and			aa.credit = bb.creditvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				left outer JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #2] like 'RBS%'
union 
SELECT		'Narrative 2  to settlements and debit value' as MatchType,
			bb.voided,bb.id,bb.accounttransactionid,dw.username,
			aa.*
FROM		#NextUnAllocatedBlock aa
join		settlement.rbsstatement bb
on			aa.[Narrative #2] = bb.Narrative2
and			aa.debit = bb.debitvalue
left		outer join Firebird.factcurrencybankaccounttransaction cc (nolock)
			on bb.accounttransactionid = cc.id
				left outer JOIN dw.dimcustomerreference dw
					on cc.customerkey = dw.customerkey 
where		[Narrative #2] like 'RBS%'
)
insert	into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],
											[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
SELECT  [Matchtype],voided, null, username,null,null,null,null,
		[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],
		[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],
		[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],
		[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],
		[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
FROM	cteNarrative1and2
go

--->next lot of unallocated 
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

--select * from #NextUnAllocatedBlock
------->Narrative 1 company name 
with	cteCompanyName as(
select	bb.email,aa.companyname,
		dw.username, 
		dw.fastpaycontactref,
		tr.*
from	dw.dimcorporateentities aa (nolock)
join	[dw].[DimUserCorporateAssociation] bb (nolock)
on		aa.id = bb.corporateentityid
join	dw.dimcustomerreference dw (nolock)
on		bb.email = dw.username
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		tr.externaltransactionid <> 0)

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])

select	'Narrative 1 & company name - currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		null,--firstname,
		null,--lastname
		--aa.amount,
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #1] = bb.companyname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go


------->Narrative 2 and company name 
with	cteCompanyName as(
select	bb.email,aa.companyname,
		dw.username, 
		dw.fastpaycontactref,
		tr.*
from	dw.dimcorporateentities aa (nolock)
join	[dw].[DimUserCorporateAssociation] bb (nolock)
on		aa.id = bb.corporateentityid
join	dw.dimcustomerreference dw (nolock)
on		bb.email = dw.username
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		tr.externaltransactionid <> 0)

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
-------------------------------------->
select	'Narrative 2 & company name- currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		null,--firstname,
		null,--lastname
		--aa.amount,
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #2] = bb.companyname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go

-->next unallocated block 

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

-------------------------full name narrative 1 -----------
with	cteCompanyName as(
select	dw.username,
		dw.Firstname+' '+LastName as Fullname  ,
		dw.fastpaycontactref,
		tr.*
from	dw.dimcustomerreference dw (nolock)
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs'
and		externaltransactionid <> 0 )

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])

-------------------------------------->
select	'Narrative 1 & full name - currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		null,--firstname,
		null,--lastname
		--aa.amount,
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #1] = bb.fullname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go

-->next unallocated block 

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go


---next up full name and narrative 2 
with	cteCompanyName as(
select		dw.username,
		dw.Firstname+' '+LastName as Fullname  ,
		dw.fastpaycontactref,
		tr.*
from	dw.dimcustomerreference dw (nolock)
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs')

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])

-------------------------------------->
select	'Narrative 2 & full name- currencybankaccounttransaction',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		null,--firstname,
		null,--lastname
		--aa.amount,
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #2] = bb.fullname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go

-------------------------------------------------------------->
IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

---next up full name and narrative 2 
with	cteCompanyName as(
select		dw.username,
		dw.Firstname+' '+LastName as Fullname  ,
		dw.fastpaycontactref,
		tr.*
from	dw.dimcustomerreference dw (nolock)
join	[FireBird].[FactCurrencyBankAccountTransaction] tr (nolock)
on		dw.customerkey = tr.customerkey
where	tr.datekey>20161201
and		tr.externaltransref = 'rbs')

insert into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])

-------------------------------------->
select	'Narrative 3 & full name',
		null,--voided
		null,--tradeid
		id , -- AccountTransactionId_FireBird
		username,
		null,--primarytelephone
		null,--customerrefno
		null,--firstname,
		null,--lastname
		--aa.amount,
		aa.*
from	#NextUnAllocatedBlock aa join cteCompanyName bb
on		aa.[Narrative #3] = bb.fullname
and		aa.datekey = bb.datekey
and		aa.amount = bb.amount
order	by rownum
go
----------------------------------------------------------------------------------------------------------------

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
-----------------------------------remove irreconcilable items  

insert	into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select  aa.[Narrative #5]+' - cannot be reconciled internally',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,aa.*
from	#NextUnAllocatedBlock aa
where	[Narrative #5]  = 'CCY A/C TFR'
or		[Narrative #5] = 'INTER A/C TFR'

insert	into dbo.[BankReconciliationResult]([MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select  aa.[Narrative #4]+' - cannot be reconciled internally',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,aa.*
from	#NextUnAllocatedBlock aa
where	[Narrative #4]  = 'CCY A/C TFR'
or		[Narrative #4] = 'INTER A/C TFR'
--do the next unallocated block

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go


----------------------------->CURRENCBANKACCOUNTSUMMARY
with	cteMultipleMatch as (
-->MATCHING ON FASTPAYCONTACTREF
select	'FastPayContactRef - CurrencyBankAccountTransaction' as MatchType,
		dc.USERNAME AS UserName,cb.id as AccounTransactionid,
		dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.fastpaycontactref
join	firebird.[FactCurrencyBankAccountTransaction] cb
on		dc.customerkey = cb.customerkey
and		#NextUnAllocatedBlock.datekey = cb.datekey 
and		#NextUnAllocatedBlock.amount = cb.amount 
AND		 CB.EXTERNALTRANSACTIONID <> 0 --just added this for dupe removal
and		#NextUnAllocatedBlock.fastpaycontactref <> '' 

and		cb.externaltransref = 'rbs'
--order by rownum
union	
-->NOW ATTEMPT MATCH ON PHONE NUMBER

select	'PhoneNumber - CurrencyBankAccountTransaction' as MatchType,
		dc.USERNAME AS UserName,cb.id as AccounTransactionid,
		dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.fastpaycontactref
join	firebird.[FactCurrencyBankAccountTransaction] cb
on		dc.customerkey = cb.customerkey
and		#NextUnAllocatedBlock.datekey = cb.datekey 
and		#NextUnAllocatedBlock.amount = cb.amount 
WHERE	LEN(#NextUnAllocatedBlock.fastpaycontactref) > 5 
--order by rownum

union
-->NOW ATTEMPT MATCH ON REFERENCE NUMBER
select	'FireBirdCustomerRefNo - CurrencyBankAccountTransaction' as MatchType,
		dc.USERNAME AS UserName,cb.id as AccounTransactionid,
		dc.primarytelephone,dc.customerrefno,dc.firstname,dc.lastname,
		#NextUnAllocatedBlock.* 
from	#NextUnAllocatedBlock 
join	 dw.dimcustomerreference dc
on		#NextUnAllocatedBlock.fastpaycontactref = dc.customerrefno
join	firebird.[FactCurrencyBankAccountTransaction] cb
on		dc.customerkey = cb.customerkey
and		#NextUnAllocatedBlock.datekey = cb.datekey 
and		#NextUnAllocatedBlock.amount = cb.amount 
WHERE	LEN(#NextUnAllocatedBlock.fastpaycontactref) > 5 

) 

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select		[MatchType],[username],AccounTransactionid,[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from		cteMultipleMatch
go

-----next unallocated block 

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go
----------------->

with cteNarrative1To2 as (
SELECT	'Narrative 1 to Narrative 2 matchup' as DerivedMatchtype, 
		--aa.[Narrative #1] ,
		rbs.narrative1 as narrative1Rbs,
		--aa.[Narrative #2],
		rbs.narrative2 as narrative2Rbs,
		--aa.[Narrative #3],
		rbs.narrative3 as Narrative3Rbs,
		bb.[amount] as AmountAcTransaction,
		bb.status,
		bb.externaltransactionid,
		bb.externaltransref,
		bb.id as AccountTransactionid,
		aa.*,
		--dw.fastpaycontactref,
		dw.firstname,
		dw.lastname,
		dw.username as username_custref,
		dw.primarytelephone,
		dc.[companyname],
		dw.customerrefno
FROM	#NextUnAllocatedBlock aa
join	firebird.factcurrencybankaccounttransaction bb 
on		aa.datekey  = bb.datekey 
and		aa.amount = bb.amount 
and		bb.externaltransref = 'RBS'
join	dw.dimcustomerreference dw
on		bb.customerkey = dw.customerkey 
left	outer join [dw].[DimUserCorporateAssociation] da
on		dw.username = da.email 
left	outer join dw.dimcorporateentities dc
on		da.corporateentityid = dc.id
join	settlement.rbsstatement rbs
on		bb.externaltransactionid = rbs.id
WHERE	[NARRATIVE #3] LIKE 'FP%'
AND		DEBIT IS NULL
AND		EXTERNALTRANSACTIONID <> 0 )

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select		[DerivedMatchType],username_custref,AccountTransactionid,[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from		cteNarrative1To2 
where		narrative1rbs = [Narrative #2]
order by	rownum
go
--next unallocated lot 

IF		OBJECT_ID('TEMPDB..#NextUnAllocatedBlock') IS NOT NULL
DROP	TABLE #NextUnAllocatedBlock
go
select	aa.* 
into	#NextUnAllocatedBlock
from	#temp1 aa
left	outer join	[dbo].[BankReconciliationResult] bb 
on		aa.Rownum = bb.Rownum
where	bb.rownum is null
go

-->analyse the next lot 

select	* 
from	#NextUnAllocatedBlock
where	debit is null

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select		'Not auto reconcile please search manually' ,NULL,NULL,NULL,NULL,NULL,NULL,[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from		#NextUnAllocatedBlock 
where		debit is null

insert into dbo.[BankReconciliationResult]([MatchType],[userid],[AccountTransactionId_FireBird],[primarytelephone],[customerrefno],[firstname],[lastname],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2])
select		'Outgoing funds from bank to outside 3rd party - not reconciled ' ,NULL,NULL,NULL,NULL,NULL,NULL,[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from		#NextUnAllocatedBlock 
where		debit is NOT null

select	[MatchType],[Voided],[TradeId],[AccountTransactionId_FireBird],[userid],[Rownum],[fastpaycontactref],[DateKey],[Sort Code],[Account Number],[Account Alias],[Account Short Name],[Currency],[Account Type],[BIC],[Bank Name],[Branch Name],[Date],[Narrative #1],[Narrative #2],[Narrative #3],[Narrative #4],[Narrative #5],[Type],[Debit],[Credit],[Amount],[Allocated],[CtpyTrade],[VALITOR],[CREDORAX],[Manual Payment],[RBSmargin],[Client Money],[Contra],[Client 1],[Client 2]
from	[BankReconciliationResult]

---------------------------------------------------------------------------------------------------------------------------------
select top 10 * from firebird.factcurrencybankaccounttransaction

select top 20 * from settlement.rbsstatement 

sele