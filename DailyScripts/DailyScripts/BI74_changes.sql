select	top 10 * from FireBird.FactPaymentTransfer order by id desc 

set		statistics io on

select * from dw.IpCountNewOldCustomers
order	 by transfercreateddate



alter view dw.IpCountNewOldCustomers
as
with cte1 as 
(select	cast (aa.CreatedDate as date) as TransferCreatedDate,
		aa.CreatedDate as TransferCreatedDateTime, 
		bb.SourceCreationDate as AccountCreationDate,
		datediff(mm,aa.CreatedDate,bb.SourceCreationDate)  as datediff_in_months,
		abs(datediff(mm,aa.CreatedDate,bb.SourceCreationDate))  as abs_datediff,
		IsExistingCustomer = case when abs(datediff(mm,aa.CreatedDate,bb.SourceCreationDate)) > 1 then 'Existing customer' else 'New customer' END,
		IsIpAtSignUp = case ISNULL(BB.WantToSendMoney,0) 
		when 1 then 'Ip Customer' 
		when 0 then 'Card Customer' 
		end,
		BB.WantMultiCurrencyCard,
		ISNULL(BB.WantToSendMoney,0) AS WantToSendMoney,
		BB.CustomerKey ,
		[dw].[UdfGetGBPValue](aa.BuyAmount,aa.BuyCcyCode) as GBPBUYAmount,
		aa.BuyAmount,
		aa.BuyCcyCode,
		aa.CreatedDateKey,
		aa.id
		--,aa.*
from	FireBird.FactPaymentTransfer  aa (nolock)
join	dw.DimCustomerReference bb (nolock)
on		aa.CustomerKey = bb.customerkey 
where	aa.CreatedDateKey > 20151231)
select	sum(GBPBUYAmount) as SumGBPBuyAmount,
		count(id) as IPPaymentCount,
		count(distinct(customerkey)) as UniqueCustomers,
		TransferCreatedDate,
		IsExistingCustomer,
		IsIpAtSignUp
		from cte1 
		group by 
		TransferCreatedDate,
		IsExistingCustomer,
		IsIpAtSignUp
		order by transfercreateddate



order	by aa.id desc
go


select * from dw.DimCustomerReference where customerkey =  132274

select * from dw.FactApplicants where customerkey = 132274

select * from dw.dimcustomerreference where contactemailaddress = 'nicholas.pearson@npaconsult.co.uk'

select * from dw.factapplicants where email = 'nicholas.pearson@npaconsult.co.uk'



SELECT	COUNT(CUSTOMERKEY) AS COUNT_ , 
		CUSTOMERKEY
FROM	DW.FactApplicants 
GROUP	BY CustomerKEY
ORDER	BY 1 DESC 

select * from dw.FactApplicants where customerkey = 506707


select max(valitoraccountid) from DW.DimCustomerReference (nolock)

--393823

select * from dw.DimXERates
