select * from [Firebird_To_Sage_Matches]
where fb_companyname like '%midwinter%'

--LOOK AT firebird TO SAGE DISTINCT MATCHES 
with	cte1 as (select distinct fb_companyname ,[account manager],comp_sales_person 
				from [Firebird_To_Sage_Matches])
SELECT	id,companyname,registrationnumber,
		salescontactref,accountcontactref,
		bb.*,fb_datecreated,fb_Dateupdated
FROM	[dw].[DimCorporateEntities]  aa (nolock)
left	outer join cte1 bb
on		aa.companyname = bb.fb_companyname
order	by fb_datecreated desc 

-----------------------------------------------------SEARCH BY GIVEN USER EMAIL ADDRESS----------------------------------------------------------------------------
declare @emailaddress varchar(255)

select	@emailaddress = 'roth.jim@gmail.com'

--does this email have a user corporate association first?
select  * from dw.dimusercorporateassociation (nolock) where email = @emailaddress
--does the user go over to match 
select  * from [dbo].[Firebird_To_Sage_Matches] where fb_username = @emailaddress 

--Do they have trades?
select * from firebird.factcurrencybanktrade (nolock) where userid = @emailaddress

select * from NonRetailTradesSpotFwdFlex (nolock)where userid = @emailaddress

-------------------------------------------------------search by approximate company name in firebird and sage

select aa.id,companyname,registrationnumber,salescontactref,accountcontactref,fb_datecreated,bb.email,bb.dateadded,aa.dwdateinserted,bb.dwdateinserted
FROM	[dw].[DimCorporateEntities]  aa (nolock)
left	outer join dw.dimusercorporateassociation bb(nolock)
on		aa.id=bb.corporateentityid
where   companyname like '%midwinter%' ----->put company name here 
order	by fb_datecreated desc 

select * from   [dbfx].[ClientListCRM] where comp_name like '%taylor67%' ----->SAGE put company name here 


