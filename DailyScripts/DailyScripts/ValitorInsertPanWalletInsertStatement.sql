/*
After you have pulled the data from the warehouse 
to live and joined it up to get the full names and emails
insert data as follows:

1 - Make sure that the wallet id row exists in the valitor.wallet
table 


3 - Set isarchved to 0 and isdirty to 0 for all records being inserted 

4 - No Values suppllied for pan status (from the valitor data dump) so insert pan status to NULL


6 - All card sequence numbers are 000, 000 appears like a default value to me?

7 - DateCreated uses getdate() , as in today 

*/


--backup both tables to begin with 

select * into valitor.pan_backup_pre_insert from valitor.pan
go

select * into valitor.wall_backup_Pre_insert from valitor.wallet 
go

/*
first thing to do is to work out which wallet id and username combinations do not have an entry 
in the wallet table
*/
with	cte1 as (
select	aa.*,
		bb.[id] AS IdColumnInWalletTable
from	[2017ExpiryOutPut] aa
left	outer join	Valitor.Wallet_test bb
on		aa.valitorwalletid = bb.valitorwalletid
and		aa.accountid = bb.accountid)
select	* from cte1 where IdColumnInWalletTable is null

/*
now insert the wallet record first
*/

with	cte1 as (
select	aa.*,
		bb.[id] AS IdColumnInWalletTable
from	[2017ExpiryOutPut] aa
left	outer join	Valitor.Wallet_test bb
on		aa.valitorwalletid = bb.valitorwalletid
and		aa.accountid = bb.accountid)

insert	into valitor.Wallet_test(ValitorWalletId,AccountId,IsArchived)

select	distinct ValitorWalletId,accountid,0
from	cte1 
where	IdColumnInWalletTable is null
-->



-->then use that result set as a part of the overall insert 

IF OBJECT_ID('tempdb..#TempInsertFrom') IS NOT NULL
DROP TABLE #TempInsertFrom
go

with	cte1 as 
(
select	wl.Id as IdColumnInWalletTable,
		aa.*
from	[2017ExpiryOutPut] aa
join	valitor.wallet_test wl
on		aa.[Valitorwalletid] = wl.valitorwalletid
AND		aa.accountid = wl.accountid
)

/*
insert into valitor.pan_test(ValitorPanId,
						MaskedPan,
						Expiry,
						WalletId,
						PanStatusCode,
						CardStatusCode,
						DateCreated,
						IsDirty,
						CardHolderName,
						CardHolderEmail,
						IsArchived,
						CardSequenceNumber)
						*/


select	distinct panid,
		Kortn�mer_6_4,
		substring([expiry date],4,2)+ substring([expiry date],1,2) as expiry,
		IdColumnInWalletTable,
		NULL as PanStatusCode,
		'V' as CardStatusCode,
		getdate() as DateCreated,
		0 as IsDirty,
		FULLNAME,
		[AccountId],
		0 as IsArchived,
		[sequence number]
into	#TempInsertFrom
from	cte1 


with	cte1 as(
select	aa.*, 
		bb.id as RowIdOfExisting
from	#TempInsertFrom aa
left	outer join	valitor.pan_test bb
on		aa.panid = bb.valitorpanid
and		aa.idcolumninwallettable = bb.walletid)
select * from cte1 where RowIdOfExisting is null



select top 10 * from #TempInsertFrom
select top 10 * from valitor.pan_test




exec sp_help 'valitor.pan_test'


------visually inspect all records with the insertion of today 

select	aa.*, bb.*
from	valitor.pan_test aa
join	valitor.wallet_test bb 
on		aa.walletid = bb.id
where	aa.datecreated = '2017-03-10 15:59:50.277'


select cast(getdate() as date)

select * from valitor.pan_test where expiry = '2003'
order by datecreated desc 
