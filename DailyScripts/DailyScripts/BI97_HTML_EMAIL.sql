DECLARE @subjecttext nvarchar(max),
		@HeadAndTitle nvarchar(max),
		@tableHTML  NVARCHAR(MAX) 
		
select @subjecttext ='Daily Rev report for '
		+datename(dw,report_date) + ' '
		+datename(day,report_Date) + ' ' 
		+datename(month,report_Date) + ' ' 
		+datename(YEAR,report_Date)  	
		from	ssisconfigurationdb.support.DailyRevenueEmail
where	ROWNUMBER = 1	;  
		
  
select	@HeadAndTitle = '
<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 0.5px solid black;
    border-collapse: collapse;
	text-align: center;
	padding: 5px;
}
</style>
</head>
<body>
<H1 style="font-size:90%;"> Daily Revenue Report for '
		+datename(dw,report_date) + ' '
		+datename(day,report_Date) + ' ' 
		+datename(month,report_Date) + ' ' 
		+datename(YEAR,report_Date) + 
		+ '<br>'
		+'covering the period '
		+cast(cast (start_period as smalldatetime) as NVARCHAR(MAX) )+' '
		+'to '+
		cast(cast (end_period as smalldatetime) as NVARCHAR(MAX) )+
		'</H1>' 
from	ssisconfigurationdb.support.DailyRevenueEmail
where ROWNUMBER = 1
;

---print @title

SET @tableHTML =  
 
	@HeadAndTitle +  
    N'<table border="2">' +  
    N'<tr><th">Deals</th><th>Revenue</th>' +  
    N'<th>Entity</th> <th>[Corporate FX monthly total budget]</th><th>[Corporate FX daily total budget]</th> <th>[Retail FX monthly total budget]</th> <th>[Retail FX daily total budget]</th> </tr>' +  
    CAST ( ( select	SUM(count_)  as td,
		'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td, 
		'',
		ENTITY as td,
		'',
		min([Corporate FX monthly total budget]) as td,
		'',
		min([Corporate FX daily total budget]) as td,
		'',
		min([Retail FX monthly total budget]) as td,
		'',
		min([Retail FX daily total budget]) as td,
		''
FROM	ssisconfigurationdb.support.DailyRevenueEmail
GROUP	BY ENTITY
for		xml path('tr'),type
    ) AS NVARCHAR(MAX) ) 
	+ N'</table>
	<br>
	<p style="color:red;">Breakdown as follows:</p>
	<br>'
	+
	N'<table border = "2";><caption style="color:Green">Talksheet:</caption>' +  
    N'<tr><th>Deals</th><th>Revenue</th><th>AccountType</th>' +  
    N'<th>Entity</th></tr>' +  
    CAST ( ( 
		select	sum(count_) as td,'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td,'',
		isnull(AccountType,'') as td,'',
		Entity as td
from	ssisconfigurationdb.support.DailyRevenueEmail
where	sourcesystem = 'talksheet'
group by AccountType,
		entity 
for xml path('tr'),type 
    ) AS NVARCHAR(MAX) ) 
	+  N'</table>' 
	+'<br>'
	+
	N'<table border = "2"><caption style="color:Green">Firebird:</caption>' +  
    N'<tr><th>Deals</th><th>Revenue</th><th>TradeReason</th>' +  
    N'<th>Entity</th></tr>' +  
    CAST ( ( 
		select	sum(count_) as td,'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td,'',
		isnull(TradeReason,'') as td,'',
		Entity as td
from	ssisconfigurationdb.support.DailyRevenueEmail
where	sourcesystem = 'firebird'
group by TradeReason,
		entity 
for xml path('tr'),type 
    ) AS NVARCHAR(MAX) ) 
	+  N'</table>
	<br>
	<img src="https://az695721.vo.msecnd.net/images/consumer/caxtonfx-logo.min.png" alt="caxtonfx">
	' 
	;  
  
EXEC msdb.dbo.sp_send_dbmail 
	@recipients='farayi.nzenza@caxtonfx.com;
				carolyn.ahara@caxtonfx.com;
				edward.gott@caxtonfx.com;
				errol.izzet@caxtonfx.com;
				Matthew.webb@caxtonfx.com',  
    @subject = @subjecttext /*'Daily Revenue report'*/,  
    @body = @tableHTML,  
    @body_format = 'HTML' ;  

