select top 10 * from  [dbo].[NonRetailTradesSpotFwdFlex_FB] where FB_Companyname  is null ORDER BY TRADEID DESC

select top 10 * from  [dbo].[MigratedCorpsTradeView] where Companyname is null order by tradeid desc

----------------------------------------------------------------------------------------------------------------------------------------
WITH	CTE1 AS (
SELECT	AA.* ,bb.firstname,bb.lastname
FROM	[NonRetailTradesSpotFwdFlex_fb] AA
JOIN	dw.dimcustomerreference bb
on		aa.customerkey = bb.customerkey
WHERE	TRADEDATE > '2017-06-30' 
AND		FB_COMPANYNAME IS NULL)
SELECT	DISTINCT USERID,firstname,lastname ,fb_Salescontactref,fb_accountcontactref
into	#temp1
FROM	CTE1

select	aa.* 
into	_AccountManager_Updates 
from	FilledIn_JulyTradesWithNoCompany aa
join	#temp1 bb on aa.userid = bb.userid 
order	by 1 

select * from _AccountManager_Updates

Update	_AccountManager_Updates 
set		[Account Manager] = 'Jack Lane-Matthews'
where	[Account Manager]  = 'JLM'

Update	_AccountManager_Updates 
set		[Account Manager] = 'Anthony Creese'
where	[Account Manager]  = 'AC'

Update	_AccountManager_Updates 
set		[Account Manager] = 'Adam Stevens'
where	[Account Manager] =  'AS'

Update	_AccountManager_Updates 
set		[Account Manager] = 'Matthew Downward'
where	[Account Manager] =  'MD'

Update	_AccountManager_Updates 
set		[Account Manager] = 'James Tinsley'
where	[Account Manager] =  'JT'

