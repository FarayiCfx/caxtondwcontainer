alter procedure [dbo].[GetDebtorsAndCreditors_Variable] @startdate datetime, @enddate datetime 
as 
/*
Farayi Nzenza 

This will report on trades and money owed on the trades 

This has to be wrapped in a stored procedure 
because of the need to use temp tables, index 
them on the fly and do lookups to trade detail 
table

The need to work out when money comes in per trade

Slight change from the original version in that 
we want the money coming in over all time, not just 
the cutoff at monthend, but also display and pivot all 
payments after the month end cutoff 

*/
set		nocount on

IF		OBJECT_ID('tempdb..#MoneyPaidOut') IS NOT NULL
DROP	TABLE #MoneyPaidOut 

declare @startdateinteger int
		,@enddateinteger int 
		,@startofmonthinteger int
		,@endofperiodinteger int 

select	@startdateinteger = cast(convert(varchar(12),@startdate,112) as int)
select	@enddateinteger =  cast(convert(varchar(12),@enddate,112) as int)
select	@startofmonthinteger = cast(convert(varchar(12), DATEADD(month, DATEDIFF(month, 0, @startdate), 0),112) as int)
select	@endofperiodinteger = cast(convert(varchar(12),EOMONTH (@startdate,0),112) as int)
	
--debug section 
/*
print	@startdateinteger 

print	@enddateinteger 

print	@startofmonthinteger

print	@endofperiodinteger 
*/
select	sum(amount) as AmountPaidOut
		,externaltransactionid
		,cast(transactiondatetime as date) as DatePaidOut
into	#MoneyPaidOut
from	[firebird].factcurrencybankaccounttransaction (nolock)
where		externaltransref = 'trade'
and			amount > 0
and			datekey between @startdateInteger  and @enddateinteger
and			([status] like 'credit for buy%' or [status] like 'credit for trade%')
group	by	externaltransactionid
			,cast(transactiondatetime as date) 

select		aa.id as TradeId,
			aa.buyccycode
			,aa.BuyAmount
			,AA.SellCcyCode
			,aa.SellAmount
			--,aa.Settled
			,cast(aa.TradeDate as Date) as TradeDate
			,cast(aa.TradeEnddate as Date) as [SettlementDate]
			--,isnull(aa.HelpdeskUserId,'') as HelpdeskUserId
			,aa.UserId
			,BB.tradeid_summarytable
			,isnull(bb.AmountPaidInWithinPeriod,0) as AmountPaidInWithinPeriod
			,isnull(aa.TradeReason,'') as TradeReason
			,aa.TradeProfit
			,isnull(buyamount,0) - isnull(AmountPaidOut,0) as payable
			,isnull(sellamount,0) - isnull(AmountPaidInWithinPeriod,0) as receivable
			,isnull(AmountPaidInWithinPeriod,0) as AmountPaidIn
			--,cc.AmountPaidOut
			,cc.*
			, isnull(dd.AmountPaidInAfterPeriod,0) as AmountPaidInAfterPeriod
			, isnull(dd.CountPaymentsInAfterPeriod,0) as CountPaymentsInAfterPeriod
			, DatePayInAfterPeriod
			
from	firebird.[factCurrencyBankTrade] AS aa (nolock)
OUTER	APPLY	(select tradeid as tradeid_summarytable , 
						sum(amount) as AmountPaidInWithinPeriod,
						count(bb.id) as CountPaymentsInWithinPeriod
				from	firebird.[factCurrencyBankTradeDetail] bb (nolock)
				where	bb.tradeid = aa.id
				and		bb.paiddatekey between @startdateinteger and @endofperiodinteger
				GROUP	BY BB.tradeid) 
				BB

----next outer apply is for stuff after the calendar month 				
OUTER APPLY (
				select tradeid as tradeid_summarytable_after_period , 
						sum(amount) as AmountPaidInAfterPeriod,
						count(bb.id) as CountPaymentsInAfterPeriod,
						max(cast (paiddate as date)) as DatePayInAfterPeriod
				from	firebird.[factCurrencyBankTradeDetail] bb (nolock)
				where	bb.tradeid = aa.id
				and		bb.paiddatekey > @endofperiodinteger
				GROUP	BY BB.tradeid) 
				dd
left		outer join #MoneyPaidOut  cc
on			aa.id = cc.externaltransactionid
where 		aa.isunwound = 0
			and		aa.isreversed = 0
			and		aa.reversal = 0 
			and		aa.tradedatekey between @startdateinteger and @enddateinteger
			and		aa.tradereason <> 'card load'
			and		aa.userid <> 'val_monitor'
GO

exec	[dbo].[GetDebtorsAndCreditors_Variable] @startdate = '2017-12-01', @enddate = '2017-12-31' 

exec	[dbo].[GetDebtorsAndCreditors_Variable] @startdate = '2017-11-01', @enddate = '2017-11-30'
 
select	* 
from	firebird.[factCurrencyBankTradeDetail]
where	TradeId in (2228714)

declare @val varchar(1000) 

select  @val =  coalesce(@val,'') + cast(paiddate as varchar(50)) + ' ; '  
from	firebird.[factcurrencybanktradedetail]
where	tradeid in (2228714)

select	@val

----------------------------------------------------------------------------------------------------------
create	function dbo.PivotTextWithCommaSeparator (@inputcolumn varchar(255))
returns nvarchar(255)
as 
begin
declare @outputcolumn nvarchar(255)

select  @outputcolumn =  coalesce(@inputcolumn,'') + @inputcolumn + ' ; '  

return	@outputcolumn 
end

exec sp_who2 active