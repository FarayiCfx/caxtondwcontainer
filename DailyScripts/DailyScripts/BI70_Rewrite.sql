
alter	view [dw].[BI-70_MC_retail_no_first_load]
as
with	cte1 as (
select	CustomerKey,
		count(id) as count_of_load,
		sum(abs(Amount)) as LoadTotal
from	FireBird.[FactCurrencyBankAccountTransaction] (nolock)
where	TransactionDateTime > cast(dateadd(yyyy,-1,getdate()) as date) 
group	by CustomerKey
),
cte2	as (
select	aa.CustomerKey,
		aa.FirstName, 
		aa.LastName,
		isnull (aa.ContactEmailAddress,aa.UserName) as ContactEmailAddress,
		aa.SourceCreationDate as AccountCreationDate,
		[dw].[UdfGetCardClaimValue](aa.customerkey) as CardClaimValue
from	dw.DimCustomerReference aa (NOLOCK)
where aa.SourceCreationDate > cast(dateadd(yyyy,-1,getdate()) as date))

select	cte2.*,
		isnull(cte1.count_of_load,0) as count_of_load,
		isnull(cte1.LoadTotal,0) as LoadTotal
from	cte2 
left	outer join	cte1
on		cte1.customerkey = cte2.customerkey
where	(cte2.CardClaimValue = 'Retail:PrimaryAccount:PortalCards'
and		AccountCreationDate < DATEADD(d, -0, DATEDIFF(d, 0, GETDATE())))
and		cte1.customerkey is NULL
GO

select	* 
from	[dw].[BI-70_MC_retail_no_first_load] 
order	by AccountCreationDate ASC

/*
with cte1 as (
select  aa.CustomerKey,
		VALITOR_MASTERCARD_CardLoadCount, 
        aa.AccountTypeRef, 
		bb.SourceCreationDate, 
        bb.FirstName, 
		bb.LastName, 
		bb.ContactEmailAddress, 
		[dw].[UdfGetCardClaimValue](bb.customerkey) as CardClaimValue
from    dw.CustomerRFMAggregations aa (nolock)
right   outer join    dw.DimCustomerReference bb (nolock)
on      aa.CustomerKey = bb.CustomerKey
where   VALITOR_MASTERCARD_CardLoadCount = 0
and     bb.SourceCreationDate > '2016-03-12 00:00:00.000'
		and bb.AccountTypeRefDerived in('Retail card - Firebird'
							,'Card & money transfer - Firebird'
							,'Retail card - self migrated user'
							)
and     bb.ContactEmailAddress not like '%@caxtonfx.com')
select	* from cte1 where cardclaimvalue = 'Retail:PrimaryAccount:PortalCards'
*/



SELECT DATEADD(d, -1, DATEDIFF(d, 0, GETDATE()) AS [Midnight Yesterday];
SELECT DATEADD(d, -0, DATEDIFF(d, 0, GETDATE())) AS [Midnight Today];
SELECT TOP 10 * FROM VALITOR.FACTWALLETTRANSACTIONS
---------------------------------------------------------------------------

select	* 
from	FireBird.[FactCurrencyBankAccountTransaction] (nolock)
where	customerkey is NULL
order	by id desc 
--27130 

select	*  
from	[dw].[BI-70_MC_retail_no_first_load]
where	count_of_load = 0 
order	by AccountCreationDate desc
--14052

EXEC SP_WHO2 ACTIVE