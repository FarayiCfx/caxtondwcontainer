





CREATE TABLE [staging].[DimCustomerReference](
	[Id] [nvarchar](256) NULL DEFAULT ((0)),
	[CustomerRefNo] [nvarchar](max) NULL DEFAULT ((0)),
	[FastpayContactRef] [nvarchar](max) NULL DEFAULT ((0)),
	[CardsContactRef] [nvarchar](max) NULL DEFAULT ((0)),
	[ValitorAccountId] [bigint] NULL DEFAULT ((0)),
	[IsActive] [bit] NULL DEFAULT ((0)),
	[UserName] [nvarchar](512) NULL,
	[IsMigratedCustomer] [bit] NULL DEFAULT ((0)),
	[DBCARDSContact_id] [int] NULL DEFAULT ((0)),
	[DBCARDSAccount_Id] [int] NULL DEFAULT ((0)),
	[DBCARDSParent_id] [int] NULL DEFAULT ((0)),
	[DBFXContact_id] [int] NULL DEFAULT ((0)),
	[DBFXAccount_Id] [int] NULL DEFAULT ((0)),
	[DBFXParent_id] [int] NULL DEFAULT ((0)),
	[OriginalSystemSourceId] [int] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[Gender] [nvarchar](20) NULL,
	[DateOfBirth] [datetime] NULL,
	[AddressLine1] [nvarchar](255) NULL,
	[AddressLine2] [nvarchar](255) NULL,
	[City] [nvarchar](200) NULL,
	[County] [nvarchar](200) NULL,
	[Postcode] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[ContactEmailAddress] [nvarchar](255) NULL,
	[PrimaryTelephone] [nvarchar](20) NULL DEFAULT ((0)),
	[SecondaryTelephone] [nvarchar](20) NULL DEFAULT ((0)),
	[TertiaryTelephone] [nvarchar](20) NULL DEFAULT ((0)),
	[ApplicantId] [bigint] NULL,
	[AffiliateCode] [nvarchar](255) NULL,
	[Status] [int] NULL,
	[EKYCStatus] [int] NULL,
	[PreAuthStatus] [int] NULL,
	[RegistrationDate] [datetime] NULL,
	[PostcodeStatus] [int] NULL,
	[WantMultiCurrencyCard] [bit] NULL,
	[WantToSendMoney] [bit] NULL,
	[Voucher] [nvarchar](255) NULL,
	[DuplicateOf] [nvarchar](255) NULL,
	[QuoteID] [bigint] NULL,
	[InitialLoad] [decimal](18, 2) NULL,
	[WSECenter] [bigint] NULL,
	[HeardAboutUs] [nvarchar](255) NULL,
	[selctionoption] [nvarchar](40) NULL,

	[OpenDealDailyLimit] [int] NOT NULL DEFAULT ((0)),
	[OpenDealValueLimit] [decimal](18, 2) NOT NULL DEFAULT ((0)),
	[IsSafeListed] [bit] NOT NULL DEFAULT ((0)),
	[DefaultCurrency] [nvarchar](255) NULL,
	[AccountTypeRef] [nvarchar](255) NULL,
	[MigrationStatus] [nvarchar](20) NULL,
	[ApplicationState] [int] NOT NULL DEFAULT ((0)),

	[AccountTypeRefDerived] [nvarchar](255) NULL,
	[MigrationDetectByEtl] [bit] NULL DEFAULT ((0)),
	[RecordType] [int] NULL,
	[MigrationDetectByEtlDate] [datetime] NULL DEFAULT ('1800-01-01 00:00:000'),
	[SourceCreationDate] [datetime] NULL,
	[SourceUpdateDate] [datetime] NULL,
	[DWDateInserted] [datetime] NULL,
	[DWDateUpdated] [datetime] NULL)
GO


EXEC SP_HELP '[staging].[DimCustomerReference]'




select max(valitoraccountid) from DW.DimCustomerReference (nolock)

exec sp_help 'dw.dimcustomerreference'



set IDENTITY_INSERT dw.dimcustomerreference  on 
go
insert into dw.dimcustomerreference (CustomerKey,
Id,
CustomerRefNo,
FastpayContactRef,
CardsContactRef,
ValitorAccountId,
IsActive,
UserName,
IsMigratedCustomer,
DBCARDSContact_id,
DBCARDSAccount_Id,
DBCARDSParent_id,
DBFXContact_id,
DBFXAccount_Id,
DBFXParent_id,
OriginalSystemSourceId,
Title,
FirstName,
LastName,
Gender,
DateOfBirth,
AddressLine1,
AddressLine2,
City,
County,
Postcode,
Country,
ContactEmailAddress,
PrimaryTelephone,
SecondaryTelephone,
TertiaryTelephone,
ApplicantId,
AffiliateCode,
Status,
EKYCStatus,
PreAuthStatus,
RegistrationDate,
PostcodeStatus,
WantMultiCurrencyCard,
WantToSendMoney,
Voucher,
DuplicateOf,
QuoteID,
InitialLoad,
WSECenter,
HeardAboutUs,
selctionoption,
--OpenDealDailyLimit,
--OpenDealValueLimit
--IsSafeListed,
---DefaultCurrency,
--AccountTypeRef,
--MigrationStatus,
--ApplicationState,
AccountTypeRefDerived,
MigrationDetectByEtl,
RecordType,
MigrationDetectByEtlDate,
SourceCreationDate,
SourceUpdateDate,
DWDateInserted,
DWDateUpdated)


select 
CustomerKey
,Id
,CustomerRefNo
,FastpayContactRef
,CardsContactRef
,ValitorAccountId
,IsActive
,UserName
,IsMigratedCustomer
,DBCARDSContact_id
,DBCARDSAccount_Id
,DBCARDSParent_id
,DBFXContact_id
,DBFXAccount_Id
,DBFXParent_id
,OriginalSystemSourceId
,Title
,FirstName
,LastName
,Gender
,DateOfBirth
,AddressLine1
,AddressLine2
,City
,County
,Postcode
,Country
,ContactEmailAddress
,PrimaryTelephone
,SecondaryTelephone
,TertiaryTelephone
,ApplicantId
,AffiliateCode
,Status
,EKYCStatus
,PreAuthStatus
,RegistrationDate
,PostcodeStatus
,WantMultiCurrencyCard
,WantToSendMoney
,Voucher
,DuplicateOf
,QuoteID
,InitialLoad
,WSECenter
,HeardAboutUs
,selctionoption

,AccountTypeRef
,MigrationDetectByEtl
,RecordType
,MigrationDetectByEtlDate
,SourceCreationDate
,SourceUpdateDate
,DWDateInserted
,DWDateUpdated
from [dw].[DimCustomerReference_old_19_oct_2016]


select * from [17_10_16_DimCustomerRefUpdate7Columns]


select	aa.OpenDealDailyLimit,aa.OpenDealValueLimit,aa.IsSafeListed,aa.DefaultCurrency,aa.AccountTypeRef,aa.MigrationStatus,aa.ApplicationState,
	bb.*
from	dw.dimcustomerreference aa join [17_10_16_DimCustomerRefUpdate7Columns] as bb
on		aa.id = bb.Id
 

update   aa
set		aa.OpenDealDailyLimit = bb.OpenDealDailyLimit,
		aa.OpenDealValueLimit = bb.OpenDealValueLimit,
		aa.IsSafeListed = bb.IsSafeListed,
		aa.DefaultCurrency = bb.DefaultCurrency,
		aa.AccountTypeRef = bb.AccountTypeRef,
		aa.MigrationStatus = bb.MigrationStatus,
		aa.ApplicationState = bb.ApplicationState
		from  [dw].[DimCustomerReference] as aa
		inner join [17_10_16_DimCustomerRefUpdate7Columns] as bb
		on aa.id = bb.Id


ALTER TABLE staging.dimcustomerreference ALTER COLUMN originalsystemsourceid INT NULL

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [originalsystemsourceid]
GO


ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [Id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [CustomerRefNo]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [FastpayContactRef]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [CardsContactRef]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [ValitorAccountId]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [IsActive]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [IsMigratedCustomer]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [DBCARDSContact_id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [DBCARDSAccount_Id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [DBCARDSParent_id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [DBFXContact_id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [DBFXAccount_Id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [DBFXParent_id]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [PrimaryTelephone]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [SecondaryTelephone]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [TertiaryTelephone]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ((0)) FOR [MigrationDetectByEtl]
GO

ALTER TABLE [staging].[DimCustomerReference] ADD  DEFAULT ('1800-01-01 00:00:000') FOR [MigrationDetectByEtlDate]
GO

-------------------------------------------------------------------------

select * from dw.DimCustomerReference
where cast(dwdateinserted as date) = cast(getdate() as date)

