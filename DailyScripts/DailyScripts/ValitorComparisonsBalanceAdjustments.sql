/*
First query is to work out everything in Valitor via the left outer join,
that may not show up in FireBird

The second one is to try and show everything that has appeared in Firebird
under card loads that we think should also appear in FireBird but has not
*/
select	ExtractDateTime,
		[Type],
		aa.[Id],
		PanId,
		AccountId,
		ReferenceNumber,
		BillingAmount,
		BillingCurrency,
		RegistrationDate,
		SettlementId,
		aa.TransactionDateTime as ValitorTransactionDateTime,
		TransactionKey,
		TransactionKeyCategory,
		WalletId,
		ReferenceId,
		DownloadFileName,
		bb.*
from	[VALITOR].[FACT_WALLETTRANSACTIONS_EXTRACT] aa (nolock)
left outer	join	[FireBird].[CurrencyBankAccountTransaction] bb (nolock)
on		aa.referenceid = bb.externaltransactionid
WHERE	BB.externaltransref = 'valitor'
AND		AA.[TYPE] = 'BalanceAdjustment'
AND		(AA.TRANSACTIONDATETIME BETWEEN	'2015-10-01 00:00:00.000' AND '2015-11-01 00:00:00.000')


--------------------------------------------------------------

--non matching - these records appear in firebird not in valitor 
select	ExtractDateTime,
		bb.*
from	[VALITOR].[FACT_WALLETTRANSACTIONS_EXTRACT] aa (nolock)
right outer	join	[FireBird].[CurrencyBankAccountTransaction] bb (nolock)
on		aa.referenceid = bb.externaltransactionid
WHERE	BB.externaltransref = 'valitor'
--and		BB.externaltransactionid is not null
AND		bb.[status] = 'Clear funds from balance for card load'
AND		(bb.TRANSACTIONDATETIME BETWEEN	'2015-10-01 00:00:00.000' AND '2015-11-01 00:00:00.000')
AND AA.REFERENCEID IS NULL
