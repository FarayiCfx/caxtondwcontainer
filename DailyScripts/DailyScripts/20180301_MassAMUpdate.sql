-->BACKUP THE TABLE 
SELECT	* 
INTO	firebird.useraccountmanagerassociation_BACKUP_01032018
FROM	firebird.useraccountmanagerassociation

-->make ben an AM
insert into Firebird.AccountManager (UserName,FullName,FB_DateCreated,AccountTypeRef)
values('Ben.Blacker@CAXTONFX.COM', 'Ben Blacker',GETDATE(),'Corporate')

-->GET RECORD COUNTS
select * from firebird.useraccountmanagerassociation aa --4347
select * from newsalesam --331

-->SHOW ME THE SPELLINGS ARE THE SAME
select distinct fullname from firebird.accountmanager order by fullname
select distinct [NEw am] from newsalesam


--NOW LETS SEE WHATS SPELT WRONGLY IN THE CLIENT EMAIL 
select	aa.* 
		,bb.Email
from	newsalesam aa
left	outer join	AspNetUsers bb
on		aa.[Client Email] = bb.Email
order	by Email

-->fix the nulls in the new am table 
select * from AspNetUsers where Email like '%lorne@flavoursholidays.com%'

select * from AspNetUsers where Email like '%wardlawltdtashowstars@caxtonfx%'

select * from AspNetUsers where Email like '%jfleming@westernwear.co.uk%'

select * from AspNetUsers where Email like '%piers.seed@lifesourcesupplements.co.uk%'
;
UPDATE	newsalesam 
SET		[Client Email] = 'lorne@flavoursholidays.com'
WHERE	[Client Email] like '%lorne@flavoursholidays.com%'

UPDATE	newsalesam 
SET		[Client Email] = 'wardlawltdtashowstars@caxtonfx.com'
WHERE	[Client Email] like '%wardlawltdtashowstars@caxtonfx.com%'

UPDATE	newsalesam 
SET		[Client Email] = 'jfleming@westernwear.co.uk'
WHERE	[Client Email] like '%jfleming@westernwear.co.uk%'

UPDATE	newsalesam 
SET		[Client Email] = 'piers.seed@lifesourcesupplements.co.uk'
WHERE	[Client Email] like '%piers.seed@lifesourcesupplements.co.uk%'
--------------------------------------------------------------------------------------------
--NO MORE SPELLING MISTAKES
select	aa.* 
		,bb.Email
from	newsalesam aa
join	AspNetUsers bb
on		aa.[Client Email] = bb.Email
order	by Email

--temp table for updates 
drop table if exists #temp1

select	aa.*	
		,bb.*	 
		,dd.id as newid_am
		, dd.UserName as accountmanageremail
		,dd.FullName as fullname
		into #temp1
from	newsalesam aa
join	firebird.useraccountmanagerassociation bb
on		aa.[client email] = bb.userid
join	firebird.accountmanager dd
on		aa.[new am] = dd.fullname
and		dd.accounttyperef ='corporate'

select * from #temp1

--update section 
update	aa
Set		aa.accountmanagerid = bb.newid_am
--select newid_am,aa.*  , bb.*
from	firebird.useraccountmanagerassociation aa
join	#temp1 bb 
on		aa.Id = bb.Id

-------
drop table if exists #tempinsert
;
with cteam as (select * from Firebird.AccountManager where AccountTypeRef = 'corporate')

select	cc.Id as AccountManagerId
		,[client email] as userid 
into	#tempinsert
from	newsalesam aa
left	outer join	Firebird.UserAccountManagerAssociation bb
on		aa.[client email] = bb.UserId
join	cteam cc
on		aa.[new am] = cc.FullName
where	bb.UserId is null

-->check temp table 
select * from #tempinsert

insert	into Firebird.UserAccountManagerAssociation(AccountManagerId,UserId,DateCreated,IsDeleted,FB_DateCreated)
select	AccountManagerId, userid,GETDATE(),0,GETDATE()
from	#tempinsert



