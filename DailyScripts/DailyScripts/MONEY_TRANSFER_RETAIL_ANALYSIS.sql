select top 10 * from dbfx.deallist order by dealdate desc




/*
The below query usese two ctes , one for all the trades logged and another for all the 
trades in the last 12 months 
*/
drop table ADHOC.MT_23K_ANALYSIS
go

with cteAlltimeTrades as (
select	customerkey,
		count(rowid) as Talksheet_MT_DealCount_Alltime,
		min(dealdate) as Talksheet_MT_MinDealDate_Alltime,
		max(dealdate) as Talksheet_MT_MaxDealDate_Alltime,
		sum(revenue) as Talksheet_MT_RevenueSum_Alltime
from	dbfx.deallist (nolock)
group	by customerkey)

,

cte12month as (
select	customerkey,
		count(rowid) as Talksheet_MT_DealCount_12Months,
		min(dealdate) as Talksheet_MT_MinDealDate_12Months,
		max(dealdate) as Talksheet_MT_MaxDealDate_12Months,
		sum(revenue) as Talksheet_MT_RevenueSum_12Month
from	dbfx.deallist (nolock)
where   dealdate >= '2015-05-01 00:00:00.000'
group	by customerkey)

select	title,aa.firstname,aa.lastname,contactemailaddress,dateofbirth,
		datediff(yy,dateofbirth,getdate()) as age,
		gender = case when gender = 'unknown' and title = 'Mr' then 'Male'
				when gender = 'unknown' and title = 'Miss' then 'Female'
				when gender = 'unknown' and title = 'Mrs' then 'Female'
				when gender = 'unknown' and title = 'Mr.' then 'Male'
		else gender end
		,aa.accounttyperef

		,isnull(cte12month.Talksheet_MT_DealCount_12Months,0) as Talksheet_MT_DealCount_12Months
		,isnull(cte12Month.Talksheet_MT_RevenueSum_12Month,0) as Talksheet_MT_RevenueSum_12Month

		,cteAlltimeTrades.*
	
		into ADHOC.MT_23K_ANALYSIS

from	dw.dimcustomerreference aa
join	cteAlltimeTrades 
on		aa.customerkey = cteAlltimeTrades.customerkey
left	outer join cte12Month
on		aa.customerkey  = cte12Month.customerkey 
where	aa.accounttyperef <> 'Corporate Money transfer - Talksheet'
and		Talksheet_MT_DealCount_Alltime = 0

select * from ADHOC.MT_23K_ANALYSIS
---ORDER BY TALKSHEET_MT_MAXDEALDATE_ALLTIME ASC



 


----------------------------------------------------------------------------------------------------------------------------------------------


with cte1 as (
select	customerkey,
		count(rowid) as Talksheet_MT_DealCount_12Months,
		min(dealdate) as Talksheet_MT_MinDealDate_12Months,
		max(dealdate) as Talksheet_MT_MaxDealDate_12Months,
		sum(revenue) as Talksheet_MT_RevenueSum_12Month
from	dbfx.deallist (nolock)
where   dealdate >= '2015-05-01 00:00:00.000'
group	by customerkey)

select	aa.id as firebirdid,title,aa.firstname,aa.lastname,contactemailaddress,dateofbirth,
		datediff(yy,dateofbirth,getdate()) as age,
		gender = case when gender = 'unknown' and title = 'Mr' then 'Male'
				when gender = 'unknown' and title = 'Miss' then 'Female'
				when gender = 'unknown' and title = 'Mrs' then 'Female'
				when gender = 'unknown' and title = 'Mr.' then 'Male'
		else gender end
		,aa.accounttyperef
		,aa.sourcecreationdate as accountcreationdate
		,cc.MoneyTransfer_Firebird_TradeCount 
		,isnull(cte1.Talksheet_MT_DealCount_12Months,0) as Talksheet_MT_DealCount_12Months
		,isnull(cte1.Talksheet_MT_MinDealDate_12Months,'') as Talksheet_MT_MinDealDate_12Months
		,isnull(cte1.Talksheet_MT_MaxDealDate_12Months,'') as Talksheet_MT_MaxDealDate_12Months
		,isnull(cte1.Talksheet_MT_RevenueSum_12Month,0) as Talksheet_MT_RevenueSum_12Month
		into #temp2	
from	dw.dimcustomerreference aa
left	outer join	cte1 
on		aa.customerkey = cte1.customerkey
left	outer join  [dw].[CustomerRFMAggregations] cc
on		aa.customerkey = cc.customerkey 
where	aa.accounttyperef in ('MoneyTransfer - Firebird','Retail Money Transfer - Talksheet')
and		cte1.customerkey is null
