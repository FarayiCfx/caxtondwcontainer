-->Dormancy fees spreadsheet
select	* from dormancy_fees_

select	distinct cardid from dormancy_fees_

exec	sp_help tblcfxcurrencycarddetails
go

select	* from tblcfxcurrencycarddetails where cardid = '2507225769793719'
-->pickup email addresses
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
with	cteEmailRanks as (
select	contact_id,
		email_address,  
		rank() over(partition by contact_id order by contact_email_id ) as [Rank]
from	contact_email)

select	identity(int, 1,1) as id,
		contact_id,email_address,
		'Email '+ cast( [Rank] as varchar(5)) as EmailRank
into	#temp1
from	cteEmailRanks
order	by contact_id 

------------------------------------------------------------------------------------------------------------
--create a temp table for all known email addresses per customer 
IF		OBJECT_ID('tempdb..#temp2') IS NOT NULL
DROP	TABLE #temp2
go

with	cte1 as (
select	contact_id,email_address as email_address1,emailrank
from	#temp1
where	emailrank = 'EMAIL 1'),

cte2	as (select contact_id,email_address as email_Address2,emailrank
from	#temp1
where	emailrank = 'EMAIL 2'),

cte3 as (select contact_id,email_address as email_address3,emailrank
from #temp1
where emailrank = 'EMAIL 3')

select	cte1.contact_id,
		cte1.email_address1,
		isnull(cte2.email_address2,'') as email_address2,
		isnull(cte3.email_address3,'') as email_address3, 
		cc.contact_salutation,
		cc.contact_first_name,
		cc.contact_last_name
		into #temp2
from	cte1 
left	outer join	cte2
on		cte1.contact_id = cte2.contact_id
left	outer join cte3 
on		cte1.contact_id = cte3.contact_id
join dbo.contact cc 
on		cte1.contact_id = cc.contact_id
-----------------------

select *from #temp2 

with cteFinalResultSet as (
select	bb.*
		,aa.contact_id 
from	tblcfxcurrencycarddetails aa
right	outer join dormancy_fees_ bb
on		aa.cardid = bb.cardid)
select	aa.*,bb.*
from	cteFinalResultSet aa
left	outer join #temp2 bb
on		aa.contact_id = bb.contact_id


