select	top 5 * from dw.dimusercorporateassociation aa
select		top 5 * from dw.dimcorporateentities bb

select top 10 * from valitor.factwallettransactions


with cte1 as 
(
select	distinct companyname, 
				corporateentityid,
				email
from	dw.dimusercorporateassociation aa 
join	dw.dimcorporateentities bb
on		aa.corporateentityid = bb.id)
select * from cte1 where companyname like '%travel%'
------------------------------------------------------------------------------------------------->
with	ctedistinctentities as (
select	distinct companyname, 
				corporateentityid,
				email
from	dw.dimusercorporateassociation aa 
join	dw.dimcorporateentities bb
on		aa.corporateentityid = bb.id)

select		/*top 100*/	bb.customerkey,
			bb.Username,
			tr.Description,
			tr.wallettransaction_key,
			tr.billingamount,	
			tr.billingcurrency,
			tr.transactiondatetime ,
			[dw].[UdfGetCurrencyAlphaCode] (tr.billingcurrency)
from	[dw].[DimUserCorporateAssociation] aa 
join	dw.dimcustomerreference bb
on		aa.email = bb.username
join	valitor.factwallettransactions tr
on		bb.customerkey  = tr.customerkey 

where	tr.description in ('load','unload')
order	by 1 

------------------------------------------------------------------------------------------------->
alter view CorpCardLoadRedemptionReport
as
with	ctedistinctentities as (
select	distinct	companyname, 
					corporateentityid,
					email
from	dw.dimusercorporateassociation aa 
join	dw.dimcorporateentities bb
on		aa.corporateentityid = bb.id)

select		aa.Companyname,
			/*sum(billingamount) as*/ 
			[Load \ Unload Amount] = case tr.description 
			when 'load' then sum(abs(tr.billingamount))
			when 'unload' then sum(tr.billingamount * -1)
			end
			,
			--bb.customerkey,
			bb.Username,
			tr.[Description],
			[dw].[UdfGetCurrencyAlphaCode] (tr.billingcurrency) as CurrencyCode,
			cast(tr.transactiondatetime as date) as TransactionDate,
			DATENAME(MONTH,tr.transactiondatetime) as [Month],
			DATENAME(YEAR,tr.transactiondatetime) as [Year]
from	ctedistinctentities aa 
left	outer join	dw.dimcustomerreference bb (nolock)
on		aa.email = bb.username
left	outer join	valitor.factwallettransactions tr (nolock)
on		bb.customerkey  = tr.customerkey 
where	tr.description in ('load','unload')
group	by	aa.companyname,
			--bb.customerkey,
			bb.username,
			tr.[description],	
			tr.billingcurrency,
			cast(tr.transactiondatetime as date) ,
			datepart(ww,tr.transactiondatetime),
			DATENAME(MONTH,tr.transactiondatetime),
			DATENAME(YEAR,tr.transactiondatetime)
			
--------------------------------------------------------------------------->
select * from CorpCardLoadRedemptionReport 
where companyname = 'british red cross'
order by transactiondate asc


exec sp_who2 active
