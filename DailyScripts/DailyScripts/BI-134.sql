--first import the table that 
select * from bi_134

--valitor side of the equation 
with	cte1 as (
SELECT	aa.*, bb.CustomerKey, bb.UserName 
FROM	[dbo].[BI_134] aa
left	outer join dw.DimCustomerReference bb
on		aa.emailaddress = bb.UserName)
select	tr.CustomerKey,
		companyname,
		emailaddress,
		abs(tr.BillingAmount) as BillingAmount,
		tr.TransactionDateTime,
		tr.Description,
		crr.NameCode as CurrencyCode,
		'MasterCard' as CardScheme
from	cte1 
join	valitor.FactWalletTransactions tr
on		cte1.customerkey = tr.CustomerKey
join	[dw].[DimISOCurrencyName] crr
on		tr.BillingCurrency = crr.ISOCode
where	tr.Description = 'load'
order by companyname,tr.TransactionDateTime

-------------------------------------------------------------------------------------------------
with	cte1 as (SELECT	aa.*, 
		bb.CustomerKey, 
		bb.UserName ,
		isnull(bb.DBCARDSContact_id,0) as DBCardsContact_id,
		bb.DBCARDSAccount_Id,bb.AccountTypeRef, 
		bb.AccountTypeRefDerived
FROM	[dbo].[BI_134] aa
left	outer join dw.DimCustomerReference bb
on		aa.emailaddress = bb.ContactEmailAddress
where	isnull(bb.DBCARDSContact_id,0) > 0 )
select	cte1.CUSTOMERKEY,
		companyname,
		emailaddress,
		ld.AMTTXN as BillingAmount,
		ld.TransactionDate as TransactionDatetime,
		ld.LoadType as [Description],
		ld.Card_currency,
		'VISA' as CardScheme
from	cte1 
join	dbo.fistalksheetallloads ld
on		cte1.customerkey = ld.customerkey
where	ld.TransactionDate BETWEEN '2015-12-01 00:00:00.000' and '2016-12-01 00:00:00.000'
order	by companyname 
GO
