
select * from FIS_Output_Raphaels where lastname = 'palmer'
order by avlbal desc

select * from fis_data where avlbal = '8705.57'

select * from fis_data where cardid = '2507537740396590'
select * from caxprogbalexp where cardid = '2507537740396590'

-->pickup email addresses
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
with	cteEmailRanks as (
select	contact_id,
		email_address,  
		rank() over(partition by contact_id order by contact_email_id ) as [Rank]
from	contact_email)

select	identity(int, 1,1) as id,
		contact_id,email_address,
		'Email '+ cast( [Rank] as varchar(5)) as EmailRank
into	#temp1
from	cteEmailRanks
order	by contact_id 

select * from #temp1

------------------------------------------------------------------------------------------------------------
--create a temp table for all known email addresses per customer 
IF		OBJECT_ID('tempdb..#temp2') IS NOT NULL
DROP	TABLE #temp2
go

with cte1 as (
select contact_id,email_address as email_address1,emailrank
from #temp1
where emailrank = 'EMAIL 1'),

cte2 as (select contact_id,email_address as email_Address2,emailrank
from #temp1
where emailrank = 'EMAIL 2'),

cte3 as (select contact_id,email_address as email_address3,emailrank
from #temp1
where emailrank = 'EMAIL 3')

select	cte1.contact_id as contact_id_emailtable,
		cte1.email_address1,
		isnull(cte2.email_address2,'') as email_address2,
		isnull(cte3.email_address3,'') as email_address3, 
		cc.contact_salutation,
		cc.contact_first_name,
		cc.contact_last_name
		into #temp2
from	cte1 
left	outer join	cte2
on		cte1.contact_id = cte2.contact_id
left	outer join cte3 
on		cte1.contact_id = cte3.contact_id
join	dbo.contact cc 
on		cte1.contact_id = cc.contact_id

--just check to make sure that everything pivoted correctly 
select * from #temp2 where email_address1 = 'bozena@entitymail.co.uk'
select * from [notes] where contact_id = 160535


--------------------------------------------------------------------------------------------------------------------------------------------

IF		OBJECT_ID('FIS_Output_Raphaels') IS NOT NULL
DROP	TABLE FIS_Output_Raphaels
go
with	cte1 as
(
select	CASE WHEN cardtype = 0 THEN 'PRIMARY' ELSE 'SECONDARY' END AS [Card Type]
		,aa.*
		,bb.contact_id 
from	fis_Data aa
inner	join tblcfxcurrencycarddetails bb (nolock)
on		aa.cardid = bb.cardid
),
cteFraudNotes as(
SELECT	distinct [Contact_ID]
		,'Fraud note in TS' as [Fraud_note]
 FROM	[dbcards].[dbo].[Notes] (nolock)
 where	note like '%fraud%'
)
select	AA.*
		,BB.* 
		,cteFraudNotes.[Fraud_note]
into	FIS_Output_Raphaels
from	cte1 aa
left	outer join	#temp2 bb
on		aa.contact_id = bb.contact_id_emailtable
left	outer join cteFraudNotes
on		aa.contact_id = cteFraudNotes.contact_id

-----

select	row_number() over(order by email_address1) as RowNumber,
		* 
from	FIS_Output_Raphaels 
where	fraud_note is not null
