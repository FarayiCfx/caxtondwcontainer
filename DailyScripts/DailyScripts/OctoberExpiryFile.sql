-->start by counting everything first
select  * 
from	valitor.pan (nolock)
where	expiry = 1710
order	by 1 desc 

-->then retrieve the data set from production 
-->filtering by card status as active
select  valitorpanid
		,maskedpan
		,Expiry
		,isnull(title,'') as title
		,firstname
		,lastname
		,address1
		,isnull(address2,'') as address2
		,city
		,isnull(county,'') as county
		,postcode
		,username
from	valitor.pan aa (nolock)
join	valitor.wallet bb (nolock)
on		aa.WalletId = bb.id
join	aspnetusers cc (nolock)
on		bb.accountid = cc.username
where	aa.expiry = 1710
and		cardstatuscode = 'v'
order	by 1 desc 

