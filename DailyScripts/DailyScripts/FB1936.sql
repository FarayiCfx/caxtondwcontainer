use dbcards 
go

--this is for email addresses 

---select top 10 * from contact_email

IF		OBJECT_ID('tempdb..##dbcardsemails') IS NOT NULL
DROP	TABLE ##dbcardsemails
go
select	distinct	contact_id,email_address, 
					rank() over(partition by contact_id order by email_address) as rank_
into	##dbcardsemails 
FROM	CONTACT_EMAIL (nolock)
where	email_address is not null and email_address <> ''
go
-------------------------------------------------------------------------------------------------------------------------
--create a distinct list of email addresses from dbfx agains which you will deduplicate
IF		OBJECT_ID('tempdb..##dbfxemails') IS NOT NULL
DROP	TABLE ##dbfxemails

select	distinct email_address 
into	##dbfxemails
from	dbfx..contact_email (nolock)
go

--------------------------------------------------------------------------------------------------------------------------
IF		OBJECT_ID('tempdb..##extracttable') IS NOT NULL
DROP	TABLE ##extracttable

with cteAllCardsEmailAddresses as(
select	cc.contact_id,contact_salutation,contact_first_name,contact_middle_init,contact_last_name, 
		db1.email_address as emailaddress1
from	dbcards..contact cc
join	##dbcardsemails db1
on		cc.contact_id  = db1.contact_id 
where 	db1.[rank_] = 1	
and		cc.parent_id is null --only primary contacts

union	all

select	cc.contact_id,contact_salutation,contact_first_name,contact_middle_init,contact_last_name, 
		db1.email_address as emailaddress1
from	dbcards..contact cc
join	##dbcardsemails db1
on		cc.contact_id  = db1.contact_id 
where 	db1.[rank_] = 2
and		cc.parent_id is null --only primary contacts
)
select  count(bb.email_address) as count_ --this count is a custom count for richard cole
from 	cteAllCardsEmailAddresses aa
inner	join ##dbfxemails bb
on		aa.emailaddress1 = bb.email_address
--where	bb.email_address is null

    


select  identity (int, 1,1) as [RowId],
		rank() over (partition by emailaddress1 order by contact_id asc) as RankValue,
		aa.*
into	##extracttable
from 	cteAllCardsEmailAddresses aa
left	outer join ##dbfxemails bb
on		aa.emailaddress1 = bb.email_address
where	bb.email_address is null
go
     
 









--------------------------------------------------------------------------------------------------------------------------------------

with	cte1 as (
select	contact_first_name,contact_last_name, emailaddress1 as ContactEmailAddress
from	DedupedListFromTalksheet 
union
select	firstname as contact_firstname,lastname as contact_lastname ,ContactEmailAddress
from	dw.dimcustomerreference
where	originalsystemsourceid = 1)
select	count(ContactEmailAddress) as count_, ContactEmailAddress
from	cte1 
group by ContactEmailAddress 
order by count_ desc 

with	cte1 as (
select	contact_first_name,contact_last_name, emailaddress1 as ContactEmailAddress
from	DedupedListFromTalksheet 
union
select	firstname as contact_firstname,lastname as contact_lastname ,ContactEmailAddress
from	dw.dimcustomerreference
where	originalsystemsourceid = 1)
select	distinct contactemailaddress from cte1 

select * from ##extracttable
