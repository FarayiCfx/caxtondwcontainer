
DROP	TABLE temp1 
go
WITH	cte1 AS (
SELECT	aa.CustomerKey,bb.BillingAmountGBP,bb.TransactionDateTime AS LoadDateTimeAtValitor,
		COALESCE(aa.contactemailaddress,aa.username) AS Email , 
		aa.firstname,aa.lastname, aa.registrationdate,
		RANK() over(partition BY bb.customerkey ORDER BY bb.wallettransaction_key) AS loadrankpayment
FROM	dw.DimCustomerReference aa
JOIN	valitor.FactWalletTransactions bb
ON		aa.CustomerKey = bb.CustomerKey
WHERE	CAST(aa.registrationdate AS DATE) = '2016-04-12'
AND		bb.Description = 'load')
SELECT	ISNULL([dw].[UdfGetCardClaimValue] (customerkey),'NonFireBirdCard') AS CardType,* 
INTO	temp1
FROM	cte1 
WHERE	loadrankpayment = 1
GO


DROP	TABLE temp2
go
WITH	cte1 AS (
SELECT	aa.id,aa.CustomerKey,bb.BillingAmountGBP,bb.TransactionDateTime AS LoadDateTimeAtValitor,
		COALESCE(aa.contactemailaddress,aa.username) AS Email , 
		aa.firstname,aa.lastname, aa.registrationdate,
		RANK() over(partition BY bb.customerkey ORDER BY bb.wallettransaction_key) AS loadrankpayment,
		aa.AccountTypeRef
FROM	dw.DimCustomerReference aa
JOIN	valitor.FactWalletTransactions bb
ON		aa.CustomerKey = bb.CustomerKey
WHERE	CAST(aa.registrationdate AS DATE) > '2016-05-22' AND CAST(aa.registrationdate AS DATE) <= '2016-05-30'
AND		bb.Description = 'load')
SELECT	--ISNULL([dw].[UdfGetCardClaimValue] (customerkey),'NonFireBirdCard') AS CardType,
		* 
INTO	temp2
FROM	cte1 
WHERE	loadrankpayment = 1


SELECT * FROM dbo.temp2 order by registrationdate asc


