-->exec [dw].[MergeBuyAndHoldRfmStats]

CREATE procedure [dw].[MergeBuyAndHoldRfmStats]
as
/*
Merge the load totals, counts and min and max dates to the 
customer aggregate RFM table 
*/
with	cteBuyAndHoldStats as(
select	sum(TradeProfit) GBPTradeProfit , 
		count(id) as Count,
		min(TradeDate) as MinTradeDate,
		max(TradeDate) as MaxTradeDate,
		customerkey 
from	FireBird.FactCurrencyBankTrade (nolock)
where	TradeReason =  'Buy and Hold'
group	by customerkey)

merge	DW.CustomerRFMAggregations rfm 
using	(
select	ck.customerkey,
		isnull (cte.GBPTradeProfit,0) as [BuyAndHold_Tradeprofit_GBP_Total],
		isnull (cte.count,0) as [BuyAndHold_TradeCount],
		isnull (cte.MinTradeDate , '1900-01-01 00:00:00.000') as [BuyAndHold_FirstDate],
		isnull (cte.MaxTradeDate, '1900-01-01 00:00:00.000') as [BuyAndHold_LastDate]
from	dw.dimcustomerreference ck
left	outer join cteBuyAndHoldStats cte
on		ck.customerkey = cte.customerkey ) as source
on		rfm.customerkey = source.customerkey 
when	matched 
then	update 
set		rfm.[BuyAndHold_Tradeprofit_GBP_Total] = source.[BuyAndHold_Tradeprofit_GBP_Total],
		rfm.[BuyAndHold_TradeCount] = source.[BuyAndHold_TradeCount],
		rfm.[BuyAndHold_FirstDate] = source.[BuyAndHold_FirstDate],
		rfm.[BuyAndHold_LastDate] = source.[BuyAndHold_LastDate],
		rfm.DWDateUpdated = getdate()
		
when	not matched
then	insert (CustomerKey,
				[BuyAndHold_Tradeprofit_GBP_Total],
				[BuyAndHold_TradeCount],
				[BuyAndHold_FirstDate],
				[BuyAndHold_LastDate],
				DWDateUpdated)
				values (
				source.CustomerKey,
				source.[BuyAndHold_Tradeprofit_GBP_Total],
				source.[BuyAndHold_TradeCount],
				source.[BuyAndHold_FirstDate],
				source.[BuyAndHold_LastDate],
				getdate())
		;

GO


