select		count(*) as count,
			registrationdatekey 
from		dw.DailyApplicationsToAccounts 
group by	registrationdatekey
order by	RegistrationDateKey desc
go
-------------------------------------------------------------------
select	count(aa.applicationkey) as count_,
		aa.registrationdatekey,
		bb.applicationstates
from	dw.factapplicants aa
join	dw.dimapplicationvalidationresults bb
on		aa.applicationstate = bb.applicationstatenaturalkey
group	by aa.registrationdatekey,
		bb.applicationstates
order by aa.registrationdatekey
desc
go
----------------------------------------------------------------------------

select	aa.*,
		bb.applicationstates
from	dw.factapplicants aa
join	dw.dimapplicationvalidationresults bb
on		aa.applicationstate = bb.applicationstatenaturalkey
where	bb.ApplicationStates in ('unknown')
order	by aa.registrationdatekey
desc
go

select	* from dw.DimApplicationValidationResults


select
Date,
Type,
''''+ID as [ID],
[Reconciliation Identifier],
Payment,
[Order],
Terminal,
Shop,
[Transaction Currency],
[Transaction Amount],
[Exchange Rate],
[Settlement Currency],
[Settlement Amount],
[Fixed Fee],
[Fixed Fee VAT],
[Rate Based Fee],
[Rate Based Fee VAT],
[FileName]
from		[altapay].[Fact_AltaPayFundingFiles] (NOLOCK)
where		date_key between 20160701 AND 20160731
order by	date_key ASC 
