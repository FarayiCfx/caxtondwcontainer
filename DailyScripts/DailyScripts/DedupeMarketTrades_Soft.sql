/*
Soft delete duplicate market trades (run each section separateky and look at output)

1 - Start by creating a temp table to view records once you are done 

2 - Run the cursor until no more output is receievd 

3 - Cross reference with the temp table to the live table and check the FB_DATEDELETED columns

*/
drop table if exists #temp1
;
select	[BookName], [BookOpenDate], [SellCcy], [SellCcyAmount], [BuyCcy], [BuyCcyAmount]
into	#temp1
from	[CurrencyBank].[MarketTrade]
where	fb_datedeleted is null
group	by [BookName], [BookOpenDate], [SellCcy], [SellCcyAmount], [BuyCcy], [BuyCcyAmount]
having	count(*) > 1
go

-->then eyeball the table
select * from #temp1
order by 6
go


----------------------------------------------------------------------------------------------->
declare c cursor for

select	[BookName], [BookOpenDate], [SellCcy], [SellCcyAmount], [BuyCcy], [BuyCcyAmount]
from	[CurrencyBank].[MarketTrade]
where	fb_datedeleted is null
group	by [BookName], [BookOpenDate], [SellCcy], [SellCcyAmount], [BuyCcy], [BuyCcyAmount]
having	count(*) > 1

declare @bookname nvarchar(256)
declare @bookopendate datetime
declare @sellccy nvarchar(3)
declare @sellccyamount decimal(18,2)
declare @buyccy nvarchar(3)
declare @buyccyamount decimal(18,2)
declare @count bigint
set		@count = 0

open c
fetch next from c into @bookname, @bookopendate, @sellccy, @sellccyamount, @buyccy, @buyccyamount
while @@fetch_status = 0
begin
 
 set @count = @count + 1
 print @count
 declare @id bigint
 
 select top 1 @id = id 
 from	[CurrencyBank].[MarketTrade]
 where	bookname = @bookname and bookopendate = @bookopendate and sellccy = @sellccy
		and sellccyamount = @sellccyamount and buyccy = @buyccy and buyccyamount = @buyccyamount
		and fb_Datedeleted is null

 print @id

 update		[CurrencyBank].[MarketTrade] 
 set		fb_datedeleted = getdate()
 where		[Id] = @id
 
 fetch next from c into @bookname, @bookopendate, @sellccy, @sellccyamount, @buyccy, @buyccyamount
end

close		c
deallocate	c


-----then run the below to check
select	bb.id,aa.[BookName], bb.[BookOpenDate], bb.[SellCcy], bb.[SellCcyAmount], bb.[BuyCcy], bb.[BuyCcyAmount],fb_datedeleted
from	#temp1 aa
join	currencybank.markettrade bb
on		aa.bookname = bb.bookname
and		aa.bookopendate = bb.bookopendate
and		aa.sellccy = bb.sellccy
and		aa.sellccyamount = bb.sellccyamount
and		aa.buyccy = bb.buyccy
and		aa.buyccyamount = bb.buyccyamount 
order	by 2,3,4,5,6

-->select any random trades below to check from above list
select * from currencybank.markettrade 

