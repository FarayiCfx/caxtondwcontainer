SELECT * FROM dw.MinimumLoadTransactionKey order by customerkey asc 

SELECT * FROM valitor.FactWalletTransactions

SELECT TOP 100 * FROM [dw].[DailyApplicationsToAccounts]

--all apps
DROP TABLE #allapps
GO
create view dw.AllApplications
as
/*
Because of a limitation in TABLEAU with not being able to use 
CTE's (common table expressions) views have to be created 
*/

SELECT	[product],
		[type],
		RegistrationDate,
		COUNT(applicationkey) AS TotalApplications
---INTO	#ALLAPPS
FROM	[dw].[DailyApplicationsToAccounts] (nolock)
WHERE	registrationdatekey >= 20160427
GROUP	BY registrationdatekey,[product],[type], RegistrationDate
go


----account creation sameday 

create view dw.AccountsCreatedSameDay
as
--DROP TABLE #account_creation_sameday 
--GO
SELECT	[product] as [product_sameday],
		[type] as [type_Sameday],
		RegistrationDate as [reg_Datesameday],
		COUNT(applicationkey) AS TotalAccountsOnSameday,
		SUM(FirstLoadAmount) as sumfirstload,
		AVG(FirstLoadAmount) as avgfirstload
--INTO	#account_creation_sameday
FROM	[dw].[DailyApplicationsToAccounts] (nolock)
WHERE	registrationdatekey = 20160523
and		datediff (dd,registrationdate,creationdate) = 0
GROUP	BY registrationdatekey,[product],[type], RegistrationDate
go

--creation after reg date

create view dw.AccountsAfterRegdate 
as
--drop	table #afterregdate
--go
SELECT	[product] as [product_after_regdate],
		[type] as [type_after_regdate],
		[RegistrationDate] as AccountDate_after_regdate,
		COUNT(applicationkey) AS TotalAccountsAfterRegdate/*,
		SUM(FirstLoadAmount) as sumfirstload_after_regdate,
		AVG(FirstLoadAmount) as avgfirstload_after_regdate*/
--INTO	#afterregdate
FROM	[dw].[DailyApplicationsToAccounts] (nolock)
WHERE	registrationdatekey = 20160427
AND		DATEDIFF(dd,registrationdate,CreationDate) > 0
GROUP	BY registrationdatekey,[product],[type], RegistrationDate


create view dw.AccountsCreatedFromPreviousRegDate
as
--accounts created on this date but registered on date previous 
--drop table #accountcreated_frompreviousregdate
--go
SELECT	[product] as [product_previousregdate] ,
		[type] as [type_previousregdate] ,
		[creationdate] as [creationdate_previousregdate],
		COUNT(applicationkey) AS AccountsCreatedFromPreviousRegDates
		--into #accountcreated_frompreviousregdate
FROM	[dw].[DailyApplicationsToAccounts] (nolock)
WHERE	creationdatekey >= 20160427
AND		DATEDIFF(dd,registrationdate,CreationDate) > 0
GROUP	BY [product],[type],[creationdate]

----------------------------------------------------------------------------------------------------
select * from dw.AllApplications 
select * from dw.AccountsCreatedSameDay 
select * from dw.AccountsAfterRegdate
select * from dw.AccountsCreatedFromPreviousRegDate


select	AA.*,
		BB.TotalAccountsOnSameday,
		bb.SumFirstLoad,
		bb.AvgFirstLoad,
		cc.TotalAccountsAfterRegDate,
		dd.AccountsCreatedFromPreviousRegDates
from	dw.AllApplications aa

left	outer join	dw.AccountsCreatedSameDay bb
on		aa.[registrationdate] = bb.reg_datesameday
and		aa.[type] = bb.[type_sameday]

left	outer join  dw.AccountsAfterRegdate  cc
on		aa.[registrationdate] = cc.accountdate_after_regdate 
and		aa.[type] = cc.type_after_regdate

left	outer	join dw.AccountsCreatedFromPreviousRegDate dd
on		aa.[registrationdate] = dd.[creationdate_previousregdate]
and		aa.[type] = dd.[type_previousregdate]
order	by registrationdate, product asc


