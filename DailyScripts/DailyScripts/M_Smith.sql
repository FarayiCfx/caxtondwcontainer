---add these columns first:

alter	table dbo.fisTALKSHEETALLLOADS 
ADD		Card_Product varchar(100)

update	dbo.fisTALKSHEETALLLOADS  
set		card_product = case left(CardNumber,6)	when '459500' then 'GBP RETAIL'
										when '459501' then 'USD RETAIL'
										when '459499' then 'EUR RETAIL'
										when '459566' then 'EUR CORP'
										when '459567' then 'GBP CORP'
										when '459568' then'USD CORP'
										END

										
alter	table dbo.fisTALKSHEETALLLOADS 
ADD		Card_currency varchar(20)
go

update	dbo.fisTALKSHEETALLLOADS  
set		Card_currency = case left(CardNumber,6)	when '459500' then 'GBP'
										when '459501' then 'USD'
										when '459499' then 'EUR'
										when '459566' then 'EUR'
										when '459567' then 'GBP'
										when '459568' then'USD'
										END
										GO
----
IF	OBJECT_ID('tempdb..#temp1') IS NOT NULL
    DROP TABLE #temp1
GO
with cte1 as 
(select		cust.CustomerKey, 
			cust.FirstName, 
			cust.LastName,	
			cust.DBCARDSContact_id, 
			cust.DBCARDSAccount_Id, 
			cust.DBCARDSParent_id,						
			cust.AccountTypeRefDerived,
			acc.Account_full_name,
			cast(acc.insertdate as date) as CompanyInsertDate
from		dw.DimCustomerReference cust
join		[dw].[DimAccountTalksheet] acc
on			cust.DBCARDSAccount_Id = acc.Account_id
where		DBCARDSAccount_Id <> 0)
select		sum(ld.TransactionAmount) as sumloads_gbp,
			account_full_name,
			CARD_PRODUCT,
			CARD_CURRENCY,
			CompanyInsertDate
into		#temp1
from		cte1 
join		dbo.fistalksheetallloads ld
on			cte1.customerkey = ld.customerkey
where		ld.transactiondate > '2014-01-01 00:00:00'
and			ld.card_product in ('EUR CORP','GBP CORP','USD CORP')
group by	account_full_name ,CARD_PRODUCT,CARD_CURRENCY,CompanyInsertDate
order		by sumloads_gbp desc 
--select * from #temp1

--so on this side you need to filter by cardbin

--NON FUNDED CREDITS here 
IF	OBJECT_ID('tempdb..#temp2') IS NOT NULL
    DROP TABLE #temp2
go

with		cte1 as 
(select		cust.CustomerKey, 
			cust.FirstName, 
			cust.LastName,	
			cust.DBCARDSContact_id, 
			cust.DBCARDSAccount_Id, 
			cust.DBCARDSParent_id,						
			cust.AccountTypeRefDerived,
			acc.Account_full_name,
			cast(acc.insertdate as date) as CompanyInsertDate
from		dw.DimCustomerReference cust
join		[dw].[DimAccountTalksheet] acc
on			cust.DBCARDSAccount_Id = acc.Account_id
where		DBCARDSAccount_Id <> 0)
select		SUM(AMTTXN) AS sum_non_funded_credits,
			ACCOUNT_FULL_NAME,
			CARD_PRODUCT,
			CARD_CURRENCY,
			CompanyInsertDate
into		#temp2
from		cte1 
join		dbo.fistalksheetallloads ld
on			cte1.customerkey = ld.customerkey
where		ld.transactiondate > '2014-01-01 00:00:00'
and			ld.card_product in	('EUR CORP','GBP CORP','USD CORP')
AND			LOADTYPE =			'Non Funded Credit'
GROUP BY	ACCOUNT_FULL_NAME,
			CARD_PRODUCT,
			CARD_CURRENCY,companyinsertdate
			ORDER BY SUM_non_funded_credits DESC
			--select * from #temp2
---------------------------------------------------------------------------------------------------------------------------------------------------
IF		OBJECT_ID('tempdb..#final_Set') IS NOT NULL
DROP	TABLE #final_set
go

select	aa.*, datediff(mm,aa.companyinsertdate,getdate()) as MonthsOnOurBooks,
		isnull(bb.SUM_non_funded_credits,0) as SUM_non_funded_credits,
		isnull(	(cast(
		[dw].[UdfGetGBPValueFromFinanceExchangeRatesNoDate](sum_non_funded_credits,bb.card_currency)as decimal(10,2))),
			0)
		as sum_non_funded_credits_gbp,

		cast(
		aa.sumloads_gbp+
		
		isnull((
		[dw].[UdfGetGBPValueFromFinanceExchangeRatesNoDate](sum_non_funded_credits,bb.card_currency)),0) as decimal (10,2))as all_loads_in_gbp

		into #final_set

from	#temp1  aa 
full	outer join #temp2  bb
on		aa.account_full_name = bb.account_full_name
and		aa.card_product = bb.card_product
and		aa.card_currency = bb.card_currency
and		aa.CompanyInsertDate = bb.companyinsertdate
order	by all_loads_in_gbp desc

select * from #final_set order by all_loads_in_gbp desc

IF	 OBJECT_ID('tempdb..#top100') IS NOT NULL
DROP TABLE #top100
go
select	top 100 * 
into	#top100
from	#final_set aa
left	outer join sic_from_marketing bb
on		aa.account_full_name = bb.company_name
order	by all_loads_in_gbp DESC

select * from #top100
where account_full_name = 'Bili Managment Uk Ltd'

select	count(account_full_name) as count_,
		account_full_name 
from	#top100
group	by account_full_name
order	by 1 desc 



select *	from #final_Set aa
left		outer join sic_from_marketing bb
on			aa.account_full_name = bb.company_name
order		by all_loads_in_gbp DESC



-->run the following on caxton db1 

IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
GO
select	 identity(int, 1,1) as rownum ,
		cc.contact_id,aa.Account_short_name, crd.CardNumber,
		CardProduct = case left(crd.CardNumber,6)	when '459500' then 'GBP RETAIL'
										when '459501' then 'USD RETAIL'
										when '459499' then 'EUR RETAIL'
										when '459566' then 'EUR CORP'
										when '459567' then 'GBP CORP'
										when '459568' then'USD CORP'
										END
into	#temp1
from	contact cc (nolock)
join	Account aa (nolock)
on		cc.account_id = aa.Account_id
join	tblCfxCurrencyCardDetails crd
on		cc.contact_id = crd.Contact_ID
where crd.CardNumber is not NULL



select	count(rownum) as count_of_cards,
		account_short_name, 
		cardproduct
from	#temp1 
where	cardproduct is not NULL
group	by	account_short_name , 
			cardproduct
order	by	account_short_name
-----
----then spend location data is below



IF		OBJECT_ID('tempdb..#temp1Location') IS NOT NULL
DROP	TABLE #temp1Location
go
select	count(fc.FinAdvKey) as TransactionCount,
		sum(fc.AMTTXN_gbp) SumGBPValue,
		acc.Account_short_name,
		fc.TERMLOCATION, 
		fc.termcity ,
		fc.TERMTYPE
		into #temp1Location
from	dw.DimCustomerReference cust
join	dw.DimAccountTalksheet acc
on		cust.DBCARDSAccount_Id = acc.Account_id
join	fis.FactFinAdv fc
on		cust.CustomerKey = fc.customerkey
group	by	acc.Account_short_name,
			fc.TERMLOCATION,
			fc.TERMCITY

		select	* 
		from	#temp1Location aa
		left	outer join  sic_from_marketing sc
		on aa.account_short_name = sc.company_name
		order by account_short_name ,transactioncount