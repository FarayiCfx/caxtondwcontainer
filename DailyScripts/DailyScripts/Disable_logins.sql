--------------------------------------------------------------THE ACTUAL QUERY IS BELOW; THIS IS WHAT YOU SHOULD RUN, THE LINES ABOVE ARE JUST THE DISCOVERY QUERIES---- 


select * from SCY_USERS_BACKUP_PRE_CARD_MIGRATION_03_10_2016



--FIRST TAKE A BACKUP OF THE TABLE
SELECT *	INTO SCY_USERS_BACKUP_PRE_CARD_MIGRATION_03_10_2016 
FROM		SCY_USERS
GO
--THEN LOOK AT THE TABLE TO MAKE SURE ITS BEEN DONE --
SELECT * FROM SCY_USERS_BACKUP_PRE_CARD_MIGRATION_03_10_2016
;
--FIND OUT ALL OF THE CORP CARDS CONTACTS then set their login attempts to 99 so that spCFXWebValidateUser does not work, any login attempt above 3 will fail a login
with	cteCORPCARDS as (
select	contact_id ,cardnumber 
from	tblCfxCurrencyCardDetails (nolock)
where	left(cardnumber,6) = '459566'---EUR CORP
OR		left(cardnumber,6) = '459567'---GBP CORP
or		left(CardNumber,6) = '459568')---USD CORP

update	scy_users 
set		LOGINATTEMPTS = 99
WHERE	CONTACT_ID IN
(
select	distinct contact_id from cteCORPCARDS 
)
--(23345 row(s) affected)


--CHECK THE ONES THAT YOU HAVE UPDATED 
SELECT *	FROM SCY_USERS 
WHERE		LOGINATTEMPTS <> 99
ORDER		BY LASTLOGIN

--then set back the oxfam and red cross users 
update	scy_users 
set		LOGINATTEMPTS = 1
WHERE	CONTACT_ID IN
(
select  contact_id 
from	Contact
where	account_id in 
(select account_id 
from	account 
where	Account_short_name like '%Oxfam%'
or		Account_short_name like '%british red cross%')
)
--(186 row(s) affected)


--just in case you need to roll back the disable run this section 
with	cteCORPCARDS as (
select	contact_id ,cardnumber 
from	tblCfxCurrencyCardDetails (nolock)
where	left(cardnumber,6) = '459566'---EUR CORP
OR		left(cardnumber,6) = '459567'---GBP CORP
or		left(CardNumber,6) = '459568')---USD CORP

update	scy_users 
set		LOGINATTEMPTS = 1
WHERE	CONTACT_ID IN
(
select	distinct contact_id from cteCORPCARDS 
)


--------------------------------------this below group of queries is just the exploring \ finding out things query, the actual ones are below

select	top 10 * 
from	account where Account_short_name like '%tour construct%'

select  * 
from	Contact
where	account_id in 
(select account_id 
from	account 
where	Account_short_name like '%tour construct%')

select	* from syscomments where text like '%login%'

exec	sp_helptext 'spgetusers'

select	* from users

DECLARE @contactid bigint 

select	@ContactID = 296488 

IF		ISNULL(@ContactID,0) > 0
BEGIN

SELECT	* FROM tblcfxcurrencycarddetails WHERE Contact_ID = @ContactID
--SELECT * FROM tblcfxcurrencycarddetails WHERE ScyRegPKID IN
--(SELECT ScyRegPKID FROM tblcfxcurrencycarddetails WHERE Contact_ID = @ContactID)
SELECT	*	FROM SCY_RegExt WHERE pkiD IN 
			(SELECT ScyRegPKID FROM tblcfxcurrencycarddetails WHERE Contact_ID = @ContactID) 
SELECT		dbo.fndecrypt(password,'') Password, * FROM scy_Users WHERE contact_ID = @ContactID
--SELECT dbo.fndecrypt(CardPIN,'') CardPIN, * FROM tblcfxcurrencycarddetails WHERE contact_ID = @ContactID 
SELECT	ContactRef AS TsMainLoginNumber, (SELECT dbo.fndecrypt(password,'') Password FROM scy_Users WHERE contact_ID = @ContactID) AS 'PASSWORD', * FROM Contact WHERE Contact_ID = @ContactID
--SELECT * FROM tblCfxcurrencycard WHERE CurrencyCardID IN ( SELECT CurrencyCardDetailID FROM tblcfxcurrencycarddetails WHERE Contact_ID = @ContactID)
--SELECT * FROM Contact WHERE contactref = @ContactID
end
go

select top 10 * from SCY_USERS where contact_id = 296488

--set login attempts back to 1 to allow others to login 
update	scy_users set LOGINATTEMPTS = 1 
where	CONTACT_ID in (select  contact_id 
from	Contact
where	account_id in 
(select account_id 
from	account 
where	Account_short_name like '%tour construct%'))

update scy_users set active = 1 where CONTACT_ID = 288326

SELECT	* FROM tblcfxcurrencycarddetails WHERE Contact_ID = 288326

exec sp_depends 'SCY_USERS'

exec sp_spaceused 'tblcfxcurrencycarddetails'

select * from Contact_Email where email_address = 'jo.merson@rupertmerson.com'





