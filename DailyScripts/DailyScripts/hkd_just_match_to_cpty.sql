with cte1 as (
select [Name]
  +' - '
  +convert(varchar(24) ,bb.BookOpenDate,103)
  +' '
  +convert(varchar(24) ,bb.BookOpenDate,108) as NetBookName,
  aa.id as TradeId,
  mk.buyccyamount - aa.buyamount as totalprofit,
  aa.UserId, 
  aa.SellCcyCode, 
  aa.BuyCcyCode, 
  aa.BuyCcyClientTradingBookId as TradeTableBuyCcyClientTradingBookId,
  aa.BuyAmount,
  aa.SellAmount,
  aa.TradeProfit,
  aa.TradeDate,
  aa.TradeEndDate,
  datediff(dd,aa.TradeDate,aa.TradeEndDate) AS DaysToSettlement,
  aa.TradeReason,
  aa.isreversed,
  aa.isunwound,
  aa.BuyCcyClientTradingBookId,
  bb.id as ClientTradeBookId ,
  bb.name,
  bb.ccycode,
  mk.id as MarketTradeId,
  mk.bookname,
  mk.counterpartyrefno,
  mk.sellccy as MarcketTradeSellCCY,
  mk.sellccyamount as MarketTradeSellCCYAmount,
  mk.buyccy as MarketTradeBuyCCY,
  mk.buyccyamount  as MarketTradeBuyCCYAmount,
  mk.bookopendate
from currencybank.Trade aa (nolock)
join currencybank.ClientTradingBook bb (nolock)
on  aa.BuyCcyClientTradingBookId = bb.Id
--and  aa.SellCCyClientTradingBookId = bb.id
join CurrencyBank.MarketTrade mk (nolock)
on  bb.Name = mk.BookName
and  convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
and  aa.BuyCcyCode = mk.BuyCcy
and aa.sellccycode = mk.sellccy)
select * from cte1 
where tradeid = 1730007