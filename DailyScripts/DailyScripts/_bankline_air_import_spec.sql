select	* from	RBS.EOD_StagingTable
-->

-->
CREATE			TABLE RBS.EOD_StagingTable
(RowId			int identity (1,1),
RowData			nvarchar(255),
[FileName]		varchar(255),
DWDateInserted	datetime)
-->

-->
select  dbo.UFN_SEPARATES_COLUMNS(rowdata,1,'|') AS [Record Identifier],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,2,'|') AS [Sort Code],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,3,'|') AS [Account Number],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,4,'|') AS [Account Alias],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,5,'|') AS [Account Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,6,'|') AS [Account Currency] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,7,'|') AS [Date] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,8,'|') AS [Last nights ledger] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,9,'|') AS [Todays ledger] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,10,'|') AS [Last nights cleared] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,11,'|') AS [Todays cleared] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,12,'|') AS [Start of day ledger] ,
		dbo.UFN_SEPARATES_COLUMNS(rowdata,13,'|') AS [Start of day cleared],
		rowdata
 from	RBS.EOD_StagingTable
 where	left(rowdata,3) = 'BE|'
-->

-->
select  dbo.UFN_SEPARATES_COLUMNS(rowdata,1,'|') AS [Record Identifier],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,2,'|') AS [Sort Code],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,3,'|') AS [Account Number],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,4,'|') AS [Account Alias],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,5,'|') AS [Account Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,6,'|') AS	[Account Currency],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,7,'|') AS	[Account Type],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,8,'|') AS	[Bank Identifier Code],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,9,'|') AS	[Bank Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,10,'|') AS [Bank Branch Name],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,11,'|') AS [Transaction Date],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,12,'|') AS [Narrative Line 1],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,13,'|') AS [Narrative Line 2],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,14,'|') AS [Narrative Line 3],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,15,'|') AS [Narrative Line 4],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,16,'|') AS [Narrative Line 5],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,17,'|') AS [Transaction Type],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,18,'|') AS [Debit Value],
		dbo.UFN_SEPARATES_COLUMNS(rowdata,19,'|') AS [Credit Value],
		[FileName]
 from	RBS.EOD_StagingTable
 where	left(rowdata,2) = 'S|'
-->


CREATE	TABLE RBS.EOD_NonUsefulOutput
(RowId	int identity (1,1),
RowData nvarchar(255))

-->

CREATE	TABLE RBS.EODFile_BalanceRecord
(RowId	int identity  (1,1),
[Record Identifier] nvarchar(2) NULL,
[Sort Code] nvarchar(6) NULL,
[Account Number] nvarchar(34) NULL,
[Account Alias] nvarchar (35) NULL,
[Account Name] nvarchar(35) NULL,
[Account Currency] nvarchar(3) NULL,
[Date] nvarchar(15) NULL,
[Last nights ledger] nvarchar(17)NULL,
[Todays ledger] nvarchar(17) NULL,
[Last nights cleared] nvarchar(17) NULL,
[Todays cleared] nvarchar(17) NULL,
[Start of day ledger] nvarchar(17) NULL,
[Start of day cleared] nvarchar (17) NULL,
[FileName] varchar(255) NULL,
[DWDateInserted] datetime) 
-->

-->
DROP TABLE RBS.EODFile_StatementRecord
GO
CREATE TABLE RBS.EODFile_StatementRecord
(RowId int identity  (1,1),
[Record Identifier] nvarchar(2) NULL,
[Sort Code] nvarchar(6) NULL,
[Account Number] nvarchar(34) NULL,
[Account Alias] nvarchar (35) NULL,
[Account Name] nvarchar(35) NULL,
[Account Currency] nvarchar(3) NULL,
[Account Type] nvarchar(20) NULL,
[Bank Identifier Code] nvarchar(20) NULL,
[Bank Name] nvarchar(35) NULL,
[Bank Branch Name] nvarchar(27) NULL,
[Transaction Date] nvarchar(10) NULL,
[Narrative Line 1] nvarchar(25) NULL,
[Narrative Line 2] nvarchar(25) NULL,
[Narrative Line 3] nvarchar(25) NULL,
[Narrative Line 4] nvarchar(25) NULL,
[Narrative Line 5] nvarchar(25) NULL,
[Transaction Type] nvarchar(3) NULL,
[Debit Value] nvarchar(15) NULL,
[Credit Value] nvarchar(15) NULL,
[FileName] varchar(255) NULL,
[DWDateInserted] datetime)
--->


------------------------------------------------------------------------->
CREATE FUNCTION dbo.UFN_SEPARATES_COLUMNS(
 @TEXT      varchar(8000)
,@COLUMN    tinyint
,@SEPARATOR char(1)
)RETURNS varchar(8000)
AS
  BEGIN
       DECLARE @POS_START  int = 1
       DECLARE @POS_END    int = CHARINDEX(@SEPARATOR, @TEXT, @POS_START)

       WHILE (@COLUMN >1 AND @POS_END> 0)
         BEGIN
             SET @POS_START = @POS_END + 1
             SET @POS_END = CHARINDEX(@SEPARATOR, @TEXT, @POS_START)
             SET @COLUMN = @COLUMN - 1
         END 

       IF @COLUMN > 1  SET @POS_START = LEN(@TEXT) + 1
       IF @POS_END = 0 SET @POS_END = LEN(@TEXT) + 1 

       RETURN SUBSTRING (@TEXT, @POS_START, @POS_END - @POS_START)
  END
GO
-->