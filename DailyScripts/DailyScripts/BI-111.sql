-->on prod do this part 1:
select	aa.*, bb.CompanyName, bb.TalksheetMigrated
from	[dbo].[UserCorporateAssociation] aa 
join	corporateentities bb
on		aa.CorporateEntityId = bb.Id
where	talksheetmigrated = 1

-- then go to datawarehouse and do this part 2:
IF		OBJECT_ID('tempdb..#BI111') IS NOT NULL
DROP	TABLE #BI111
GO
select	aa.CustomerKey,bb.DBCARDSAccount_Id,cc.account_full_name,
		bb.ContactEmailAddress,FIS_VISA_CardLoadCount, FIS_VISA_TotalSpendGBP,FIS_VISA_TransactionCount,
		FIS_VISA_FirstLoadDate, 
		FIS_VISA_LastLoadDate, 
		FIS_VISA_LastSpendDate, 
		CAST(convert(varchar,FIS_VISA_LastSpendDate,112) as int) AS FIS_VISA_LastSpendDate_DATEKEY,
		aa.DWDateUpdated, 
		bb.AccountTypeRefDerived
INTO	dbo.BI111
from	dw.CustomerRFMAggregations aa
join	dw.DimCustomerReference bb
on		aa.CustomerKey = bb.CustomerKey
join	BI111_COMPANY_NAME cc
on		bb.DBCARDSAccount_Id = cc.account_id
where	bb.AccountTypeRefDerived = 'Corporate Card - Talksheet'
order	by FIS_VISA_LastSpendDate DESC
--545 263 rows 
select * from	#bi111 aa 

order by fis_visa_lastspenddate_datekey

--

SELECT	count(*) as  [ActiveClientsSpendingOnVisa_oct2016onwards]
FROM	#BI111 
where	fis_visa_lastspenddate_datekey > 20161001

SELECT	count(*) as InactiveVisa_ClientsLastSpent_Jul_Sep_2016  
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20160401 and 20160631

SELECT	count(*) as   InactiveVisa_ClientsLastSpent_Jan_March_2016 
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20160101 and 20160331

SELECT	count(*) as   InactiveVisa_ClientsLastSpent_Oct_Dec_2015 
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20151001 and 20151231


/*
SELECT	count(*) as  [ActiveClientSpendingOnVisa]
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20160701 and 20160931
order	by fis_visa_lastspenddate_datekey

SELECT	*  
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20160401 and 20160631
order	by fis_visa_lastspenddate_datekey

SELECT	*  
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20160101 and 20160331
order	by fis_visa_lastspenddate_datekey

SELECT	*  
FROM	#BI111 
where	fis_visa_lastspenddate_datekey between 20151001 and 20151231
order	by fis_visa_lastspenddate_datekey

SELECT	*  
FROM	#BI111 
where	fis_visa_cardloadcount = 0 

SELECT COUNT(*) AS [ActiveClientsSpendingDownBalance] FROM #BI111 WHERE FIS_VISA_LASTSPENDDATE 
 */

--exec [dw].[MergeFISLoadStatsToAggregateTable_FromTalkSheet]
--GO
--exec [dw].[MergeFISSpendStatsToAggregateTable]
--GO

---truncate  table FIS.TalkSheetAllLoads

exec sp_who2 active

dbcc inputbuffer (110)


select * from dbo.BI111_COMPANY_NAME

