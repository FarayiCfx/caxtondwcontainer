--do backup of corp entities table first 

select * into corporateentities_backup06_06_17
from corporateentities
--5543

--Look at the result set 
select	aa.*,
		bb.salescontactref,
		bb.accountcontactref , 
		bb.registrationnumber
from	[dbo].[UpdateFireBirdWithCrm_part1]  aa join corporateentities bb
on		aa.[companyname] = bb.[companyname]

--update the data 

update	aa
set		aa.registrationnumber = bb.registrationnumber,
		aa.salescontactref = bb.[salesman crm],
		aa.accountcontactref = bb.[CRM ACCOUNT MANAGER]
from	corporateentities aa
join	[UpdateFireBirdWithCrm_part1] bb
on		aa.[companyname] = bb.[companyname]
--190 rows 


-->everything went bad - need to roll back this update ofr one field 
update	aa
set		aa.accountcontactref = bb.[accountcontactref]
from	corporateentities aa
join	corporateentities_backup06_06_17 bb
on		aa.[companyname] = bb.[companyname]
where	aa.companyname in (select companyname from [UpdateFireBirdWithCrm_part1])