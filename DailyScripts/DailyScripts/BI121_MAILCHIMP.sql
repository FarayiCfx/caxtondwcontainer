
-------------------------------------------------------------------------------------------
-->so this is my filter
select		count(customerkey) as custkey_,
			AccountTypeRef,
			AccountTypeRefDerived 
from		dw.DimCustomerReference 
where		(AccountTypeRef = 'retail' 
		or	AccountTypeRef is null)
		and AccountTypeRefDerived 
		not in ('Corporate Money transfer - Talksheet'
				,'Retail Money Transfer - Talksheet'
				,'Retail Card - Talksheet'
				,'Unknown talksheet'
				,'Corporate Card - Talksheet')
GROUP		by AccountTypeRef,AccountTypeRefDerived 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
alter view dw.[BI-121 MailChimp]
as
select	cust.customerkey,cust.id as FireBirdCustId,
		cust.FirstName, 
		cust.LastName, 
		cust.ContactEmailAddress, 
		DATEDIFF(yy,cust.DateOfBirth,getdate())as Age, 
		cust.AddressLine1, cust.AddressLine2, cust.County, cust.Postcode,
		--cust.MigrationStatus, cust.WantToSendMoney,
		IpMigrationStatus = case when isnull(cust.MigrationStatus,'0') <> '0'  and cust.WantToSendMoney =  1 then 'IpMigration' else '' end, 
		cust.AccountTypeRefDerived, cust.SourceCreationDate, 
		rfm.VALITOR_MASTERCARD_CardLoadCount, 
		rfm.VALITOR_MASTERCARD_LoadTotalGBP, 
		rfm.VALITOR_MASTERCARD_FirstLoadDate, 
		rfm.VALITOR_MASTERCARD_LastLoadDate,
		rfm.BuyAndHold_TradeCount, 
		rfm.BuyAndHold_Tradeprofit_GBP_Total, 
		rfm.BuyAndHold_FirstDate, 
		rfm.BuyAndHold_LastDate,
		rfm.BuyAndSend_TradeCount, 
		rfm.BuyAndSend_Tradeprofit_GBP_Total,
		rfm.BuyAndSend_FirstDate,
		rfm.BuyAndSend_LastDate
from	dw.DimCustomerReference cust
join	dw.CustomerRFMAggregations rfm
on		cust.CustomerKey = rfm.CustomerKey
where	(cust.AccountTypeRef = 'retail' 
		or	cust.AccountTypeRef is null)
		and cust.AccountTypeRefDerived 
		not in ('Corporate Money transfer - Talksheet'
				,'Retail Money Transfer - Talksheet'
				,'Retail Card - Talksheet'
				,'Unknown talksheet'
				,'Corporate Card - Talksheet')

				grant select on dw.[BI-121 MailChimp]to cfx_datareader


				select * FROM dw.[BI-121 MailChimp]
				where ipMigrationStatus <>''


				select * from firebird.factcurrencybanktrade
				where tradereason = 'Guranteed Buy-Back'