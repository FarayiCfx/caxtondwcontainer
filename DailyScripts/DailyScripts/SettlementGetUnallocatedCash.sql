drop procedure if exists settlement.GetUnAllocatedCash
go
create procedure settlement.GetUnAllocatedCash @startdate datetime , @enddate datetime 
as 
with cte1 as (
select	id as StatementId
		,aa.accountnumber
		, aa.AccountCurrency
		,aa.CreditValue 
		,aa.Narrative1
		,aa.Narrative2
		,aa.Narrative3
		,aa.Narrative4
		,aa.TransactionType
		,aa.TransactionDate
		,DateMatched
		,aa.Accounttransactionid
		,aa.Voided
from	Settlement.RbsStatement aa (nolock)
join	[Settlement].[AccountInclusionListCashAllocations] bb
on		aa.accountnumber = bb.accountnumber 
where	voided = 0
and		transactiondate between @startdate and @enddate
and		transactiontype not in ('bln','chg','d/d','scr','bgc')
)

select	*	from cte1 
where		narrative1 not like 'L1%'
or			narrative1 not like 'N1%'
or			narrative1 not like 'n2%'
or			narrative1 not like 'valitor%'
or			narrative1 not like '%COLCFX%'
or			narrative3 not like 'credorax%'
or narrative1 not like '%CAXTON FX LTD CA%' 
or narrative1 not like '%CAXTON FX LTD CL%'
or narrative1 not like '%CAXTON FX CLIENT M%'
or narrative1 not like '%CAXTON FX LTD CLIE%'
or narrative1 not like '%CAXTON FX LTD TR%'
or narrative1 not like '%CM%'
or narrative1 not like '%CAXTON FX LTD%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%CAXTON FX LIMITED%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%CAXTON FX LTD TRAD%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%CAXTON FX LTD TR%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%Lloyds%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%Erste%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%Saxo%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%Fraud%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 
or narrative1 not like '%Card to trading%' and accountcurrency = 'GBP' and [transactiontype] = 'EBP' 

/*
create nonclustered index ncc_trandate_type on  settlement.rbsstatement(transactiondate,TransactionType)
include( id ,accountnumber, AccountCurrency,CreditValue ,Narrative1,Narrative2,Narrative3,Narrative4,DateMatched,Accounttransactionid,Voided)
*/

