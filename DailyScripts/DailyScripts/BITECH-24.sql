

DROP	VIEW [UnAllocatedFunds]
GO

create	view [UnAllocatedFunds]
as
WITH	CTE1 AS (
select	id as StatementId,accountnumber AccountCurrency,CreditValue as Amount,Narrative1,Narrative2,Narrative3,Narrative4,
		TransactionType,TransactionDate,DateMatched,Accounttransactionid,Voided
from	Settlement.RbsStatement aa (nolock)
where	voided = 0
--and	accounttransactionid = 0
and		narrative1 not like 'wst%'
and		narrative1 not like 'SWTCXTNFX%'
and		narrative1 not like 'L1%'
and		narrative1 not like 'N1%'
and		narrative1 not like 'N2%'
and		narrative1 <> 'CAXTONFX'
and		narrative1 not like 'CAXTONFXLTD%' 
AND		narrative1 not like 'CAXTONFX LTD%'
AND		TRANSACTIONDATE BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND  DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) + 1, 0))
select	* from cte1 where transactiontype not in ('bln','chg','d/d','scr','bgc')



SELECT	DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 

select	top 10 * 
from	settlement.rbsstatement
where	cast(transactiondate as date) = '2017-05-01'
