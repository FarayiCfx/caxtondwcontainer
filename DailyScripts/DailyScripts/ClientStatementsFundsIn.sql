select	* from dw.dimcustomerreference where username = 'alamex@caxtonfx.com'

select	* from [FireBird].[DimCurrencyBankAccountSummary] where userid = 'alamex@caxtonfx.com'

select	* 
from	firebird.factcurrencybankaccounttransaction aa
where	aa.customerkey = 576011
and		externaltransref = 'trannsfer'

select	* from currencybank.currency

exec	GetAccountSummaryCreditsAndDebits  'alamex@caxtonfx.com'
---------------------------------------------------------------------------------------
drop procedure GetAccountSummaryCreditsAndDebits


alter		procedure GetAccountSummaryCredits 
@username	nvarchar(1024)
as
declare @customerkey bigint 

select	@customerkey = customerkey 
from	dw.dimcustomerreference 
where	username = @username 

select	--aa.Id,
		convert (varchar(30), aa.TransactionDatetime,104) as TransactionDate,
		aa.id as AccounTransactionId,
		aa.Amount,
		aa.Cleared,
		aa.[Status],
		aa.ExternalTransactionId,
		aa.ExternalTransRef,
		curr.buyccycode as Currency,
		ISNULL(bb.Narrative1,'') AS Narrative1
		--bb.Accounttransactionid,
		--bb.Settlementtype as RbsSettlementType 
from	firebird.factcurrencybankaccounttransaction aa
left	outer join	settlement.rbsstatement bb 
on		aa.externaltransactionid = bb.id
and		aa.externaltransref = 'rbs'
join	dw.dimcustomerreference dw
on		dw.customerkey = aa.customerkey 
join	[FireBird].[DimCurrencyBankAccountSummary] summ
on		aa.accountsummaryid = summ.id 
join	currencybank.currency curr
on		summ.currencyid = curr.id
where	aa.customerkey = @customerkey 
and		amount > 0
order	by aa.TransactionDatetime desc

---------------------------------------------------------------------------------------------------------------

alter		procedure GetAccountSummaryDebits
@username	nvarchar(1024)
as
declare @customerkey bigint 

select	@customerkey = customerkey 
from	dw.dimcustomerreference 
where	username = @username 
select	--aa.Id,
		convert (varchar(30), aa.TransactionDatetime,104) as TransactionDate,
		aa.Id as AccountTransactionId,
		aa.Amount,
		aa.Cleared,
		aa.[Status],
		aa.ExternalTransactionId,
		aa.ExternalTransRef,
		curr.buyccycode as Currency,
		ISNULL(bb.Narrative1,'') AS Narrative1 , 
		isnull (ben.[name],'') as BeneficiaryName ,
		isnull(trd.tradereason,'') as TradeReason
		--bb.Accounttransactionid,
		--bb.Settlementtype as RbsSettlementType 
from	firebird.factcurrencybankaccounttransaction aa
left	outer join	settlement.rbsstatement bb 
on		aa.externaltransactionid = bb.id
and		aa.externaltransref = 'rbs'
join	dw.dimcustomerreference dw
on		dw.customerkey = aa.customerkey 
join	[FireBird].[DimCurrencyBankAccountSummary] summ
on		aa.accountsummaryid = summ.id 
join	currencybank.currency curr
on		summ.currencyid = curr.id
left	outer join [FireBird].[FactPaymentTransfer] trf
on		aa.externaltransactionid = trf.id 
and		aa.externaltransref = 'transfer'
left outer join	[FireBird].[DimPaymentBeneficiary] ben
on		trf.beneficiaryid = ben.id
left outer join [firebird].[factcurrencybanktrade] trd
on		aa.externaltransactionid = trd.id
and		aa.externaltransref = 'trade'
where	aa.customerkey = @customerkey  
and		amount < 0
order	by aa.TransactionDatetime desc



exec GetAccountSummaryCredits 'alamex@caxtonfx.com'

exec GetAccountSummaryDebits 'alamex@caxtonfx.com'

exec GetTradesByUserId 'alamex@caxtonfx.com'


select * from firebird.factcurrencybanktrade  where id = 1642490

select * from firebird.factpaymenttransfer where id = 490909

exec sp_who2 active



select top 100 [username] from dw.dimcustomerreference (nolock) where id <> '0' 
order by customerkey desc

exec sp_who2 active
