select * from  [staging].[finadv]

IF		OBJECT_ID('[staging].[finadv]', 'U') IS NOT NULL
DROP	TABLE [staging].[finadv]; 
go
create	table [staging].[finadv]
(
MTID int null,
LOCALDATE datetime null,
LOCALTIME int null,
TLOGID int null,
ORGTLOGID int null,
ITEMID int null,
ORGITEMID int null,
PAN bigint null,
CARDID bigint null,
CRDPRODUCT nvarchar (255) null,
PROGRAMID nvarchar(255) null,
BRNCODE nvarchar (255) null,
TXNCODE int null,
TXNSUBCODE int null,
CURTXN int null ,
AMTTXN decimal (38,0) null,
AMTFEE decimal (38,0) null,
AMTTXNCB decimal (38,0),
CURSET bigint null, 
RATESET decimal (38,0) null,
AMTSET decimal (38,0) null,
BILLAMT decimal (38,0),
ACCCUR int null, 
ACCNO bigint null, 
ACCTYPE bigint null,
BILLCONVRATE decimal (38,0) null,
APPROVALCODE nvarchar (255) null,
CORTEXDATE datetime null,
STAN int null,
RRN bigint null,
TERMCODE nvarchar(255) null,
CRDACPTID nvarchar(255) null,
TERMLOCATION nvarchar(255) null,
TERMSTREET nvarchar(255) null,
TERMCITY nvarchar(255) null,
TERMCOUNTRY nvarchar(255) null,
[SCHEMA] nvarchar(255) null,
ARN decimal (38,0) null,
FIID nvarchar(255) null,
RIID nvarchar(255) null,
REASONCODE int null,
CHIC nvarchar (255) null,
CHAC int null,
CHP int null,
CP int null,
CDIM nvarchar(255) null,
CHAM int null,
CHA int null,
TVR nvarchar(255) null,
MSGSRC int null,
RCC int null,
MCC bigint null,
CBACKIND nvarchar(255) null,
TXNDATE datetime null,
TXNTIME int null,
TERMTYPE nvarchar(255) null,
CTXDATELOCAL datetime null,
CTXTIMELOCAL int null,
AIID bigint null,
DLVCYCLE int null,
ACTIONCODE int null,
RSPCODE int null,
[FileName] varchar (100),
[Dateinserted]  datetime null
)
go
alter table [staging].[finadv] add constraint df_getdate_finadv default getdate() for [DateInserted]
go


select * from [staging].[finadv]
