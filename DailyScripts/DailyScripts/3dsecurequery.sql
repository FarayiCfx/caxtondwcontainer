select	top 10 * from dw.factapplicants 

--query for applications 
select	count(applicationkey) as count_of_applications,
		[affiliatecode],registrationdatekey 
from	dw.factapplicants 
where	registrationdatekey > 20160519
group	by [affiliatecode],registrationdatekey 
order	by registrationdatekey,affiliatecode asc 

--query for account creation 
select	count(applicationkey) as count_of_successfullaccountcreation,
		[affiliatecode],registrationdatekey 
from	dw.factapplicants 
where	registrationdatekey > 20160519
and		customerkey > 0
group	by [affiliatecode],registrationdatekey 
order	by registrationdatekey,affiliatecode asc 

select	top 10 * from valitor.factpancreatedextract
where	walletid in (250263)
order	by 1 desc 


select * from dw.dimcustomerreference where customerkey = 140265

exec sp_spaceused 'valitor.factpancreatedextract'

drop view dw.[3DSecurePanCustomerDetails]

create view dw.[3DSecurePanCustomerDetails]
as
select	aa.customerkey,aa.title,aa.firstname,aa.lastname,coalesce(aa.username,aa.contactemailaddress) as [email],
		primarytelephone,bb.walletid,bb.panid,aa.sourcecreationdate as accountcreationdate, bb.timeofcreation as pancreationdatetime
from	dw.dimcustomerreference aa (nolock)
join	valitor.factpancreatedextract bb (nolock)
on		aa.customerkey = bb.customerkey 
order	by aa.customerkey desc

--275 416 rows yesterday 
--275 846 rows today 


select top 10 * from dw.dimvalitorwallet


select * from  dw.[3DSecurePanCustomerDetails]
order by customerkey desc 
