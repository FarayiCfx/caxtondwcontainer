use [FirebirdFx-prod]
go

-->check the EU member states table
select * from EU_Member_states_28

-->start by counting everything first (for the relevant month and year)
select  * 
from	valitor.pan (nolock)
where	expiry = 1801
order	by 1 desc ;

select  * 
from	valitor.pan (nolock)
where	expiry = 1802
order	by 1 desc ;

-->then retrieve the data set from production 
-->filtering by card status as active
with	cte1 as (
select  valitorpanid
		,CC.ID
		,maskedpan
		,Expiry
		,isnull(title,'') as title
		,firstname
		,lastname
		,Address1
		,isnull(address2,'') as address2
		,city
		,isnull(county,'') as county
		,postcode
		,cc.Country
		,ContactEmail = case isnull(ContactEmail,'') when '' then username else ContactEmail end
		,cardstatuscode
		,CardStateDescription =    case cardstatuscode when 'V' then 'Active card'
                                                    when 'U' then 'Pending production'
                                                    when 'M' then 'Pending picture'
                                                    when 'F' then 'In production'
                                                    when 'P' then 'In transit'
                                                    when 'A' then 'Blocked'
                                                    when 'L' tHen 'Terminated'
                                                    end
from	valitor.pan aa (nolock)
join	valitor.wallet bb (nolock)
on		aa.WalletId = bb.id
join	AspNetUsers cc (nolock)
on		bb.accountid = cc.username
where	aa.expiry in (1801,1802)
and		cardstatuscode in ('p','u','m','f')
 )
select	* 
from	cte1 
WHERE	COUNTRY IN (SELECT CODE FROM EU_Member_states_28)  -->make sure the  countrcode is part of the eea area 
OR		COUNTRY IN (SELECT countryname FROM EU_Member_states_28)  --> or the country name itself 

-------------------------------------



with	cte1 as (
select  valitorpanid
		,CC.ID
		,maskedpan
		,Expiry
		,isnull(title,'') as title
		,firstname
		,lastname
		,Address1
		,isnull(address2,'') as address2
		,city
		,isnull(county,'') as county
		,postcode
		,cc.Country
		,ContactEmail = case isnull(ContactEmail,'') when '' then username else ContactEmail end
		,cardstatuscode
		,CardStateDescription =    case cardstatuscode when 'V' then 'Active card'
                                                    when 'U' then 'Pending production'
                                                    when 'M' then 'Pending picture'
                                                    when 'F' then 'In production'
                                                    when 'P' then 'In transit'
                                                    when 'A' then 'Blocked'
                                                    when 'L' tHen 'Terminated'
                                                    end
from	valitor.pan aa (nolock)
join	valitor.wallet bb (nolock)
on		aa.WalletId = bb.id
join	AspNetUsers cc (nolock)
on		bb.accountid = cc.username
where	aa.expiry in (1801,1802)
and		cardstatuscode in ('p','u','m','f')
 )
select	* 
from	cte1 
WHERE	COUNTRY IN (SELECT CODE FROM EU_Member_states_28)  -->make sure the  countrcode is part of the eea area 
OR		COUNTRY IN (SELECT countryname FROM EU_Member_states_28) 

exec sp_who2 active

exec sp_spaceused 'valitor.pan'

