--this first lot of queries is just a discovery step, to see how things are in the table
--the main thing you are looking for is that only the dates that you want are in there 
select count(*) from [dbo].[Lost_calls]
select count(*) from all_calls

select * from [dbo].[Lost_calls]
select * from [dbo].[all_calls]

-------------------------------------------------------------------------------------------------
select	count(caller) as count_allcalls, caller as [caller_id_all]
from	all_calls
group	by [caller]
order	by 1 desc

select	count(caller) as count_, [caller]
from	lost_calls
group	by [caller]
order	by 1 desc
--okay so if we use two cte's

--------the actual work is below ----------------------------------------------

IF OBJECT_ID('tempdb..#all_calldata') IS NOT NULL DROP TABLE #all_calldata
GO
with	cteAllCalls as(
select	count(caller) as count_allcalls, caller as [caller_id_all]
from	all_calls
group	by [caller]
),
cteLostCalls as(
select	count(caller) as count_lostcalls,caller as [caller_id_lost]
from	lost_calls
group	by [caller])

select	*	
into	#all_calldata
from	cteallcalls 
left	outer join ctelostcalls
on		cteallcalls.caller_id_all = ctelostcalls.caller_id_lost

select * from #all_calldata
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

--send this query to Alana in Excel
with		cte1 as (
select		*,'0'+caller_id_lost as fullnumber	
from		#all_calldata
where		count_lostcalls is not null
and			count_allcalls = count_lostcalls
/*and count_lostcalls = 1*/)
select		cte1.count_allcalls,cte1.count_lostcalls,cte1.caller_id_lost , ''''+fullnumber as fullnumber,
			isnull(bb.UserName,'no match in database') as username,
			isnull(bb.ContactEmailAddress,'no match in database') as email, 
			isnull(bb.FirstName,'no match in database') as firstname,
			isnull(bb.LastName,'no match in database') as lastname 
			--INTO #STATSTABLE
from	cte1
left	outer join dw.DimCustomerReference bb
on		cte1.fullnumber = bb.PrimaryTelephone
order by 1 desc


--Send this one to Amanda
with	cte1 as (
select		*,'0'+caller_id_lost as fullnumber	
from		#all_calldata
where		count_lostcalls is not null
and			count_allcalls = count_lostcalls
and count_lostcalls = 1)
select		cte1.count_allcalls,cte1.count_lostcalls,cte1.caller_id_lost , ''''+fullnumber as fullnumber,
			isnull(bb.UserName,'no match in database') as username,
			isnull(bb.ContactEmailAddress,'no match in database') as email, 
			isnull(bb.FirstName,'no match in database') as firstname,
			isnull(bb.LastName,'no match in database') as lastname
from	cte1
left	outer join dw.DimCustomerReference bb
on		cte1.fullnumber = bb.PrimaryTelephone
order by 1 asc


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--answered calls = appear in all calls not in lost
select * from #all_calldata where count_lostcalls is null

--lost calls never answered once = all lost calls that appear in both lists with the same count of attempts
select *	from #all_calldata
where		count_lostcalls is not null
and			count_allcalls = count_lostcalls
and			count_lostcalls = 1
order by 1 

--lost calls never answered called more than once  = all lost calls that appear in both lists with the same count of attempts
select *	from #all_calldata
where		count_lostcalls is not null
and			count_allcalls = count_lostcalls
and			count_lostcalls > 1
order by 1 

--lost calls that eventualy got answered = number appears in lost but then appears in all with a different count (important distinction)
select *	from #all_calldata
where		count_lostcalls is not null
and			count_allcalls <> count_lostcalls
order by 1 

--examples
select * from all_calls where caller = '7780371167'

select * from lost_calls where caller = '7780371167'