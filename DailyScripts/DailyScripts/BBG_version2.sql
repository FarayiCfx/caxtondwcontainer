alter view BBG_Version2
as
with	cte1 as (
select	UserOfferAssociationId
		,cast ([BUYBACK-BUY-VALUE] as float) as [BUYBACK-BUY-VALUE]
		,cast ([BUYBACK-BUY-RATE] as float) as [BUYBACK-BUY-RATE]
		,[BUYBACK-BUY-CCY]
		,cast([BUYBACK-FEE] as float) as [BUYBACK-FEE]
from
(select	userofferassociationid,code,value
from	userofferredemptionmetadata  (nolock)
where	offercatalogid = 17) 
as sourcetable
pivot(
min(value) 
for code in 
([BUYBACK-BUY-VALUE],[BUYBACK-BUY-RATE],[BUYBACK-BUY-CCY],[BUYBACK-FEE])
)as pivottable),
cte2 as (
select	aa.email
		,Redeemed = case  when aa.ExecutionDate is not null then 1 else 0 end 
		,Expired = case when aa.DateExpired is not null then 1 else 0 end
		,cast(aa.Datecreated as Date) as DateCreated
		,cast(convert(char(8), DateCreated, 112) as int) DateCreatedInteger
		,cast(RedemptionStartPeriod as Date) RedemptionStart
		,cast(RedemptionEndPeriod as Date) RedemptionEnd
		,cast(ExecutionDate as Date) as ExecutionDate
		,cast(DateExpired as Date) as DateExpired
		,datediff(d,DateCreated,RedemptionStartPeriod) as DaysBetweenCreationAndRedemption
		,cte1.*
		,[BUYBACK-BUY-VALUE]/[BUYBACK-BUY-RATE] as GBPValue
		,RedemptionValue = case when aa.executiondate is not null then  [BUYBACK-BUY-VALUE] else 0 end
		,RedemptionGBPValue = case when aa.executiondate is not null then [BUYBACK-BUY-VALUE]/[BUYBACK-BUY-RATE] else 0 end
from	userofferassociation aa (nolock)
join	cte1 on aa.id = cte1.userofferassociationid)

select * from cte2  
/*
select	DateCreated
		,[BUYBACK-BUY-CCY]
		,sum([BUYBACK-BUY-VALUE]) as SumTotalBought
		,sum(GBPValue) as SUMGBPValue
		,count(userofferassociationid) as [CountOfOffersBought]
		,datename(month,redemptionstart) as [RedemptionMonth]
		,datename(year,redemptionstart) as [RedemptionYear]
		,RedeemedStatus
		,ExpiredStatus
from	cte2
group	by	datename(month,redemptionstart)
			,datename(year,redemptionstart)
			,DateCreated
			,[BUYBACK-BUY-CCY]
			,RedeemedStatus
			,ExpiredStatus
--order		by DateCreated,[buyback-buy-ccy] asc
			go
*/

select 

with cte1 as(
select	sum(RedemptionGBPValue) as RedemptionGBPValue
		,datepart(month,DateCreated) as Month_
		,datepart(year,DateCreated) as Year_
		,datepart(day,datecreated) as dAY_
from	BBG_Version2
group	by 	datepart(month,DateCreated) 
		,datepart(year,DateCreated) 
		,datepart(day,datecreated) )
		select	* 
		from	cte1 
		where	month_ = 8
		order	by year_


