with cteCards as (
select	userid,
		claimtype,
		claimvalue
from	[AspNetUserClaims] (nolock)
where	claimtype = 'AccountType'
and		claimvalue like '%PortalCards'),

cteTransfer as (select	userid,
		claimtype,
		claimvalue
from	[AspNetUserClaims] (nolock)
where	claimtype = 'AccountType'
and		claimvalue like '%PortalTransfer')

select	[companyname]
		,bb.email
		,firstname
		,lastname
		,dd.claimvalue as [HasCards?]
		,ee.claimvalue as [HasMoneyTransfer?]
from	corporateentities aa 
join	usercorporateassociation bb
on		aa.id = bb.corporateentityid 
join    aspnetusers cc
on		bb.email = cc.email
left	outer join cteCards dd
on		cc.id = dd.userid 
left	outer join cteTransfer ee
on		cc.id = ee.userid 


-----------------------------------------------------------------------------------------------------
select top 10 * from [usercorporateassociation]
select top 10 * from [aspnetusers] 
select top 10 * from [dbo].[CorporateEntities]


