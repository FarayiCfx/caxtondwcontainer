  exec sp_who2 active
  SELECT * FROM #knightmove

  IF OBJECT_ID('tempdb..#knightmove') IS NOT NULL
  DROP TABLE #knightmove
  go
  CREATE TABLE #knightmove 
  (RowId INT IDENTITY (1,1),
  Value char(15),
  XAxis int,
  YAxis int,
  TotalCoordinate int)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('A',1,1)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('B',2,1)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('C',3,1)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('D',4,1)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('E',5,1)

  --ROW 2 
  insert into #knightmove (Value,XAxis,YAxis)
  values ('F',1,2)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('G',2,2)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('H',3,2)

 insert into #knightmove (Value,XAxis,YAxis)
 values ('I',4,2)

 insert into #knightmove (Value,XAxis,YAxis)
 values ('J',5,2)

  --ROW 3
  insert into #knightmove (Value,XAxis,YAxis)
  values ('K',1,3)

  insert into #knight (Value,XAxis,YAxis)
  values ('L',2,3)

  insert into #knightmove (Value,XAxis,YAxis)
  values ('M',3,3)

 insert into #knightmove (Value,XAxis,YAxis)
 values ('N',4,3)

 insert into #knightmove (Value,XAxis,YAxis)
 values ('O',5,3)

 --ROW 4
 insert into #knightmove (Value,XAxis,YAxis)
 values (1,2,4)

 insert into #knightmove (Value,XAxis,YAxis)
 values (2,3,4)

 insert into #knightmove (Value,XAxis,YAxis)
 values (3,4,4)

 

 with cte1 as (
 --valid 1 - (horizontal) minus one left and minus two down 
 select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.XAxis -1 = r2.XAxis 
 and	r1.YAxis = r2.yAxis  - 2
 union all 
  --valid 2 - (horizontal) minus one left and + two up 
 select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.XAxis -1 = r2.XAxis 
 and	r1.YAxis = r2.yAxis  + 2
 union all 
 --valid 3 (horizontal) one right and + two down 
  select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value  in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.XAxis +1 = r2.XAxis 
 and	r1.YAxis = r2.yAxis  - 2

 union ALL
  --valid 4 (one horizontal two vertical)  
  select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.XAxis +1 = r2.XAxis 
 and	r1.YAxis = r2.yAxis  + 2
 union all 
 ----------------------------------------------------------------
 ---valid 5 (vertical one down two right)
   select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.YAxis +1  = r2.yAxis 
 and	r1.XAxis  +2 = r2.XAxis 
 union ALL
  ---valid 6 (vertical one down two left)
   select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value  in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.YAxis +1  = r2.yAxis 
 and	r1.XAxis  -2 = r2.XAxis 
 UNION all
   ---valid 6 (vertical one up two left)
   select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.YAxis -1  = r2.yAxis 
 and	r1.XAxis  -2 = r2.XAxis 

 union all
   ---valid 7 (vertical one up two right)
   select r1.value as value1,
		r2.value as value2 ,
		IsVowel = case when r2.value in ('A','E','I','O')
		THEN 1 ELSe 0 END,
		r1.XAxis as r1axis,
		r2.XAxis as r2xaxis,
		r1.yaxis as r1yaxis,
		r2.yaxis as r2yaxis 
 from	#knightmove r1
 join	#knightmove r2 
 on		r1.YAxis -1  = r2.yAxis 
 and	r1.XAxis  +2 = r2.XAxis )
 select * into --insert valid moves into this table  
 #intermediate
 from cte1 

 --ten sequence chain : -> 
  IF OBJECT_ID('tempdb..#knightmove') IS NOT NULL
  DROP TABLE #knightmove
  go

select	d.value, r1.value2 as r1value2, 
		r2.value2 as r2value , r3.value2 as r3value2, 
		r4.value2 as r4value2, r5.value2 as r5value2, 
		r6.value2 as r6value2,
		r7.value2 as r7value2, r8.value2 as r8value2, 
		r9.value2 as r9value2 
		
		into #results 

from	#knightmove d

inner	join #intermediate r1
on		d.value = r1.value1

inner	join #intermediate r2 
on		r1.value2 = r2.value1

inner	join #intermediate r3 
on		r2.value2 = r3.value1 

inner	join #intermediate r4
on		r3.value2 = r4.value1

inner join #intermediate r5
on		r4.value2 = r5.value1

inner join #intermediate r6 
on		r5.value2 = r6.value1

inner join #intermediate r7 
on		r6.value2 = r7.value1 

inner join #intermediate r8 
on		r7.value2 = r8.value1 

inner join #intermediate r9
on	r8.value2 = r9.value1 
 WHERE  ( CASE WHEN d.value IN ( 'A', 'E', 'I', 'O' ) THEN 1  ELSE  0  end + r1.isvowel + r2.isvowel
         + r3.isvowel + r4.isvowel
         + r5.isvowel + r6.isvowel
         + r7.isvowel + r8.isvowel
         + r9.isvowel ) <= 2


		 select * from #results -- 599930 row count in total 


 select top 10 * from #knightmove
 select * from #intermediate



