select	 *
from	dw.ipcountnewoldcustomers
order by transfercreateddate



select	convert(numeric (5,2) ,sum(uniquecustomers)) /
		convert (numeric(5,2),sum (case when IsIpatsignup = 'card customer'then uniquecustomers else 0 end))as penetration_rate,
		*
		from cte1

------
select	convert(numeric(10,2),sum (case when IsIpatsignup = 'card customer'then uniquecustomers else 0 end)) as cardstotal,
		convert (numeric(10,2),sum(uniquecustomers)) as sumuniqueforday,

		convert(numeric (10,2) ,sum(uniquecustomers)) /
		convert (numeric(10,2),sum (case when IsIpatsignup = 'card customer'then uniquecustomers else 0 end))as penetration_rate,


		datepart (mm,transfercreateddate)
from	dw.ipcountnewoldcustomers
group	by datepart (mm,transfercreateddate)
order	by datepart (mm,transfercreateddate)
go
