/*
Start by creating a table to store all of the 
the targets , at the moment it is a static list

The calculations have been kept in TSQL rather than 
try to do them in power BI as teh DAX formula's
arent working fr the rollups at the moment 
*/
drop	table if exists [AmPortfolioTarget]
go
CREATE	TABLE [AmPortfolioTarget]
(		PortfolioTargetRowId int identity(1,1),
		YearlyTarget bigint null,
		MonthlyTarget as YearlyTarget / 12,   
		QuarterlyTarget as YearlyTarget / 4,
		[Descriptor] nvarchar(255))

-->then insert into the target tables 

insert	into AmPortfolioTarget (YearlyTarget,Descriptor)
values	(462500,'Quarter 1')

insert	into AmPortfolioTarget (YearlyTarget,Descriptor)
values	(462500,'Quarter 2')

insert	into AmPortfolioTarget (YearlyTarget,Descriptor)
values	(462500,'Quarter 3')

insert	into AmPortfolioTarget (YearlyTarget,Descriptor)
values	(462500,'Quarter 4')
drop table if exists dw.dimdateFinancialPeriods
;

select * from [amportfoliotarget]

/*
Create a new calendar table , to support 
the Financial year groupings 
*/

drop table if exists dw.dimdateFinancialPeriods
;
with	cte1 as (
select  FiscalYear= case when	DATEPART(MM,[date]) between 1 AND 2 then CAST(datepart(YYYY,[date])-1 AS int) ELSE [YEAR] end 
		,FiscalQuarter = case	when DATEPART(MM,[date]) between 3 and 5 then 1 
								when DATEPART(MM,[date]) between 6 and 8 then 2
								when DATEPART(MM,[date]) between 9 and 11 then 3
								when DATEPART(MM,[date]) in (12,1,2) then 4
								end
,FiscalYearName = case when	DATEPART(MM,[date]) between 1 AND 2 then 'FY-' + CAST(datepart(YYYY,[date])-1 AS varchar(5)) ELSE 'FY-' + [YEAR] END
,FiscalMonthRank = case	when [MonthName] = 'March' then 1 
					when [Monthname] = 'April' then 2
					when [MonthName] = 'May' then 3
					when [MonthName] = 'June' then 4 
					when [MonthName] = 'July' then 5
					when [MonthName] = 'August' then 6
					when [MonthName] = 'September' then 7
					when [MonthName] = 'October' then 8 
					when [MonthName] = 'November' then 9 
					when [MonthName] = 'December' then 10 
					when [MonthName] = 'January' then 11
					when [Monthname] = 'February' then 12
					end
					
,*

from	dw.dimdate
where	id >= 19000301)
select  * ,cast(replicate('0',2-len([FiscalMonthRank])) as varchar(2)) + CAST([FiscalMonthRank] AS varchar(2)) +'-'+[MonthName] AS FinancialMonth
into	dw.dimdateFinancialPeriods
from	cte1 

select * from dw.dimdateFinancialPeriods


---------------------------------------------Overview section--------------------------

with	cteQuarterMonth as (
select	TradeId
		,datepart(year,TradeDate) as [Year]
		,datepart(MONTH,TradeDate) as [Month]
		--,datepart(QQ,TradeDate) as [Quarter]
		,DATENAME(MONTH,TradeDate) as [MonthName]
		,TradeProfit 
		,FB_AccountContactRef
		, bb.FiscalQuarter
		,CAST(bb.FiscalYear as int) as FiscalYear
		, bb.FiscalYearName
		, bb.FiscalMonthRank
from	NonRetailTradesSpotFwdFlex_FB aa
join	dw.dimdateFinancialPeriods bb
on		aa.tradedatekey = bb.ID
where	tradedatekey >= 20170301 --order by [Year],[Month]
)
,
cte2	as 
(
select	SUM(TradeProfit) as RevenueMTD
		,[year]
		,FiscalYear
		,[MONTH]
		,FiscalQuarter
		,FB_AccountContactRef
		,[MonthName]
		,FiscalMonthRank
from	cteQuarterMonth aa
JOIN	[AmPortfolioTarget]  bb
on		aa.FiscalQuarter = bb.PortfolioTargetRowId
group	by FiscalYear
		,[Year]
		,[MONTH]
		,FiscalQuarter
		,FB_AccountContactRef
		,[MonthName]
		,FiscalMonthRank
)

-->select	*  from	cte2 
-->so we have established that the row count is 142
--select	* into	#temp2 from	cte2 

select	RevenueMTD

		,(RevenueMTD/MonthlyTarget) * 100 AS [% Target MTD]

		,SUM(RevenueMTD) 
		over (partition by fb_accountcontactref,[fiscalyear],[fiscalquarter] order by [fiscalyear],[fiscalmonthrank] asc 
		rows between unbounded preceding and current row)	as RevenueQTD
		
		,(sum(revenuemtd) 
		over (partition by fb_accountcontactref,[fiscalyear],[fiscalquarter] order by [fiscalyear],[fiscalmonthrank] asc 
		rows between unbounded preceding and current row) / quarterlytarget) * 100  as [%target qtd]
		
		,sum(RevenueMTD) 
		over (partition by FB_AccountContactRef,[fiscalyear] order by [fiscalyear],[fiscalmonthrank] asc
		rows between unbounded preceding and current row) as RevenueYTD 
		
		,(SUM(RevenueMTD) 
		over (partition by fb_accountcontactref,[fiscalyear] order by [fiscalyear],[fiscalmonthrank] asc 
		rows between unbounded preceding and current row) / yearlytarget) * 100 as [%target ytd]
		
		,[YEAR] as CalendarYear
		,[FiscalYear] as [FinancialYear]
		,[MONTH]
		,[FiscalQuarter] as [FinancialQuarter]
		,FB_AccountContactRef
		,[MonthName]
		,[FiscalMonthRank]
	--,cast(replicate('0',2-len([month])) as varchar(2))  + cast([MONTH] as varchar(2)) +'-' + [MonthName] as MonthNumber
,cast(replicate('0',2-len([FiscalMonthRank])) as varchar(2)) + CAST([FiscalMonthRank] AS varchar(2)) +'-'+[MonthName] AS FinancialMonth
		--,MonthlyTarget
		--,QuarterlyTarget
		--,YearlyTarget 
from	cte2 
join	AmPortfolioTarget bb
on		cte2.FiscalQuarter = bb.PortfolioTargetRowId


---------Main Book section------------------------------------------------------------------->
;
with	cteRowset as 
(
select	tradeid
		,datepart(year,TradeDate) as [Year]
		,datepart(MONTH,TradeDate) as [Month]
		,datepart(QQ,TradeDate) as [Quarter]
		,DATENAME(MONTH,TradeDate) as [MonthName]
		,TradeDate
		,TradeProfit 
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,UserId
		,GBPTurnover = case SellCcyCode when 'gbp' then sellamount else [dw].[UdfGetGBPValueXIGNITE](SellAmount,SellCcyCode) end
		,bb.FiscalYear
		,bb.FiscalQuarter
		,bb.FiscalYearName
		,bb.fiscalmonthrank
		,bb.FinancialMonth
--,cast(replicate('0',2-len([FiscalMonthRank])) as varchar(2)) + CAST([FiscalMonthRank] AS varchar(2)) +'-'+[MonthName] AS FinancialMonth
from	NonRetailTradesSpotFwdFlex_FB aa
join	dw.dimdateFinancialPeriods bb
on		aa.tradedatekey = bb.ID
where	tradedatekey >= 20170101--start at 2016 --starting at 2016 is not right - it take 3:45 minutes just to get the data 01:59 from 2017 onwards
)									
,
cte2	as (
select	sum(tradeprofit) as revenuemtd
		,count(tradeid) as tradecount
		,sum(gbpturnover) gbpturnover
		,max(tradedate) as lasttradedate
		,[year]
		,[month]
		,[quarter]
		,fb_companyname
		,fb_accountcontactref
		,fb_salescontactref
		,[monthname]
		,userid
		,fiscalyear
		,fiscalquarter
		,fiscalyearname
		,fiscalmonthrank
		,FinancialMonth	
from	cteRowset aa
JOIN	[AmPortfolioTarget]  bb
on		aa.[Quarter] = bb.PortfolioTargetRowId
group	by	[year]
			,[month]
			,[quarter]
			,fb_companyname
		,fb_accountcontactref
		,fb_salescontactref
		,[monthname]
		,userid
		,fiscalyear
		,fiscalquarter
		,fiscalyearname
		,fiscalmonthrank
		,FinancialMonth) -->let cte2 be the first round of aggregations you perform for which you have extracted the data 
	,
cteCYLYComparison as(
select	ty.*
		,ly.GBPTurnover AS LYGBPTurnover
		,ly.RevenueMTD as LYRevenueMTD
		,ly.[Year] as LYyear
		,ly.FiscalYear as LYFiscalYear
		,ly.[Month] as LYmonth
		,ly.[Quarter] as LYquarter
		,ly.FB_Companyname as LYFB_Companyname 
--into	#CYLY
from	cte2 ty
LEFT	OUTER join	cte2 as ly 
on		ty.[FB_Companyname] = ly.FB_Companyname
and		ty.UserId = LY.UserId
and		ty.[MonthName] = ly.[MonthName]
and		ty.[FiscalYear]-1 = LY.[FiscalYear])
	
select	[YEAR]
		,[Month]
		,FiscalMonthRank
		,[FiscalYear]
		,[FiscalQuarter]
		,[fiscalyearname]
		,[FinancialMonth]
		,isnull(fb_companyname,'') as ClientName
		,isnull(userid,'') as Username
		,isnull(fb_accountcontactref,'') as AccountManager
		,LastTradeDate
		,revenuemtd
		,gbpturnover as MTDTurnOver
		,(gbpturnover/nullif(revenuemtd,0))/100 as MTDMargin
		,isnull(tradecount,0)as MTDTradeCount

		,sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)/FiscalMonthRank as AverageMonthRev
		,sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)/FiscalMonthRank as AverageMonthTurnover

		,((sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)
		/
		nullif(sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank),0)) / FiscalMonthRank) / 100 as AverageMonthMargin 

		,SUM(revenuemtd) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDRevenue 
		,SUM(gbpturnover) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDTurnover

		,(SUM(gbpturnover) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) 
		/
		nullif(SUM(revenuemtd) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) ,0) ) /100 as QTDMargin 

		,SUM(tradecount) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDTradeCount 

		,sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDRevenue 
		,sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDTurnover 
		,sum(tradecount) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDTradeCount
		
		,(sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) 
		/
		nullif(sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank),0)
		) / 100 as YTDMargin 

		,sum(lyrevenuemtd) over (partition by fb_companyname,userid)   as lyrevenue
		,sum(lygbpturnover) over (partition by fb_companyname,userid)   as lyturnover

		,isnull(lygbpturnover,0) as lastyearturnoveronecolumn 

		,(sum(lygbpturnover) over (partition by fb_companyname,userid)
		/
		nullif(sum(lyrevenuemtd) over (partition by fb_companyname,userid),0))
		/100 as lastyearmargin 
		
from	cteCYLYComparison



------------------------------MonthlyTab-----------------------------------------------------------------
with	cteThisYear as (
select	aa.tradeid
		--,datepart(year,TradeDate) as [Year]
		--,datepart(MONTH,TradeDate) as [Month]
		--,datepart(QQ,TradeDate) as [Quarter]
		--,DATENAME(MONTH,TradeDate) as [MonthName]
		,bb.fiscalyear
		,bb.FinancialMonth
		, bb.FiscalQuarter
		, bb.[MonthName]
		,TradeDate
		,TradeProfit 
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,UserId
		,GBPTurnover = case SellCcyCode when 'gbp' then sellamount else [dw].[UdfGetGBPValueXIGNITE](SellAmount,SellCcyCode) end
from	NonRetailTradesSpotFwdFlex_FB aa
join	dw.dimdateFinancialPeriods bb
on aa.tradedatekey = bb.id
where	tradedatekey >= 20170101)  --what is the rowcount for this ?
,
cte2	as (
select	SUM(TradeProfit) as RevenueMTD
		,FiscalYear
		,FinancialMonth
		,FiscalQuarter
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,[MonthName]
		,userid
		,COUNT(TradeId) as TradeCount
		,sum(GBPTurnover) GBPTurnover
		,MAX(TradeDate) as LastTradeDate
from	cteThisYear aa
JOIN	[AmPortfolioTarget]  bb
on		aa.FiscalQuarter = bb.PortfolioTargetRowId
group	by FiscalYear
		,FinancialMonth
		,FiscalQuarter
		,FB_AccountContactRef
		,FB_SalesContactRef
		,[MonthName]
		,FB_CompanyName
		,UserId) -->let cte2 be the first round of aggregations you perform for which you have extracted the data 
,
cte3 as 	
(select	 TradeCount 
		,RevenueMTD
		,GBPTurnover

		,SUM(RevenueMTD) 
		over (partition by fb_Companyname,[userid],[FiscalYear],[FinancialMonth] order by [FiscalYear],[FinancialMonth] asc 
		rows between unbounded preceding and current row)	as MTDRevenue

		,SUM(GBPTurnover) 
		over (partition by fb_Companyname,[userid],[FiscalYear],[FinancialMonth] order by [FiscalYear],[FinancialMonth] asc 
		rows between unbounded preceding and current row)	as MTDTurnOver

		,FiscalYear
		,FinancialMonth
		,FiscalQuarter
		,cte2.FB_AccountContactRef
		,cte2.FB_SalesContactRef
		,cte2.FB_Companyname
		,cte2.UserId
		,[MonthName]
		--,MonthlyTarget
		--,QuarterlyTarget
		--,YearlyTarget
from	cte2 
join	AmPortfolioTarget bb
on		cte2.FiscalQuarter = bb.PortfolioTargetRowId)

--then pivot the result set 
select [FiscalYear],fb_Accountcontactref,fb_Companyname,userid,[01-March],[02-April],[03-May],[04-June],[05-July],[06-August],[07-September],[08-October],[09-November],[10-December],[11-January],[12-February]
from 
(select revenuemtd,[FiscalYear],FinancialMonth,fb_Accountcontactref,fb_Companyname,userid from cte3) as sourcetable
pivot 
(
sum(revenuemtd) for FinancialMonth in ([01-March],[02-April],[03-May],[04-June],[05-July],[06-August],[07-September],[08-October],[09-November],[10-December],[11-January],[12-February])
) as pivottable;
--------currency rally table 

with	cte1 as (
select  FB_AccountContactRef
		,fb_Companyname
		,userid
		,buyccycode
		,sellccycode
		,buyccycode+ '/' +SellCcyCode as BuySell
		,GBPTurnover = case SellCcyCode when 'gbp' then sellamount else [dw].[UdfGetGBPValueXIGNITE](SellAmount,SellCcyCode) end
		,BB.FiscalYearName AS FinancialYear
		,BB.FiscalQuarter as FinancialQuarter
		,bb.financialmonth 
		, AA.TradeDate
from	NonRetailTradesSpotFwdFlex_FB AA
JOIN	dw.dimdateFinancialPeriods BB
ON AA.tradedatekey = BB.ID
where	tradedatekey >= 20170301)
select	 SUM(GBPTurnover) AS GBPTURNOVER
		,max(tradedate) as MaxTradeDate
		,FB_AccountContactRef
		,fb_Companyname
		,userid
		,buyccycode
		,sellccycode
		,BuySell
		,FinancialYear
		--,FinancialQuarter
		--,FinancialMonth
	
from	cte1
GROUP	BY	FB_AccountContactRef
			,fb_Companyname
			,userid
			,buyccycode
			,sellccycode
			,BuySell
			,FinancialYear
			
			
---------------------Updated main book section-----------------------------------------------------------
;


with	cteRowset as 
(
select	tradeid
		,datepart(year,TradeDate) as [Year]
		,datepart(MONTH,TradeDate) as [Month]
		,datepart(QQ,TradeDate) as [Quarter]
		,DATENAME(MONTH,TradeDate) as [MonthName]
		,TradeDate
		,TradeProfit 
		,FB_CompanyName
		,FB_AccountContactRef
		,FB_SalesContactRef
		,UserId
		,GBPTurnover = case SellCcyCode when 'gbp' then sellamount else [dw].[UdfGetGBPValueXIGNITE](SellAmount,SellCcyCode) end
		,bb.FiscalYear
		,bb.FiscalQuarter
		,bb.FiscalYearName
		,bb.fiscalmonthrank
		,bb.FinancialMonth
--,cast(replicate('0',2-len([FiscalMonthRank])) as varchar(2)) + CAST([FiscalMonthRank] AS varchar(2)) +'-'+[MonthName] AS FinancialMonth
from	NonRetailTradesSpotFwdFlex_FB aa
join	dw.dimdateFinancialPeriods bb
on		aa.tradedatekey = bb.ID
where	tradedatekey >= 20170101--start at 2016 --starting at 2016 is not right - it take 3:45 minutes just to get the data 01:59 from 2017 onwards
)									
,
cte2	as (
select	sum(tradeprofit) as revenuemtd
		,count(tradeid) as tradecount
		,sum(gbpturnover) gbpturnover
		,max(tradedate) as lasttradedate
		,max(fiscalmonthrank) as fiscalmonthrank
		,MAX(FiscalQuarter) as fiscalquarter
		--,[year]
		--,[month]
		--,[quarter]
		,fb_companyname
		,fb_accountcontactref
		,fb_salescontactref
		--,[monthname]
		,userid
		,fiscalyear
		--,fiscalquarter
		,fiscalyearname
		--,fiscalmonthrank
		--,FinancialMonth	
from	cteRowset aa
JOIN	[AmPortfolioTarget]  bb
on		aa.[Quarter] = bb.PortfolioTargetRowId
group	by	--[year]
			--,[month]
			--,[quarter]
			fb_companyname
		,fb_accountcontactref
		,fb_salescontactref
		--,[monthname]
		,userid
		,fiscalyear
		--,fiscalquarter
		,fiscalyearname
		--,fiscalmonthrank
		--,FinancialMonth
		) 
		-->let cte2 be the first round of aggregations you perform for which you have extracted the data 

		--select * into #temp1 from cte2 

		--select * from #temp1 where FB_Companyname = 'ABFA Ltd'
		--GO

		--ok so I have records which are in 2018
		--select * from #temp1 where fiscalyear  = '2018'
	,
cteCYLYComparison as(
select	ty.*
		,ly.GBPTurnover AS LYGBPTurnover
		,ly.RevenueMTD as LYRevenueMTD
		--, as LYyear
		,ly.FiscalYear as LYFiscalYear
		--,ly.[Month] as LYmonth
		--,ly.[Quarter] as LYquarter
		,ly.FB_Companyname as LYFB_Companyname 
from	cte2 ty
LEFT	OUTER join	cte2 as ly 
on		ty.[FB_Companyname] = ly.FB_Companyname
and		ty.UserId = LY.UserId
--and		ty.[MonthName] = ly.[MonthName]
and		ty.[FiscalYear]-1 = LY.[FiscalYear])
	
-->select	* into #cyly from cteCYLYComparison

--> select * from #cyly where FB_Companyname = 'ABFA Ltd'

select	--[YEAR]
		--,[Month]
		FiscalMonthRank
		,[FiscalYear]
		--,[FiscalQuarter]
		,[fiscalyearname]
		--,[FinancialMonth]
		,isnull(fb_companyname,'') as ClientName
		,isnull(userid,'') as Username
		,isnull(fb_accountcontactref,'') as AccountManager
		,LastTradeDate
		,revenuemtd
		,gbpturnover as MTDTurnOver
		,(gbpturnover/nullif(revenuemtd,0))/100 as MTDMargin
		,isnull(tradecount,0)as MTDTradeCount

		,sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)/FiscalMonthRank as AverageMonthRev
		,sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)/FiscalMonthRank as AverageMonthTurnover

		,((sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank)
		/
		nullif(sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank),0)) / FiscalMonthRank) / 100 as AverageMonthMargin 

		,SUM(revenuemtd) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDRevenue 
		,SUM(gbpturnover) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDTurnover

		,(SUM(gbpturnover) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) 
		/
		nullif(SUM(revenuemtd) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) ,0) ) /100 as QTDMargin 

		,SUM(tradecount) over (partition by fb_companyname,userid, FiscalYear,fiscalquarter order by FiscalYear,FiscalMonthRank asc ) as QTDTradeCount 

		,sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDRevenue 
		,sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDTurnover 
		,sum(tradecount) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) as YTDTradeCount
		
		,(sum(gbpturnover) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank) 
		/
		nullif(sum(revenuemtd) over (partition by fb_companyname,userid, FiscalYear order by fiscalmonthrank),0)
		) / 100 as YTDMargin 

		,sum(lyrevenuemtd) over (partition by fb_companyname,userid)   as lyrevenue
		,sum(lygbpturnover) over (partition by fb_companyname,userid)   as lyturnover

		,isnull(lygbpturnover,0) as lastyearturnoveronecolumn 

		,(sum(lygbpturnover) over (partition by fb_companyname,userid)
		/
		nullif(sum(lyrevenuemtd) over (partition by fb_companyname,userid),0))
		/100 as lastyearmargin 
		
from	cteCYLYComparison