USE [datawarehouse]
GO

/****** Object:  StoredProcedure [dbo].[GetNonRetailTradesSpotFwdFlex]    Script Date: 19/06/2017 15:29:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[GetNonRetailTradesSpotFwdFlex]	@startdate datetime = null,
												@enddate datetime = null, 
												@AccountManagerSage varchar(50) = null,
												@SalesPersonSage nvarchar(3996) = null
AS

DECLARE @startdatekey int , @enddatekey int 

select	@startdatekey = cast(convert(char(8), @startdate, 112) as int),
		@enddatekey  =  cast(convert(char(8), @enddate, 112) as int)

		if		isnull (@AccountManagerSage ,'') = '' and isnull (@SalesPersonSage ,'') = '' --if both account manager and salesperson are null
				
				begin
		select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber,
				*
		from	NonRetailTradesSpotFwdFlex
		where	tradedatekey between @startdatekey and @enddatekey 
				end

	else		if isnull (@AccountManagerSage ,'') = '' and isnull (@SalesPersonSage ,'') <> '' -- is salesperson is populated use that and date
		begin
				select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber,
				*
				from	NonRetailTradesSpotFwdFlex
				where	tradedatekey between @startdatekey and @enddatekey 
				and		[SalesPerson(sage)] = @SalesPersonSage
		end
		else  if isnull (@AccountManagerSage ,'') <> '' and isnull (@SalesPersonSage ,'') <> '' --if non of the optional parameters are null

		BEGIN
		select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber,
				*
				from	NonRetailTradesSpotFwdFlex
				where	tradedatekey between @startdatekey and @enddatekey 
				and		[SalesPerson(sage)] = @SalesPersonSage and [AccountManager(Sage)] = @AccountManagerSage
		END

		ELSE
			begin 
			select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber, --your last case is if account manager is supplied 
				*
				from	NonRetailTradesSpotFwdFlex
				where	tradedatekey between @startdatekey and @enddatekey 
				and		[AccountManager(sage)] = @AccountManagerSage
				end

				 
GO

