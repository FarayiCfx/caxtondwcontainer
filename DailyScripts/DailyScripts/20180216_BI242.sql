/*
first of all get a distinct list of all 
the currencies we have 
*/
drop	table if exists BI242_AGGREGATE ;

with	ctecurrencylist as (
select	distinct Currency,numericcode 
from	[dw].[DimCurrencyCodesISO4217])

select	aa.wallettransaction_key
		,	aa.billingamount
		,	aa.billingamountgbp
		,	aa.billingcurrency
		,	aa.transactionamount
		,	aa.transactioncurrency
		,	aa.transactiondatetime
		,	datename(yyyy,transactiondatetime) as [year]
		,	datepart(mm,transactiondatetime) as [month] 
		,	datename(mm,transactiondatetime) as [monthname] 
		,	cc.*
into	BI242_AGGREGATE 
from	valitor.factwallettransactions aa (nolock)
left	outer join	ctecurrencylist cc
on		aa.transactioncurrency = cc.numericcode
where	transactionkeycategory = 'sale'
and		transactiondatekey > 20170131
--> 04:15 secs to get the data through --4 828 535 rows @ 16/2/2018

select	top 10 * from BI242_AGGREGATE

select	distinct [month]
		,[monthname]
		,[year]
,cast(replicate('0',2-len([month])) as varchar(2))  + cast([MONTH] as varchar(2)) +'-' + [MonthName] +'-'+[Year]
		 as [MonthNumber&Year]
from	BI242_AGGREGATE
order by 3,1 asc 

/*
To fit the shape required by the ticket the data needs to be pivoted
*/ 
DROP TABLE IF EXISTS BI242_pivotoutput;

with cte1 as 
(
select Currency,[02-February-2017],[03-March-2017],[04-April-2017],[05-May-2017],[06-June-2017],[07-July-2017],[08-August-2017],[09-September-2017],[10-October-2017],[11-November-2017],[12-December-2017],[01-January-2018],[02-February-2018]
from 
(
select	Currency,billingamountgbp,
		cast(replicate('0',2-len([month])) as varchar(2))  + cast([MONTH] as varchar(2)) +'-' + [MonthName] +'-'+[Year]
		 as [MonthNumber&Year]
from	BI242_AGGREGATE) as SourceTable
PIVOT
(
SUM(billingamountgbp) for [MonthNumber&Year] in ([02-February-2017],[03-March-2017],[04-April-2017],[05-May-2017],[06-June-2017],[07-July-2017],[08-August-2017],[09-September-2017],[10-October-2017],[11-November-2017],[12-December-2017],[01-January-2018],[02-February-2018])
)as pivottable 
)
select	*
into	BI242_pivotoutput
from	cte1
order	by Currency ASC

/*
then set preloadable values, case statement is fine 
for now because it's a one off

*/

select	IsPreloadable = 
		case when Currency IN ('US Dollar'
								,'Euro'
								,'Pound Sterling'
								,'Australian Dollar'
								,'Canadian Dollar'
								,'New Zealand Dollar'
								,'Hong Kong Dollar'
								,'Swiss Franc'
								,'Yen'
								,'Danish Krone'
								,'Rand'
								,'Zloty'
								,'Swedish Krona'
								,'Norwegian Krone'
								,'Forint') then 'True' else 'False' end
		,*
from	BI242_pivotoutput
where	currency is not null




select * from BI242_pivotoutput
