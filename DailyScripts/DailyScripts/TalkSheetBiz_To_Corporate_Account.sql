IF OBJECT_ID('tempdb..#tmp') IS NOT NULL
DROP TABLE #tmp
go
create table #tmp (contactref varchar(20), companyname varchar(250), compl int, msg varchar(200))
insert into #tmp values (41923, '27 Ten Productions Ltd', 0, '')

select  * from #tmp

/*
** Script to add a TalksheetBiz user to a corporate account as Admin user
*/

Declare @fastpayref varchar(20), @companynm VARCHAR(100), @action int = 0

       DECLARE       MyCursor CURSOR FOR
       SELECT TOP 50
              contactref
              ,companyname
       FROM    #tmp
       WHERE  compl = 0 


       OPEN MyCursor

       FETCH NEXT FROM MyCursor
       INTO @fastpayref, @companynm

       WHILE @@FETCH_STATUS = 0
       BEGIN

              declare @talksheetbizuser varchar(100) = '27tenproductions@caxtonfx.com'  -- Change this to user email
              declare @corporateentityid int = 458 -- Change this to corporate entity id
              declare @corpprodid int = 5
              declare @retailprodid int = 4
              declare @userid VARCHAR(100) = ''
              declare @corporateuserid VARCHAR(100) = ''

              select @talksheetbizuser = email from aspnetusers where fastpaycontactref = @fastpayref

              select @corporateentityid = id from corporateentities where companyname = @companynm

              SELECT @userid = id from aspnetusers where email = @talksheetbizuser
              select @corporateuserid =  @userid
              select @userid, @talksheetbizuser, @corporateentityid, @corporateuserid

              select * from aspnetuserclaims where userid = @userid
              select corporateuserid, * from corporateentities where id = @corporateentityid

              IF (select count(*) from corporateentities where id = @corporateentityid and corporateuserid is null) = 0
              BEGIN
                           IF @action = 1
                           BEGIN
                                  UPDATE #tmp set compl = 2, msg = 'FAILED AS THIS CORPORATION ALREADY HAS OWNER' where contactref = @fastpayref
                           END
                           select 'ALREADY HAS CORP OWNER'
                      FETCH NEXT FROM MyCursor INTO @fastpayref, @companynm
                           CONTINUE
              END
              

              If (select count(*) from aspnetuserclaims where (charindex('Cards', Claimvalue) > 0 or charindex('Corporate', Claimvalue) > 0 or charindex('Retail', claimvalue) > 0) and userid = @userid) > 0
              BEGIN
                           IF @action = 1
                           BEGIN
                                  UPDATE #tmp set compl = 2, msg = 'FAILED AS THIS USER HAS CARDS CLAIMS' where contactref = @fastpayref
                           END

                           select 'HAS CARD CLAIMS'
                      FETCH NEXT FROM MyCursor INTO @fastpayref, @companynm
                           CONTINUE
              END    
              if ((select count(*) from aspnetusers where fastpaycontactref = @fastpayref) = 0)
              BEGIN
                           IF @action = 1
                           BEGIN
                                  UPDATE #tmp set compl = 2, msg = 'FAILED AS THIS USER IS NOT ON FIREBIRD' where contactref = @fastpayref
                           END
                           select 'USER NOT ON FIREBIRD'
                      FETCH NEXT FROM MyCursor INTO @fastpayref, @companynm
                           CONTINUE
              END
              IF (select count(*) from aspnetuserclaims where userid = @userid) = 0
              BEGIN
                           IF @action = 1
                           BEGIN
                                  UPDATE #tmp set compl = 2, msg = 'FAILED AS USER HAS NO CLAIMS' where contactref = @fastpayref
                           END
                           select 'USER HAS NO CLAIMS'
                      FETCH NEXT FROM MyCursor INTO @fastpayref, @companynm
                           CONTINUE
              END


              if (select count(*) from corporateentities where companyname = @companynm) = 0
              BEGIN
                           IF @action = 1
                           BEGIN
                                  UPDATE #tmp set compl = 2, msg = 'FAILED AS THIS COMPANY IS NOT ON FIREBIRD' where contactref = @fastpayref
                           END
                           select 'COMPANY NOT ON FIREBIRD'
                      FETCH NEXT FROM MyCursor INTO @fastpayref, @companynm
                           CONTINUE
              END
              if ( @userid is null or @talksheetbizuser is null or @corporateentityid is null or @corporateuserid     is null)
              BEGIN
                           IF @action = 1
                           BEGIN
                                  UPDATE #tmp set compl = 2, msg = 'FAILED AS PROBLEM WITH FINDING USER or COMPANY' where contactref = @fastpayref
                           END
                           select 'FAILED TO FIND USER / COMPANY'
                      FETCH NEXT FROM MyCursor INTO @fastpayref, @companynm
                           CONTINUE
              END

              -----------
              --Check no card claims first!!!!!
              ------------
              select 'PROCESS'
              IF @action = 1
              BEGIN
              BEGIN TRY
                     BEGIN TRANSACTION
                           SELECT 'TRANSACTION STARTED'
                           -- 1> Insert null records for applicant and aspnetuser into corporate applicants and corporateusers
                           INSERT INTO corporateapplicants (id) select top 1 id from applicants where email = @talksheetbizuser and CreationDate is not null
                           INSERT INTO corporateusers (id) select id from aspnetusers where email = @talksheetbizuser

                           -- 2> Update accounttype ref in both Applicants and Aspnetusers
                           UPDATE applicants set AccountTypeRef = 'Corporate' where email = @talksheetbizuser
                           UPDATE aspnetusers SET AccountTypeRef = 'Corporate' where email = @talksheetbizuser
                           -- 3> Update product association from retail to corporate
                           UPDATE [dbo].[UserProductAssociation] SET ProductId = @corpprodid where email = @talksheetbizuser and productid = @retailprodid

                           -- 4> Update Claims from retail to corporate
                           UPDATE [dbo].[AspNetUserClaims] SET CLAIMVALUE = replace(ClaimValue, 'TalksheetBiz', 'Corporate') WHERE userid = @userid

                           -- 5> Insert User Corporate Association Entry
                           INSERT INTO [dbo].[AspNetUserClaims] VALUES (@userid, 'CorporateAccess', @corporateuserid  + ':Edit:CorporateEntity:' + convert(varchar(100), @corporateentityid))

                           -- 6> Insert User Corporate Association
                           INSERT INTO [dbo].[UserCorporateAssociation] values (@corporateentityid, @talksheetbizuser, 3, getdate(), getdate(), null, null)

                           -- 7> Update owner id and customerreference
                           Update corporateentities set corporateuserid=@userId, customerreference = substring(companyName, 0, 22) where id = @corporateEntityId
                           UPDATE #tmp set compl = 1, msg = 'SUCCESS' where contactref = @fastpayref

                     COMMIT TRANSACTION
                     SELECT 'TRANSACTION COMPLETE'
              END TRY
              BEGIN CATCH
                     IF @@TRANCOUNT > 0
                     BEGIN
                           UPDATE #tmp set compl = 2, msg = 'ERROR OCCURED' where contactref = @fastpayref
                           ROLLBACK TRAN
                           SELECT @@error
                           SELECT 'ERROR OCCURED'
                     END

              END CATCH
              END
                     
              FETCH NEXT FROM MyCursor 
              INTO @fastpayref, @companynm
       END

       CLOSE MyCursor
       DEALLOCATE MyCursor

--select * from applicants where fastpaycontactref = '41923'
--update #tmp set compl = 2, msg = 'Retail client' where companyname = 'Leif Cid'

-- create table #tmp (contactref varchar(20), companyname varchar(250), compl int, msg varchar(200))

