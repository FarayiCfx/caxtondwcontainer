select	top 10 * from [FIS].[FactCARDLOAD]

exec	sp_Spaceused '[fis].[factcardload]'

select	* from [dw].[DimTalksheetCardsAndContacts]  where Contact_ID = 361802

select top 10 * from [dw].[DimAccountTalksheet]

drop	table fis.fis_all_data

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--start with transactions 
with	cte1 as (
select	aa.ITEMID,	bb.DBCARDSContact_id,cc.account_full_name,bb.ContactEmailAddress, bb.UserName,
		bb.DBCARDSAccount_Id,BB.LASTNAME,BB.FIRSTNAME,
		aa.customerkey,aa.BILLCONVRATE,
		aa.TXNDATE as localdate,pan,curtxn
		,currencycode = case when	left(aa.pan,6)= '459566' then 'eur' --dont like hard coding stuff but time sensitive reports so will do this for now
							when	left(aa.pan,6)= '459568' then 'usd'
							when	left(aa.pan,6)= '459567' then 'gbp'
							END
		,-amttxn as amttxn
		,-ee.fee as amtfee
		,-aa.BILLAMT as billamt
		,termlocation
		,TERMCITY
		,termcountry
		,'Card Transaction' as [Description] 
		--into format_table 
from	[FIS].[FactFinAdv] aa (nolock)
join	dw.DimCustomerReference bb (nolock)
on		aa.customerkey = bb.customerkey 
left	outer join [dw].[DimAccountTalksheet] cc (nolock)
on		bb.DBCARDSAccount_Id = cc.[Account_id]
left outer join	TalkSheetFees ee
on		aa.ITEMID = ee.itemid
where	left(aa.PAN,6) =	'459566'
or		left(aa.PAN,6) =	'459567'
or		left(aa.PAN,6) =	'459568'
union	ALL
select	aa.CardLoadID as ITEMID,
		bb.DBCARDSContact_id,
		cc.account_full_name,
		bb.ContactEmailAddress, bb.UserName,
		bb.DBCARDSAccount_Id,BB.LASTNAME,BB.FIRSTNAME,
		aa.customerkey
		,1 as BILLCONVRATE
		,aa.transactiondate  as localdate
		,cardnumber as pan
		,'' as curtxn,
		CurrencyCode = case when left(aa.CardNumber,6)= '459566' then 'EUR' --DONT LIKE HARD CODING STUFF BUT TIME SENSITIVE REPORTS SO WILL DO THIS FOR NOW
							WHEN LEFT(aa.CardNumber,6)= '459568' THEN 'USD'
							WHEN LEFT(aa.CardNumber,6)= '459567' THEN 'GBP'
							END
		, amttxn
		,0 as amtfee
		,  BILLAMT
		,'Caxton fx'termlocation,
		'' as TERMCITY,
		'GB' AS termcountry,
		'Card Load' as [Description]  
from	[fis].[TalkSheetAllLoads] aa (nolock)
join	dw.DimCustomerReference bb (nolock)
on		aa.customerkey = bb.customerkey 
left	outer join [dw].[DimAccountTalksheet] cc (nolock)
on		bb.DBCARDSAccount_Id = cc.[Account_id]
where aa.transactiondate > '2014-12-31 23:59:59.000'
)
select	* 
into	fis.fis_all_data
from	cte1 

SELECT	itemid as item_id,[description],* 
FROM	fis.fis_all_data
where	account_full_name = 'Fideres Advisors Llp' 
and localdate > '2014-12-31 23:59:59.000'
--and		[description] = 'card load'
order	by localdate DESC


select * from fis.fis_all_Data where itemid = 

exec	sys.sp_spaceused ''

select	min(txndate) from fis.FactFinAdv (nolock)
---'2013-12-31 00:00:00.000'

select	min(localdate) from fis.FactFinAdv (nolock)

--drop table fis.fis_all_data

exec	sp_spaceused 'fis.FactCARDLOAD'

exec	sp_spaceused 'fis.factfinadv'

exec	sp_spaceused 'fis.factfee'

exec	sp_spaceused 'fis.factfinrev'

-------------------------------------
exec sp_help 'fis.FactCARDLOAD'

exec sp_help 'fis.factfinadv'

exec sp_help 'fis.factfee'

exec sp_help 'fis.factfinrev'

------------------------------------------
select top 10 * from fis.FactCARDLOAD

select top 10 * from fis.factfinadv

select top 10 * from fis.factfee

select top 10 * from fis.factfinrev

exec sp_help 'fis.factfinadv_staging'



-------------------------------------
/*
truncate table fis.FactCARDLOAD

truncate table fis.factfinadv

truncate table fis.factfee

truncate table fis.factfinrev
*/



CREATE TABLE [fis].[TalkSheetAllLoads](
	[CustomerKey] bigint , 
	[CardLoadID] [int] NOT NULL,
	[CARDID] [varchar](255) NULL,
	[TransactionDate] [smalldatetime] NULL,
	[CardNumber] [varchar](50) NULL,
	[LoadType] [varchar](50) NULL,
	[TranType] [varchar](11) NOT NULL,
	[TranDescription] [varchar](14) NOT NULL,
	[TranLocation] [varchar](9) NOT NULL,
	[TranCountry] [varchar](2) NOT NULL,
	[AMTTXN] [money] NULL,
	[CalcConv] [int] NOT NULL,
	[Fee] [int] NOT NULL,
	[BILLAMT] [money] NULL, 
	[DWdateInserted] datetime
) ON [PRIMARY]



select isnull(max(cardloadid),0) as Max_ from [fis].[TalkSheetAllLoads]

select top 100 * from [fis].[TalkSheetAllLoads]

select * from FIS.FACTfinrev

select	aa.ITEMID,	bb.DBCARDSContact_id,cc.account_full_name,bb.ContactEmailAddress, bb.UserName,
		bb.DBCARDSAccount_Id,BB.LASTNAME,BB.FIRSTNAME,
		aa.customerkey,aa.BILLCONVRATE,
		aa.TXNDATE as localdate,pan,curtxn
		,currencycode = case when	left(aa.pan,6)= '459566' then 'eur' --dont like hard coding stuff but time sensitive reports so will do this for now
							when	left(aa.pan,6)= '459568' then 'usd'
							when	left(aa.pan,6)= '459567' then 'gbp'
							END
		,amttxn as amttxn
		,ee.fee as amtfee
		,aa.BILLAMT as billamt
		,termlocation
		,TERMCITY
		,termcountry
		,'Card Transaction' as [Description] 
		--into format_table 
from	[FIS].[FactFinrev] aa (nolock)
join	dw.DimCustomerReference bb (nolock)
on		aa.customerkey = bb.customerkey 
left	outer join [dw].[DimAccountTalksheet] cc (nolock) 
on		bb.DBCARDSAccount_Id = cc.[Account_id]
left outer join	TalkSheetFees ee
on		aa.ITEMID = ee.itemid
where	left(aa.PAN,6) =	'459566'
or		left(aa.PAN,6) =	'459567'
or		left(aa.PAN,6) =	'459568'