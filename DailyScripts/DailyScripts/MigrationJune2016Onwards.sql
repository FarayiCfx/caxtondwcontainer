use dbcards 
go 
 /* 
 Adhoc sql query, that retreives data for 
 cards expiring in a certain month, the input for the date 
 should by yymm e.g. if you want to check for expiries that 
 happen in jan 16 you should put in 1601 
 */ 
 IF OBJECT_ID('tempdb.dbo.##primarycontact', 'U') IS NOT NULL 
 DROP TABLE ##primarycontact; 

 IF OBJECT_ID('tempdb.dbo.##allsecondarycontacts', 'U') IS NOT NULL 
 DROP TABLE ##allsecondarycontacts; 
 /* 
 start by getting the primary users expiry date and card id 
 put this into ##primarycontact 
 */ 

 declare @expiry_yymm as varchar (4) 

 select	 @expiry_yymm = '1611' --set expiry date here 

 SELECT ccd.contact_id 
 -- ,CardCurrency 
 -- ,ccd.Cardnumber 
 ,SUBSTRING (ccd.Cardnumber,1 , 6 ) AS BINS 
 ,'"'+ cardid +'"' as CardId 
 ,CASE 
        WHEN ccd.cardtype = 0 THEN 'Primary Card' 
        WHEN ccd.cardtype = 1 THEN 'Secondary Card' 
 END AS 'Card Type' 
 ,ISNULL(con.parent_id,0) AS parent_id 
 ,ISNULL(ce2.email_address,'') as parent_email_address 
 ,Expdate 
 ,cardstatus 
 ,ccd.CardCurrency 
 , 
 (select count(distinct (cardcurrency)) 
 from tblcfxcurrencycarddetails tt (nolock) 
 where contact_id = ccd.contact_id 
 --and tt.expdate = @expiry_yymm 
 ) as distinct_currency_count_per_contact 

 , con.contact_first_name 
 , con.contact_last_name 
 , LOWER(ce.email_address) AS ContactEmailAdd 
 --, ce.contact_email_id, ce.def_email 
 , con.dob 
 , CASE 
 WHEN LOWER(sre.Gender) = '0' OR LOWER(sre. Gender) = 'UNKNOWN' OR LOWER(sre.Gender) IS NULL THEN 'not entered' 
 WHEN LOWER(sre.Gender) = 'male' THEN 'male' 
 WHEN LOWER(sre.Gender) = 'female' THEN 'female' 
 END AS 'Gender' 
 , su.LastLogin AS LastLoginDate 
 into ##primarycontact 
 FROM tblcfxcurrencycarddetails ccd 
 INNER JOIN Contact con ON con.contact_id = ccd.contact_id 
 INNER JOIN Contact_Email ce ON ce.contact_id = con.contact_id 
 INNER JOIN SCY_RegExt sre ON ccd.scyRegPKId = sre.pkid 
 INNER JOIN scy_Users su ON su.contact_id = con.contact_id 
 left outer JOIN Contact_Email ce2 on CON.parent_id = ce2.contact_id 
 WHERE Expdate = @expiry_yymm 
 AND cardstatus IN ('00','01') 
 AND ce.def_email = 1 
 AND ccd.CardCurrency IN (13, 14, 15) 
 --ORDER BY ccd.contact_id, ccd.CardCurrency 
 go 

 select top 20 * from SCY_RegExt

 ---------------------------------------------------------------------------------------------------------------------- 
 --populate ##allsecondarycontacts table 

 with cte1 as ( 
 select distinct cc.contact_id as secondary_contactid, --you need to capture a distinct list 
 cc.contact_salutation as SecondarySalutation, 
 cc.contact_first_name as SecondaryFirstName, 
 cc.contact_last_name as SecondaryLastName, 
 cc.dob as SecondaryDateOfBirth, 
 cc.parent_id 
 from tblcfxcurrencycarddetails cd 
 join contact cc 
 on cd.contact_id = cc.contact_id) 
 select * 
 into ##allsecondarycontacts 
 from cte1 where parent_id is not null 
 GO 
 -------- 

 -->PRIMARY list for migrations developer (normally Alan) 
 select distinct contact_id, --you need to supply a distinct list 
 contact_first_name as PrimaryFirstName, 
 contact_last_name as PrimaryLastName, 
 contactemailadd as PrimaryEmailAddress , 
 distinct_currency_count_per_contact, 
 dob 
 from ##primarycontact order by 1 asc; 


 -->Secondary list for migrations developer (normally Alan) 
 with	cteDistinctPrimaries as ( 
 select distinct contact_id, 
		contact_first_name as PrimaryFirstName, 
		contact_last_name as PrimaryLastName, 
		contactemailadd as PrimaryEmailAddress , 
		distinct_currency_count_per_contact, 
 dob 
 from	##primarycontact) 
 select bb.* 
 from	cteDistinctPrimaries aa 
 join	##allsecondarycontacts bb 
 on		aa.contact_id = bb.parent_id 
 order	by 1 

 --------------------------------------------belt and braces union query for marketing---------------------------------------- 
 ; 
 select distinct contact_first_name as FirstName, 
 contact_last_name as LastName, 
 contactemailadd as EmailAddress 
 from ##primarycontact 
 -->Secondary list for migrations developer (normally Alan) 