select * from [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]


alter	VIEW [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
as
with	cteResultSet as (
select	
		aa.id as TradeId,
		aa.buyccycode,
		aa.BuyAmount, 
		AA.SellCcyCode,
		aa.SellAmount,
		aa.Settled, 
		cast(aa.TradeDate as Date) as TradeDate, 
		cast(aa.TradeEndDate as Date) as [SettlementDate],
		isnull(aa.HelpdeskUserId,'') as HelpdeskUserId,
		aa.UserId ,
		BB.tradeid_summarytable,
		isnull(bb.AmountPaid,0) as AmountPaid, 
		isnull(aa.TradeReason,'') as TradeReason,
		aa.TradeProfit
from	[Firebird].[FactCurrencyBankTrade] AS aa
OUTER	APPLY	(select tradeid as tradeid_summarytable , 
						sum(amount) as AmountPaid
				from	[Firebird].[FactCurrencyBankTradeDetail] bb
				where	bb.tradeid = AA.id
				GROUP	BY BB.tradeid) BB)
				select	TradeDate,
						TradeId ,
						TradeProfit,
						buyccycode,
						BuyAmount,
						SellCcyCode,
						SellAmount,
						Settled,
						[SettlementDate],
						HelpdeskUserId,
							isnull(dce.companyname,'') as CompanyName,
							isnull(dce.SalesContactRef,'') as SalesContactRef,
							isnull(dce.AccountContactRef,'') AccountContactRef,
						UserId,
						tradeid_summarytable,
						AmountPaid,
						/*'GREEN' as [Booked_Text], 
						3 as Booked,*/

						PartFunded_Text =	case	when AmountPaid = SellAmount  then 'GREEN' 
													when AmountPaid < SellAmount and settlementdate > getdate() then 'AMBER'
													when AmountPaid < SellAmount and settlementdate < getdate() then 'RED' -->settlementdate has passed so its red
											end,

						PartFunded =		case	when AmountPaid = SellAmount  then 3
													when AmountPaid < SellAmount and settlementdate > getdate() then 2
													when AmountPaid < SellAmount and settlementdate < getdate() then 1 -->settlementdate has passed so its red
											end,

						Funded_Text =	case	when AmountPaid = SellAmount then 'GREEN : AmountPaid = SellAmount' 
												when amountpaid < sellamount and settlementdate > getdate() then 'AMBER : AmountPaid less than sell amount and SettlementDate in future' -->settlementdate has not passed but trade is at least partially funded  
												WHEN AmountPaid < sellamount and settlementdate < getdate() then 'RED : AmountPaid less than sell amount and SettlementDate has passed' END,

						Funded =		case	when	AmountPaid = SellAmount then 3
												when amountpaid < sellamount and settlementdate > getdate() then 2 --settlementdate has not passed but trade is at least partially funded  
												WHEN AmountPaid < sellamount and settlementdate < getdate() then 1 END,

						IsSettledText = case	when	settled = 1 then 'GREEN' 
												when	settled = 0 and settlementdate < getdate() then 'RED'
												when	settled = 0 and settlementdate > getdate() then 'AMBER' --settlementdate is in the future from here 
										END,

						IsSettled = case	when	settled = 1 then 3 
											when	settled = 0 and settlementdate < getdate() then 1
											when	settled = 0 and settlementdate > getdate() then 2 --settlementdate is in the future from here 
											END, 
				TradeReason,
				bb.AccountTypeRef
				from	cteResultset
				join	dw.dimcustomerreference bb(nolock)
				on 		cteresultset.userid = bb.UserName 
				left	outer join dw.dimusercorporateassociation duc (nolock)
				on		bb.username = duc.email 
				left	outer join dw.dimcorporateentities dce 
				on		duc.corporateentityid = dce.id
				left outer join -- add this join to get the crm account names 
				where	tradedate > '2015-12-31'
				and		tradereason <> 'card load'
				and		accounttyperef <> 'Retail'
GO


select count(*) from [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]

select	top 1000 * 
from	[dw].[FactCurrencyTradeCrossApplyToDetail] 
order	by 1 desc 

select	top 100 * 
into dw.testdef
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
order	by 1 desc 

exec sp_help 'dw.testdef'


alter procedure	GetNonRetailTradesByDate
					@startdate datetime,
					@enddate datetime
as

set nocount on

declare @begin date,
		@end date

select	@begin = cast (@startdate as date) ,
		@end = cast (@enddate as date)

select	TradeDate,UserId,CompanyName,HelpdeskUserId,AccountTypeRef,SalesContactRef,
		buyccycode,buyamount,sellccycode,sellamount,tradeprofit,tradereason,accountcontactref
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
where	TradeDate between @begin and @end
order	by tradeid desc 


exec GetNonRetailTradesByDate @startdate ='2017-04-05 13:25:34.397' ,@enddate ='2017-04-05 13:25:34.397'

select distinct (salescontactref) from [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]

select top 100 * from [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]



ALTER procedure	GetNonRetailTradesByCompanyname
					@companyname nvarchar(255)
as

set nocount on

select	TradeDate,UserId,CompanyName,HelpdeskUserId,AccountTypeRef,SalesContactRef,
		buyccycode,buyamount,sellccycode,sellamount,tradeprofit,tradereason,accountcontactref
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
where	companyname =  @companyname


select distinct CompanyName from [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]

EXEC GetNonRetailTradesByCompanyname 'GetNonRetailTradesByCompanyname'

select distinct helpdeskuserid from dw.[FactCurrencyTradeCrossApplyToDetail_NonRetail]

select distinct salescontactref from dw.[FactCurrencyTradeCrossApplyToDetail_NonRetail]

drop procedure GetNonRetailTradesBySalesPerson

create procedure	GetNonRetailTradesBySalesPerson_helpdeskid
					@helpdeskuserid nvarchar(255)
as

set		nocount on

select	TradeDate,UserId,CompanyName,HelpdeskUserId,AccountTypeRef,SalesContactRef,
		buyccycode,buyamount,sellccycode,sellamount,tradeprofit,tradereason,accountcontactref
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
where	HelpdeskUserId =  @helpdeskuserid

exec GetNonRetailTradesBySalesPerson_helpdeskid 'george.hunt@CAXTONFX.com'

alter procedure	GetNonRetailTradesBySalesContactRef
					@SalesContactRef nvarchar(255)
as

set nocount on

select	TradeDate,UserId,CompanyName,HelpdeskUserId,AccountTypeRef,SalesContactRef,
		buyccycode,buyamount,sellccycode,sellamount,tradeprofit,tradereason,accountcontactref
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
where	SalesContactRef =  @SalesContactRef


select	TradeDate,UserId,CompanyName,HelpdeskUserId,AccountTypeRef,SalesContactRef,
		buyccycode,buyamount,sellccycode,sellamount,tradeprofit,tradereason,accountcontactref
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
where	salescontactref =  'Clark Davidson'

select	sum(tradeprofit) as TotalTradePofit,
		companyname
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
group	by companyname 
order	by 1 desc

select	sum(tradeprofit) as TotalTradePofit,
		salescontactref
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
group	by salescontactref 
order	by 1 desc

select	sum(tradeprofit) as TotalTradePofit,companyname,
		userid
from	[dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
group	by companyname,
		userid
order	by 1 desc 


select * from [dw].[FactCurrencyTradeCrossApplyToDetail_NonRetail]
where userid  = 'rjohnson@cafedirect.co.uk'

select distinct helpdeskuserid from dw.[FactCurrencyTradeCrossApplyToDetail_NonRetail]



select top 10 * from firebird.factcurrencybanktrade order by id desc 


exec sp_who2 active

dbcc inputbuffer(107)

select max(valitoraccountid) from DW.DimCustomerReference (nolock) 
