select	AA.CustomerKey, sum (ABS(aa.BillingAmountGBP) ) AS BillingAmountGBP, 
		BB.NameCode	
from	valitor.FactWalletTransactions aa
join	dw.DimISOCurrencyName bb
on		aa.BillingCurrencyKeyISO = bb.ISOCurrency_KEY
where	aa.CustomerKey = 7673 and Description = 'Load'
group by aa.CustomerKey, bb.NameCode

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
with cte1 as (
select CUSTOMERKEY,[EUR],[GBP],[USD],[AUD],[CAD],[NZD],[HKD],[CHF],[JPY],[DKK],[ZAR],[PLN],[SEK],[NOK],[HUF]
FROM
(
select	AA.CustomerKey, ABS(aa.BillingAmountGBP) AS BillingAmountGBP, 
		ISNULL(BB.NameCode,0) AS NameCode	
from	valitor.FactWalletTransactions aa
join	dw.DimISOCurrencyName bb
on		aa.BillingCurrencyKeyISO = bb.ISOCurrency_KEY
where	/*aa.CustomerKey = 7673 and*/ Description = 'Load') as SourceTable
PIVOT
(
SUM(SourceTable.BillingAmountGBP) 
FOR NAMECODE IN([EUR],[GBP],[USD],[AUD],[CAD],[NZD],[HKD],[CHF],[JPY],[DKK],[ZAR],[PLN],[SEK],[NOK],[HUF])
) AS PIVOTTABLE)
select	BB.FirstName,
		BB.LastName,
		bb.ContactEmailAddress,
		BB.AffiliateCode,
		BB.HeardAboutUs,
		bb.customerkey,
		isnull([EUR],0) AS EUR_TOTAL,
		isnull([GBP],0) AS GBP_TOTAL,
		isnull([USD],0) AS USD_TOTAL,
		isnull([AUD],0) AS AUD_TOTAL,
		isnull([CAD],0) AS CAD_TOTAL,
		isnull([NZD],0) AS NZD_TOTAL,
		isnull([HKD],0) AS HKD_TOTAL,
		isnull([CHF],0) AS CHF_TOTAL,
		isnull([JPY],0) AS JPY_TOTAL,
		isnull([DKK],0) AS DKK_TOTAL,
		isnull([ZAR],0) AS ZAR_TOTAL,
		isnull([PLN],0) AS PLN_TOTAL,
		isnull([SEK],0) AS SEK_TOTAL,
		isnull([NOK],0) AS NOK_TOTAL,
		isNull([HUF],0) AS HUF_TOTAL
		FROM	CTE1 JOIN DW.DimCustomerReference BB
		ON		CTE1.CUSTOMERKEY = BB.CUSTOMERKEY 


		exec sp_help 'dw.dimcustomerreference'

					


---------------------------------------------------------------------------------------------------
select * from [dw].[MinimumLoadTransactionKey]

with cte1 as (
select	cc.contactemailaddress,bb.NameCode,aa.*	
from	valitor.FactWalletTransactions aa
join	dw.DimISOCurrencyName bb
on		aa.BillingCurrencyKeyISO = bb.ISOCurrency_KEY
join	dw.DimCustomerReference cc
on		aa.CustomerKey = cc.CustomerKey
where	aa.CustomerKey = 7673 and Description = 'Load')
select	customerkey,id,namecode,contactemailaddress,transactiondatekey,billingamountgbp,
		billingamount,transactionamountgbp,billingCurrency, billingcurrencykeyiso,
		transactioncurrency,transactioncurrencykeyiso,registrationdate,transactiondatetime,
		transactionkeycategory, merchantcity,merchantname,
		transactionamount,description, 
		downloadfilename
from	cte1 order by transactiondatetime


select * from dw.DimCurrencyCodesISO4217
