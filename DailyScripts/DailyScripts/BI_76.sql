drop table ADHOC.MT_23K_ANALYSIS
go
with	cteAlltimeTrades as (
select	customerkey,
		count(rowid) as Talksheet_MT_DealCount_Alltime,
		min(dealdate) as Talksheet_MT_MinDealDate_Alltime,
		max(dealdate) as Talksheet_MT_MaxDealDate_Alltime,
		sum(revenue) as Talksheet_MT_RevenueSum_Alltime
from	dbfx.deallist (nolock)
group	by customerkey)

--select * from cteAllTimeTrades
,
cte12month as (
select	customerkey,
		count(rowid) as Talksheet_MT_DealCount_12Months,
		min(dealdate) as Talksheet_MT_MinDealDate_12Months,
		max(dealdate) as Talksheet_MT_MaxDealDate_12Months,
		sum(revenue) as Talksheet_MT_RevenueSum_12Month
from	dbfx.deallist (nolock)
where   dealdate >= '2015-10-01 00:00:00.000'
group	by customerkey)

--select * from cte12month 

select	title
		,aa.firstname
		,aa.lastname
		,contactemailaddress
		,dateofbirth
		,datediff(yy,dateofbirth,getdate()) as age
		,gender = case when gender = 'unknown' and title = 'Mr' then 'Male'
				when gender = 'unknown' and title = 'Miss' then 'Female'
				when gender = 'unknown' and title = 'Mrs' then 'Female'
				when gender = 'unknown' and title = 'Mr.' then 'Male'
		else gender end
		,aa.accounttyperef
		,aa.AccountTypeRefDerived

		,isnull(cte12month.Talksheet_MT_DealCount_12Months,0) as Talksheet_MT_DealCount_12Months
		,isnull(cte12Month.Talksheet_MT_RevenueSum_12Month,0) as Talksheet_MT_RevenueSum_12Month

		,isnull(cteAlltimeTrades.Talksheet_MT_DealCount_Alltime,0) as Talksheet_MT_DealCount_Alltime
		,ISNULL(cteAlltimeTrades.Talksheet_MT_RevenueSum_Alltime,0) as Talksheet_MT_RevenueSum_Alltime
	
		into ADHOC.MT_23K_ANALYSIS

from	dw.dimcustomerreference aa (nolock)
left	outer join	cteAlltimeTrades 
on		aa.customerkey = cteAlltimeTrades.customerkey
left	outer join cte12Month
on		aa.customerkey  = cte12Month.customerkey 
--where	aa.AccountTypeRefDerived <> 'Corporate Money transfer - Talksheet'
--and	Talksheet_MT_DealCount_Alltime < 1

--select		distinct accounttyperef from dw.DimCustomerReference

select *	FROM ADHOC.MT_23K_ANALYSIS
where		accounttyperefderived in ('Retail Money Transfer - Talksheet')
and			Talksheet_MT_DealCount_Alltime = 0
and			contactemailaddress <> ''
and			contactemailaddress not like '%caxton%'
union 
select *	FROM ADHOC.MT_23K_ANALYSIS
where		accounttyperefderived in ('Retail Money Transfer - Talksheet')
and			Talksheet_MT_DealCount_12Months = 0
and			contactemailaddress <> ''
and			contactemailaddress not like '%caxton%'

