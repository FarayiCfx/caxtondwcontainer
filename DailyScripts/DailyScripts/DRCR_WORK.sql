select top 10 * from Settlement.FileImport

select	* 
from	Settlement.RbsStatement aa
join	CurrencyBank.Trade bb
on		cast(aa.TradeFundingHintList as int) = bb.Id
where	AccountTransactionId > 0


drop table if exists #temp1 ;

select	  bb.id
		, bb.TransactionDateTime
		, bb.Amount
		, bb.Balance
		, bb.Status
		, bb.AccountSummaryId
		, aa.DebitValue
		, aa.CreditValue
		, aa.AccountName
		, aa.AccountCurrency
		, aa.Narrative1
		, aa.Narrative2
		, aa.Narrative3
		, aa.Narrative4
		, aa.Narrative5
		, aa.TransactionType
		, aa.MatchType
		, aa.DateMatched
		, aa.TransactionDate as TranDateInStmtTable
		, aa.DateCreated
		, aa.SettlementType
into	 #temp1 
from	Settlement.RbsStatement aa (nolock) 
right	outer join	CurrencyBank.AccountTransaction bb (nolock)
on		aa.AccountTransactionId = bb.id
and		bb.ExternalTransRef = 'rbs'
where	aa.AccountTransactionId > 0

--lets use one person as an example
select top 10 * from #temp1  where Narrative1 like 'TRANREF%552851' ORDER BY 1 DESC

select top 10 * from payment.transfer where id = 552851

select * from currencybank.accountsummary where id = 630559

select * from CurrencyBank.AccountTransaction where AccountSummaryId = 630559

select top 10 * from CurrencyBank.TradeDetail order by 1 desc

select top 10 * from CurrencyBank.AccountTransaction where id = 9731075

select * from CurrencyBank.AccountTransaction where accountsummaryid IN (628063)

select * from Settlement.RbsStatement where AccountTransactionId = 9731074

select * from CurrencyBank.AccountSummary WHERE UserId ='greg.branch@scio-capital.com'

select top 10 * from [Payment].[Transfer] where id = 552655
select top 10 * from CurrencyBank.AccountTransaction where id = 9726310

select top 10 * from CurrencyBank.Trade

exec sp_help 'firebird.factCurrencyBankAccountTransaction'
exec sp_help 'Settlement.RbsStatement'

select * from firebird.FactCurrencyBankTrade where id = 2201252

select	* 
from	firebird.factCurrencyBankAccountTransaction 
where	accountsummaryid = 600377
order	by id asc

select	top 10	[dbo].[ufn_separates_columns]([fileentry],11,'|')
				,* 
from			settlement.rbsstatement  order by id desc 