insert into staging.ContactEmailsWarehouse(username_warehouse,ContactEmailaddress_warehouse,[Id_warehouse],CustomerKey)
select	username  , 
		ContactEmailAddress,
		[id] ,
		CustomerKey
from	dw.DimCustomerReference (NOLOCK)
where	username is not null
order by username asc

select count(*) from staging.ContactEmailsWarehouse (nolock)
select count(*) from staging.ContactEmailsProduction (nolock)


-------------------------------------------------------------------------------
 

create table staging.ContactEmailsProduction
(RowId			int identity (1,1),
username		nvarchar (512) null,
contactemail	nvarchar (max) null,
[id]			nvarchar (256))


create table staging.ContactEmailsWarehouse
(Row_id			int identity (1,1),
username_warehouse nvarchar (512),
contactemailaddress_warehouse nvarchar (max) ,
id_warehouse	nvarchar (max) ,
customerkey		bigint)


with cte1 as (
select * from staging.ContactEmailsProduction aa
join staging.ContactEmailsWarehouse bb
on aa.username = bb.username_warehouse
and aa.id = bb.id_warehouse)

drop table dw.ContactEmailChangeAuditLog
go

create table dw.ContactEmailChangeAuditLog
(Rowid int identity(1,1),
username, 
OldContactEmail nvarchar(max),
NewContactEmail nvarchar(max),
Customerkey bigint,
DWDateChangeLog datetime )

exec sp_help 'dw.dimcustomerreference'



alter procedure dw.dimcustomercontactemailchangetype1scd
as
set		nocount on

if		object_id('tempdb..#temp1') is not null
drop	table #temp1;

with	cte1 as (
select	* from staging.contactemailsproduction aa
join	staging.contactemailswarehouse bb
on		aa.username = bb.username_warehouse
and		aa.id = bb.id_warehouse)

select	* into #temp1
from	cte1 where contactemail <> contactemailaddress_warehouse
;
update	t1
set		t1.contactemailaddress = #temp1.contactemail 

output	deleted.contactemailaddress,inserted.contactemailaddress,#temp1.customerkey,getdate()
into	dw.contactemailchangeauditlog --write to the audit log 

from	dw.dimcustomerreference_testupdates t1
join	#temp1 on t1.customerkey = #temp1.customerkey  
go
-------------------------------------------------------------------------------------------------------------
exec dw.DimCustomerContactEmailChangeType1SCD


---testing rig
truncate	table dw.ContactEmailChangeAuditLog 
go
drop		table dw.DimCustomerReference_testupdates
go
select		*	into dw.DimCustomerReference_testupdates 
from		dw.DimCustomerReference (nolock)
go
-----

with		cte1 as (
select		* from staging.ContactEmailsProduction aa
join		staging.ContactEmailsWarehouse bb
on			aa.username = bb.username_warehouse
and			aa.id = bb.id_warehouse)

select		* 
from		cte1 where contactemail <> contactemailaddress_warehouse
go

select		* from dw.ContactEmailChangeAuditLog
