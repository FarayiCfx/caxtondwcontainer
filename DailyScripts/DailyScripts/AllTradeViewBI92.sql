exec sp_spaceused '[FireBird].[FactCurrencyBankTradeDetail]'


select * from [Firebird].[FactCurrencyBankTrade]

select * from [FireBird].[FactCurrencyBankTradeDetail]
where	tradeid = 1043849




select	*
from	[Firebird].[FactCurrencyBankTrade] aa
JOIN	[FireBird].[FactCurrencyBankTradeDetail] bb
on		aa.Id = bb.tradeid 
where	tradeid = 1043849

 

select	aa.id,aa.buyccycode,aa.BuyAmount, 
		aa.SellAmount,
		aa.Settled, 
		aa.TradeDate, 
		aa.TradeEndDate as [SettlementDate],
		aa.UserId as [*user*],
		bb.*	
from	[Firebird].[FactCurrencyBankTrade] aa
JOIN	[FireBird].[FactCurrencyBankTradeDetail] bb
on		aa.Id = bb.tradeid 
where	tradeid = 1043849
--------------------------------------------------------------------------------------
DROP VIEW DW.FactCurrencyTradeCrossApplyToDetail
GO
CREATE VIEW DW.FactCurrencyTradeCrossApplyToDetail
as
with cteResultSet as (
select	aa.id as TradeId,
		aa.buyccycode,
		aa.BuyAmount, 
		AA.SellCcyCode,
		aa.SellAmount,
		aa.Settled, 
		cast(aa.TradeDate as Date) as TradeDate, 
		cast(aa.TradeEndDate as Date) as [SettlementDate],
		aa.UserId ,
		BB.*

from	[Firebird].[FactCurrencyBankTrade] AS aa
OUTER	apply	(select tradeid as tradeid_summarytable , 
						sum(amount) as AmountPaid
				from	[FireBird].[FactCurrencyBankTradeDetail] bb
				where	bb.tradeid = AA.id
				GROUP	BY BB.tradeid) BB)
				select	* ,
						'GREEN' as [Booked_Text], 
						PartFunded_Text = case when AmountPaid < SellAmount then 'GREEN' else 'RED' end,
						PartFunded = case when AmountPaid < SellAmount then 1 else 0 end
						Funded_Text = case When AmountPaid = SellAmount then 'GREEN' else 'RED' END,
						Funded = case When AmountPaid = SellAmount then 1 else 0 END,
						IsSettledText = case	when settled = 0 and settlementdate < getdate() then 'RED'
										when	settled = 0 and settlementdate > getdate() then 'AMBER'
										when	settled = 1 then 'GREEN'
										END,
						IsSettled = case	when settled = 0 and settlementdate < getdate() then 3
										when	settled = 0 and settlementdate > getdate() then 2
										when	settled = 1 then 1
										END

				
				from cteResultset

select	tradeid , 
		sum(amount) as AmountSum
from	[FireBird].[FactCurrencyBankTradeDetail]
where	tradeid = 1043849
group	by tradeid 


set statistics time on 
set statistics io on 

select * from DW.FactCurrencyTradeCrossApplyToDetail 
where tradeid = 1043849


