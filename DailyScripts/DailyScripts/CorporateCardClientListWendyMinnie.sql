select top 5 * from dw.dimcorporateentities order by 1 desc 

select top 5 * from dw.dimusercorporateassociation 

select top 5 * from reference.industry

select top 5 * from reference.industry

select top 5 * from dw.dimcustomerreference 

alter view CorpCardClientsList 
as 
select	ri.[text] as Industry,
		bb.companyname,
		isnull([dw].[UdfGetCardClaimValue](dw.customerkey),'') as ClaimValue,
		CorporateRole = case  aa.corporaterole	when 0 then 'User'
												when 1 then 'Signatory'
												when 2 then 'Admin'
												when 3 then 'Account Owner'
												end,
												dw.Title,
												dw.FirstName,
												dw.LastName,
												dw.AddressLine1,
												dw.AddressLine2,
												dw.City,
												replace (dw.county,'unknown','') as County,
												dw.Postcode,
												replace(dw.country,'unknown','') as Country,
												dw.PrimaryTelephone,
												dw.username as EmailAddress,
												isnull(bb.currencypairstraded,'') as [Currency Paired],
												isnull(TradingAs,'') as TradingAs,
												TalkSheetMigrated = case TalksheetMigrated when 1 then 'True'
												when 0 then 'False' end ,
												cast (aa.DateAdded as Date) as OnboardDate
from	dw.dimusercorporateassociation  aa (nolock)
left	outer join  dw.dimcorporateentities bb (nolock)
on		aa.corporateentityid = bb.id
join	dw.dimcustomerreference dw (nolock)
on		aa.email = dw.username
left	outer join reference.industry ri (nolock)
on		bb.industryid = ri.id
order	by aa.DateAdded desc




select top 10 * from valitor.factwallettransactions
where description = 'load'
