create VIEW ReActivatedCards
as
with	cteFirstInSequence as (
select	Id
		,walletid 
		,valitorpanid
		,panstatuscode
		,cardstatuscode
		,CardStatusText = case cardstatuscode when 'V' then 'Active'
						when 'U' then 'Inactive'
						when 'M' then 'Inactive'
						when 'F' then 'Inactive'
						when 'P' then 'Inactive'
						when 'A' then 'Blocked'
						when 'L' then 'Terminated'
						end
		,isreissued
		,datecreated
		,isdirty
		,expiry
		,cardsequencenumber
		,fb_datecreated
from	valitor.pan (nolock)
WHERE	cardsequencenumber = '000'
and		isdirty = 0
and		cardstatuscode = 'v'),

		cteNextSequence as (
select	Id as IdNext
		,valitorpanid as ValitorPanIdNext
		,panstatuscode as panstatuscodeNext
		,cardstatuscode as CardStatusCodeNext
		,CardStatusText_ReIssue = case cardstatuscode when 'V' then 'Active'
						when 'U' then 'Inactive'
						when 'M' then 'Inactive'
						when 'F' then 'Inactive'
						when 'P' then 'Inactive'
						when 'A' then 'Blocked'
						when 'L' then 'Terminated'
						end
		,IsCardReactivated = case cardstatuscode when 'v' then 'Re-Activated card' else 'No Re-Activation of card' end 
		,datecreated as DatecreatedNext
		,isdirty as IsDirtyNext
		,expiry as ExpiryNext
		,cardsequencenumber as CardSequencenumberNext
		,fb_datecreated AS fb_datecreatedNext
		,isreissued as reissuedNext
from	valitor.pan (nolock)
WHERE	cardsequencenumber <> '000'
and		isdirty = 0
)

, cteIsActiveOrNot as (
select	aa.id
		,Datediff(mm,cardload_lastdate,getdate()) as MonthSinceLastLoad
		,"IsActiveInLast6Months" = case when Datediff(mm,cardload_lastdate,getdate()) < 6 then 'Active Customer' else 'InActive Customer' end

from	valitor.pan aa (nolock)
left	outer join dw.dimvalitorwallet bb (nolock)
on		aa.walletid = bb.id
left	outer join	dw.dimcustomerreference cc
on		bb.customerkey = cc.customerkey 
left	outer join	[dw].[CustomerRFMAggregations] ee
on		cc.customerkey = ee.customerkey )

select			aa.*
				,bb.CardStatusText_ReIssue
				,isnull(bb.IsCardReactivated,'No Re-Activation of card') as IsCardReactivated
				,bb.CardStatusCodeNext
				,cc.MonthSinceLastLoad
				,cc.Isactiveinlast6months

from			cteFirstInSequence aa 
left outer		join cteNextSequence bb
on				aa.valitorpanid = bb.ValitorPanIdNext 
left outer		join cteIsActiveOrNot cc
on				aa.id = cc.id

------------------------------------------------------------------------------------------------------------------
create view ReactivationRate 
as
with	cte1 as (
select	expiry
		,[Active Customer : Re-Activated card] 
		,[Active Customer : No Re-Activation of card]
		,[InActive Customer : Re-Activated card]
		,[InActive Customer : No Re-Activation of card]
		
			
from 
(select	id
		,expiry
		,IsActiveInLast6Months +' : '+IsCardReactivated as [Status]
from	ReActivatedCards
where	expiry >= 1701) as sourcetable 

pivot
(
count(id)
for [status] in ([Active Customer : Re-Activated card]
				,[Active Customer : No Re-Activation of card]
				,[InActive Customer : No Re-Activation of card]
				,[InActive Customer : Re-Activated card])
)AS pivottable)

select	*
		,cast ([Active Customer : Re-Activated card] as decimal(6,2))
		/
		(
		cast([Active Customer : Re-Activated card] as decimal(6,2)) + 
		cast([Active Customer : No Re-Activation of card] as decimal(6,2)) 
		) * 100 as [ActiveCustomer%Reactivation]
		,
		(
		cast([InActive Customer : Re-Activated card]  as decimal(8,2))
		/
		(cast([InActive Customer : No Re-Activation of card] as decimal(8,2)) +
		cast([InActive Customer : Re-Activated card] as decimal (8,2)))
		) * 100 as [Inactive%Reactivation] 

		,	[Active Customer : Re-Activated card] 
			+[Active Customer : No Re-Activation of card]
			+[InActive Customer : Re-Activated card]
			+[InActive Customer : No Re-Activation of card] as [TotalMailed \ Eligible]
		
		,	(
			cast([Active Customer : Re-Activated card] as decimal(8,2)) + cast ([Active Customer : Re-Activated card] as decimal(8,2))
			)

			/

			(cast([Active Customer : Re-Activated card] as decimal(8,2))
			+cast([Active Customer : No Re-Activation of card] as decimal(8,2))
			+cast([InActive Customer : Re-Activated card] as decimal(8,2)) 
			+cast([InActive Customer : No Re-Activation of card]  as decimal(8,2))
			) * 100 as [OverallActivation%]
		
from	cte1 
order by 1 


select * from ReactivationRate order by 1 



----------------------------------------------------------------------------------------------------------------
select  count(*) 
from	valitor.pan
where	expiry = 1706
and		cardstatuscode = 'v'
and		isdirty = 0
and		cardsequencenumber = '000'
------------------------------------------------------------------	-------------------------------------------


