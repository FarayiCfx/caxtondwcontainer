select top 10 * from [fis].[factfinadv]

IF			OBJECT_ID('adhoc.top_fis_countries', 'U') IS NOT NULL
DROP		TABLE adhoc.top_fis_countries;
select		customerkey,termcountry,sum(amttxn_gbp) as FisGbpSum
into		adhoc.top_fis_countries
from		fis.factfinadv (nolock)
group by	customerkey,termcountry

select	* ,  
		[dw].[UdfGetCountryNameFrom2LetterCode] (TERMCOUNTRY) as country
from	adhoc.top_fis_countries order by customerkey,FisGbpSum

IF		OBJECT_ID('adhoc.top_valitor_countries', 'U') IS NOT NULL
DROP	TABLE adhoc.top_valitor_countries;
select	customerkey as customerkeyvalitor ,
		merchantcountrycode,
		sum(transactionamountgbp) as ValitorGbpSum
into	adhoc.top_valitor_countries
from	valitor.factwallettransactions 
where	type = 'presentment' and transactionkeycategory = 'sale'
group	by customerkey , merchantcountrycode

select top 10 * from adhoc.top_valitor_countries

select	*,dw.udfgetcountryname(merchantcountrycode) as country
from	adhoc.top_valitor_countries
where	isnumeric(merchantcountrycode) = 1 


IF		OBJECT_ID('adhoc.top_country_spends', 'U') IS NOT NULL
DROP	TABLE adhoc.top_country_spends;
;
with	cte1 as (
select	* ,  
		[dw].[UdfGetCountryNameFrom2LetterCode] (TERMCOUNTRY) as country
from	adhoc.top_fis_countries),
cte2 as (select	*,dw.udfgetcountryname(merchantcountrycode) as countryvalitor
from	adhoc.top_valitor_countries
where	isnumeric(merchantcountrycode) = 1)
select * into adhoc.top_country_spends
from	cte1 full outer join cte2 on 
cte1.customerkey = cte2.customerkeyvalitor
and cte1.country = cte2.countryvalitor
order by cte1.customerkey 

select * from adhoc.top_country_spends


IF		OBJECT_ID('adhoc.rankedspendbycustomer', 'U') IS NOT NULL
DROP	TABLE adhoc.rankedspendbycustomer;

with cteCrossJoin as(
select isnull(fisgbpsum,0) + isnull(valitorgbpsum,0) as TotalInGbp,
coalesce(customerkey,customerkeyvalitor) as customerkey,
coalesce(country,countryvalitor) as [country]
from adhoc.top_country_spends
)
select	* ,rank() over (partition by customerkey order by totalingbp desc) as rankspend
into adhoc.rankedspendbycustomer
from	ctecrossjoin
order	by customerkey


select aa.* ,bb.contactemailaddress,firstname,lastname
from adhoc.rankspendbycustomer aa
join dw.dimcustomerreference bb 
on aa.customerkey = bb.customerkey
and aa.rankspend = 1
