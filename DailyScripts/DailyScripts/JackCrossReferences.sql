--all corp spot trade revenue 
----spot trades in total Jack Bassett calculations 
with cte1 as(
select * from [dbo].[CurrencyBankTradeAccTypeRef]
where cast (tradedate as date) between '2017-07-01' and '2017-07-31'
and		reversal = 0
and		isreversed = 0
and		isunwound = 0)
select *	from cte1 
where		accounttyperef <> 'retail'
and			tradereason in ('buy and send','buy and hold')

	select	count(*)   AS COUNT_, CAST(TRADEDATE AS DATE) AS DATE_, TRADEREASON
	from	currencybank.trade (nolock)
	where	cast(tradedate as date) between '2017-07-01' and '2017-08-01'
	and		tradereason = 'card load'
	GROUP BY  CAST(TRADEDATE AS DATE) , TRADEREASON
	ORDER BY 2 ASC 
	go

	select count(*) AS COUNT_ , CAST(TRADEDATE AS DATE) AS DATE_ , TRADEREASON
	from	[dbo].[CurrencyBankTradeAccTypeRef] (nolock)
	where	cast(tradedate as date) between '2017-07-01' and '2017-08-01'
	and		tradereason = 'card load'
	GROUP BY  CAST(TRADEDATE AS DATE) , TRADEREASON
	ORDER BY 2 ASC 

	CREATE TABLE #TEMPTABLE
	(
	Rowid int identity (1,1)
	,tradeid bigint 
	)

	SELECT * FROM #TEMPTABLE

	TRUNCATE TABLE #TEMPTABLE



 with cte1 as ( 
 select * from currencybank.trade (nolock)
 where cast(tradedate as date) between '2017-07-01' and '2017-07-31' 
 )

 select aa.*,bb.* 
 INTO #TEMP2
 from	#temptable aa
 left	outer join cte1 bb
 on		aa.tradeid  = bb.id

 SELECT * FROM #temptable

 SELECT COUNT(TRADEID) , TRADEID FROM #TEMP2 GROUP BY TRADEID ORDER BY 1 DESC 
  
  SELECT * FROM CURRENCYBANK.TRADE WHERE ID = 1919084

 SELECT * FROM [dbo].[CurrencyBankTradeAccTypeRef] WHERE ID = 1919084


WITH CTE1 AS 
(
select	* 
from	[dbo].[CurrencyBankTradeAccTypeRef] (nolock)
where	cast(tradedate as date) between '2016-08-18' and '2017-08-18' 
)
SELECT	DISTINCT USERID , ACCOUNTTYPEREF, CORPSALESCONTACTREF,CORPAMCONTACTREF,RETAILAMCONTACTREF 
FROM	CTE1 
WHERE	TRADEREASON IN ('BUY AND SEND','BUY AND HOLD')
AND		RETAILAMCONTACTREF IS NULL
AND		CORPAMCONTACTREF IS NULL
--AND		CORPSALESCONTACTREF IS NULL
AND		ACCOUNTTYPEREF <> 'RETAIL'
ORDER BY 1 


WITH CTE1 AS 
(
select	* 
from	[dbo].[CurrencyBankTradeAccTypeRef] (nolock)
where	cast(tradedate as date) between '2017-08-11' and '2017-08-18' 
)
SELECT	DISTINCT USERID , ACCOUNTTYPEREF, CORPSALESCONTACTREF,CORPAMCONTACTREF,RETAILAMCONTACTREF 
FROM	CTE1 
WHERE	TRADEREASON IN ('BUY AND SEND','BUY AND HOLD')
AND		RETAILAMCONTACTREF IS NULL
AND		CORPAMCONTACTREF IS NULL
--AND		CORPSALESCONTACTREF IS NULL
AND		ACCOUNTTYPEREF <> 'RETAIL'
ORDER BY 1 