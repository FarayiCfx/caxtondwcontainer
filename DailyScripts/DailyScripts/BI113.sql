select	distinct accounttyperef,accounttyperefderived 
from	dw.DimCustomerReference


select top 10  * from dbfx.DealList order by dealdate desc 

exec sp_spaceused'dbfx.DealList' 

select	customerkey,
		originator,
		count(rowid) as count_,
		max(dealdate) as maxdealdate
		from dbfx.deallist 
		group by customerkey, originator
order	by customerkey 


IF OBJECT_ID('tempdb..#Results') IS NOT NULL
DROP TABLE #Results
go
with cteDealListMetrics as (
select	customerkey,
		originator,
		count(rowid) as count_,
		max(dealdate) as maxdealdate
		from dbfx.deallist (nolock)
		group by customerkey, originator)
select	aa.AccountTypeRefDerived, 
		aa.CustomerKey, 
		aa.ContactEmailAddress, 
		aa.DBFXContact_id, 
		aa.DBCARDSContact_id, 
		bb.originator, 
		bb.count_, 
		isnull(bb.maxdealdate,'1900-01-01 00:00:00.000') as maxdealdate
into	#results
from	dw.DimCustomerReference aa
left	outer join	cteDealListMetrics bb
on		aa.CustomerKey = bb.CustomerKey
where	AccountTypeRefDerived 
in		('Corporate Money transfer - Talksheet',
		'Retail Money Transfer - Talksheet')

--total database numbers 
select	count(distinct customerkey)  as count_,
		accounttyperefderived 
from	#results
group	by accounttyperefderived

--Total inactive client database count
select	count(*) as total_inactive_both_types 
from	#results
where	maxdealdate < dateadd(mm,-24,getdate())

--Total inactive FastPay database count
select	count(*) as total_inactive_retail  	
from	#results
where	maxdealdate < dateadd(mm,-24,getdate())
and		accounttyperefderived = 'Retail Money Transfer - Talksheet'


select * from #results where customerkey = 448097

with cte2Types as (
select	count(customerkey) as count_, 
		customerkey
from	#results
group by customerkey
having count(customerkey) > 1)
select * from #results where customerkey in (select customerkey from cte2types)
