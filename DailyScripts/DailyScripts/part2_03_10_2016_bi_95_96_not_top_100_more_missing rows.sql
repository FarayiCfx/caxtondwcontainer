select top 10 * from account where Account_short_name like '%valski%'


IF		OBJECT_ID('tempdb..#temptable') IS NOT NULL
DROP	TABLE #temptable
go
create	table #temptable	(idrow int identity (1,1),
							company_name varchar(255)  COLLATE SQL_Latin1_General_CP1_CI_AS)
GO

insert into #temptable(company_name) values('Dmacs Ltd')
insert into #temptable(company_name) values('Cnc World Ltd')
insert into #temptable(company_name) values('Prenton High School For Girls Visa')
insert into #temptable(company_name) values('Parkside Federation Academies')
insert into #temptable(company_name) values('Active Retail Equity Ltd')

select distinct company_name from #temptable

--------------------------------------
IF		OBJECT_ID('tempdb..##finaloutput_bi95') IS NOT NULL
DROP	TABLE ##finaloutput_bi95
go
with	cte1 as (
select	rank() over (partition by aa.Account_short_name order by bb.branch_id desc) as rank,
		cast(aa.Account_id as int) Account_id,
		bb.branch_id,
		aa.Account_short_name as [Company Name], 
		aa.Account_main_contact, 
		bb.branch_address1 as Registered_Address1, 
		bb.branch_address2 as Registered_Address2, 
		cc.CityName as Registered_city, 
		ss.State as Registered_County, 
		bb.branch_zip as Registered_Postcode,
		cr.Country as Registered_country ,
		aa.Account_full_name as  [Company Full name]
from	account aa
left	outer JOIN	branch bb
on		aa.Account_id = bb.account_id
left outer join	CityName cc
on		bb.branch_city = cc.CityName_ID
left outer join 	dbo.[state] ss
on		bb.branch_state = ss.State_ID
left outer join	country cr
on		bb.branch_country = cr.Country_ID
where	aa.Account_short_name in(select company_name from #temptable)),

cte2 as (select distinct company_name from #temptable)

select	* 
into	##finaloutput_bi95
from	cte2 AA LEFT OUTER JOIN CTE1 BB ON 
		AA.company_name = BB.[Company Name]
--and		bb.[rank] = 1
-->
--the following is the list to send to Beata 
--select * from ##finaloutput_bi95 

select	distinct company_name, registered_address1,registered_address2,registered_city,registered_county,
		registered_postcode,registered_country,isnull([company full name],'not found in talksheet') as [TradingAs]
from	##finaloutput_bi95
-----------------------------------------------------------------------------------------------------------------
IF	 OBJECT_ID('tempdb..#emailwithranks') IS NOT NULL
DROP TABLE #emailwithranks
go
;

with	cte1 as (
select	rank() over (partition by contact_id order by Contact_Email_id desc) as rank_,
		* 
from	contact_email (nolock))
select	*  into #emailwithranks 
from	cte1 where rank_ = 1 ;


with		cteEuroCorp as (
select		Contact_ID,
			isnull(CurrencyCardDetailID,'') as [EURCARDID],
			isnull(CardNumber,'') AS [EURCARDNUMBER],
			[EURCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459566')
,
cteGBPCorp as (
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [GBPCARDID],
        isnull(CardNumber,'') AS [GBPCardNumber],
		[GBPCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459567')
,
cteUSDCorp as(
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [USDCARDID],
        isnull(CardNumber,'') AS [USD Card Number],
		[USDCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459568')

select  aa.contact_id ,
        isnull(aa.parent_id,0) as parent_id,
        acc.Account_short_name as [Company Name (not for upload)],
        AA.contact_first_name,
        AA.contact_last_name,
        Gender = case    when contact_salutation like 'Ms%' then 'Female'
                        when contact_salutation like 'Mrs%' then 'Female'
                        when contact_salutation like 'Miss%' then 'Female' 
                        when contact_salutation like 'Mr%' then 'Male'
                        else 'not supplied'
                        end ,
                        dob ,
						case	when isnull(aa.parent_id,0) = 0 then 'Yes'
								when isnull(aa.parent_id,0) > 0 then 'No'
						end as IsAdmin ,
        datediff(yy,isnull(dob,''),getdate()) as Age, 
        eml.email_address ,
        aa.account_id as [Card group name],
        isnull(usd.usdcardid,'') as usdcardid,
        isnull(usd.[usd card number],'') as [usd card number],
		[USDCardstatus],
        isnull(eur.[eurcardid],'') as [EURCARDID],
        isnull(eur.[EURCARDNUMBER],'') as [EURCARDNUMBER],
		[EURCardStatus],
        isnull(gbp.[GBPCARDID],'') as [GBPCARDID],
        isnull(gbp.[GBPCardNumber],'') as [GBPCardNumber],
		[GBPCARDSTATUS]
from    Contact aa
left    outer join cteEuroCorp eur
on        aa.contact_id = eur.contact_id
left    outer join cteGBPCorp gbp
on        aa.contact_id = gbp.contact_id 
left    outer join cteUSDCorp usd
on        aa.contact_id = usd.contact_id
left    outer join #emailwithranks eml
on        aa.contact_id = eml.contact_id
 left outer join    Account acc
on        aa.account_id = acc.Account_id
where    aa.contact_id in
(select    contact_id 
from    Contact
where    account_id in (select account_id from ##finaloutput_bi95)
)
order    by aa.account_id,aa.parent_id ASC

-------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------with status numbers only below 
IF	 OBJECT_ID('tempdb..#emailwithranks') IS NOT NULL
DROP TABLE #emailwithranks
go
;

with	cte1 as (
select	rank() over (partition by contact_id order by Contact_Email_id desc) as rank_,
		* 
from	contact_email (nolock))
select	*  into #emailwithranks 
from	cte1 where rank_ = 1 ;


with		cteEuroCorp as (
select		Contact_ID,
			isnull(CurrencyCardDetailID,'') as [EURCARDID],
			isnull(CardNumber,'') AS [EURCARDNUMBER],
			[CardStatus] as [EURCardStatus]  
			
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459566')
,
cteGBPCorp as (
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [GBPCARDID],
        isnull(CardNumber,'') AS [GBPCardNumber],
		 [CardStatus] as [GBPCardStatus] 
			
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459567')
,
cteUSDCorp as(
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [USDCARDID],
        isnull(CardNumber,'') AS [USD Card Number],
		[CardStatus] as [USDCardStatus]
			
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459568')

select  aa.contact_id ,
        isnull(aa.parent_id,0) as parent_id,
        acc.Account_short_name as [Company Name (not for upload)],
        AA.contact_first_name,
        AA.contact_last_name,
        Gender = case    when contact_salutation like 'Ms%' then 'Female'
                        when contact_salutation like 'Mrs%' then 'Female'
                        when contact_salutation like 'Miss%' then 'Female' 
                        when contact_salutation like 'Mr%' then 'Male'
                        else 'not supplied'
                        end ,
                        dob ,
						case	when isnull(aa.parent_id,0) = 0 then 'Yes'
								when isnull(aa.parent_id,0) > 0 then 'No'
						end as IsAdmin ,
        datediff(yy,isnull(dob,''),getdate()) as Age, 
        eml.email_address ,
        aa.account_id as [Card group name],
        isnull(usd.usdcardid,'') as usdcardid,
        isnull(usd.[usd card number],'') as [usd card number],
		[USDCardstatus],
        isnull(eur.[eurcardid],'') as [EURCARDID],
        isnull(eur.[EURCARDNUMBER],'') as [EURCARDNUMBER],
		[EURCardStatus],
        isnull(gbp.[GBPCARDID],'') as [GBPCARDID],
        isnull(gbp.[GBPCardNumber],'') as [GBPCardNumber],
		[GBPCARDSTATUS]
		into #temp1
from    Contact aa
left    outer join cteEuroCorp eur
on        aa.contact_id = eur.contact_id
left    outer join cteGBPCorp gbp
on        aa.contact_id = gbp.contact_id 
left    outer join cteUSDCorp usd
on        aa.contact_id = usd.contact_id
left    outer join #emailwithranks eml
on        aa.contact_id = eml.contact_id
 join    Account acc
on        aa.account_id = acc.Account_id
where    aa.contact_id in
(select    contact_id 
from    Contact
where    account_id in (select account_id from ##finaloutput_bi95)
)
order    by aa.account_id,aa.parent_id ASC

select * from #temp1
where [usd card number] <> '' and usdcardstatus is null


select * from #temp1
where [eurcardnumber] <> '' and eurcardstatus is null

select * from #temp1
where [gbpcardnumber] <> '' and gbpcardstatus is null
