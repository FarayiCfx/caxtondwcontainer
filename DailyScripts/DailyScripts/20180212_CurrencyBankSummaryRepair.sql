-->show me everything in the account transaction table with a null key in January 
with	cte1 as (
select	 * 
from	FireBird.FactCurrencyBankAccountTransaction
where	customerkey is null
and		DateKey >= 20180101)

select	distinct userid
		--* 
from	cte1 
join	[FireBird].[DimCurrencyBankAccountSummary] bb
on		cte1.AccountSummaryId = bb.Id

--> now look in the customer reference table for the above user id's 
select	* 
from	dw.DimCustomerReference 
where	email in('ollie@wealdpackaging.co.uk')
--> are the above records in the table ? --if not then check theaspnetusers table 

--> create a temp table for the update 
drop table if exists #tempupdate 
;
select	bb.CustomerKey as CustomerKeyRefTable 
		,aa.*  
into	#tempupdate
from	[FireBird].[DimCurrencyBankAccountSummary] aa 
join	dw.DimCustomerReference bb
on		aa.UserId = bb.Email
where	aa.userid in ('ollie@wealdpackaging.co.uk')

-->check the table 
select	* from #tempupdate

update	upd
set		upd.customerkey = tmp.Customerkeyreftable 
--select upd.*, tmp.*  
from	#tempupdate tmp
join	[FireBird].[DimCurrencyBankAccountSummary] upd
on		upd.id = tmp.id

-->then rerun the commented section above look at the first column 

-->now update the firebird.currencybankaccounttransaction table 
drop	table if exists #tempupdate2 
;
with	cte1 as (
select	* 
from	FireBird.FactCurrencyBankAccountTransaction
where	customerkey is null
and		DateKey >= 20180101)

select	cte1.*
		,bb.CustomerKey as customerkeyref
into	#tempupdate2
from	cte1 
join	[FireBird].[DimCurrencyBankAccountSummary] bb
on		cte1.AccountSummaryId = bb.Id

select	* from #tempupdate2

update	upd
set		upd.customerkey = tmp.Customerkeyref
--select upd.*, tmp.*  
from	#tempupdate2 tmp
join	[FireBird].FactCurrencyBankAccountTransaction upd
on		upd.id = tmp.id

