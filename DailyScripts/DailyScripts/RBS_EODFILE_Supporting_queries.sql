--> ALL SCRIPTS SHOULD BE RUN ON DATAWAREHOUSE IN AZURE
drop table if exists rbs.EODFilesRuntimeEnumerator
go
CREATE TABLE rbs.EODFilesRuntimeEnumerator
(RowId int identity(1,1)
,FileName varchar(255)
,DWDateInserted datetime)
go

drop table if exists rbs.EODFilesImportLog
go
CREATE TABLE rbs.EODFilesImportLog
(RowId int identity(1,1)
,[FileName] varchar(255)
,[RecordType] varchar(255)
,[InsertableRowCount] int
,DateImported datetime)
go
--->
;
--->
create	procedure rbs.InsertFileEnumerator @filename varchar(255)
as
set		nocount on
insert	into rbs.EODFilesRuntimeEnumerator(FileName,DWDateInserted)
values	(@filename,getdate())


---------------------------------------------------------------------------------------------------------------------------
drop table if exists [rbs].[EODFile_StatementRecord]
go
CREATE TABLE [rbs].[EODFile_StatementRecord](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[Record Identifier] [nvarchar](2) NULL,
	[Sort Code] [nvarchar](6) NULL,
	[Account Number] [nvarchar](34) NULL,
	[Account Alias] [nvarchar](35) NULL,
	[Account Name] [nvarchar](35) NULL,
	[Account Currency] [nvarchar](3) NULL,
	[Account Type] [nvarchar](20) NULL,
	[Bank Identifier Code] [nvarchar](20) NULL,
	[Bank Name] [nvarchar](35) NULL,
	[Bank Branch Name] [nvarchar](27) NULL,
	[Transaction Date] datetime NULL,
	[DateString] [nvarchar](10) NULL,
	[Narrative Line 1] [nvarchar](25) NULL,
	[Narrative Line 2] [nvarchar](25) NULL,
	[Narrative Line 3] [nvarchar](25) NULL,
	[Narrative Line 4] [nvarchar](25) NULL,
	[Narrative Line 5] [nvarchar](25) NULL,
	[Transaction Type] [nvarchar](3) NULL,
	[Debit Value] [nvarchar](15) NULL,
	[Credit Value] [nvarchar](15) NULL,
	[FileName] [varchar](255) NULL,
	[DWDateInserted] [datetime] NULL,
	FullRowAsUQ nvarchar (510)
	---constraint uq_fullrow unique(FullRowAsUQ,[filename])
) ON [PRIMARY]
GO


-----------------------------------------------------------------------------------------
;
drop	view if exists rbs.EODFiles_ImportedView
go
create	view rbs.EODFiles_ImportedView
as 
select	distinct Filename from [rbs].[EODFile_BalanceRecord]
union	
select	distinct filename from [rbs].[EODFile_StatementRecord]

---------------------------------------------------------------------------------->
-->
drop	procedure if exists rbs.InsertStatementLines
go
create	procedure rbs.InsertStatementLines
as
/*
Stored proc to insert data from staging table 
for statement record then update the logging table 
*/
SET NOCOUNT ON

insert	into[rbs].[EODFile_StatementRecord] ([Record Identifier],[Sort Code],[Account Number],[Account Alias],[Account Name],[Account Currency],[Account Type],[Bank Identifier Code],[Bank Name],[Bank Branch Name],[Transaction Date],[DateString],[Narrative Line 1],[Narrative Line 2],[Narrative Line 3],[Narrative Line 4],[Narrative Line 5],[Transaction Type],[Debit Value],[Credit Value],[FileName],[DWDateInserted],FullRowAsUQ)

select	[dbo].[UFN_SEPARATES_COLUMNS] (RowData,1,'|') as [Record Identifier]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,2,'|') as [Sort Code]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,3,'|') as [Account Number]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,4,'|') as [Account Alias]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,5,'|') as [Account Name]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,6,'|') as [Account Currency]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,7,'|') as [Account Type]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,8,'|') as [Bank Identifier Code]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,9,'|') as [Bank Name]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,10,'|') as [Bank Branch Name]

		,TRY_CONVERT(datetime, 
		[dbo].[UFN_SEPARATES_COLUMNS] (RowData,11,'|'), 103)
		as [Transaction Date]

		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,11,'|') as [DateString]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,12,'|') as [Narrative Line 1]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,13,'|') as [Narrative Line 2]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,14,'|') as [Narrative Line 3]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,15,'|') as [Narrative Line 4]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,16,'|') as [Narrative Line 5]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,17,'|') as [Transaction Type]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,18,'|') as [Debit Value]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,19,'|') as [Credit Value]
		,[FileName]
		,getdate() 
		,RowData
from	[rbs].[EOD_StagingTable]
where	RowData like 'S|%'

insert	into  rbs.EODFilesImportLog([FileName],RecordType,[InsertableRowCount],DateImported)
select	FileName
		,'Statement record'
		,count(rowid) 
		,getdate()
from	[rbs].[EOD_StagingTable]
where	RowData like 'S|%'
group	by FileName
;
--------------------------------------------------------------------------------------------------------

drop	procedure if exists rbs.InsertBalanceRecordLines
go
create	procedure rbs.InsertBalanceRecordLines
as
/*
Stored proc to insert data from staging table 
for statement record then update the logging table 
*/
SET NOCOUNT ON
------------------------------------------------------------------------------------------------------
insert into rbs.EODFile_BalanceRecord([Record Identifier],[Sort Code],[Account Number],[Account Alias],[Account Name],[Account Currency],[Date],[Last nights ledger],[Todays ledger],[Last nights cleared],[Todays cleared],[Start of day ledger],[Start of day cleared],[FileName],[DWDateInserted])

select	[dbo].[UFN_SEPARATES_COLUMNS] (RowData,1,'|') as [Record Identifier]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,2,'|') as [Sort Code]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,3,'|') as [Account Number]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,4,'|') as [Account Alias]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,5,'|') as [Account Name]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,6,'|') as [Account Currency]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,7,'|') as [Date]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,8,'|') as [Last Nights Ledger]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,9,'|') as [Todays Ledger]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,10,'|') as [Last Nights Cleared]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,11,'|') as [Todays Cleared]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,12,'|') as [Start of Day Ledger]
		,[dbo].[UFN_SEPARATES_COLUMNS] (RowData,13,'|') as [Start of Day Cleared]
		,[FileName]
		,getdate() as DWDateInserted
from	[rbs].[EOD_StagingTable]
where	RowData like 'BE|%'

insert	into  rbs.EODFilesImportLog([FileName],RecordType,[InsertableRowCount],DateImported)
select	FileName
		,'Balance record'
		,count(rowid) 
		,getdate()
from	[rbs].[EOD_StagingTable]
where	RowData like 'BE|%'
group	by FileName