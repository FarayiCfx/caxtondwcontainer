select top 5 * from [firebird].[factcurrencybankaccounttransaction] order by 1 desc 

select	top 10000 * from [valitor].[factwallettransactions]  
where	[description] = 'load'
order	by wallettransaction_key desc

--january firebird load data counts ()
with	cteloadcountJanuary as(
select	count(id)as countFir,customerkey, [status]
from	firebird.factcurrencybankaccounttransaction
where	[status] = 'debit for card load'
and		externaltransref = 'valitor'
and		transactiondatetime between '2016-01-01 00:00:00.000' and '2016-02-01 00:00:00.000'
group	by customerkey,[status])
select	* from cteloadcountjanuary
order	by customerkey,[status]
---21828

--january valitor load data counts 
with	cteloadcountjanval as (
select	count(wallettransaction_key) as countVal,customerkey
from	[valitor].[factwallettransactions]  
where	[description] = 'load'
and		transactiondatetime between '2016-01-01 00:00:00.000' and '2016-02-01 00:00:00.000'
group	by customerkey 
)
select * from cteloadcountjanval
--31923

IF		OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN	DROP TABLE #Temp1
END;


with cteloadcountFir as(
select	count(id)as countFir,customerkey as customerkeyFireBird, [status]
from	firebird.factcurrencybankaccounttransaction
where	[status] = 'debit for card load'
and		externaltransref = 'valitor'
and		cast(transactiondatetime as date) between '2016-01-01' and '2016-02-01'
group	by customerkey,[status]),
cteloadcountjanval as (
select	count(wallettransaction_key) as countVal,customerkey as customerkeyvalitor
from	[valitor].[factwallettransactions]  
where	[description] = 'load'
and		cast(transactiondatetime as date) between '2016-01-01' and '2016-02-01'
group	by customerkey)

select *	into #temp1
from cteloadcountFir aa
join		cteloadcountjanval bb
on			(aa.customerkeyfirebird = bb.customerkeyvalitor)


select * from #temp1
where countfir < countval



---------------------------look at each row 
declare @customerkey bigint 

select	@customerkey = 113477

select	externaltransactionid,* from firebird.factcurrencybankaccounttransaction
where	customerkey = @customerkey
and		[status] = 'debit for card load'
and		externaltransref = 'valitor'
and		transactiondatetime between '2016-01-01 00:00:00.000' and '2016-02-01 00:00:00.000'
order	by 1 

select	referenceid,* from [valitor].[factwallettransactions]  
where	customerkey = @customerkey 
and		[description] = 'load'
and		transactiondatetime between '2016-01-01 00:00:00.000' and '2016-02-01 00:00:00.000'
order	by 1 




select	externaltransactionid,customerkey,status,externaltransref, transactiondatetime
from	firebird.factcurrencybankaccounttransaction
where	externaltransactionid in ('6358820051825751')

select	customerkey,referenceid,description,transactiondatetime from [valitor].[factwallettransactions]  
where	referenceid in ('6358820051825751')





select	*	from  firebird.factcurrencybankaccounttransaction
where		externaltransactionid in ('6358820051825751')






/*
status naming differences 

Clear funds from balance for card load (Migrated Visa Card)
*/