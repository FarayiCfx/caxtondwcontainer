select  top 10 itemid,TERMLOCATION,* from	dbo.finadv (nolock) where	TERMLOCATION = '494 BROOME ST'
---
DECLARE @account_id INT ,
		@start_date DATETIME ,
		@end_date DATETIME 

select	@account_id = 251266 ,@start_date = '2015-01-01 00:00:00.000', @end_date = '2016-10-12 00:00:00.000'

SELECT	0 as iteimid,cardid	collate Database_Default, Transactiondate, trantype  collate Database_Default, trandescription  collate Database_Default, 
				tranlocation	collate Database_Default, trancountry  collate Database_Default, amttxn, calcconv, fee, billamt 
				from [caxton-db1].[dbcards].[dbo].[vwcfxrptcardloadsstatement]
				WHERE  cardid COLLATE DATABASE_DEFAULT in (select cardid from [caxton-db1].dbcards.dbo.tblcfxcurrencycarddetails cd2
									inner join [caxton-db1].dbcards.dbo.contact c2 on c2.contact_id = cd2.contact_id where c2.account_id = @account_id)
							
				and TransactionDate BETWEEN @start_date and @end_date
------------------------------>

exec	[dbo].[spCfxRptCorpCardStatement_Fn]   @account_id = 251266 ,@start_date = '2015-01-01 00:00:00.000', @end_date = '2016-10-12 00:00:00.000'


alter	PROC [dbo].[spCfxRptCorpCardStatement_Fn] 
		@account_id INT = NULL,
		@start_date DATETIME = NULL,
		@end_date DATETIME = NULL

AS
BEGIN
	--SET @end_date = DATEADD(day, 1, @end_date)

	-- CREATE TEMP Table with transactions
	SELECT * INTO #tmptran FROM
				(SELECT fa.itemid,fa.CARDID CARDID, fa.TXNDATE As TransactionDate, 'CARD TRAN' AS TranType, 
					ISNULL(cdim.CDIMDescription, fa.CDIM) AS TranDescription, fa.TERMLOCATION TranLocation, fa.TERMCOUNTRY TranCountry, 
				-fa.AMTTXN AMTTXN, 0 CalcConv, (SELECT     -SUM(BILLAMT) AS Expr1
																	FROM          dbo.FEE
																	WHERE      (ITEMID = fa.ITEMID)) AS fee, -fa.BILLAMT BILLAMT

				FROM dbo.FINADV AS fa LEFT OUTER JOIN
				dbo._CardDataInputMode AS cdim ON fa.CDIM = cdim.CDIM 
				WHERE cardid in (select cardid  COLLATE DATABASE_DEFAULT from [caxton-db1].dbcards.dbo.tblcfxcurrencycarddetails cd2
									inner join [caxton-db1].dbcards.dbo.contact c2 on c2.contact_id = cd2.contact_id where c2.account_id = @account_id)
				and fa.TXNDATE BETWEEN @start_date and @end_date
									 

				UNION ALL


				SELECT  fa.itemid,fa.CARDID, fa.TXNDATE As TransactionDate, 'CARD TRAN' AS TranType, 
				ISNULL(cdim.CDIMDescription, fa.CDIM) AS TranDescription, fa.TERMLOCATION TranLocation, fa.TERMCOUNTRY TranCountry, 
				fa.AMTTXN, 0 CalcConv, (SELECT     SUM(BILLAMT) AS Expr1
																	FROM          dbo.FEE
																	WHERE      (ITEMID = fa.ITEMID)) AS fee, fa.BILLAMT 

				FROM dbo.FINREV AS fa LEFT OUTER JOIN
				dbo._CardDataInputMode AS cdim ON fa.CDIM = cdim.CDIM 
				WHERE cardid COLLATE DATABASE_DEFAULT in (select cardid from [caxton-db1].dbcards.dbo.tblcfxcurrencycarddetails cd2
									inner join [caxton-db1].dbcards.dbo.contact c2 on c2.contact_id = cd2.contact_id where c2.account_id = @account_id)
				and fa.TXNDATE BETWEEN @start_date and @end_date
							

				union ALL

				SELECT 0 as iteimid,cardid collate Database_Default, Transactiondate, trantype  collate Database_Default, trandescription  collate Database_Default, 
				tranlocation  collate Database_Default, trancountry  collate Database_Default, amttxn, calcconv, fee, billamt 
				from [caxton-db1].[dbcards].[dbo].[vwcfxrptcardloadsstatement]
				WHERE  cardid COLLATE DATABASE_DEFAULT in (select cardid from [caxton-db1].dbcards.dbo.tblcfxcurrencycarddetails cd2
									inner join [caxton-db1].dbcards.dbo.contact c2 on c2.contact_id = cd2.contact_id where c2.account_id = @account_id)
							
				and TransactionDate BETWEEN @start_date and @end_date
				) tmp



	SELECT	itemid,account_short_name, contact_last_name + ', ' + contact_first_name + ' ****' + RIGHT(cardnumber, 4) name, 
			cb.currencycode, txn.TransactionDate, txn.TranLocation, txn.TranCountry, txn.AMTTXN, txn.calcconv, txn.fee, txn.BILLAMT

	FROM	[caxton-db1].dbcards.dbo.account a INNER JOIN 
			[caxton-db1].dbcards.dbo.contact c on a.account_id = c.account_id INNER JOIN
			[caxton-db1].dbcards.dbo.tblcfxcurrencycarddetails cd on c.contact_id = cd.contact_id INNER JOIN
			[caxton-db1].dbcards.dbo.tblcfxcardbin cb on cd.cardcurrency = cb.cardcurrency

	INNER JOIN 
			#tmptran txn on txn.cardid COLLATE DATABASE_DEFAULT = cd.cardid COLLATE DATABASE_DEFAULT
	WHERE c.account_id = @account_id
	order by itemid--,account_short_name, contact_last_name, contact_first_name, right(cardnumber, 4), cb.CurrencyCode, txn.TransactionDate

END


GO




SELECT top 5	* FROM FINREV 

SELECT	count	(*) FROM FEE
where	left (pan,6) = '459566'
or		left (pan,6)	= '459567'
or		left (pan,6) = '459568'
 


 
SELECT	pan,itemid,localdate,[BILLAMT],txndate 
FROM FEE (NOLOCK)
where	left (pan,6) = '459566'
or		left (pan,6)	= '459567'
or		left (pan,6) = '459568'

select top 5 * from fee


CREATE NONCLUSTERED INDEX [ncc_pan_and_includes] ON [dbo].[FEE]
(
	pan ASC
)
INCLUDE (itemid,localdate,[BILLAMT],txndate)


 select min(localdate) as min_ from fee (nolock)


 exec sp_who2 active

 