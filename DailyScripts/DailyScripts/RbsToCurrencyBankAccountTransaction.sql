-->unallocated 
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
select	aa.id as StatementId,
		aa.AccountNumber,
		aa.AccountName,
		aa.AccountCurrency,
		aa.Narrative1,
		aa.Narrative2,
		aa.Narrative3,
		aa.Narrative4,
		aa.Narrative5,
		aa.CreditValue,
		aa.DebitValue, 
		aa.SettlementType,
		aa.UserName,
		aa.DateCreated as TransactionDate,
		aa.TradeFundingHintList,
		aa.accounttransactionid as accounttransactionid_in_statement,
		bb.id as AccountTransactionId,
		bb.TransactionDateTime,
		bb.Amount,
		bb.Cleared,
		bb.AccountSummaryId 
into	#temp1
from	Settlement.RbsStatement aa
left	outer join CurrencyBank.AccountTransaction bb
on		cast (aa.Id as nvarchar (512)) = bb.ExternalTransactionId
and		bb.ExternalTransRef = 'RBS'
where	aa.transactiondate between '2017-03-01 00:00:00.000' and '2017-03-23 00:00:00.000'


select * from #temp1 where narrative1 = 'Tranref:486890'

select * from Settlement.RbsStatement where narrative1 = 'Tranref:486890'

select * from CurrencyBank.AccountTransaction (nolock) where externaltransactionid = '6882804'

select top 20  * from CurrencyBank.AccountTransaction 


IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
with	cteCurrencyBankAccountTransaction as (
select  --CONVERT(BIGINT, id) as TableId,
		ID AS TableId,
		TransactionDateTime,
		Amount,
		Cleared,
		AccountSummaryId ,
		ExternalTransactionId,
		ExternalTransRef
from	CurrencyBank.AccountTransaction  (nolock)
where	Transactiondatetime between '2017-01-01' and '2017-04-01'
and		ExternalTransRef = 'RBS' and id <> 7016341)

select	AllocationStatus =  case isnull(aa.ACCOUNTTRANSACTIONID,'') when '' then 'UnAllocated'
		else 'Allocated' end,
		aa.id as StatementId,
		aa.AccountNumber,
		aa.AccountName,
		aa.AccountCurrency,
		aa.Narrative1,
		aa.Narrative2,
		aa.Narrative3,
		aa.Narrative4,
		aa.CreditValue,
		aa.DebitValue, 
		aa.SettlementType,
		--aa.UserName,
		aa.DateCreated as TransactionDate,
		aa.TradeFundingHintList,
		aa.accounttransactionid as accounttransactionid_in_statement,bb.* , 
		asum.userid
into	#temp1
from	Settlement.RbsStatement aa 
left	outer join	cteCurrencyBankAccountTransaction bb
on		aa.ACCOUNTTRANSACTIONID = bb.TABLEID
left	outer join currencybank.accountsummary asum
on		bb.accountsummaryid = asum.id 
where	aa.transactiondate between '2017-03-01 00:00:00.000' and '2017-03-30 00:00:00.000'



select * from #temp1 where narrative1 = 'Tranref:486890'

select top 10 * from currencybank.accountsummary 


select * from CurrencyBank.AccountTransaction 
where externaltransactionid in('264395',
'264422') 
