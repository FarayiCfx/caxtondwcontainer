
/* This table holds the kepad modelled as table*/
IF Object_id('tempdb..#keypad') IS NOT NULL
  DROP TABLE #keypad

CREATE TABLE #keypad
  (
     val CHAR(1),								-- original value
	 rownum INT,								-- row number of the value
	 colnum INT									--column number of the value
  )

/* This table stores all the combinations of current value and next value after valid knight's move, populated in SELECT...INTO.
There are three columns
	val is the starting position or original value
	nextvla is the next value after move
	isnextvowel is tinyint, if nextval is a vowel then 1 else 0
 */
IF Object_id('tempdb..#validcombination') IS NOT NULL
  DROP TABLE #validcombination

/* This is the final results table , populated in SELECT...INTO query at last  */
IF Object_id('tempdb..#Results') IS NOT NULL
  DROP TABLE #results


/* Pupulate the keypad table */
INSERT INTO #keypad
SELECT 'A',1,1
UNION
SELECT 'B',1,2
UNION
SELECT 'C',1,3
UNION
SELECT 'D',1,4
UNION
SELECT 'E',1,5
UNION
SELECT 'F',2,1
UNION
SELECT 'G',2,2
UNION
SELECT 'H',2,3
UNION
SELECT 'I',2,4
UNION
SELECT 'J',2,5
UNION
SELECT 'K',3,1
UNION
SELECT 'L',3,2
UNION
SELECT 'M',3,3
UNION
SELECT 'N',3,4
UNION
SELECT 'O',3,5
UNION
SELECT '1',4,2
UNION
SELECT '2',4,3
UNION
SELECT '3',4,4



SELECT val, nextval, isnextvowel
 INTO #validcombination
FROM  (
SELECT d1.val,d2.val AS nextval,CASE
                                         WHEN d2.val IN ( 'A', 'E', 'I', 'O' )
                                       THEN 1
                                         ELSE 0
                                       END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.rownum - 1 = d2.rownum
                  AND d2.colnum = d1.colnum - 2
UNION
SELECT d1.val,d2.val AS nextval,CASE
                                          WHEN d2.val IN ( 'A', 'E', 'I', 'O' )
                                        THEN 1
                                          ELSE 0
                                        END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.rownum - 1 = d2.rownum
                  AND d2.colnum = d1.colnum + 2
UNION
SELECT d1.val,d2.val AS nextval,CASE
                                           WHEN d2.val IN ( 'A', 'E', 'I', 'O' )
                                         THEN 1
                                           ELSE 0
                                         END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.rownum + 1 = d2.rownum
                  AND d2.colnum = d1.colnum - 2
UNION
SELECT d1.val,d2.val AS nextval,CASE
                                            WHEN d2.val IN ( 'A', 'E', 'I', 'O'
                                                           ) THEN
                                            1
                                            ELSE 0
                                          END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.rownum + 1 = d2.rownum
                  AND d2.colnum = d1.colnum + 2
UNION
--case2
SELECT d1.val,d2.val AS nextval,CASE
                                         WHEN d2.val IN ( 'A', 'E', 'I', 'O' )
                                       THEN 1
                                         ELSE 0
                                       END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.colnum - 1 = d2.colnum
                  AND d2.rownum = d1.rownum - 2
UNION
SELECT d1.val,d2.val AS nextval,CASE
                                           WHEN d2.val IN ( 'A', 'E', 'I', 'O' )
                                         THEN 1
                                           ELSE 0
                                         END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.colnum - 1 = d2.colnum
                  AND d2.rownum = d1.rownum + 2
UNION
SELECT d1.val,d2.val AS nextval,CASE
                                          WHEN d2.val IN ( 'A', 'E', 'I', 'O' )
                                        THEN 1
                                          ELSE 0
                                        END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.colnum + 1 = d2.colnum
                  AND d2.rownum = d1.rownum - 2
UNION
SELECT d1.val,d2.val AS nextval,CASE
                                            WHEN d2.val IN ( 'A', 'E', 'I', 'O'
                                                           ) THEN
                                            1
                                            ELSE 0
                                          END isnextvowel
FROM   #keypad d1
       INNER JOIN #keypad d2
               ON d1.colnum + 1 = d2.colnum
                  AND d2.rownum = d1.rownum + 2
) a


/* self join the valid combinations table to itselt to find 10 key sequence,
the sum on vowel in where clause filters out more that two vowels. */

SELECT d.val s1,r1.nextval s2,r2.nextval s3,r3.nextval s4,r4.nextval s5,
       r5.nextval s6,
       r6.nextval s7,r7.nextval s8,r8.nextval s9,r9.nextval s10
INTO   #results
FROM   #keypad d
       INNER JOIN #validcombination r1
               ON d.val = r1.val
       INNER JOIN #validcombination r2
               ON r1.nextval = r2.val
       INNER JOIN #validcombination r3
               ON r2.nextval = r3.val
       INNER JOIN #validcombination r4
               ON r3.nextval = r4.val
       INNER JOIN #validcombination r5
               ON r4.nextval = r5.val
       INNER JOIN #validcombination r6
               ON r5.nextval = r6.val
       INNER JOIN #validcombination r7
               ON r6.nextval = r7.val
       INNER JOIN #validcombination r8
               ON r7.nextval = r8.val
       INNER JOIN #validcombination r9
               ON r8.nextval = r9.val
WHERE  ( CASE WHEN d.val IN ( 'A', 'E', 'I', 'O' ) THEN 1  ELSE  0  end + r1.isnextvowel + r2.isnextvowel
         + r3.isnextvowel + r4.isnextvowel
         + r5.isnextvowel + r6.isnextvowel
         + r7.isnextvowel + r8.isnextvowel
         + r9.isnextvowel ) <= 2

		 select * from #results



PRINT @@rowcount
PRINT Datediff(ms, @timetest, CURRENT_TIMESTAMP)