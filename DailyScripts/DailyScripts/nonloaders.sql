if		object_id('tempdb..#temp1') is not null
drop	table #temp1
go

with	cteJanuaryCustomers as
(select	customerkey,title,firstname,lastname,contactemailaddress,
		accounttyperef,datepart(yyyy,sourcecreationdate) as [year],datepart(month,sourcecreationdate) as [month]
from	dw.dimcustomerreference 
where	sourcecreationdate between '2016-01-01 00:00:00.000' and '2016-02-01 00:00:00.000'
and		accounttyperef = 'Retail card - Firebird'),
cteloadcountallyearround as(
select	count(id)as count,customerkey
from	firebird.factcurrencybankaccounttransaction
where	[status] = 'debit for card load'
and		externaltransref = 'valitor'
group	by customerkey )

select	aa.*, isnull(bb.customerkey,0) as LoadedCustKey
into	#temp1
from	cteJanuaryCustomers aa 
left	join	cteloadcountallyearround bb
on		aa.customerkey = bb.customerkey
order	by loadedcustkey desc

select  * from #temp1 order by loadedcustkey desc


select * from dw.dimcustomerreference where cardscontactref =  162964
---------------------------------------------------------------------------------------------------------------------------
;
with	cteFailedPayments as(
select	distinct customerkey,[status],transactionamount
from	[firebird].[factpaymentcardtransaction] (nolock)
		where status <> 'succeeded')
		select *	from #temp1 aa
		join		cteFailedPayments bb
		on			aa.customerkey = bb.customerkey
WHERE	aa.LOADEDCUSTKEY = 0
order	by aa.customerkey


with	cteFailedPayments as(
select	distinct customerkey
from	[firebird].[factpaymentcardtransaction] (nolock)
		where status <> 'succeeded')
		select * from #temp1 aa
		join	cteFailedPayments bb
		on aa.customerkey = bb.customerkey
WHERE	aa.LOADEDCUSTKEY = 0
order	by aa.customerkey


select * from [firebird].[factpaymentcardtransaction] where customerkey = 251704


select	isnull(contactemailaddress,username) as emailaddress,
		firstname,lastname,sourcecreationdate, accounttyperef 
from	dw.dimcustomerreference 
where	sourcecreationdate > '2016-02-02 00:00:00.000'
and		accounttyperef = 'Retail card - Firebird'
order	by sourcecreationdate asc 

