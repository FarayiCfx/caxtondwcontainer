select top 5 * from		contact  order by 1 desc

select top 5 * from		account 

select top 5 * from		branch

select top 5 * from		[dbo].[Contact_phone]




select * from [dbo].[Phone_Type]


with cteNonBlankNumbersRanked as
(select *,rank() over (partition by contact_id order by  phone_type_id desc) as sequence 
from contact_phone (nolock) where
phone_number is not null and phone_number <> '')


exec sp_spaceused 'contact'
--348 688

exec sp_spaceused 'account'
--316 498

with DedupedEmails as (
select	*, 
rank()	over (partition by contact_id order by  updatedate desc) as sequence
from	contact_email (nolock))
/*
then go ahead to pick up the result set 
*/
select	cc.contact_id as TSContact_Id,
		isnull(aa.Account_id,0) as TSAccount_id,
		isnull(cc.parent_id,0) as TSParent_Id,
		
		cc.contact_salutation,
		cc.contact_first_name as FirstName,
		cc.contact_last_name as LastName,	

		Gender = case	when contact_salutation like 'Ms%' then 'Female'
						when contact_salutation like 'Mrs%' then 'Female'
						when contact_salutation like 'Miss%' then 'Female' 
						when contact_salutation like 'Mr%' then 'Male'
						else ''
						end ,

		cast(isnull(dob,'')as datetime) as DateOfBirth,
		
		isnull(br.branch_address1,'') as AddressLine1, 
		isnull(br.branch_address2,'') as AddressLine2,
		isnull(ct.cityname,'') as City, 
		isnull(st.[State],'') as [County],
		isnull(cn.Country,'') as [Country],
		isnull(br.branch_zip,'') as PostCode,
		isnull(eml.email_address,'') as [ContactEmailAddress],
		cast(replace(replace(isnull(cc.InsertDate,''),char(10),''),char(13),'')  as datetime)as SourceDateInserted,
		cast(replace(replace(isnull(cc.UpdateDate,''),char(10),''),char(13),'')  as datetime)as SourceDateUpdated
from	contact cc (nolock)
left	outer join	account aa (nolock) 
on		cc.account_id = aa.account_id
left	outer join	branch br (nolock)
on		aa.Account_main_branch = br.branch_id 
left	outer join	cityname ct (nolock)
on		br.branch_city = ct.CityName_ID
left	outer join	[state] st (nolock)
on		br.branch_state = st.State_ID
left	outer join	country cn (nolock)
on		br.branch_country = cn.Country_ID
left	outer join	DedupedEmails eml (nolock)
on			cc.contact_id = eml.contact_id
			and			eml.def_email = 1 
			and			eml.sequence = 1




select * from ##temp1



select	count(TScontact_Id) as count_,TScontact_Id 
from	##temp1
group by TScontact_Id
order by count_ desc

select *	from contact 
where		contact_id = 84095

select *	from ##temp1
where		contact_id in (373068,373067,373066,373065,
			373064,373063,373062,373060,373059,373058,281516)




select top 10 * from scy_registration

select	top 10 /*cn.contact_id,contact_first_name,contact_last_name,contactref,
		parent_id,*/tcd.*,sr.*
from	contact cn
join	tblCfxCurrencyCardDetails tcd
on		cn.contact_id = tcd.contact_id
join	scy_registration sr on 
		tcd.scyregpkid = sr.pkid
order	by 1 desc 

