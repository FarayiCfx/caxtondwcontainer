-->First create the view NonRetailTradesSpotFwdFlex (check the definition in the database)

-->
drop procedure	GetNonRetailTradesSpotFwdFlex
go
create procedure GetNonRetailTradesSpotFwdFlex	@startdate datetime = null,
												@enddate datetime = null, 
												@AccountManagerSage varchar(50) = null,
												@SalesPersonSage nvarchar(3996) = null
AS

DECLARE @startdatekey int , @enddatekey int 

select	@startdatekey = cast(convert(char(8), @startdate, 112) as int),
		@enddatekey  =  cast(convert(char(8), @enddate, 112) as int)

		if		isnull (@AccountManagerSage ,'') = '' and isnull (@SalesPersonSage ,'') = '' --if both account manager and salesperson are null
				
				begin
		select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber,
				*
		from	NonRetailTradesSpotFwdFlex
		where	tradedatekey between @startdatekey and @enddatekey 
				end

	else		if isnull (@AccountManagerSage ,'') = '' and isnull (@SalesPersonSage ,'') <> '' -- is salesperson is populated use that and date
		begin
				select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber,
				*
				from	NonRetailTradesSpotFwdFlex
				where	tradedatekey between @startdatekey and @enddatekey 
				and		[SalesPerson(sage)] = @SalesPersonSage
		end
		else  if isnull (@AccountManagerSage ,'') <> '' and isnull (@SalesPersonSage ,'') <> '' --if non of the optional parameters are null

		BEGIN
		select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber,
				*
				from	NonRetailTradesSpotFwdFlex
				where	tradedatekey between @startdatekey and @enddatekey 
				and		[SalesPerson(sage)] = @SalesPersonSage and [AccountManager(Sage)] = @AccountManagerSage
		END

		ELSE
			begin 
			select	ROW_NUMBER() OVER(ORDER BY tradedatekey ASC) as RowNumber, --your last case is if account manager is supplied 
				*
				from	NonRetailTradesSpotFwdFlex
				where	tradedatekey between @startdatekey and @enddatekey 
				and		[AccountManager(sage)] = @AccountManagerSage
				end

------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
exec GetNonRetailTradesSpotFwdFlex @startdate = '2017-05-18 09:44:14.130',@enddate = '2017-05-22 09:44:14.130',@AccountManagerSage = null,@SalesPersonSage = null
go

exec GetNonRetailTradesSpotFwdFlex @startdate = '2017-05-18 09:44:14.130',@enddate = '2017-05-22 09:44:14.130',@AccountManagerSage = null, @SalesPersonSage = 'NG'
go

exec GetNonRetailTradesSpotFwdFlex @startdate = '2017-05-18 09:44:14.130',@enddate = '2017-05-22 09:44:14.130',@AccountManagerSage = 'James Tinsley',@SalesPersonSage = null
go


exec GetNonRetailTradesSpotFwdFlex @startdate = '2017-02-18 09:44:14.130',@enddate = '2017-05-22 09:44:14.130',@AccountManagerSage = 'Matt Downward',@SalesPersonSage = null
go




