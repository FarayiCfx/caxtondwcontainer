-->make sure to update the dw rfm table uses these stored procs below:
exec dw.MergeBuyAndHoldRfmStats
GO
exec dw.MergeBuyAndSendrfmStats
GO
exec dw.MergeCardLoadrfmStats
GO

--> then do this one for less than 20k
select	aa.FirstName, 
		aa.LastName, 
		isnull(replace(aa.ContactEmailAddress,'unknown',aa.UserName),aa.UserName) as ContactEmailAddress,
		DATEDIFF(YEAR,aa.DateOfBirth,GETDATE()) as Age,
		aa.AddressLine1, 
		aa.AddressLine2, 
		aa.County,
		aa.Postcode,
		bb.BuyAndSend_TradeCount, 
		bb.BuyAndSend_Tradeprofit_GBP_Total,  
		bb.BuyAndSend_LastDate
from	dw.DimCustomerReference aa (NOLOCK)
join	dw.CustomerRFMAggregations bb (NOLOCK)
on		aa.CustomerKey = bb.CustomerKey
where	bb.BuyAndSend_TradeCount <> 0
and		bb.BuyAndSend_Tradeprofit_GBP_Total <= 20000.00
AND		bb.BuyAndSend_LastDate > DATEADD(mm,-12,getdate())
order	by bb.BuyAndSend_Tradeprofit_GBP_Total
desc

-->then do this one for over 20k

select	aa.FirstName, 
		aa.LastName, 
		isnull(replace(aa.ContactEmailAddress,'unknown',aa.UserName),aa.UserName) as ContactEmailAddress,
		DATEDIFF(YEAR,aa.DateOfBirth,GETDATE()) as Age,
		aa.AddressLine1, 
		aa.AddressLine2, 
		aa.County,
		aa.Postcode,
		bb.BuyAndSend_TradeCount, 
		bb.BuyAndSend_Tradeprofit_GBP_Total,  
		bb.BuyAndSend_LastDate
from	dw.DimCustomerReference aa (NOLOCK)
join	dw.CustomerRFMAggregations bb (NOLOCK)
on		aa.CustomerKey = bb.CustomerKey
where	bb.BuyAndSend_TradeCount <> 0
and		bb.BuyAndSend_Tradeprofit_GBP_Total > 20000.00
AND		bb.BuyAndSend_LastDate > DATEADD(mm,-12,getdate())
order	by bb.BuyAndSend_Tradeprofit_GBP_Total
desc

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------USING Sal's list instead 
drop table #tempResults
go
select	
isnull(aa.UserName,replace(aa.ContactEmailAddress,'unknown',aa.UserName)) as ContactEmailAddress,
aa.FirstName, 
		aa.LastName, 
		aa.Gender,
		DATEDIFF(YEAR,aa.DateOfBirth,GETDATE()) as Age,
		aa.AddressLine1, 
		aa.AddressLine2, 
		aa.County,
		aa.Postcode
		--bb.BuyAndSend_TradeCount, 
		--bb.BuyAndSend_Tradeprofit_GBP_Total,  
		--bb.BuyAndSend_LastDate
into	#tempResults
from	dw.DimCustomerReference aa (NOLOCK)
join	dw.CustomerRFMAggregations bb (NOLOCK)
on		aa.CustomerKey = bb.CustomerKey
--where	bb.BuyAndSend_TradeCount <> 0
--and	bb.BuyAndSend_Tradeprofit_GBP_Total > 20000.00
--AND	bb.BuyAndSend_LastDate > DATEADD(mm,-12,getdate())
---order	by bb.BuyAndSend_Tradeprofit_GBP_Total
---desc

-------
select	aa.*,
		bb.* 
from	[dbo].[20K trades] aa
left	outer join	#tempResults bb
on		aa.userid = bb.contactemailaddress


select * from [dbo].[20K trades]

select * from FireBird.FactCurrencyBankTrade where userid = 'k.barnett@taylorwessing.com'

select * from dw.DimCustomerReference where CustomerKey = 156065
select * from dw.CustomerRFMAggregations where customerkey = 156065

