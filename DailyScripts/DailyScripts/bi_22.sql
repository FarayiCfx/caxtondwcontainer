/*
Simple and straight forward query to retrieve data 
for corporate request to get emails and phone numbers 
from dbfx database
*/


DROP TABLE #temp2

WITH DedupedEmails as (
select	*, 
rank()	over (partition by contact_id order by  updatedate desc) as sequence
from	contact_email (nolock))
/*
then go ahead to pick up the result set 
*/
select	cc.contact_id as TSContact_Id,
		isnull(aa.Account_id,0) as TSAccount_id,
		isnull(cc.parent_id,0) as TSParent_Id,
		
		cc.contact_salutation,
		cc.contact_first_name as FirstName,
		cc.contact_last_name as LastName,	

		Gender = case	when contact_salutation like 'Ms%' then 'Female'
						when contact_salutation like 'Mrs%' then 'Female'
						when contact_salutation like 'Miss%' then 'Female' 
						when contact_salutation like 'Mr%' then 'Male'
						else ''
						end ,

		cast(isnull(dob,'')as datetime) as DateOfBirth,
		
		/*CAST(isnull(br.branch_address1,'') as varchar(100)) as AddressLine1, 
		cast(isnull(br.branch_address2,'') as varchar(100)) as AddressLine2,
		isnull(ct.cityname,'') as City, 
		isnull(st.[State],'') as [County],
		isnull(cn.Country,'') as [Country],
		isnull(br.branch_zip,'') as PostCode,*/
		isnull(eml.email_address,'') as [ContactEmailAddress]
		/*,
		CAST(replace(replace(isnull(aa.InsertDate,''),char(10),''),char(13),'')  as datetime)as RegistrationDate,
		cast(replace(replace(isnull(cc.InsertDate,''),char(10),''),char(13),'')  as datetime)as SourceCreationDate,
		cast(replace(replace(isnull(cc.UpdateDate,''),char(10),''),char(13),'')  as datetime)as SourceDateUpdated,
		CASE	WHEN aa.contactxref = cc.contact_id 
				THEN 'Retail Money Transfer - Talksheet' ELSE 'Corporate Money transfer - Talksheet' END AS AccountTypeRef,
				--aa.contactxref as accountcontactxref,
				--cc.contact_id as contact_contact_id,
				CASE WHEN aa.contactxref = cc.contact_id THEN '' ELSE aa.account_full_name END AS account_full_name*/
				INTO #temp1
from	contact cc (nolock)
left	outer join	account aa (nolock) 
on		cc.account_id = aa.account_id
left	outer join	branch br (nolock)
on		aa.Account_main_branch = br.branch_id 
left	outer join	cityname ct (nolock)
on		br.branch_city = ct.CityName_ID
left	outer join	[state] st (nolock)
on		br.branch_state = st.State_ID
left	outer join	country cn (nolock)
on		br.branch_country = cn.Country_ID
left	outer join	DedupedEmails eml (nolock)
on			cc.contact_id = eml.contact_id
			and			eml.def_email = 1 
			and			eml.sequence = 1
			where cc.contact_id > 0

			SELECT * FROM #temp1

			/*one off request - wont spend time writing up nice pivot queries */
		
WITH	cte1 AS (SELECT contact_id AS contact_id1,phone_number AS phone_number1 FROM contact_phone where phone_type_id =1 AND phone_number IS NOT null),
		cte2 AS	(SELECT contact_id AS contact_id2,phone_number AS phone_number2 FROM contact_phone where phone_type_id =2 AND phone_number IS NOT NULL),
		cte3 AS (SELECT contact_id AS contact_id3,phone_number AS phone_number3 FROM contact_phone where phone_type_id =3 AND phone_number IS NOT NULL),
		cte4 AS (SELECT contact_id AS contact_id4,phone_number AS phone_number4 FROM contact_phone where phone_type_id =4 AND phone_number IS NOT NULL)
	
			SELECT	aa.FirstName,aa.LastName,aa.ContactEmailAddress, cte1.phone_number1, cte2.phone_number2, cte3.phone_number3, cte4.phone_number4
			FROM #temp1 aa
			LEFT OUTER JOIN cte1 
			ON aa.TSContact_Id  =cte1.contact_id1
			LEFT OUTER JOIN cte2
			ON aa.TSContact_Id = cte2.contact_id2
			LEFT OUTER JOIN cte3
			ON aa.TSContact_Id = cte3.contact_id3
			LEFT OUTER JOIN cte4 
			ON aa.tscontact_id = cte4.contact_id4

					


		

			
			
			
	
