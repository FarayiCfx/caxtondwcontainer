-->total count of the bankline file itself
select * from BankLineFileImport_Dec2016

-->13394 records

-->pick out the transaction references and put the data into a temp table first and count through what you have available  
IF		OBJECT_ID('#result_set') IS NOT NULL
DROP	TABLE #result_set
go
-->
with	cte1 as (
select	TRANREFID1 = case when [Narrative #1] like '%TRANREF%' then 
		REPLACE(replace([Narrative #1],'TRANREF',''),':','')	
					END,
					
					TRANREFID2 = case when [Narrative #2] like '%TRANREF%' then 
		REPLACE(replace([Narrative #2],'TRANREF',''),':','')	
					END,
					* 
from	BankLineFileImport_Dec2016)
select	COALESCE(tranrefid1,tranrefid2) as TranRefIdCoalesce,
		cte1.*
into	#result_set 
from	cte1 

-->?how many of these records have a usable transaction reference?
select * from #result_set where TranRefIdCoalesce is not null
-->6001 rows 

-->now go and join this result set to the transfer table 
select  aa.debit,
		aa.credit,
		aa.currency,
		bb.BuyAmount,
		bb.BuyCcyCode,
		aa.*,
		bb.*
from	#result_set aa
join	FireBird.FactPaymentTransfer bb
on		aa.TranRefIdCoalesce = bb.Id
order	by bb.CreatedDateKey
-->6001

select top 10 * from  FireBird.FactPaymentTransfer
order by id desc 



-->go one step further and retrieve the trade id
IF		OBJECT_ID('#result_set2') IS NOT NULL
DROP	TABLE #result_set2;
go
select	TranRefIdCoalesce,
		aa.[sort code],
		aa.[account number],
		aa.[account alias],
		aa.[account short name],
		aa.currency,
		aa.[account type],
		aa.bic,
		aa.[bank name],
		aa.[branch name],
		aa.[date],
		aa.[narrative #1],
		aa.[narrative #2],
		aa.[narrative #3],
		aa.[narrative #4],
		aa.[narrative #5],
		aa.[type],
		aa.debit,
		aa.credit,
		bb.id as TransferId,
		bb.userid,
		trd.id as TradeId,
		trd.UserId as TradeUserId,
		trd.HelpDeskUserId,
		trd.TradeReason
into	#result_set2
from	#result_set aa
join	FireBird.FactPaymentTransfer bb
on		aa.TranRefIdCoalesce = bb.Id
left	outer join FireBird.FactCurrencyBankTrade trd --additional join to currencybanktrade
on		bb.Id = trd.TransferId
order	by bb.CreatedDateKey

SELECT	* FROM #RESULT_SET2 WHERE TRADEID IS NULL
---------end of section allocating TRANREFs------------------------------------------------------------------>
-->what is the next thing that you have to resolve?
-->all of the non allocated cash transactions 
-->and the tranrefidcoalesce is null beause you have 
-->resolved those in the 2nd hash table above
select	* 
from	BankLineFileImport_Dec2016
where	allocated = ''
-->3665

--the next step is to now look for fastpay types I can reconcile
with cte1 as (
select	dbo.getnumbers2([Narrative #2]) as fastpaycontactref,
		CAST(convert(varchar,bl.Date,112) as int) as DateKey,
		* 
from	BankLineFileImport_Dec2016 bl
where	bl.allocated = ''
and		bl.type = 'bac'
and		bl.[narrative #3] like 'FP%')
select	* 
from	cte1 
join	dw.DimCustomerReference bb
on		cte1.fastpaycontactref = bb.FastpayContactRef
join	FireBird.FactCurrencyBankTrade tr
on		bb.CustomerKey = tr.CustomerKey
and		cte1.DateKey = tr.TradeDateKey
--where	cte1.fastpaycontactref = '49105'
order	by transferid





CREATE Function dbo.GetNumbers2(@Data VarChar(8000))
Returns VarChar(8000)
AS
Begin	
    Return Left(
             SubString(@Data, PatIndex('%[0-9]%', @Data), 8000), 
             PatIndex('%[^0-9]%', SubString(@Data, PatIndex('%[0-9]%', @Data), 8000) + 'X')-1)
End