select * from [Firebird].[SalesPerson] 
-->
/*
insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('richard.rawlinson@caxtonfx.com','Richard Rawlinson','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('jamie.lewis@caxtonfx.com','Jamie Lewis','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('ben.blacker@caxtonfx.com','Ben Blacker','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('steven.godfrey@caxtonfx.com','Steven Godfrey','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('clark.davidson@caxtonfx.com','Clark Davidson','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('tansel.ali@caxtonfx.com','Tansel Ali','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('jonathan.shufflebotham@caxtonfx.com','Jonathan Shufflebotham','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('oliver.hatcher@caxtonfx.com','Oliver Hatcher','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('ravinder.sidhu@caxtonfx.com','Ravinder Sidhu','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('ian.mansfield@caxtonfx.com','Ian Mansfield','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('thomas.hambly@caxtonfx.com','Thomas Hambly','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('jordan.jones@caxtonfx.com','Jordan Jones','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('andrew.aikins@caxtonfx.com','Andrew Aikins','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('nicholas.goodman@caxtonfx.com','Nicholas Goodman','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('nathan.cook@caxtonfx.com','Nathan Cook','Corporate',getdate())

insert	into [Firebird].[SalesPerson] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('joseph.wass@caxtonfx.com','Joseph Wass','Corporate',getdate())

*/

select * from [Firebird].[AccountManager]

/*
insert	into [Firebird].[AccountManager] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('matthew.downward@caxtonfx.com','Matthew Downward','Corporate',getdate())
-->

insert	into [Firebird].[AccountManager] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('adam.stevens@caxtonfx.com','Adam Stevens','Corporate',getdate())

insert	into [Firebird].[AccountManager] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('rehan.ansari@caxtonfx.com','Rehan Ansari','Corporate',getdate())

insert	into [Firebird].[AccountManager] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('jack.lane-matthews@caxtonfx.com','Jack Lane-Matthews','Corporate',getdate())

insert	into [Firebird].[AccountManager] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('anthony.creese@caxtonfx.com','Anthony Creese','Corporate',getdate())

insert	into [Firebird].[AccountManager] (Username,FullName,AccountTypeRef,FB_DateCreated)
values	('james.tinsley@caxtonfx.com','James Tinsley','Corporate',getdate())
*/