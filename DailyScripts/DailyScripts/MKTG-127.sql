--load volumes from valitor 
select	count(id) as count_
		,[description] 
		,transactiondatekey
		,BILLINGCURRENCY
from	valitor.factwallettransactions (nolock)
where	transactiondatekey = 2010302
and		[description] = 'load'
AND		LABEL = 'GB3'
group	by [description] ,transactiondatekey


alter view RetailLoadsBycurrencyValitor
as
select	SUM(ABS(BILLINGAMOUNT)) AS load_amount
		,count(aa.id) as count_of_loads
		,[description] 
		,CAST (EXTRACTDATETIME AS DATE) as ExtractDate
		,BILLINGCURRENCY
		,bb.accounttyperef 
		,cc.currencyname
from	valitor.factwallettransactions aa (nolock)
left	outer join	dw.dimcustomerreference bb (nolock)
on		aa.customerkey = bb.customerkey 
left	outer join [dw].[DimISOCurrencyName] cc
on		aa.billingcurrency = cc.isocode
where	CAST (EXTRACTDATETIME AS DATE) >= '2016-01-01'
and		[description] = 'load'
AND		LABEL = 'GB3'
and accounttyperef = 'Retail'
GROUP BY [description] 
		,CAST (EXTRACTDATETIME AS DATE)
		,BILLINGCURRENCY
		,bb.accounttyperef 
		,cc.currencyname


		select sum(load_amount) from RetailLoadsBycurrencyValitor where datepart(year,extractdate)  ='2016' 

		select sum(load_amount) from RetailLoadsBycurrencyValitor where datepart(year,extractdate)  ='2017' 


		select * from RetailLoadsBycurrencyValitor where extractdate = '2016-01-01'

		select * from RetailLoadsBycurrencyValitor where extractdate = '2017-01-01'

	with cte1 as (
	select dateadd(yyyy,-1,extractdate) as previousyear,* 
	from RetailLoadsBycurrencyValitor
	where datepart(yyyy,extractdate) = datepart(yyyy,getdate())
	)
	select aa.* 
			,bb.extractdate as lastyear
			,bb.load_amount as load_amount_last_year
			,bb.count_of_loads as load_count_last_year
	from	cte1 aa
	join	RetailLoadsBycurrencyValitor bb
	on		aa.previousyear = bb.ExtractDate 
	and		aa.billingcurrency = bb.billingcurrency

	exec sp_who2 active
