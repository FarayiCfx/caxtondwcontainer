DROP TABLE support.DailyRevenueEmail
GO
create table support.DailyRevenueEmail
(	RowNumber int identity (1,1),
	[count_] [int] NULL,
	[RevenueGBP] [decimal](38, 10) NULL,
	[Date_] [date] NULL,
	[AccountType] [nvarchar](50) NULL,
	[Entity] [nvarchar](20) NOT NULL,
	[TradeReason] [nvarchar](50) NULL,
	[SourceSystem] [nvarchar](20) NOT NULL,
	[Start_period] [datetime] NULL,
	[End_Period] [datetime] NULL,
	[Report_Date] [date] null,
	[Corporate FX monthly total budget] [numeric](28, 2) NULL,
	[Corporate FX daily total budget] [numeric](28, 2) NULL,
	[Retail FX monthly total budget] [numeric](28, 2) NULL,
	[Retail FX daily total budget] [numeric](28, 2) NULL,
	[DateInserted] datetime null)

GO
-->
select * from support.DailyRevenueEmail
GO

--->

select	SUM(count_)  as td,'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td, '',
		ENTITY as td,''
FROM	ssisconfigurationdb.support.DailyRevenueEmail
GROUP	BY ENTITY
for xml path('tr'),type
--->

select cast(cast (start_period as smalldatetime) as NVARCHAR(MAX))
from ssisconfigurationdb.support.DailyRevenueEmail

--->talskheet table
select	sum(count_) as td,'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td,'',
		isnull(AccountType,'') as td,'',
		Entity as td
from	ssisconfigurationdb.support.DailyRevenueEmail
where	sourcesystem = 'talksheet'
group by AccountType,
		entity 

-->firebird table results
		select	sum(count_) as td,'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td,'',
		isnull(TradeReason,'') as td,'',
		Entity as td
from	ssisconfigurationdb.support.DailyRevenueEmail
where	sourcesystem = 'firebird'
group by TradeReason,
		entity 

		-------------------------------------------------------------
		----added the estimation columns (for the first table)

select	SUM(count_)  as td,
		'',
		'�'+ cast(convert(numeric(10,2),sum(revenuegbp)) as varchar(20)) as td, 
		'',
		ENTITY as td,
		'',
		min([Corporate FX monthly total budget]) as td,
		'',
		min([Corporate FX daily total budget]) as td,
		'',
		min([Retail FX monthly total budget]) as td,
		'',
		min([Retail FX daily total budget]) as td,
		''
FROM	ssisconfigurationdb.support.DailyRevenueEmail
GROUP	BY ENTITY
for		xml path('tr'),type

select * from ssisconfigurationdb.support.DailyRevenueEmail

		
