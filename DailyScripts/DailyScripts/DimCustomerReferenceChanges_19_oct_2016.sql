--start by dropping the indexes 
/****** Object:  Index [ix_username_dimcustomer]    Script Date: 10/19/2016 10:38:50 AM ******/
DROP INDEX [ix_username_dimcustomer] ON [dw].[DimCustomerReference]
GO

/****** Object:  Index [IX_UQ_NONCLUSTERED_fbid]    Script Date: 10/19/2016 10:38:50 AM ******/
DROP INDEX [IX_UQ_NONCLUSTERED_fbid] ON [dw].[DimCustomerReference]
GO

/****** Object:  Index [IX_UQ_NONCLUSTERED_DBFXcontactid]    Script Date: 10/19/2016 10:38:50 AM ******/
DROP INDEX [IX_UQ_NONCLUSTERED_DBFXcontactid] ON [dw].[DimCustomerReference]
GO

/****** Object:  Index [IX_UQ_NONCLUSTERED_DBCARDScontactid]    Script Date: 10/19/2016 10:38:50 AM ******/
DROP INDEX [IX_UQ_NONCLUSTERED_DBCARDScontactid] ON [dw].[DimCustomerReference]
GO

/****** Object:  Index [ix_ncc_postcode]    Script Date: 10/19/2016 10:38:50 AM ******/
DROP INDEX [ix_ncc_postcode] ON [dw].[DimCustomerReference]
GO
------------------------------------------------------
exec sp_rename '[dw].[DimCustomerReference]','[dw].[DimCustomerReference_old_19_oct_2016]'
go
------------------------------------------------------
--creaste the new dw.dimcustomerref table 


CREATE TABLE [dw].[DimCustomerReference](
	[CustomerKey] [bigint] IDENTITY(1,1) NOT NULL,
	[Id] [nvarchar](256) NULL DEFAULT ((0)),
	[CustomerRefNo] [nvarchar](max) NULL DEFAULT ((0)),
	[FastpayContactRef] [nvarchar](max) NULL DEFAULT ((0)),
	[CardsContactRef] [nvarchar](max) NULL DEFAULT ((0)),
	[ValitorAccountId] [bigint] NULL DEFAULT ((0)),
	[IsActive] [bit] NULL DEFAULT ((0)),
	[UserName] [nvarchar](512) NULL,
	[IsMigratedCustomer] [bit] NULL DEFAULT ((0)),
	[DBCARDSContact_id] [int] NULL DEFAULT ((0)),
	[DBCARDSAccount_Id] [int] NULL DEFAULT ((0)),
	[DBCARDSParent_id] [int] NULL DEFAULT ((0)),
	[DBFXContact_id] [int] NULL DEFAULT ((0)),
	[DBFXAccount_Id] [int] NULL DEFAULT ((0)),
	[DBFXParent_id] [int] NULL DEFAULT ((0)),
	[OriginalSystemSourceId] [int] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[Gender] [nvarchar](20) NULL,
	[DateOfBirth] [datetime] NULL,
	[AddressLine1] [nvarchar](255) NULL,
	[AddressLine2] [nvarchar](255) NULL,
	[City] [nvarchar](200) NULL,
	[County] [nvarchar](200) NULL,
	[Postcode] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[ContactEmailAddress] [nvarchar](255) NULL,
	[PrimaryTelephone] [nvarchar](20) NULL DEFAULT ((0)),
	[SecondaryTelephone] [nvarchar](20) NULL DEFAULT ((0)),
	[TertiaryTelephone] [nvarchar](20) NULL DEFAULT ((0)),
	[ApplicantId] [bigint] NULL,
	[AffiliateCode] [nvarchar](255) NULL,
	[Status] [int] NULL,
	[EKYCStatus] [int] NULL,
	[PreAuthStatus] [int] NULL,
	[RegistrationDate] [datetime] NULL,
	[PostcodeStatus] [int] NULL,
	[WantMultiCurrencyCard] [bit] NULL,
	[WantToSendMoney] [bit] NULL,
	[Voucher] [nvarchar](255) NULL,
	[DuplicateOf] [nvarchar](255) NULL,
	[QuoteID] [bigint] NULL,
	[InitialLoad] [decimal](18, 2) NULL,
	[WSECenter] [bigint] NULL,
	[HeardAboutUs] [nvarchar](255) NULL,
	[selctionoption] [nvarchar](40) NULL,

	[OpenDealDailyLimit] [int] NOT NULL DEFAULT ((0)),
	[OpenDealValueLimit] [decimal](18, 2) NOT NULL DEFAULT ((0)),
	[IsSafeListed] [bit] NOT NULL DEFAULT ((0)),
	[DefaultCurrency] [nvarchar](255) NULL,
	[AccountTypeRef] [nvarchar](255) NULL,
	[MigrationStatus] [nvarchar](20) NULL,
	[ApplicationState] [int] NOT NULL DEFAULT ((0)),

	[AccountTypeRefDerived] [nvarchar](255) NULL,
	[MigrationDetectByEtl] [bit] NULL DEFAULT ((0)),
	[RecordType] [int] NULL,
	[MigrationDetectByEtlDate] [datetime] NULL DEFAULT ('1800-01-01 00:00:000'),
	[SourceCreationDate] [datetime] NULL,
	[SourceUpdateDate] [datetime] NULL,
	[DWDateInserted] [datetime] NULL,
	[DWDateUpdated] [datetime] NULL,
 CONSTRAINT [PK_CUSTOMER_KEY_new10192016] PRIMARY KEY CLUSTERED 
(
	[CustomerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



---------------------------------recreate the indexes 
CREATE NONCLUSTERED INDEX [ix_ncc_postcode] ON [dw].[DimCustomerReference]
(
	[Postcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UQ_NONCLUSTERED_DBCARDScontactid] ON [dw].[DimCustomerReference]
(
	[DBCARDSContact_id] ASC,
	[Id] ASC
)
WHERE ([DBCARDSContact_Id]<>(0))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [IX_UQ_NONCLUSTERED_DBFXcontactid]    Script Date: 10/19/2016 10:40:48 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_UQ_NONCLUSTERED_DBFXcontactid] ON [dw].[DimCustomerReference]
(
	[DBFXContact_id] ASC
)
WHERE ([DBFXContact_Id]<>(0))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_UQ_NONCLUSTERED_fbid] ON [dw].[DimCustomerReference]
(
	[Id] ASC
)
WHERE ([Id]<>'0')
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [ix_username_dimcustomer] ON [dw].[DimCustomerReference]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


select top 10 * from fis.FactFinAdv

select top 10 * from fis.factcardload