SELECT TOP 10 * FROM staging.FactApplicants (nolock)
ORDER BY id 

SELECT TOP 100 * FROM dw.DimCustomerReference


EXEC  [DW].[MergeFactApplicants] 

CREATE PROCEDURE [DW].[MergeFactApplicants] 
AS 
/*
Farayi Nzenza

This is the merge for the records from the applicants table ,
The important data points that appear to change are the created
date value because for some records KYC checks take more than 
24 hours and this is reflected in the same row, so we have an unusual 
situation of a merge taking place for a fact record (not straight 
insert), this is also closely integrated with the customer key, they 
only get a created date if an account gets created in the aspnetusers 
table , therefore we can still retrieve a customer key for records 
that initially go in as unknown customer key
*/
MERGE	[DW].[FactApplicants] AS TARGET
USING	(SELECT isnull (bb.CustomerKey,-1) AS CustomerKey,	
		dw.UdfGetDateInteger(RegistrationWithNoTime) RegistrationDateKey,
		dw.UdfGetDateInteger(CreationDateWithNoTime) CreationDateKey,
		aa.*
		FROM [staging].[FactApplicants] AA (NOLOCK)
		LEFT OUTER JOIN DW.DimCustomerReference BB (nolock)
		ON aa.Email = bb.UserName
		AND LEFT(bb.id,3) <> 'MIG' )AS SOURCE
ON		(TARGET.ID = SOURCE.ID)
WHEN	MATCHED THEN 
UPDATE  SET 
			CustomerKEY = ISNULL(source.CustomerKey,-1),
			RegistrationDateKey = source.RegistrationDateKey,
			CreationDateKey = source.CreationDateKey,
			AffiliateCodeKey = 0,
			id = SOURCE.Id,
			Email = source.Email,
			Title = source.Title,
			FirstName = source.FirstName,
			LastName = source.LastName,
			MothersMaidenName = source.MothersMaidenName,
			PrimaryContactNo = source.PrimaryContactNo,
			SecondaryContactNo = source.SecondaryContactNo,
			Address1 = source.Address1,
			Address2 = source.Address2,
			City = source.City,
			County = source.County,
			Postcode = source.Postcode,
			Country = source.Country,
			ContactEmail = source.ContactEmail,
			CustomerRefNo = source.CustomerRefNo,
			FastpayContactRef = source.FastpayContactRef,
			CardsContactRef = source.CardsContactRef,
			DateofBirth = source.DateofBirth,
			AffiliateCode = source.AffiliateCode,
			Status = source.Status,
			EKYCStatus = source.EKYCStatus,
			PreAuthStatus = source.PreAuthStatus,
			Notes = source.Notes,
			RegistrationDate = source.RegistrationDate,
			PostcodeStatus = source.PostcodeStatus,
			WantMultiCurrencyCard = source.WantMultiCurrencyCard,
			WantToSendMoney = source.WantToSendMoney,
			PasswordHash = source.PasswordHash,
			Voucher = source.Voucher,
			DuplicateOf = source.DuplicateOf,
			QuoteID = source.QuoteID,
			InitialLoad = source.InitialLoad,
			CreationDate = source.CreationDate,
			Gender = source.Gender,
			WSECenter = source.WSECenter,
			HeardAboutUs = source.HeardAboutUs,
			PrimaryAccountId = source.PrimaryAccountId,
			SelctionOption = source.SelctionOption,
			AccountTypeRef = source.AccountTypeRef,
			--DWDateInserted =  source.DWDateInserted,
			DWDateUpdated =  source.DWDateUpdated

		
WHEN		NOT MATCHED THEN 
INSERT		(CustomerKEY,
			RegistrationDateKey,
			CreationDateKey,
			AffiliateCodeKey,
			Id,
			Email,
			Title,
			FirstName,
			LastName,
			MothersMaidenName,
			PrimaryContactNo,
			SecondaryContactNo,
			Address1,
			Address2,
			City,
			County,
			Postcode,
			Country,
			ContactEmail,
			CustomerRefNo,
			FastpayContactRef,
			CardsContactRef,
			DateofBirth,
			AffiliateCode,
			Status,
			EKYCStatus,
			PreAuthStatus,
			Notes,
			RegistrationDate,
			PostcodeStatus,
			WantMultiCurrencyCard,
			WantToSendMoney,
			PasswordHash,
			Voucher,
			DuplicateOf,
			QuoteID,
			InitialLoad,
			CreationDate,
			Gender,
			WSECenter,
			HeardAboutUs,
			PrimaryAccountId,
			SelctionOption,
			AccountTypeRef,
			DWDateInserted,
			DWDateUpdated

)
VALUES		(source.CustomerKEY,
			source.RegistrationDateKey,
			source.CreationDateKey,
			0,
			source.Id,
			source.Email,
			source.Title,
			source.FirstName,
			source.LastName,
			source.MothersMaidenName,
			source.PrimaryContactNo,
			source.SecondaryContactNo,
			source.Address1,
			source.Address2,
			source.City,
			source.County,
			source.Postcode,
			source.Country,
			source.ContactEmail,
			source.CustomerRefNo,
			source.FastpayContactRef,
			source.CardsContactRef,
			source.DateofBirth,
			source.AffiliateCode,
			source.Status,
			source.EKYCStatus,
			source.PreAuthStatus,
			source.Notes,
			source.RegistrationDate,
			source.PostcodeStatus,
			source.WantMultiCurrencyCard,
			source.WantToSendMoney,
			source.PasswordHash,
			source.Voucher,
			source.DuplicateOf,
			source.QuoteID,
			source.InitialLoad,
			source.CreationDate,
			source.Gender,
			source.WSECenter,
			source.HeardAboutUs,
			source.PrimaryAccountId,
			source.SelctionOption,
			source.AccountTypeRef,
			source.DWDateInserted,
			source.DWDateUpdated

			


);



