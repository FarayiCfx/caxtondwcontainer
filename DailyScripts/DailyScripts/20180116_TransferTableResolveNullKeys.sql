use datawarehouse
/*
16/01/2018

Farayi

Fix records in the transfer table without customer keys

Show me records with no matching 
email address in customer reference table
but that show up in the transfers table 
*/
drop	table if exists #temp1 
;
with	cte1 as (
select	* 
from	FireBird.FactPaymentTransfer aa
where	CustomerKey is null)
select	dm.email
		,cte1.*
into	#temp1
from	cte1 
left	outer join dw.DimCustomerReference dm
on		cte1.UserId = dm.Email
where	dm.Email is null
;
select	distinct userid 
from	#temp1
where	UserId not like '%removed%' 
and		UserId not like '%cancelled%'
--> 17/01/2018 --> 17 rows 


/*
Now update the transfer table with the customer key 
*/
drop	table if exists #updatetable
;
with	cte1 as (
select	* 
from	FireBird.FactPaymentTransfer aa
where	CustomerKey is null)
select	dm.email
		,dm.CustomerKey as custkey_reftable
		,cte1.*
into	#updatetable
from	cte1 
join	dw.DimCustomerReference dm
on		cte1.UserId = dm.Email

select	* from #updatetable
;
--the update statement below
update	t1
set		t1.CustomerKey = #updatetable.custkey_reftable
--select t1.CustomerKey, #updatetable.custkey_reftable, t1.Id, #updatetable.Id
from	FireBird.FactPaymentTransfer t1
join	#updatetable 
on		t1.Id = #updatetable.Id
