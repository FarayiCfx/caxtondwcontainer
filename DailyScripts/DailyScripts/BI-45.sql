CREATE view dw.[BI-45 AffiliateSalesReport]
as
with cteApplication
as(
select		count(applicationkey) as count_of_applications,
			isnull([affiliatecode],'No affiliate code presented') as affiliate_code,
			registrationdatekey ,
			cast(RegistrationDate as date) as registration_date
from		dw.factapplicants 
where		registrationdatekey > 20151231
group		by [affiliatecode],
			registrationdatekey ,
			cast(RegistrationDate as date) )
			,
cteAccounts as (
select		count(applicationkey) as count_of_succesful_registrations,
			isnull([affiliatecode],'No affiliate code presented') as affiliate_code,
			registrationdatekey ,
			cast(RegistrationDate as date) as registration_date
from		dw.factapplicants 
where		registrationdatekey > 20151231
and			CustomerKEY > 0
group		by [affiliatecode],
			registrationdatekey ,
			cast(RegistrationDate as date) )

select	aa.registration_date,aa.affiliate_code,aa.count_of_applications,
		isnull( bb.count_of_succesful_registrations,0) as count_of_succesful_registrations
from	cteApplication aa left outer join cteAccounts bb 
on		aa.registrationdatekey = bb.registrationdatekey
and		aa.affiliate_code = bb.affiliate_code
GO


grant select on dw.[BI-45 AffiliateSalesReport] to cfx_datareader

