

---start by checking for all the wrong values 
iF OBJECT_ID('dbo.NewUsersWrongIdentityBeforeUpdate', 'U') IS NOT NULL
DROP TABLE dbo.NewUsersWrongIdentityBeforeUpdate; 
go
select	aa.id as customerportalid,
		bb.id existingid,
		bb.username existingusername,
		aa.valitoraccountid as valitor_accountid_fromcustportal,
		bb.valitoraccountid as valitoraccountidBeforeUpdate
into	dbo.NewUsersWrongIdentityBeforeUpdate 
from	aspnetuserscustomerportal aa (nolock)
right	outer join	aspnetusers bb (nolock)
on		aa.id = bb.id
where	aa.id is null

select * from NewUsersWrongIdentityBeforeUpdate

--now create the identity fix table 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'[dbo].[AspNetUsers_IdentityFix]', N'U') IS NOT NULL
BEGIN
DROP TABLE [dbo].[AspNetUsers_IdentityFix]
END


CREATE TABLE [dbo].[AspNetUsers_IdentityFix](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](256) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[FirstName] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[MothersMaidenName] [nvarchar](max) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[County] [nvarchar](max) NULL,
	[Postcode] [nvarchar](256) NULL,
	[Country] [nvarchar](max) NULL,
	[ContactEmail] [nvarchar](max) NULL,
	[CustomerRefNo] [nvarchar](max) NULL,
	[FastpayContactRef] [nvarchar](max) NULL,
	[CardsContactRef] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[DateofBirth] [datetime] NOT NULL,
	[AffiliateCode] [nvarchar](max) NULL,
	[SecondaryContactNo] [nvarchar](max) NULL,
	[ValitorAccountId] [bigint] IDENTITY(1,1) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Gender] [nvarchar](max) NULL,
	[OpenDealDailyLimit] [int] NOT NULL,
	[OpenDealValueLimit] [decimal](18, 2) NOT NULL,
	[WSECenter] [bigint] NULL,
	[IsSafeListed] [bit] NOT NULL,
	[DefaultCurrency] [nvarchar](max) NULL,
	[AccountTypeRef] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers_IdentityFix] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[AspNetUsers_IdentityFix] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [DateofBirth]
GO

ALTER TABLE [dbo].[AspNetUsers_IdentityFix] ADD  DEFAULT ((0)) FOR [IsActive]
GO

ALTER TABLE [dbo].[AspNetUsers_IdentityFix] ADD  DEFAULT ((0)) FOR [OpenDealDailyLimit]
GO

ALTER TABLE [dbo].[AspNetUsers_IdentityFix] ADD  DEFAULT ((0)) FOR [OpenDealValueLimit]
GO

ALTER TABLE [dbo].[AspNetUsers_IdentityFix] ADD  DEFAULT ((0)) FOR [IsSafeListed]
GO

CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex_IdentityFix] ON [dbo].[AspNetUsers_IdentityFix]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


/****** Object:  Index [IX_CoveringIndex_Postcode]    Script Date: 03/12/2015 15:31:12 ******/
CREATE NONCLUSTERED INDEX [IX_CoveringIndex_Postcode] ON [dbo].[AspNetUsers_IdentityFix]
(
	[Postcode] ASC
)
INCLUDE ( 	[FirstName],
	[LastName],
	[PhoneNumber],
	[Email],
	[UserName]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [IX_SearcHelpdesk_Email]    Script Date: 03/12/2015 15:31:50 ******/
CREATE NONCLUSTERED INDEX [IX_SearcHelpdesk_Email] ON [dbo].[AspNetUsers_IdentityFix]
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [IX_SearcHelpdesk_FirstLastName]    Script Date: 03/12/2015 15:32:39 ******/
CREATE NONCLUSTERED INDEX [IX_SearcHelpdesk_FirstLastName] ON [dbo].[AspNetUsers_IdentityFix]
(
	[FirstName] ASC,
	[LastName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [IX_SearcHelpdesk_PhoneNumber]    Script Date: 03/12/2015 15:32:57 ******/
CREATE NONCLUSTERED INDEX [IX_SearcHelpdesk_PhoneNumber] ON [dbo].[AspNetUsers_IdentityFix]
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [IX_SearcHelpdesk_Postcode]    Script Date: 03/12/2015 15:33:14 ******/
CREATE NONCLUSTERED INDEX [IX_SearcHelpdesk_Postcode] ON [dbo].[AspNetUsers_IdentityFix]
(
	[Postcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/****** Object:  Index [IX_SearcHelpdesk_UserName]    Script Date: 03/12/2015 15:33:32 ******/
--CREATE NONCLUSTERED INDEX [IX_SearcHelpdesk_UserName] ON [dbo].[AspNetUsers_IdentityFix]
--(
	--[UserName] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO


--to get data into the table set identity insert on 
SET		IDENTITY_INSERT [DBO].[ASPNETUSERS_IDENTITYFIX] ON
GO

INSERT	INTO [DBO].[ASPNETUSERS_IDENTITYFIX] (Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LastName,MothersMaidenName,Address1,Address2,City,County,Postcode,Country,ContactEmail,CustomerRefNo,FastpayContactRef,CardsContactRef,Title,DateofBirth,AffiliateCode,SecondaryContactNo,ValitorAccountId,IsActive,Gender,OpenDealDailyLimit,OpenDealValueLimit,WSECenter,IsSafeListed,DefaultCurrency,AccountTypeRef)
SELECT	Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LastName,MothersMaidenName,Address1,Address2,City,County,Postcode,Country,ContactEmail,CustomerRefNo,FastpayContactRef,CardsContactRef,Title,DateofBirth,AffiliateCode,SecondaryContactNo,ValitorAccountId,IsActive,Gender,OpenDealDailyLimit,OpenDealValueLimit,WSECenter,IsSafeListed,DefaultCurrency,AccountTypeRef 
FROM	[DBO].[aspnetuserscustomerportal]
GO
--number should be (174890 row(s) affected)

--then reseed identity columns 

DBCC CHECKIDENT ('[DBO].[ASPNETUSERS_IDENTITYFIX]', RESEED, 405508);
GO

--check the values you have
select ident_current('[DBO].[ASPNETUSERS_IDENTITYFIX]')

--then put back the auto generated id 
SET		IDENTITY_INSERT [DBO].[ASPNETUSERS_IDENTITYFIX] OFF 
GO

-- insert newly created users into the new table 
INSERT	INTO [DBO].[ASPNETUSERS_IDENTITYFIX] (Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LastName,MothersMaidenName,Address1,Address2,City,County,Postcode,Country,ContactEmail,CustomerRefNo,FastpayContactRef,CardsContactRef,Title,DateofBirth,AffiliateCode,SecondaryContactNo,IsActive,Gender,OpenDealDailyLimit,OpenDealValueLimit,WSECenter,IsSafeListed,DefaultCurrency,AccountTypeRef)

select	bb.Id
,bb.Email
,bb.EmailConfirmed
,bb.PasswordHash
,bb.SecurityStamp
,bb.PhoneNumber
,bb.PhoneNumberConfirmed
,bb.TwoFactorEnabled
,bb.LockoutEndDateUtc
,bb.LockoutEnabled
,bb.AccessFailedCount
,bb.UserName
,bb.FirstName
,bb.LastName
,bb.MothersMaidenName
,bb.Address1
,bb.Address2
,bb.City
,bb.County
,bb.Postcode
,bb.Country
,bb.ContactEmail
,bb.CustomerRefNo
,bb.FastpayContactRef
,bb.CardsContactRef
,bb.Title
,bb.DateofBirth
,bb.AffiliateCode
,bb.SecondaryContactNo
--, ValitorAccountid
,bb.IsActive
,bb.Gender
,bb.OpenDealDailyLimit
,bb.OpenDealValueLimit
,bb.WSECenter
,bb.IsSafeListed
,bb.DefaultCurrency
,bb.AccountTypeRef 
from	aspnetuserscustomerportal aa (nolock)
right	outer join	aspnetusers bb (nolock)
on		aa.id = bb.id
where	aa.id is null

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--then perform your foreign key management 

ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO   ---thir foreign key was giving me grief 

ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO

--********************************************then rename the existing table ------WARNING DANGER***************
EXEC SP_RENAME 'dbo.AspNetUsers','AspNetUsers_Old'
go


--then rename the identity fix table to the new one 
EXEC SP_RENAME 'dbo.AspNetUsers_IDENTITYFIX','AspNetUsers'
go

--------------------recreate the foreign keys *********************************************************************************

ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO

--job finished 

--then check your work 

select	old.*, ValitorAccountId
from	dbo.NewUsersWrongIdentityBeforeUpdate old
join	aspnetusers bb
on		old.existingid = bb.id
order by valitoraccountidbeforeupdate


update aspnetusers set AccountTypeRef = 'Retail'


--look at the table again 

exec sp_help 'aspnetusers'

exec sp_help 'aspnetuserlogins'

exec sp_help 'aspnetuserroles'

exec sp_spaceused 'aspnetusers'

exec sp_spaceused 'aspnetusers_old'


select top 10 * from aspnetusers

