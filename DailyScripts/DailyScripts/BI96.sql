select top 10 * from Contact

--rather this is where to get the company name from 


select top 10 * from [dbo].[Branch] where account_id = 163426

select top 10 * from Contact where account_id = 163426

SELECT TOP 10 * FROM [dbo].[vwCfxCardContactFullInfo]

select top 1000 * from [dbo].[tblCfxCurrencyCardDetails]

select DISTINCT cardstatus from [dbo].[tblCfxCurrencyCardDetails]

select count(currencycarddetailid) as count_,cardstatus
from  [dbo].[tblCfxCurrencyCardDetails]
group by cardstatus

select count(currencycarddetailid) as count_,[status]
from  [dbo].[tblCfxCurrencyCardDetails]
group by [status]


exec sp_spaceused '[dbo].[tblCfxCurrencyCardDetails]'


select top 10 * from [dbo].[tblCfxCurrencyCardDetails]

select * from #emailwithranks

--start by creating your email ranked table to get the lates email per contact 
IF	 OBJECT_ID('tempdb..#emailwithranks') IS NOT NULL
DROP TABLE #emailwithranks
go
;

with	cte1 as (
select	rank() over (partition by contact_id order by Contact_Email_id desc) as rank_,
		* 
from	contact_email (nolock))
select	*  into #emailwithranks 
from	cte1 where rank_ = 1 ;

-->select * from #emailwithranks

-->declare a cte for each type , 

with		cteEuroCorp as (
select		Contact_ID,
			isnull(CurrencyCardDetailID,'') as [EURCARDID],
			isnull(CardNumber,'') AS [EURCARDNUMBER],
			[EURCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459566')
,
cteGBPCorp as (
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [GBPCARDID],
        isnull(CardNumber,'') AS [GBPCardNumber],
		[GBPCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459567')
,
cteUSDCorp as(
select    Contact_ID,
        isnull(CurrencyCardDetailID,'') as [USDCARDID],
        isnull(CardNumber,'') AS [USD Card Number],
		[USDCardStatus] = CASE [CardStatus]
			WHEN '00' then 'Normal'
			when '01' then 'pin tries exceeded'
			when '02' then 'not yet issued'
			when '03' then 'card expired'
			when '04' then 'card reported lost'
			when '06' then 'customer closed'
			else [CardStatus]
			end
from    [dbo].[tblCfxCurrencyCardDetails] (nolock)
where    cast(left(CardNumber,6) as char(6)) = '459568')

select  aa.contact_id ,
        isnull(aa.parent_id,0) as parent_id,
        acc.Account_short_name as [Company Name (not for upload)],
        AA.contact_first_name,
        AA.contact_last_name,
        Gender = case    when contact_salutation like 'Ms%' then 'Female'
                        when contact_salutation like 'Mrs%' then 'Female'
                        when contact_salutation like 'Miss%' then 'Female' 
                        when contact_salutation like 'Mr%' then 'Male'
                        else 'not supplied'
                        end ,
                        dob ,
						case	when isnull(aa.parent_id,0) = 0 then 'Yes'
								when isnull(aa.parent_id,0) > 0 then 'No'
						end as Iscompanyadmin ,
        datediff(yy,isnull(dob,''),getdate()) as Age, 
        eml.email_address ,
        aa.account_id as [Card group name],
        isnull(usd.usdcardid,'') as usdcardid,
        isnull(usd.[usd card number],'') as [usd card number],
		[USDCardstatus],
        isnull(eur.[eurcardid],'') as [EURCARDID],
        isnull(eur.[EURCARDNUMBER],'') as [EURCARDNUMBER],
		[EURCardStatus],
        isnull(gbp.[GBPCARDID],'') as [GBPCARDID],
        isnull(gbp.[GBPCardNumber],'') as [GBPCardNumber],
		[GBPCARDSTATUS]
from    Contact aa
left    outer join cteEuroCorp eur
on        aa.contact_id = eur.contact_id
left    outer join cteGBPCorp gbp
on        aa.contact_id = gbp.contact_id 
left    outer join cteUSDCorp usd
on        aa.contact_id = usd.contact_id
left    outer join #emailwithranks eml
on        aa.contact_id = eml.contact_id
join    Account acc
on        aa.account_id = acc.Account_id
where    aa.contact_id in
(select    contact_id 
from    Contact
where    account_id in (select account_id from ##finaloutput_bi95)
)
order    by aa.account_id,aa.parent_id ASC
