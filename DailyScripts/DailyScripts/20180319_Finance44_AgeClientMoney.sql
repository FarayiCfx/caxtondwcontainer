/*
In step 1 create a temp table that gathers all of the transactions 
from the currency bank account transaction table that have a non zero 
figure for the balance 

Then outside of the CTE use the windowing function to find the largest transaction 
id within the year,month, currency and user id then create the month and year name  
*/
select @@SERVERNAME -->qwdf2wsfp5
 


;
DROP	TABLE IF EXISTS #TEMP1
;
with	cte1 as (
select	DATEPART(YYYY,TransactionDateTime) as [Year]
		,DATEPART(mm,TransactionDateTime) as [Month]
		,cc.BuyCcyCode as Currency
		,bb.UserId
		,aa.* 
from	FireBird.FactCurrencyBankAccountTransaction aa (nolock)
join	FireBird.DimCurrencyBankAccountSummary bb (nolock)
on		aa.AccountSummaryId = bb.Id
join	CurrencyBank.Currency cc
on		bb.CurrencyId = cc.Id
where	bb.UserId  IN(SELECT DISTINCT username FROM dbo.FINANCE44)--AccountSummaryId in (163503,163506,805580,823943)
and		aa.Balance > 0.00
)
select	MAX(id) 
over	(partition by [year],[MONTH],currency,USERID order by id asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS MaxInPartition
		,datename(month,transactiondatetime)+'-'+datename(yyyy,transactiondatetime) as [DateName]
		,* 
INTO	#TEMP1
from	cte1
order	by Id asc

---------------------------------------------------------------------------------------------------------------------------------------------------

--484534
SELECT * FROM #TEMP1 ORDER BY Balance


drop table if exists FINANCE44Output
;
select	USERID,CURRENCY,[February-2015]
,[March-2015]
,[April-2015]
,[May-2015]
,[June-2015]
,[July-2015]
,[August-2015]
,[September-2015]
,[October-2015]
,[November-2015]
,[December-2015]
,[January-2016]
,[February-2016]
,[March-2016]
,[April-2016]
,[May-2016]
,[June-2016]
,[July-2016]
,[August-2016]
,[September-2016]
,[October-2016]
,[November-2016]
,[December-2016]
,[January-2017]
,[February-2017]
,[March-2017]
,[April-2017]
,[May-2017]
,[June-2017]
,[July-2017]
,[August-2017]
,[September-2017]
,[October-2017]
,[November-2017]
,[December-2017]
,[January-2018]
,[February-2018] 
INTO		 FINANCE44Output
from
(
select	--id
		--,MaxInPartition
		[DateName]
		,UserId
		,Currency
		--,Amount
		,Balance
from	#temp1
where	id in (select distinct (maxinpartition) from #temp1)
) as sourcetable
pivot
(
sum	(balance)
for [datename] in ([February-2015]
,[March-2015]
,[April-2015]
,[May-2015]
,[June-2015]
,[July-2015]
,[August-2015]
,[September-2015]
,[October-2015]
,[November-2015]
,[December-2015]
,[January-2016]
,[February-2016]
,[March-2016]
,[April-2016]
,[May-2016]
,[June-2016]
,[July-2016]
,[August-2016]
,[September-2016]
,[October-2016]
,[November-2016]
,[December-2016]
,[January-2017]
,[February-2017]
,[March-2017]
,[April-2017]
,[May-2017]
,[June-2017]
,[July-2017]
,[August-2017]
,[September-2017]
,[October-2017]
,[November-2017]
,[December-2017]
,[January-2018]
,[February-2018])
)as pvt
ORDER BY UserId 
------

select AA.[USERID]
,AA.[CURRENCY]
,ISNULL([February-2015],0) AS [February-2015]
,ISNULL([March-2015],0) [March-2015]
,ISNULL([April-2015],0) [April-2015]
,ISNULL([May-2015],0) [May-2015]
,ISNULL([June-2015],0) [June-2015]
,ISNULL([July-2015],0) [July-2015]
,ISNULL([August-2015],0) [August-2015]
,ISNULL([September-2015],0) [September-2015]
,ISNULL([October-2015],0) [October-2015]
,ISNULL([November-2015],0) [November-2015]
,ISNULL([December-2015],0) [December-2015]
,ISNULL([January-2016],0) [January-2016]
,ISNULL([February-2016],0) [February-2016]
,ISNULL([March-2016],0) [March-2016]
,ISNULL([April-2016],0) [April-2016]
,ISNULL([May-2016],0) [May-2016]
,ISNULL([June-2016],0) [June-2016]
,ISNULL([July-2016],0) [July-2016]
,ISNULL([August-2016],0) [August-2016]
,ISNULL([September-2016],0) [September-2016]
,ISNULL([October-2016],0) [October-2016]
,ISNULL([November-2016],0) [November-2016]
,ISNULL([December-2016],0) [December-2016]
,ISNULL([January-2017],0) [January-2017]
,ISNULL([February-2017],0)[February-2017]
,ISNULL([March-2017],0)[March-2017]
,ISNULL([April-2017],0)[April-2017]
,ISNULL([May-2017],0)[May-2017]
,ISNULL([June-2017],0)[June-2017]
,ISNULL([July-2017],0)[July-2017]
,ISNULL([August-2017],0)[August-2017]
,ISNULL([September-2017],0)[September-2017]
,ISNULL([October-2017],0)[October-2017]
,ISNULL([November-2017],0)[November-2017]
,ISNULL([December-2017],0)[December-2017]
,ISNULL([January-2018],0)[January-2018]
,ISNULL([February-2018] ,0)[February-2018]
from	FINANCE44Output aa
join	FINANCE44  bb
on		aa.userid = bb.username
and		aa.Currency = BB.CURRENCY
where	UserId = 'alamex@caxtonfx.com'
;



---------------------------------------------------------------------------------------------------------------------------------------------------

/*
This is the distinct emails from the imported list to the 
output list see what is in the imported list versus the 
output list
*/
with	cteoriginallist as 
(select distinct username from finance44), --7546
cteoutputlist as 
(select distinct userid from finance44output) --7531
select	* 
into	finance44_missingemailsfromoutput
from	cteoriginallist aa 
left	outer join	cteoutputlist bb
on		aa.username =  bb.userid 
where	bb.userid is null

-->
select	* from finance44_missingemailsfromoutput
/*
These users have had their emails changed in aspnetsusers
and dw.dimcustomerreference tables, but the accountsummary
tables still have the old email addresses which is why there 
no matches 
*/
alter	table finance44_missingemailsfromoutput
add		corrected_username varchar(50) null
;
update	finance44_missingemailsfromoutput
set		corrected_username = 'peter@britishexploring.org'
where	username = 'accounts@britishexploring.org'

update	finance44_missingemailsfromoutput
set		corrected_username = 'faybhaggar@gmail.com'
where	username = 'faybhaggar@REMOVEDACCOUNT-gmail.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'Jarrod.Wisniewski@sgscol.ac.uk'
where	username = 'jarrod.wisniewski@REMOVEDACCOUNT-sgscol.ac.uk'

update	finance44_missingemailsfromoutput
set		corrected_username = 'john.cox@coplandevents.com'
where	username = 'john.cox@removedaccount-coplandevents.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'kirsty1@hooliganofcowes.co.uk'
where	username = 'kirsty@hooliganofcowes.co.uk'

update	finance44_missingemailsfromoutput
set		corrected_username = 'kirsty1@hooliganofcowes.co.uk'
where	username = 'kirsty@hooliganofcowes.co.uk'

update	finance44_missingemailsfromoutput
set		corrected_username = 'lbyrne@frutarom.com'
where	username = 'krussell@frutarom.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 's.marchant@bhhs.gdst.net'
where	username = 'l.ward@bhhs.gdst.net'

update	finance44_missingemailsfromoutput
set		corrected_username = 's.marchant@bhhs.gdst.net'
where	username = 'l.ward@bhhs.gdst.net'

update	finance44_missingemailsfromoutput
set		corrected_username = 's.marchant@bhhs.gdst.net'
where	username = 'l.ward@bhhs.gdst.net'

update	finance44_missingemailsfromoutput
set		corrected_username = 'lauren@gapforce.org'
where	username = 'marcus@gapforce.org'

update	finance44_missingemailsfromoutput
set		corrected_username = 'nic.brogden@googlemail.com'
where	username = 'nic.brogden@removedaccount-googlemail.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'nic.brogden@googlemail.com'
where	username = 'nic.brogden@removedaccount-googlemail.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'nigel@classicrock.freeserve.co.uk'
where	username = 'nigelcrickmore@yahoo.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'rosjenk@btinternet.com'
where	username = 'rosjenk@REMOVEDACCOUNT-btinternet.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'akara@nch.com'
where	username = 'sboyle@nch.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'hartleyf@cheltladiescollege.org'
where	username = 'sealeyk@cheltladiescollege.org'

update	finance44_missingemailsfromoutput
set		corrected_username = 'lesley@online-groupuk.com'
where	username = 'stephen@online-groupuk.com'

update	finance44_missingemailsfromoutput
set		corrected_username = 'termolby@aol.com'
where	username = 'terryolby@gmail.com'

-->

select	* from finance44_missingemailsfromoutput

/*
Now that you have corrections table 
do exactly the same technqiue but use 
different tables to output the data 
*/

DROP	TABLE IF EXISTS #TEMP1
;
with	cte1 as (
select	DATEPART(YYYY,TransactionDateTime) as [Year]
		,DATEPART(mm,TransactionDateTime) as [Month]
		,cc.BuyCcyCode as Currency
		,bb.UserId
		,aa.* 
from	FireBird.FactCurrencyBankAccountTransaction aa (nolock)
join	FireBird.DimCurrencyBankAccountSummary bb (nolock)
on		aa.AccountSummaryId = bb.Id
join	CurrencyBank.Currency cc
on		bb.CurrencyId = cc.Id
where	bb.UserId  IN(SELECT DISTINCT corrected_username FROM finance44_missingemailsfromoutput)--AccountSummaryId in (163503,163506,805580,823943)
and		aa.Balance > 0.00
)
select	MAX(id) 
over	(partition by [year],[MONTH],currency,USERID order by id asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS MaxInPartition
		,datename(month,transactiondatetime)+'-'+datename(yyyy,transactiondatetime) as [DateName]
		,* 
INTO	#TEMP1
from	cte1
order	by Id asc

--
SELECT * FROM #TEMP1 ORDER BY Balance

/*create a new output table, with the outstanding data */

drop table if exists FINANCE44Output_unmatchedemails
;
select	USERID,CURRENCY,[February-2015]
,[March-2015]
,[April-2015]
,[May-2015]
,[June-2015]
,[July-2015]
,[August-2015]
,[September-2015]
,[October-2015]
,[November-2015]
,[December-2015]
,[January-2016]
,[February-2016]
,[March-2016]
,[April-2016]
,[May-2016]
,[June-2016]
,[July-2016]
,[August-2016]
,[September-2016]
,[October-2016]
,[November-2016]
,[December-2016]
,[January-2017]
,[February-2017]
,[March-2017]
,[April-2017]
,[May-2017]
,[June-2017]
,[July-2017]
,[August-2017]
,[September-2017]
,[October-2017]
,[November-2017]
,[December-2017]
,[January-2018]
,[February-2018] 
INTO		 FINANCE44Output_unmatchedemails
from
(
select	--id
		--,MaxInPartition
		[DateName]
		,UserId
		,Currency
		--,Amount
		,Balance
from	#temp1
where	id in (select distinct (maxinpartition) from #temp1)
) as sourcetable
pivot
(
sum	(balance)
for [datename] in ([February-2015]
,[March-2015]
,[April-2015]
,[May-2015]
,[June-2015]
,[July-2015]
,[August-2015]
,[September-2015]
,[October-2015]
,[November-2015]
,[December-2015]
,[January-2016]
,[February-2016]
,[March-2016]
,[April-2016]
,[May-2016]
,[June-2016]
,[July-2016]
,[August-2016]
,[September-2016]
,[October-2016]
,[November-2016]
,[December-2016]
,[January-2017]
,[February-2017]
,[March-2017]
,[April-2017]
,[May-2017]
,[June-2017]
,[July-2017]
,[August-2017]
,[September-2017]
,[October-2017]
,[November-2017]
,[December-2017]
,[January-2018]
,[February-2018])
)as pvt
ORDER BY UserId 



---------------------------------------------------------------------------------------------------------------------------------------------------------->


;
with	cteCorrectedEmails as (
select	aa.* , bb.corrected_username
from	finance44 aa
join	finance44_missingemailsfromoutput bb
on		aa.username = bb.username)

select BB.username 
--,AA.[USERID]
,AA.[CURRENCY]
,ISNULL([February-2015],0) AS [February-2015]
,ISNULL([March-2015],0) [March-2015]
,ISNULL([April-2015],0) [April-2015]
,ISNULL([May-2015],0) [May-2015]
,ISNULL([June-2015],0) [June-2015]
,ISNULL([July-2015],0) [July-2015]
,ISNULL([August-2015],0) [August-2015]
,ISNULL([September-2015],0) [September-2015]
,ISNULL([October-2015],0) [October-2015]
,ISNULL([November-2015],0) [November-2015]
,ISNULL([December-2015],0) [December-2015]
,ISNULL([January-2016],0) [January-2016]
,ISNULL([February-2016],0) [February-2016]
,ISNULL([March-2016],0) [March-2016]
,ISNULL([April-2016],0) [April-2016]
,ISNULL([May-2016],0) [May-2016]
,ISNULL([June-2016],0) [June-2016]
,ISNULL([July-2016],0) [July-2016]
,ISNULL([August-2016],0) [August-2016]
,ISNULL([September-2016],0) [September-2016]
,ISNULL([October-2016],0) [October-2016]
,ISNULL([November-2016],0) [November-2016]
,ISNULL([December-2016],0) [December-2016]
,ISNULL([January-2017],0) [January-2017]
,ISNULL([February-2017],0)[February-2017]
,ISNULL([March-2017],0)[March-2017]
,ISNULL([April-2017],0)[April-2017]
,ISNULL([May-2017],0)[May-2017]
,ISNULL([June-2017],0)[June-2017]
,ISNULL([July-2017],0)[July-2017]
,ISNULL([August-2017],0)[August-2017]
,ISNULL([September-2017],0)[September-2017]
,ISNULL([October-2017],0)[October-2017]
,ISNULL([November-2017],0)[November-2017]
,ISNULL([December-2017],0)[December-2017]
,ISNULL([January-2018],0)[January-2018]
,ISNULL([February-2018] ,0)[February-2018]
from	FINANCE44Output_unmatchedemails aa
join	cteCorrectedEmails  bb
on		aa.userid = bb.corrected_username
and		aa.Currency = BB.CURRENCY
order	by 1 
