
--->CREATE FUNCTION THEN CREATE SCHEME 
create	partition function pfByYearTransactionDatetime(datetime)
as		range left
for		values('20151231','20161231','20171231','20181231')
go
CREATE	PARTITION SCHEME PSBYYEARTRANDATETIME 
AS		PARTITION pfByYearTransactionDatetime 
ALL TO ([PRIMARY])


select	wallettransaction_key
		,transactiondatekey 
		,extractdatetime
		,registrationdate
		,transactiondatetime
		,processingdate
		,timestampid
		,downloadfilename
		,datediff(dd,extractdatetime,transactiondatetime)
from	valitor.factwallettransactions (nolock) 
where	datediff(dd,extractdatetime,transactiondatetime) <> 0

select * from valitor.factwallettransactions (nolock)
where transactiondatetime is null

where	downloadfilename = 'C600-WalletTransactionExtract-20170925000000.xml'

exec sp_help 'valitor.factwallettransactions'

--downloadfilename 

SELECT	o.name objectname,i.name indexname, partition_id, partition_number, [rows]
FROM	sys.partitions p
INNER	JOIN sys.objects o ON o.object_id=p.object_id
INNER	JOIN sys.indexes i ON i.object_id=p.object_id and p.index_id=i.index_id
WHERE	o.name LIKE 'FactWalletTransactions%'