USE [datawarehouse]
GO

create	function [dw].[UdfGetGBPValueFromFinanceExchangeRates] (@transactionamount bigint,@currencycode varchar(6),@MonthInteger int,@YearInteger int)
returns decimal (20,2)
as
begin 
declare @GBPValue decimal (20,2)

select	@GBPValue = @transactionamount / Units_Per_GBP
from	dbfx.FactRates
where	CurrencySymbol = @CURRENCYCODE
and		MonthInteger = @MonthInteger
and		YearInteger = @YearInteger

return	@GBPValue
end
GO

--------------------------------------------------------------------------------------------------------------------------
create	function [dw].[UdfGetGBPValueFromFinanceExchangeRatesNoDate] (@transactionamount bigint,@currencycode varchar(6))
returns decimal (20,2)
as
begin 
declare @GBPValue decimal (20,2)

select	@GBPValue = @transactionamount / Units_Per_GBP
from	dw.LatestAvailableFXRates
where	CurrencySymbol = @CURRENCYCODE
--and		MonthInteger = @MonthInteger
--and		YearInteger = @YearInteger

return	@GBPValue
end
--------------------------------------------------------------------------------------------------------------------------
select		* 
from	FireBird.FactCurrencyBankTrade (nolock)
where	SellCcyCode <> 'gbp' 

select  * 
from	FireBird.FactCurrencyBankTrade (nolock)
where	BuyCcyCode <> 'gbp'

select  * 
from	FireBird.FactCurrencyBankTrade (nolock)
where	SellCcyCode = 'gbp' 

select  * 
from	FireBird.FactCurrencyBankTrade (nolock)
where	buyCcyCode = 'gbp' 
