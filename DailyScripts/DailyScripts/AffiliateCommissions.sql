-->here is the statement to bring over the data from dbfx talksheet 

with cte1 as 
(select	aa.[Affiliate Name], 
		br.branch_address1, 
		br.branch_address2, 
		ci.CityName, 
		cr.Country,
		aa.Affiliateaccount_Id,
		aa.[Client Name],
		--bb.ContactXref ,
		aa._contact_id, 
		cce.email_address
from	[dbo].[vwCfxAffiliate_FN] aa
join	account bb
on		aa.Affiliateaccount_Id = bb.Account_id
join	contact_email cce 
on		aa._contact_id = cce.contact_id
left	outer join	branch br 
on		bb.Account_id = br.account_id
left	outer join	CityName ci
on		br.branch_city = ci.CityName_ID
left	outer join	Country cr
on		br.branch_country = cr.Country_ID)
select	distinct [Affiliate Name], 
		branch_address1, 
		branch_address2, 
		CityName, 
		Country,
		Affiliateaccount_Id,
		[Client Name],
		_contact_id, 
		email_address
from	cte1 

select * from [dbo].[vwCfxAffiliate] where [Affiliate Name] like '%Hyndman%' order by [Deal Date] ASC

select * from [dbo].[vwCfxAffiliate_FN] where [Affiliate Name] like '%Hyndman%' order by [Deal Date] ASC

-->Then on the firebird prod databaase run this:

with	cte1 as (
SELECT	bb.Id,bb.Email,BB.USERNAME,aa.* 
from	DBFX_Affiliates aa
join	AspNetUsers bb
on		aa.email_address = bb.UserName)
SELECT	tr.Id as TradeId,
		
		TR.UserId,
		TR.TradeDate,
		TR.TradeProfit,
		tr.TradeReason, 
		tr.SellCcyCode, 
		tr.BuyCcyCode,
		tr.SellAmount, 
		tr.BuyAmount,
		0.1*TR.TradeProfit AS AffiliateCommission,
		CTE1.[AFFILIATE NAME],
		CTE1.BRANCH_ADDRESS1 as Address1,
		CTE1.BRANCH_ADDRESS2 as Address2,
		CITYNAME,
		[CLIENT NAME]
FROM	CTE1 JOIN CurrencyBank.Trade TR 
ON		CTE1.USERNAME = TR.UserId




/*
select	top 10 * from contact where contact_first_name = 'lee' and contact_first_name = 'hyndman'
where	[affiliate name] like '%hyndman%'
where	[Affiliate Name] like '%Hyndman%' 
*/