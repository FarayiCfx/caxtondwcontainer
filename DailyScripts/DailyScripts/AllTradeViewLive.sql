select		top 20 * from		[CurrencyBank].[Trade]

select		top 20 *  from		[CurrencyBank].[TradeDetail] order		by 1 desc 

exec sp_spaceused '[CurrencyBank].[Trade]'

exec sp_spaceused '[CurrencyBank].[TradeDetail]'

select	top 1000 *
from	CurrencyBank.Trade aa (nolock)
join	CurrencyBank.TradeDetail bb (nolock)
on		aa.Id = bb.TradeId
where	aa.Id = 1043849
order	by aa.id desc 


select		count(tradeid) as count_ , tradeid
from		[CurrencyBank].[TradeDetail] (nolock)
group by	tradeid
HAVING		count(tradeid)>1
order by	count_ desc

drop view	FactCurrencyTradeCrossApplyToDetail
go
CREATE		VIEW FactCurrencyTradeCrossApplyToDetail
as
with cteResultSet as (
select	aa.id as TradeId,
		aa.buyccycode,
		aa.BuyAmount, 
		AA.SellCcyCode,
		aa.SellAmount,
		aa.Settled, 
		cast(aa.TradeDate as Date) as TradeDate, 
		cast(aa.TradeEndDate as Date) as [SettlementDate],
		aa.UserId ,
		BB.tradeid_summarytable,
		isnull(bb.AmountPaid,0) as AmountPaid

from	[CurrencyBank].[Trade] AS aa
OUTER	APPLY	(select tradeid as tradeid_summarytable , 
						sum(amount) as AmountPaid
				from	[CurrencyBank].[TradeDetail] bb
				where	bb.tradeid = AA.id
				GROUP	BY BB.tradeid) BB)
				select	* ,
						'GREEN' as [Booked_Text], 
						3 as Booked,

						PartFunded_Text =	case	when AmountPaid = SellAmount  then 'GREEN' 
													when AmountPaid < SellAmount and settlementdate > getdate() then 'AMBER'
													when AmountPaid < SellAmount and settlementdate < getdate() then 'RED' -->settlementdate has passed so its red
											end,

						PartFunded =		case	when AmountPaid = SellAmount  then 3
													when AmountPaid < SellAmount and settlementdate > getdate() then 2
													when AmountPaid < SellAmount and settlementdate < getdate() then 1 -->settlementdate has passed so its red
											end,

						Funded_Text =	case	when AmountPaid = SellAmount then 'GREEN : AmountPaid = SellAmount' 
												when amountpaid < sellamount and settlementdate > getdate() then 'AMBER : AmountPaid less than sell amount and SettlementDate in future' -->settlementdate has not passed but trade is at least partially funded  
												WHEN AmountPaid < sellamount and settlementdate < getdate() then 'RED : AmountPaid less than sell amount and SettlementDate has passed' END,

						Funded =		case	when	AmountPaid = SellAmount then 3
												when amountpaid < sellamount and settlementdate > getdate() then 2 --settlementdate has not passed but trade is at least partially funded  
												WHEN AmountPaid < sellamount and settlementdate < getdate() then 1 END,

						IsSettledText = case	when	settled = 1 then 'GREEN' 
												when	settled = 0 and settlementdate < getdate() then 'RED'
												when	settled = 0 and settlementdate > getdate() then 'AMBER' --settlementdate is in the future from here 
										END,

						IsSettled = case	when	settled = 1 then 3 
											when	settled = 0 and settlementdate < getdate() then 1
											when	settled = 0 and settlementdate > getdate() then 2 --settlementdate is in the future from here 
											END
				from cteResultset 
				where tradedate > '2015-12-31'

set statistics time on 
set statistics io on 


select *	from FactCurrencyTradeCrossApplyToDetail 
where		funded <> 1
order		by tradeid DESC

select count(*)	from FactCurrencyTradeCrossApplyToDetail 

select *	from FactCurrencyTradeCrossApplyToDetail where tradeid = 1070772

select top 10 * from currencybank.trade

select top 10 * from currencybank.TradeDetail
