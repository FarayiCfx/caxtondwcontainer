select	count(*) from CurrencyBank.trade 
where	TradeDate BETWEEN '2017-02-20' and '2017-02-21'

drop table #temp1
go

select	aa.id as TradeId,
		aa.UserId, 
		aa.SellCcyCode, 
		aa.BuyCcyCode, 
		aa.BuyCcyClientTradingBookId as TradeTableBuyCcyClientTradingBookId,
		aa.BuyAmount,
		aa.TradeDate,
		aa.TradeReason,
		aa.BuyCcyClientTradingBookId,
		bb.id as ClientTradeBookId ,
		bb.name,
		bb.ccycode,
		bb.BookOpenDate as BookOpenDateInClientBook,
		mk.id as MarketTradeId,
		mk.bookname,
		mk.counterpartyrefno,
		mk.sellccy as MarketTradeSellCCY,
		mk.sellccyamount as MarketTradeSellCCYAmount,
		mk.buyccy as MarketTradeBuyCCY,
		mk.buyccyamount  as MarketTradeBuyCCYAmount,
		mk.bookopendate BookOpenDateInMarketTrade
		into #temp1
from	currencybank.Trade aa (nolock)
join	currencybank.ClientTradingBook bb (nolock)
on		aa.BuyCcyClientTradingBookId = bb.Id
join	CurrencyBank.MarketTrade mk (nolock)
on		bb.Name = mk.BookName
and		convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
and		bb.CcyCode = mk.BuyCcy
where	TradeDate BETWEEN '2017-02-20' and '2017-02-21'

select * from #temp1

--1701 with left outer join 

select distinct name from #temp1

select * from #temp1 where bookname = 'FB_21/02/2017_Normal'

select top 5 * from currencybank.Trade

select top 5 * from currencybank.MarketTrade order by 1 DESC

select  * from currencybank.MarketTrade
where bookname like 'FB_21/02/2017_Normal%'
 
select  * from currencybank.ClientTradingBook 
where name like'FB_21/02/2017_Normal%'