/*
This is an adhoc on the fly ticket, this stuff is not for re -use
if you need this often then rewrite for performance using temp tables
and index those. 

I had to clear the tables first because some trades got reversed in the intervening 
period , this might have to be done again on the next months statement

truncate table firebird.factcurrencybanktrade
go
truncate table [firebird].factcurrencybankaccounttransaction
go
*/
exec sp_spaceused 'firebird.factcurrencybanktrade'
exec sp_spaceused '[firebird].factcurrencybankaccounttransaction'

truncate table tech_26_currencybank
truncate table tech_26_trade

------------------------------------------------------------------------------->
IF		OBJECT_ID('tech_26_currencybank') IS NOT NULL
DROP	TABLE tech_26_currencybank
go
CREATE TABLE [dbo].[tech_26_currencybank](
	[Id] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Status] [nvarchar](512) NULL,
	[ExternalTransactionId] [varchar](255) NULL,
	[ExternalTransRef] [nvarchar](256) NULL,
	[datekey] [bigint] NULL
)
GO
------------------------------------------------------------------------>
IF		OBJECT_ID('tech_t6_trade') IS NOT NULL
DROP	TABLE tech_t6_trade
go
select	*		into tech_t6_trade
from			firebird.factcurrencybanktrade
				where 	isunwound = 0
				and		isreversed = 0
				and		reversal = 0 
				---and		tradedatekey between 20170601 and 20170731
				and		tradereason <> 'card load'
				go
-----------------------------------------------------------------------------------------------------------------------------
insert		into tech_26_currencybank

select		Id,Amount,Status,cast (ExternalTransactionId as varchar(255)) as ExternalTransactionId,ExternalTransRef ,datekey

from		[firebird].factcurrencybankaccounttransaction (nolock)
where		externaltransref = 'trade'
and			amount > 0
--and			datekey between 20170601 and 20170731
and			([status] like 'credit for buy%' or [status] like 'credit for trade%')
go

create       nonclustered index ix_credits_table on dbo.tech_26_currencybank (externaltransactionid)include(amount)
go

--select * from tech_26_currencybank where externaltransactionid = 1877346
------------------------------------------------------------------------------------------------------------------------------------------------------

alter		view [dbo].[clientdebt]
as

with		cteResultSet as (
select	
			aa.id as TradeId,
			aa.buyccycode,
			aa.BuyAmount, 
			AA.SellCcyCode,
			aa.SellAmount,
			--aa.Settled, 
			cast(aa.TradeDate as Date) as TradeDate, 
			cast(aa.TradeEndDate as Date) as [SettlementDate],
			--isnull(aa.HelpdeskUserId,'') as HelpdeskUserId,
			aa.UserId ,
			BB.tradeid_summarytable,
			isnull(bb.AmountPaid,0) as AmountPaidIn, 
			isnull(aa.TradeReason,'') as TradeReason,
			aa.TradeProfit

from	tech_t6_trade AS aa (nolock)

OUTER	APPLY	(select tradeid as tradeid_summarytable , 
						sum(amount) as AmountPaid
				from	firebird.[factCurrencyBankTradeDetail] bb (nolock)
				where	bb.tradeid = AA.id
						and bb.paiddatekey between 20170601 and 20170731
				GROUP	BY BB.tradeid) 
				BB
				where 	aa.isunwound = 0
				and		aa.isreversed = 0
				and		aa.reversal = 0 
				and		aa.tradedatekey between 20170601 and 20170731
				and		aa.tradereason <> 'card load'
				), 

				cteCredits as (

				select sum(amount) as AmountPaidOut,
						externaltransactionid
				from	tech_26_currencybank (nolock)
				where	externaltransref = 'trade'
				and		amount > 0
				and		datekey between 20170601 and 20170731
				and		([status] like 'credit for buy%' or [status] like 'credit for trade%')
				group	by externaltransactionid
						
						
				)
				select	TradeId ,
						TradeDate,
						[SettlementDate],
						TradeProfit,
						buyccycode,
						BuyAmount,
						bb.AmountPaidOut,
						SellCcyCode,
						SellAmount,
						AmountPaidIn,
						UserId,
						TradeReason,
						bb.externaltransactionid,
						isnull(buyamount,0)-isnull(amountpaidout,0) as payable,
						isnull(sellamount,0)-isnull(amountpaidin,0) as receivable
				from	cteResultset
				left	outer join	cteCredits bb 
				on		cteresultset.tradeid = bb.externaltransactionid

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

select	top 20 * from[dbo].[ClientDebt]

select	count(tradeid) from	[dbo].[ClientDebt]

exec	sp_help 'firebird.factcurrencybanktrade'

select	getdate()



select  * from firebird.[factCurrencyBankTrade] where id = 1798940
select  * from firebird.[factCurrencyBankTradeDetail] where tradeid = 1798940

declare @tradeid int 
select	@tradeid = 1798940

select  * from firebird.[factCurrencyBankTrade] where id = @tradeid
select  * from firebird.[factCurrencyBankTradeDetail] where tradeid = @tradeid

exec	GetDebtorsAndCreditors2  @startdate ='2017-05-31' , @enddate = '2017-05-31'

--testing EOMONTH
select EOMONTH (getdate(),0)
select EOMONTH (getdate(),1)

select cast(convert(varchar(12),EOMONTH (getdate(),0),112) as int)
select cast(convert(varchar(12),EOMONTH (getdate(),1),112) as int)

select	top 10 *

		/*cast(transactiondatetime as date) as DatePaidOut
		,Id
		,Amount
		,Status
		,cast (ExternalTransactionId as varchar(255)) as ExternalTransactionId
		,ExternalTransRef 
		,datekey*/

from		[firebird].factcurrencybankaccounttransaction (nolock)
where		externaltransref = 'trade'



alter view dw.MoneyPaidOutPerTrade 
as 
select	cast(transactiondatetime as date) as DatePaidOut
		,Id
		,Amount as AmountPaidOut
		,Status
		,cast (ExternalTransactionId as varchar(255)) as ExternalTransactionId
		,ExternalTransRef 
		,datekey

from		[firebird].factcurrencybankaccounttransaction (nolock)
where		externaltransref = 'trade'
and			amount > 0
--and			datekey > @startdateInteger  
and			([status] like 'credit for buy%' or [status] like 'credit for trade%')



select top 10 * from firebird.factcurrencybanktrade

select top 10 * from staging.firebirdfactcurrencybanktrade

alter	table firebird.factcurrencybanktrade
add 		DWDateUpdated datetime null

select top 10 *
from firebird.[factCurrencyBankTradeDetail]
where tradeid = 342366



select top 10 * from dbfx.deallist
where dealno = 34079