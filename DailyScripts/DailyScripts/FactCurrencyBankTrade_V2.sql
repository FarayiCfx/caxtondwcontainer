drop table if exists [FireBird].[FactCurrencyBankTrade_V2]
go
CREATE TABLE [FireBird].[FactCurrencyBankTrade_V2](
	[CustomerKey] [bigint] NULL,
	[TradeDateKey] [int] NULL,
	[Id] [bigint] NOT NULL,
	[UserId] [nvarchar](256) NULL,
	[SellCcyCode] [nvarchar](100) NULL,
	[BuyCcyCode] [nvarchar](100) NULL,
	[SellAmount] [decimal](18, 2) NOT NULL,
	[BuyAmount] [decimal](18, 2) NOT NULL,
	[Settled] [bit] NOT NULL,
	[BuyCcyClientTradingBookId] [bigint] NOT NULL,
	[SellCcyClientTradingBookId] [bigint] NOT NULL,
	[WorkflowInstanceId] [uniqueidentifier] NULL,
	[TradeDate] [datetime] NULL,
	[IsAutomatic] [bit] NOT NULL,
	[TradeEndDate] [datetime] NOT NULL,
	[TradeProfit] [decimal](18, 2) NOT NULL,
	[TradeProfitGBP] [decimal](18,2)not null,
	[TreasuryProfit] [decimal](18, 2) NOT NULL,
	[Reversal] [bit] NOT NULL,
	[IsReversed] [bit] NOT NULL,
	[IsUnWound] [bit] NOT NULL,
	[TradeReason] [nvarchar](50) NULL,
	[HelpdeskUserId] [nvarchar](256) NULL,
	[TransferId] [bigint] NOT NULL,
	[TradeNotes] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
	[DWDateInserted] [datetime] NULL,
	[DWDateUpdated] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.FactCurrencyBankTradeV2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT (getdate()) FOR [TradeDate]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [IsAutomatic]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [TradeEndDate]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [TradeProfit]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [TreasuryProfit]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [Reversal]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [IsReversed]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [IsUnWound]
GO

ALTER TABLE [FireBird].[FactCurrencyBankTrade_V2] ADD  DEFAULT ((0)) FOR [TransferId]
GO

CREATE NONCLUSTERED INDEX [ix_tradereason_includes] ON [FireBird].[FactCurrencyBankTrade_V2]
(
	[TradeReason] ASC
)
INCLUDE ( 	[CustomerKey],
	[UserId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [ix_trdkey_sellamount] ON [FireBird].[FactCurrencyBankTrade_V2]
(
	[TradeDateKey] ASC,
	[SellAmount] ASC
)
INCLUDE ( 	[CustomerKey],
	[UserId],
	[SellCcyCode],
	[BuyCcyCode],
	[BuyAmount],
	[Settled],
	[BuyCcyClientTradingBookId],
	[SellCcyClientTradingBookId],
	[WorkflowInstanceId],
	[TradeDate],
	[IsAutomatic],
	[TradeEndDate],
	[TradeProfit],
	[TradeProfitGBP],
	[TreasuryProfit],
	[Reversal],
	[IsReversed],
	[IsUnWound],
	[TradeReason],
	[HelpDeskUserId],
	[TransferId],
	[DWDateInserted]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [nci_wi_FactCurrencyBankTrade_C0AC507A76959A3EDCE401C7447D342C] ON [FireBird].[FactCurrencyBankTrade_V2]
(
	[CustomerKey] ASC
)
INCLUDE ( 	[BuyAmount],
	[BuyCcyCode],
	[SellAmount],
	[SellCcyCode],
	[TradeDate],
	[TradeEndDate],
	[TradeReason]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

