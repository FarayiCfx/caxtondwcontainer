drop procedure [dbo].[GetFundsOnAccount2] 
go

CREATE		procedure [dbo].[GetFundsOnAccount2] 
@username	nvarchar(1024),
@fastpay	varchar (50)
as

/*
Using the same filters that populate that tables 
simply go and get the sum totals to work out currency on 
account and present the data grouped by currency 
*/
SET NOCOUNT ON 

IF OBJECT_ID('tempdb..#FundsIn') IS NOT NULL
DROP TABLE #FundsIn

CREATE TABLE #FundsIn(
	[RowNumber] [bigint] NULL,
	[TransactionDateTime] [datetime] NULL,
	[TransactionDate] [varchar](30) NULL,
	[Amount] [decimal](20, 4) NOT NULL,
	[Cleared] [int] NOT NULL,
	[Status] [nvarchar](512) NULL,
	[ExternalTransactionId] [int] NULL,
	[ExternalTransRef] [nvarchar](256) NULL,
	[Currency] [nvarchar](100) NULL,
	[Narrative1] [varchar](255) NOT NULL
)
--------------------------------------------------------------------------------------------------------------------
insert	into #FundsIn
exec	[dbo].[GetAccountSummaryCredits4]  @username ,@fastpay
--------------------------------------------------------------------------------------------------------------------
declare @customerkey bigint 

if		ISNULL(@fastpay,'') = ('')
		
		select	@customerkey = customerkey
		from	dw.dimcustomerreference 
		where	username = @username 

else	
		select		@customerkey = customerkey 
		from		dw.dimcustomerreference 
		where		fastpaycontactref = @fastpay;


--------------------------------------------------------------------------------------------------------------------

with	cteSumOfFundsIn as (
select	sum (Amount) as [AmountIn],
		currency as CurrencyIn
		from	#FundsIn
		group by currency ),


cteTradesAmtSold as (
select	aa.sellccycode as [CurrencySold],
		sum(aa.SellAmount) as SumSellAmount
from	firebird.factcurrencybanktrade aa
join	dw.dimcustomerreference dw
on		aa.customerkey = dw.customerkey 
where	aa.customerkey = @customerkey  
group by aa.sellccycode),

cteTradesAmtBought as (
select	aa.buyccycode as [CurrencyBought],
		sum(aa.BuyAmount) as BuyAmountSum
from	firebird.factcurrencybanktrade aa
join	dw.dimcustomerreference dw
on		aa.customerkey = dw.customerkey 
where	aa.customerkey = @customerkey  
group	by aa.buyccycode), 

cteFundsOut as (

select	sum (trf.BuyAmount) as [AmountOut],
		trf.buyccycode as CurrencyOut
from	[FireBird].[FactPaymentTransfer] trf
left	outer join	[FireBird].[DimPaymentBeneficiary] ben
on		trf.beneficiaryid = ben.id
where	trf.customerkey = @customerkey  
group	by  trf.buyccycode)


select	aa.*,
		bb.*,
		cc.*,
		dd.*,
		isnull((aa.AmountIn - bb.SumSellAmount) - (cc.[BuyAmountSum]  - dd.[AmountOut]),0) as FundsOnAccountByCurrency
from	cteSumOfFundsIn aa 
left	outer join	cteTradesAmtSold bb
on		aa.currencyin = bb.[currencysold]
left	outer join	cteTradesAmtBought cc
on		aa.currencyin = cc.[CurrencyBought]
left	outer join cteFundsOut dd
on		aa.currencyin = dd.[CurrencyOut]

--select * from #fundsin

--select @customerkey 