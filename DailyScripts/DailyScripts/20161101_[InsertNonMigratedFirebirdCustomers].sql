USE [datawarehouse]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [staging].[InsertNonMigratedFirebirdCustomers] as 
/*
 Farayi Nzenza

 The ASPNETusers table is the one that collects 
 all of the data for verified and checked users that 
 now have an actual login assigned to them 

 This procedure will insert Customers From firebird that 
 have not been migrated.

 There are several types in here: 

 1 - Customers that have self migrated but somehow there are two records 
 in the aspnetusers table for the same customer,  

 2 - People that have registered normally through the site 

At the time of writing this SP the ASPNETUSERS table does not have 
an updated date column or any other way of indicating that a record has 
been changed in the source system and should thus be changed in the datawarehouse
*/
		
INSERT		INTO dw.dimcustomerreferenc (FBId,
			FBCustomerRefNo,
			FBFastpayContactRef,
			FBCardsContactRef,
			FBValitorAccountId,
			FBIsActive,
			FBUserName,
			IsMigratedCustomer,
			DBCARDSContact_id,
			DBCARDSAccount_Id,
			DBCARDSParent_id,
			DBFXContact_id,
			DBFXAccount_Id,
			DBFXParent_id,
			OriginalSystemSourceId,
			Title,
			FirstName,
			LastName,
			Gender,
			DateOfBirth,
			AddressLine1,
			AddressLine2,
			City,
			County,
			Postcode,
			Country,
			ContactEmailAddress,
			PrimaryTelephone,
			SecondaryTelephone,
			TertiaryTelephone,
			--MigrationDetectByEtl,
			--MigrationDetectByEtlDate,
			SourceInsertDate,
			SourceUpdateDate,
			DWDateInserted,
			DWDateUpdated
)

	--Output	$action, inserted.* , getdate() as AuditDate
	--into	dw.DimCustomerReferenceAudit

select 		
			Source.FBId,
			Source.FBCustomerRefNo,
			Source.FBFastpayContactRef,
			Source.FBCardsContactRef,
			Source.FBValitorAccountId,
			Source.FBIsActive,
			Source.FBUserName,
			Source.IsMigratedCustomer,
			Source.DBCARDSContact_id,
			Source.DBCARDSAccount_Id,
			Source.DBCARDSParent_id,
			Source.DBFXContact_id,
			Source.DBFXAccount_Id,
			Source.DBFXParent_id,
			Source.OriginalSystemSourceId,
			Source.Title,
			Source.FirstName,
			Source.LastName,
			Source.Gender,
			Source.DateOfBirth,
			Source.AddressLine1,
			Source.AddressLine2,
			Source.City,
			Source.County,
			Source.Postcode,
			Source.Country,
			Source.ContactEmailAddress,
			Source.PrimaryTelephone,
			Source.SecondaryTelephone,
			Source.TertiaryTelephone,
			--Source.MigrationDetectByEtl,
			--Source.MigrationDetectByEtlDate,
			Source.SourceInsertDate,
			Source.SourceUpdateDate,
			Source.DWDateInserted,
			Source.DWDateUpdated

			FROM STAGING.DIMCUSTOMERREFERENCE SOURCE (NOLOCK)
			where ismigratedcustomer = 0		
;
GO


