with	cte1 as (
select	bb.id
		,bb. transactiondatetime
		,bb.amount
		,bb.balance
		,bb.[status]
		,bb.externaltransactionid
		,bb.externaltransref
from	[FireBird].[FactCurrencyBankAccountTransaction] bb (nolock)
where	externaltransref = 'rbs'
and		transactiondatetime > '2017-08-31'
)

select	bb.*,aa.*
from	settlement.rbsstatement aa
join	cte1 bb
on		aa.accounttransactionid = bb.id 
where	aa.accounttransactionid is not null
and		aa.transactiondate between '2017-09-01' and '2017-09-22'






select top 10 * from [FireBird].[FactCurrencyBankAccountTransaction]
