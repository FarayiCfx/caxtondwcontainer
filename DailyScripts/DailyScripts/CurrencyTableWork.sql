
select * from dw.dimisocurrencyname order by 1

exec sp_help 'dw.dimxerates'

select	count(isocode)  as count_,
		isocode
from	dw.dimisocurrencyname
group	by isocode 

create			table dw.ExchangeRatesAccounts  (
RowId			int identity(1,1),
CurrencyName	NVARCHAR(510) null,
NameCode		NVARCHAR(510) null,
IsoCode			NVARCHAR(510) null,
UnitsPerGBP		DECIMAL (20,10) null,
GBPPerUnit		DECIMAL (20,10) null,
[MonthInteger]	int null,
[YearInteger]	int null, 
DWDateInserted	datetime null)


SELECT *       FROM dw.ExchangeRatesAccounts where isocode = 144

------TRUNCATE TABLE dw.ExchangeRatesAccounts

select top 10 * from valitor.factwallettransactions

IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1

SELECT	WalletTransaction_Key
		,transactiondatetime
		,transactioncurrency
		,transactionamount
		,transactionamountgbp
		,datepart(month,Transactiondatetime) as mnth_
		,datepart(year,Transactiondatetime) as yr_
		,[dw].[UdfGetGBPValueAccounts] (aa.transactionamount,transactioncurrency,datepart(month,Transactiondatetime),datepart(year,Transactiondatetime)) as output_
into	#temp1
from	valitor.factwallettransactions AA (nolock)
where	datepart (month,transactiondatetime ) in (1,2,3,4,8,11,12)
and		datepart (year,transactiondatetime ) = 2017
and		transactionamount is not null

select * from #temp1 where output_ is not null




-------------------------------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1

SELECT	WalletTransaction_Key
		,transactiondatetime
		,billingcurrency
		,billingamount
		,billingamountgbp
		,datepart(month,Transactiondatetime) as mnth_
		,datepart(year,Transactiondatetime) as yr_
		,[dw].[UdfGetGBPValueAccounts] (aa.billingamount,transactioncurrency,datepart(month,Transactiondatetime),datepart(year,Transactiondatetime)) as output_
into	#temp1
from	valitor.factwallettransactions AA (nolock)
where	datepart (month,transactiondatetime ) in (1,2,3,4,8,11,12)
and		datepart (year,transactiondatetime ) = 2017
and		billingamount is not null



select top 10 * from valitor.factwallettransactions

