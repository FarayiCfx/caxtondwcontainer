IF		OBJECT_ID('TEMPDB..#CorpFastPay') IS NOT NULL
DROP	TABLE #CorpFastPay
go
create	table #CorpFastPay(
idrow	int identity (1,1),
email_	varchar (255),
entity  varchar	(10) default 'Corporate')

-----------------------------------------------------------------------
insert into #corpfastpay(email_) values('beth@bethwightman.com')
insert into #corpfastpay(email_) values('laurence@whitebrandagency.com')
insert into #corpfastpay(email_) values('elizabeth@flooredgenius.com')
insert into #corpfastpay(email_) values('Mark.Brewer@bioss.ac.uk')
insert into #corpfastpay(email_) values('carsandparts@middlebartongarage.com')
insert into #corpfastpay(email_) values('joachim.thuex@yahoo.co.uk')
insert into #corpfastpay(email_) values('andrew@triptease.com')
insert into #corpfastpay(email_) values('robin@rmscm.co.uk')
insert into #corpfastpay(email_) values('support@cookiebite.net')
insert into #corpfastpay(email_) values('Bayo@plenishcleanse.com')
insert into #corpfastpay(email_) values('tim@firefly-collection.com')
insert into #corpfastpay(email_) values('accounts@fluidity-f2.com')
insert into #corpfastpay(email_) values('rob.cooksey@pearlanddean.com')
insert into #corpfastpay(email_) values('rebecca@boho-betty.com')
insert into #corpfastpay(email_) values('ab@thefishsociety.co.uk')
insert into #corpfastpay(email_) values('vintage@susancaplan.co.uk')
insert into #corpfastpay(email_) values('steve@admerchandise.co.uk')
insert into #corpfastpay(email_) values('info@fimark.com')
insert into #corpfastpay(email_) values('carlos@structuredrevolution.com')
insert into #corpfastpay(email_) values('info@merxconcierge.co.uk')
insert into #corpfastpay(email_) values('paul@jjvaillant.co.uk')
insert into #corpfastpay(email_) values('accounts@sterlingip.co.uk')
insert into #corpfastpay(email_) values('steve@jumbocruiser.com')
insert into #corpfastpay(email_) values('lalage@lalagebeaumont.com')
insert into #corpfastpay(email_) values('anna@plumboutique.co.uk')
insert into #corpfastpay(email_) values('accounts@autocraftequipment.co.uk')
insert into #corpfastpay(email_) values('billing@leadinglocally.com')
insert into #corpfastpay(email_) values('shohreh@tmgcl.com')
insert into #corpfastpay(email_) values('ahamer@amhresearch.com')
insert into #corpfastpay(email_) values('sales@bsgtractorsandmachinery.co.uk')
insert into #corpfastpay(email_) values('gilles.wafflart@btinternet.com')
insert into #corpfastpay(email_) values('Bursar@greenes.org.uk')
insert into #corpfastpay(email_) values('office@getcycling.org.uk')
insert into #corpfastpay(email_) values('rathbone@me.com')
insert into #corpfastpay(email_) values('finance@situ.co.uk')
insert into #corpfastpay(email_) values('abell@chartered.org')
insert into #corpfastpay(email_) values('carole.harvey@pandect.co.uk')
insert into #corpfastpay(email_) values('tansy@fmtv.london')
insert into #corpfastpay(email_) values('trisha.miller@sunamp.co.uk')
insert into #corpfastpay(email_) values('pingrulo@phmr.com')

-----
select * from #CorpFastPay





select	top 100 * from currencybank.trade  order by id desc

select	distinct(tradereason) from currencybank.trade 

select	 * from currencybank.clienttradingbook

exec	sp_help 'currencybank.trade'

exec	sys.sp_spaceused 'currencybank.trade'

exec	sp_help 'currencybank.clienttradingbook'

exec	sys.sp_spaceused 'currencybank.clienttradingbook'

select		count(id) as count_, 
			sum(tradeprofit) as sumtradeprofit,
			cast (TradeDate as date ) as Date_,
			isnull(entity,'Retail') as entity,
			aa.TradeReason
from		currencybank.trade aa
left		outer join #corpfastpay cp
on			aa.userid = cp.email_
where		TradeDate	between '2016-09-12 17:30:00.000' 
						and		'2016-09-13 17:30:00.000'
						and		aa.TradeReason <> 'card load'
group	by	cast(TradeDate as date), 
			TradeReason ,
			isnull(entity,'Retail') 
order	by	cast(TradeDate as date) 



select * from IpMigrationCustomers
wher fastpaycontactref


exec sp_who2 active
