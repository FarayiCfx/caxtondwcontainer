/*
https://caxtonfx.atlassian.net/browse/FB-1909

This ticket is an ad hoc ticket to extract data for marketing 
Known issues:

Do not under any circumstances join to the scy_registration table for anything!
I have spoken to Anita and the strategy that should be used is to 
Go from Account --> contact table , do not join contact --> scy_registration 
that is wrong wrong! Please just dont. A good example is look at the Amanda Jones
contact in dbcards and join to scy_registration on contact_id - horror show.

The reason column has new line characters embedded so strip those out
it's likely other columns have the same problem
*/

WITH CTE1 as(
select	aa.Account_id,
		CC.contact_id,
		Account_short_name as Cax_account_short_name,
		--cc.contact_salutation,
		cc.contact_first_name as FirstName,
		cc.contact_last_name as LastName,	
		datediff(yy,isnull(dob,''),getdate()) as Age,
		cast(isnull(dob,'')as datetime) as Dob,
		
		Gender = case	when contact_salutation like 'Ms%' then 'Female'
						when contact_salutation like 'Mrs%' then 'Female'
						when contact_salutation like 'Miss%' then 'Female' 
						when contact_salutation like 'Mr%' then 'Male'
						else 'not supplied'
						end ,

		isnull(br.branch_address1,'') as AddressLine1, 
		isnull(br.branch_address2,'') as AddressLine2,
		isnull(ct.cityname,'') as Cityname, 
		isnull(st.[State],'') as [State],
		isnull(cn.Country,'') as [Country],
		isnull(br.branch_zip,'') as PostCode,
		replace(replace(isnull(contact_note_1,''),char(10),''),char(13),'') as Reason,
		cast(replace(replace(isnull(aa.InsertDate,''),char(10),''),char(13),'')  as datetime)as RegistrationDate

from	contact cc (nolock)
join	account aa (nolock)
on		cc.account_id = aa.account_id
join	branch br (nolock)
on		aa.Account_main_branch = br.branch_id 
left	outer join	cityname ct (nolock)
on		br.branch_city = ct.CityName_ID
left	outer join	[state] st  (nolock)
on		br.branch_state = st.State_ID
left	outer join		country cn (nolock)
on		br.branch_country = cn.Country_ID
where	aa.account_id <> 1 
)
SELECT	distinct
		cax_account_short_name,
		firstname,
		lastname,
		age,
		dob,
		gender,
		addressline1,
		addressline2,
		cityname,
		[state],
		country,
		postcode,
		reason,
		registrationdate
FROM	CTE1 
--WHERE contact_last_name = 'Davis' --good example of new line in reason column 
--and contact_first_name = 'alan'
order	by registrationdate


