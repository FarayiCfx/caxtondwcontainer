/*
DO THIS ON DBCARDS database 
CAXTON-DB1 
*/
exec sp_help tblcfxcurrencycarddetails
go

--start by having a look at the files 
select	top 10 * from tblcfxcurrencycarddetails

select distinct status from tblcfxcurrencycarddetails

select * from [CAXprogbalexp] where pan <> 'all'
-->24599 rows 
-->24176 on the second round
-->19/7/2017 --> 22731 rows 

--join the imprted rows rows to the card details 
select	* 
from	tblcfxcurrencycarddetails aa 
join	[CAXprogbalexp] bb
on		aa.cardnumber = bb.[pan]

-->pickup email addresses
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
with	cteEmailRanks as (
select	contact_id,
		email_address,  
		rank() over(partition by contact_id order by contact_email_id ) as [Rank]
from	contact_email)

select	identity(int, 1,1) as id,
		contact_id,email_address,
		'Email '+ cast( [Rank] as varchar(5)) as EmailRank
into	#temp1
from	cteEmailRanks
order	by contact_id 

select * from #temp1

------------------------------------------------------------------------------------------------------------
--create a temp table for all known email addresses per customer 
IF		OBJECT_ID('tempdb..#temp2') IS NOT NULL
DROP	TABLE #temp2
go

with cte1 as (
select contact_id,email_address as email_address1,emailrank
from #temp1
where emailrank = 'EMAIL 1'),

cte2 as (select contact_id,email_address as email_Address2,emailrank
from #temp1
where emailrank = 'EMAIL 2'),

cte3 as (select contact_id,email_address as email_address3,emailrank
from #temp1
where emailrank = 'EMAIL 3')

select	cte1.contact_id,
		cte1.email_address1,
		isnull(cte2.email_address2,'') as email_address2,
		isnull(cte3.email_address3,'') as email_address3, 
		cc.contact_salutation,
		cc.contact_first_name,
		cc.contact_last_name
		into #temp2
from	cte1 
left	outer join	cte2
on		cte1.contact_id = cte2.contact_id
left	outer join cte3 
on		cte1.contact_id = cte3.contact_id
join dbo.contact cc 
on		cte1.contact_id = cc.contact_id

----final result output below 

with	cte1 as (
select	aa.contact_id,
		aa.currencycarddetailid,
		bb.* 
from	tblcfxcurrencycarddetails aa 
join	[CAXprogbalexp] bb
on		aa.cardnumber = bb.[pan]
)
select	aa.currencycarddetailid,
		aa.cardid as card_id_,aa.accno, aa.finamt,aa.blkamt,aa.currcode,
		aa.amtavl,aa.pan,aa.cardid,aa.expdate,aa.crdstatcode,StatusText = case aa.crdstatcode when '00' then 'Normal'
																		when '77' then 'Blocked' end,
		bb.*
from	cte1 aa
left	outer join	#temp2 bb
on		aa.contact_id = bb.contact_id
order by amtavl

