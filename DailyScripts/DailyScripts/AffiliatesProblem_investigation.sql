select top 10 * from dw.factapplicants

select count(id) as count,affiliatecode--,cast(registrationdate as date) regdate
from dw.factapplicants where registrationdate between '2016-04-01 00:00:00.000' and '2016-04-30 23:59:59.000'
group by affiliatecode---,cast(registrationdate as date)
--32 rows 


with	cte1 as 
(select email,registrationdate,affiliatecode 
from	dw.factapplicants 
where	registrationdate between '2016-04-01 00:00:00.000' and '2016-04-30 23:59:59.000')
select	count(email) as countemail,email,affiliatecode from cte1
group	by email,affiliatecode
order	by  1 desc
--6618 rows

with	cteDistinct as 
(select distinct email,affiliatecode 
from	dw.factapplicants 
where	registrationdate between '2016-04-01 00:00:00.000' and '2016-04-30 23:59:59.000')
select	count(email) as countemail,affiliatecode 
from	cteDistinct
group	by affiliatecode
order	by  1 desc

DROP TABLE #TEMP1;

with	cteDistinct as 
(select distinct email as email_firebird,affiliatecode as affiliatecode_firebird
from	dw.factapplicants 
where	registrationdate between '2016-04-01 00:00:00.000' and '2016-04-30 23:59:59.000')

select AA.*,BB.EMAIL AS EMAIL_FROM_AFFILIATES_FILES,BB.AFFILIATE AS AFFILIATE_FROM_FILES_SUPPLIED 
INTO #TEMP1
from cteDistinct aa 
left outer join dw.AffiliatesImportApril  bb
ON aa.email_firebird = bb.email
WHERE AA.AFFILIATECODE_FIREBIRD IS NOT NULL

SELECT * FROM #TEMP1
where email_firebird = '123sam765@gmail.com'

WHERE AFFILIATECODE_FIREBIRD NOT LIKE '%MONEY%SUPER%'
