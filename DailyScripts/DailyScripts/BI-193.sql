select * from dw.dimcustomerreference where customerkey = 534759

select * from dw.factaspnetuserclaims where customerkey = 534759

select * from dw.factaspnetuserclaims where userid = 'C00542650'

exec sp_help '[BI-193]'

alter view [BI-193] 
AS
  with cte1 as (
  select	customerkey,'True 'as Retail_Primary_CardClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Retail:PrimaryAccount:PortalCards'),
  cte2 as (
  select	customerkey,'True 'as Retail_Secondary_CardClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Retail:SecondaryAccount:PortalCards'),
  cte3 as (
  select	customerkey,'True 'as Retail_Primary_IPTransferClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Retail:PrimaryAccount:PortalTransfer'),
  cte4 as (
  select	customerkey,'True 'as Corporate_Primary_CardClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Corporate:PrimaryAccount:PortalCards'),
  cte5 as (
  select	customerkey,'True 'as Corporate_Primary_IPTransferClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'Corporate:PrimaryAccount:PortalTransfer'),
  cte6 as (
  select	customerkey,'True 'as Corporate_Migration_IPTransferClaim
  from		dw.factaspnetuserclaims  (nolock)
  where		claimtype = 'accounttype'
  and		claimvalueforaccounttype = 'TalksheetBiz:PrimaryAccount:PortalTransfer')

select	aa.customerkey
		,id as FirebirdId
		,username
		,title
		,firstname
		,lastname
		,gender
		,dateofbirth
		,postcode
		,aa.sourcecreationdate
		,[VALITOR_MASTERCARD_CardLoadCount]
		,[VALITOR_MASTERCARD_LoadTotalGBP]
		,[VALITOR_MASTERCARD_SpendTotalGBP]
		,[VALITOR_MASTERCARD_AverageSpendGBP]
		,[VALITOR_MASTERCARD_FirstLoadDate]
		,[VALITOR_MASTERCARD_LastLoadDate]
		--,[CardLoad_FirstDate]
		--,[CardLoad_LastDate]
		--,[CardLoad_TradeCount]
		,[BuyAndSend_TradeCount]
		,[BuyAndSend_Tradeprofit_GBP_Total]
		,[BuyAndSend_FirstDate]
		,[BuyAndSend_LastDate]
		,isnull(cte1.Retail_Primary_CardClaim,'False') as Retail_Primary_CardClaim
		,isnull(cte2.Retail_Secondary_CardClaim,'False') as Retail_Secondary_CardClaim
		,isnull(cte3.Retail_Primary_IPTransferClaim,'False') as Retail_Primary_IPTransferClaim
		,isnull(cte4.Corporate_Primary_CardClaim,'False') as Corporate_Primary_CardClaim
		,isnull(cte5.Corporate_Primary_IPTransferClaim,'False') as Corporate_Primary_IPTransferClaim
		,isnull(cte6.Corporate_Migration_IPTransferClaim,'False') as Corporate_Migration_IPTransferClaim
from	dw.dimcustomerreference aa (nolock)
left	outer join [dw].[CustomerRFMAggregations]  bb (nolock)
on		aa.customerkey = bb.customerkey 
left	outer join cte1 
on		aa.customerkey = cte1.customerkey 
left	outer join cte2 
on		aa.customerkey = cte2.customerkey 
left	outer join cte3
on		aa.customerkey = cte3.customerkey
left	outer join cte4 
on		aa.customerkey = cte4.customerkey 
left	outer join cte5 
on		aa.customerkey = cte5.customerkey 
left	outer join cte6
on		aa.customerkey = cte6.customerkey 
where	aa.id <> '0'
order	by customerkey asc


exec sp_who2 active



select * from [bi-193]  where customerkey = 158

select * from firebird.factcurrencybankaccounttransaction where customerkey = 158
order by transactiondatetime


select * from firebird.factcurrencybanktrade where customerkey = 158
