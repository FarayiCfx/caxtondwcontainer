--actual task starts here 
IF	OBJECT_ID('tempdb..#monetaryresults') IS NOT NULL
DROP TABLE #monetaryresults
go

with	cte1 as (
select	customerkey,
		[dw].[udfgetgbpvalue](buyamount,buyccycode) as gbpamount,
		id,
		cast(PayAwayDate as date) as PayAwayDate
from	firebird.factpaymenttransfer ),

cteMaxId as (
select	max(aa.id) as maxid,aa.customerkey
from	FireBird.FactPaymentTransfer aa
group	by aa.customerkey),

cteMaxValue as (select buyamount,customerkey from FireBird.FactPaymentTransfer where id in (select maxid from ctemaxid))

select	cte1.customerkey,
		sum(gbpamount) as buyamountTotalGBP,
		count(id) as counttransfers,
		max(payawaydate) as lastpayawaydate ,
		mx.buyamount as lastbuyamountGBP
into	#monetaryresults
from	cte1 join cteMaxValue mx
on		cte1.customerkey = mx.customerkey
group	by cte1.customerkey,mx.buyamount
order	by customerkey
go

----after you have worked out the monetary results then get basic customer details 

select	dw.id ,dw.FirstName, dw.LastName, 
		isnull(dw.ContactEmailAddress,dw.UserName) as EmailAddress,
		dw.SourceCreationDate,dw.IsActive,mon.*, dw.WantMultiCurrencyCard, dw.WantToSendMoney, 'international_payment_recognised' AS RecordSet
from	DW.DimCustomerReference dw
JOIN	#monetaryresults mon
on		dw.CustomerKey = mon.customerkey
where	dw.customerkey > 11 and dw.IsActive = 1
--order	by dw.SourceCreationDate


UNION --then join this up to peope that chose want to send money but did not 

select	dw.id ,dw.FirstName, dw.LastName, 
		isnull(dw.ContactEmailAddress,dw.UserName) as EmailAddress,
		dw.SourceCreationDate,dw.IsActive,mon.*, dw.WantMultiCurrencyCard, dw.WantToSendMoney,'no_international_payment_recognised' AS RecordSet
from	DW.DimCustomerReference dw
left	outer JOIN	#monetaryresults mon
on		dw.CustomerKey = mon.customerkey
where	dw.WantToSendMoney = 1
and		dw.IsActive = 1
and		dw.CustomerKey > 11
AND		mon.customerkey is null
order by dw.SourceCreationDate



---------------------------discovery queries below not actual task----------------------------------------

SELECT	[dw].[UdfGetGBPValue](BuyAmount,BuyCcyCode) as GBPAmount,
		*
from	FireBird.FactPaymentTransfer
where	customerkey = 31361
order	by id 

with cteMaxId as (
select	max(aa.id) as maxid,aa.customerkey
from	FireBird.FactPaymentTransfer aa
group	by aa.customerkey),
cteMaxValue as (select buyamount,customerkey from FireBird.FactPaymentTransfer where id in (select maxid from ctemaxid))
select * from cteMaxValue where customerkey = 113548



select * from firebird.FactPaymentTransfer
where customerkey = 113548
order by Id

