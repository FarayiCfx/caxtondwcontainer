select top 10 * from currencybank.trade

-->adhoc query - not re usable code so mix of temp tables and cte's 
IF OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP TABLE #temp1

select	* 
into	#temp1
from	currencybank.trade (nolock) 
where	tradedate between '2016-11-01'and '2016-11-30'
and		tradereason <>'card load' 
and		sellccycode <>'GBP'
and		buyccycode <>'gbp'
;

with cte1 as (
select [Name]
  +' - '
  +convert(varchar(24) ,bb.BookOpenDate,103)
  +' '
  +convert(varchar(24) ,bb.BookOpenDate,108) as NetBookName,
  aa.id as TradeId,
 mk.buyccyamount - aa.buyamount as totalprofit,
  aa.buyamount / aa.sellamount as traderate,
  mk.bankrate,
  aa.UserId, 
  aa.SellCcyCode, 
  aa.BuyCcyCode, 
  aa.BuyCcyClientTradingBookId as TradeTableBuyCcyClientTradingBookId,
	aa.SellAmount,
  aa.BuyAmount,
    aa.TradeProfit,
  aa.TradeDate,
  aa.TradeEndDate,
  datediff(dd,aa.TradeDate,aa.TradeEndDate) AS DaysToSettlement,
  aa.TradeReason,
  aa.isreversed,
  aa.isunwound,
  aa.BuyCcyClientTradingBookId,
  bb.id as ClientTradeBookId ,
  bb.name,
  bb.ccycode,
  mk.id as MarketTradeId,
  mk.bookname,
  mk.counterpartyrefno,
  mk.sellccy as MarcketTradeSellCCY,
  mk.sellccyamount as MarketTradeSellCCYAmount,
  mk.buyccy as MarketTradeBuyCCY,
  mk.buyccyamount  as MarketTradeBuyCCYAmount,
  mk.bookopendate
from currencybank.Trade aa (nolock)
join currencybank.ClientTradingBook bb (nolock)
on  aa.BuyCcyClientTradingBookId = bb.Id
--and  aa.SellCCyClientTradingBookId = bb.id
join CurrencyBank.MarketTrade mk (nolock)
on  bb.Name = mk.BookName
and  convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
and  aa.BuyCcyCode = mk.BuyCcy
and aa.sellccycode = mk.sellccy
where aa.id in (select [id]from #temp1) )
select * from cte1 
order by totalprofit

----------

select [Name]
  +' - '
  +convert(varchar(24) ,bb.BookOpenDate,103)
  +' '
  +convert(varchar(24) ,bb.BookOpenDate,108) as NetBookName,
  aa.id as TradeId,
 mk.buyccyamount - aa.buyamount as totalprofit,
  aa.buyamount / aa.sellamount as traderate,
  mk.bankrate,
  aa.UserId, 
  aa.SellCcyCode, 
  aa.BuyCcyCode, 
  aa.BuyCcyClientTradingBookId as TradeTableBuyCcyClientTradingBookId,
	aa.SellAmount,
  aa.BuyAmount,
    aa.TradeProfit,
  aa.TradeDate,
  aa.TradeEndDate,
  datediff(dd,aa.TradeDate,aa.TradeEndDate) AS DaysToSettlement,
  aa.TradeReason,
  aa.isreversed,
  aa.isunwound,
  aa.BuyCcyClientTradingBookId,
  bb.id as ClientTradeBookId ,
  bb.name,
  bb.ccycode,
  mk.id as MarketTradeId,
  mk.bookname,
  mk.counterpartyrefno,
  mk.sellccy as MarcketTradeSellCCY,
  mk.sellccyamount as MarketTradeSellCCYAmount,
  mk.buyccy as MarketTradeBuyCCY,
  mk.buyccyamount  as MarketTradeBuyCCYAmount,
  mk.bookopendate
from currencybank.Trade aa (nolock)
join currencybank.ClientTradingBook bb (nolock)
on  aa.BuyCcyClientTradingBookId = bb.Id
--and  aa.SellCCyClientTradingBookId = bb.id
join CurrencyBank.MarketTrade mk (nolock)
on  bb.Name = mk.BookName
and  convert(varchar(24) ,bb.BookOpenDate,120)  = convert(varchar(24) ,mk.BookOpenDate,120) 
/*and  aa.BuyCcyCode = mk.BuyCcy
and aa.sellccycode = mk.sellccy*/
where aa.id = 1407430

select * from currencybank.trade where id = 1407430

select * from currencybank.markettrade where id in (33913,33914)
