/*
THis is adhoc code and is not yet ticketed 
A proper ssrs report needs to be created 
*/

select  count(*) from [dbo].[CounterPartyRefReport]
--609660

-->temp 1 sums all individual trades 
IF		OBJECT_ID('tempdb..#temp1') IS NOT NULL
DROP	TABLE #temp1
go
with	cteIndividualTrades as(
select	sum(buyamount) as sum_trade_side,
		counterpartyrefno
from	counterpartyrefreport
group	by counterpartyrefno)
select	aa.*, 
		bb.BuyCCYAmount,
		bb.BuyCCYAmount - sum_trade_side as Difference_,
		cast (bb.bookopendate as date) BookOpenDate
into	#temp1
from	cteIndividualTrades aa
join	[CurrencyBank].[MarketTrade] bb
on		aa.counterpartyrefno = bb.counterpartyrefno

--just do a check of what you have for a specific date range 
select	* 
from	#temp1 
where	difference_ = 0
AND		BOOKOPENDATE BETWEEN '2015-03-01' AND '2017-02-27'
ORDER	BY BOOKOPENDATE
---------------------------------------
drop table dbo.counterpartydownload

--here is what I copy and past to send to finance 
select	* 
into	dbo.counterpartydownload
from	[dbo].[CounterPartyRefReport] 
where	counterpartyrefno in(
select	counterpartyrefno 
from	#temp1 
where	difference_ = 0
AND		BOOKOPENDATE BETWEEN '2015-03-01' AND '2017-02-27')
order	by tradedate asc



select * from dbo.counterpartydownload





