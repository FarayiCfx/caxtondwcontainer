/* 
-->If re importing the entire set of files please truncate tables as below and delete all files in the filesystem then rerun the job
truncate table  [rbs].[EOD_StagingTable]
truncate table  [rbs].[EODFile_StatementRecord]
truncate table	[rbs].[EODFile_BalanceRecord]
truncate table  rbs.EODFilesImportLog
truncate table	[rbs].[EODFilesRuntimeEnumerator]
*/
select * from rbs.EODFilesImportLog
select * from rbs.EODFilesRuntimeEnumerator

select * from [rbs].[EOD_StagingTable]

select * from rbs.EODFiles_ImportedView where Filename = 'GPL.PTAIR.M8056120.AE.S0641791.T001.P001'

-->This is the select FILELIST FOR DOWNLOAD 
select	replace(enum.FileName,'RBS/','') as FileName
		,enum.FileName as FilenameAndBlobContainer
		--,*
from	rbs.EODFilesRuntimeEnumerator enum
left	outer join	rbs.EODFiles_ImportedView im
on		replace(enum.FileName,'RBS/','') = im.FileName
where	im.FileName is null

--check files are coming in 
select	* from rbs.EODFilesImportLog where filename = 'GPL.PTAIR.M8056120.AE.S0641791.T001.P001'

exec	sp_spaceused '[rbs].[EODFile_StatementRecord]'
exec	sp_spaceused '[rbs].[EODFile_BalanceRecord]'

select	top 10 *  from [rbs].[EODFile_StatementRecord]

select	top 10 * from rbs.EODFile_BalanceRecord

exec	sp_who2 active

select top 10 * from [rbs].[EOD_StagingTable]
