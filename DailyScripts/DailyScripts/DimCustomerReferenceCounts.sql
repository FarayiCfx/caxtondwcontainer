/*
This will show me records from 2 systems
the last union being records we believe have already ben migrated
*/
select	count(customerkey) as count_ , 
		originalsystemsourceid
from	[dw].[DimCustomerReference] (nolock)
group	by  originalsystemsourceid
union	all 
select	count(customerkey) as count_ , 
		originalsystemsourceid
from	[dw].[DimCustomerAlreadyMigratedTSCustomers] (nolock)
group	by  originalsystemsourceid


exec sp_Spaceused '[dw].[DimCustomerReference]'

exec sp_Spaceused '[dw].[DimCustomerAlreadyMigratedTSCustomers]'


truncate table [dw].[DimCustomerReference]

truncate table [dw].[DimCustomerAlreadyMigratedTSCustomers]



select top 5 * from [dw].[DimCustomerAlreadyMigratedTSCustomers] order by 1 desc

select top 5 * from [dw].[DimCustomerReference] order by 1 asc


/*
this is the query to run on caxton db-1
to count all of the records that will 
be selected from dbcards

This query below is wrong!
*/

with cte1 as (
SELECT	CC.CONTACT_ID,CC.ACCOUNT_ID, PARENT_ID, 
		contact_salutation as [Title],
		contact_first_name as FirstName,
		contact_last_name as LastName,
		[Gender],
		cc.[dob] as DateOfBirth,
		[Address1] as [AddressLine1],
		[Address2] as [AddressLine2],
		[towncity] as [CITY], 
		[COUNTYSTATE] as COUNTY,
		POSTCODE,
		[COUNTRY],
		[EMAILADDRESS] AS [ContactEmailAddress],
		[TELEPHONE] AS [PrimaryPhoneNumber],
		[MOBILEPHONE] AS [SecondaryContactNumber],
		cc.[Insertdate] as [SourceDateInserted],
		cc.[UpdateDate]  as [SourceDateUpdated] 
from	contact cc (NOLOCK)
left	outer join	scy_registration rg (nolock)
on		cc.contact_id = rg.contact_id
join	scy_regext ext
on		rg.pkid = ext.pkid)
select count(CONTACT_ID) from cte1

exec sp_help '[dw].[DimCustomerReference]'


select cast (isnull([fbcardscontactref],0) as int) as fbcardscontactref,*
FROM DW.DIMCUSTOMERREFERENCE (nolock)