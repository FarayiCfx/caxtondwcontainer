
/*
So how do you determine an avs match ?
Not sure you can but below I have done the 
following:

created a cte that looks for all payments 
that have occurred more than once, summed
them up and counted them, then gone ahead 
to join them to the imported transactions table 
*/
with	cteAVSFailures as(
select	sum(cast([Transaction Amount] as decimal(38,2)) ) as SumTotalPerPayment,
		count([payment]) as CountOfPaymentid,
		[payment]
from	[altapay].[Fact_AltaPayFundingFiles] (nolock)
where	(payment <> '' and payment <> '0') 
and		(date between '2015-10-01 00:00:00.000' and '2015-11-01 00:00:00.000')
group	by [payment]
having	count([payment]) > 1)--21890

select	*	
from	cteAVSFailures cte  --613 rows 
join	[altapay].[Fact_AltaPayFundingFiles] fl (nolock)
ON		cte.payment = fb.payment 
ORDER	BY cte.PAYMENT;



