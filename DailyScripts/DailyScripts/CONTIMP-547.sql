IF OBJECT_ID('tempdb..#secondarycardclaims') IS NOT NULL
DROP TABLE #secondarycardclaims
go
with	cte1 as (
select	aa.* 
		,bb.username
from	[dw].[FactAspNetUserClaims] aa
JOIN	dw.dimcustomerreference bb
on		aa.customerkey = bb.customerkey 
where	claimvalue like '%View:CurrencyCards%')
select	count(customerkey) as count_
		,customerkey  
		,username
into	#secondarycardclaims
from	cte1 
group	by	customerkey 
			,username
order		by 1 desc 

------------------------------------------------

IF OBJECT_ID('tempdb..#primaryretail') IS NOT NULL
DROP TABLE #primaryretail
go
select		aa.* 
into		#primaryretail
from		[dw].[FactAspNetUserClaims] aa
where		claimtype = 'AccountType' 
and			claimvalueforaccounttype like 'Retail:%:PortalCards' order by 1 desc 

select	* 
from	#secondarycardclaims aa
join	#primaryretail bb
on		aa.customerkey = bb.customerkey 
where	aa.count_ >= 6 
order	by 1 desc 




select * from #primaryretail

select	*  from	#secondarycardclaims where count_ >= 6 order by count_ desc 

select * from [dw].[FactAspNetUserClaims] aa
where		claimtype = 'AccountType' 
