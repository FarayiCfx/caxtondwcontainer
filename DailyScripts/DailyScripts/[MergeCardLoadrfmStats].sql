
CREATE procedure [dw].[MergeCardLoadrfmStats]
as
/*
Merge the load totals, counts and min and max dates to the 
customer aggregate RFM table 
*/
with	cteCardloadStats as(
select	sum(TradeProfit) GBPTradeProfit , 
		count(id) as Count,
		min(TradeDate) as MinTradeDate,
		max(TradeDate) as MaxTradeDate,
		customerkey 
from	FireBird.FactCurrencyBankTrade (nolock)
where	TradeReason =  'Card load'
group	by customerkey)

merge	DW.CustomerRFMAggregations rfm 
using	(
select	ck.customerkey,
		isnull (cte.GBPTradeProfit,0) as [CardLoad_Tradeprofit_GBP_Total],
		isnull (cte.count,0) as [CardLoad_TradeCount],
		isnull (cte.MinTradeDate , '1900-01-01 00:00:00.000') as [CardLoad_FirstDate],
		isnull (cte.MaxTradeDate, '1900-01-01 00:00:00.000') as [CardLoad_LastDate]
from	dw.dimcustomerreference ck
left	outer join cteCardloadStats cte
on		ck.customerkey = cte.customerkey ) as source
on		rfm.customerkey = source.customerkey 
when	matched 
then	update 
set		rfm.[CardLoad_Tradeprofit_GBP_Total] = source.[CardLoad_Tradeprofit_GBP_Total],
		rfm.[CardLoad_TradeCount] = source.[CardLoad_TradeCount],
		rfm.[CardLoad_FirstDate] = source.[CardLoad_FirstDate],
		rfm.[CardLoad_LastDate] = source.[CardLoad_LastDate],
		rfm.DWDateUpdated = getdate()
		
when	not matched
then	insert (CustomerKey,
				[CardLoad_Tradeprofit_GBP_Total],
				[CardLoad_TradeCount],
				[CardLoad_FirstDate],
				[CardLoad_LastDate],
				DWDateUpdated)
				values (
				source.CustomerKey,
				source.[CardLoad_Tradeprofit_GBP_Total],
				source.[CardLoad_TradeCount],
				source.[CardLoad_FirstDate],
				source.[CardLoad_LastDate],
				getdate())
		;

GO



exec [dw].[MergeCardLoadrfmStats]

