truncate table tradeprofitoutput
go

declare book_cursor cursor
	
--change the filters to not include fastpay, reversal and card load books
for 

select * from dbo.bookname 
where		fullname not like '%fastpay%' 
and			fullname not like '%reversal%'
and			fullname not like 'FB_CardsLoads%'


declare @fullname varchar(128) 
declare @datestring varchar(24)
declare @name nvarchar(max);
declare @opendate datetime;
declare @marketopendate datetime;
declare @profit decimal(16,2)
declare @selltotal decimal(16,2)
declare @correctedprofit decimal(16,2)
declare @totalprofit decimal(16,2)
declare @soldtotal decimal(16,2)
declare @booksold decimal(16,2)
declare @partiallyunwound_totalprofitline bit
declare @partiallyunwound_soldline bit
declare @tradeid bigint 

open	book_cursor

fetch	next from book_cursor 
into	@fullname

while	@@fetch_status = 0
begin
	--print @fullname
	set @name = LEFT(@fullname,len(@fullname)-23)
	--print @name	
	set @datestring = RIGHT(@fullname,23) 
	--print @datestring
	set @opendate = @datestring;
	set @marketopendate = substring(@datestring,0,len(@datestring)-2) + '000';
	--print @opendate
	--print @marketopendate;

	with trade_cte as
	(
		select * from [CurrencyBank].[Trade]
		where 
		isunwound = 0 and isreversed = 0
		AND (CurrencyBank.Trade.SellCcyCode = 'GBP' OR CurrencyBank.Trade.BuyCcyCode = 'GBP')
		and
		(
			[BuyCcyClientTradingBookId] in
			(
			select id from [CurrencyBank].[ClientTradingBook]
			where name = @name and bookopendate = @opendate
			)
			or [SellCcyClientTradingBookId] in
			(
			select id from [CurrencyBank].[ClientTradingBook]
			where name = @name and bookopendate = @opendate
			)
		)
	)
	select @profit = sum(tradeprofit), @soldtotal = sum(sellamount) ,@tradeid = id from trade_cte  group by [id]
	 ;
	IF @profit IS NULL 
		SET @profit = 0
-------------------------------------------------------------------------------------------------------------	

	select @totalprofit = sum(sellccyamount) from [CurrencyBank].[MarketTrade]
		where bookname = @name and bookopendate = @marketopendate
		and sellccy = 'GBP' and buyccy = 'GBP'

	select @booksold = sum(sellccyamount) from [CurrencyBank].[MarketTrade]
		where bookname = @name and bookopendate = @marketopendate
		and sellccy = 'GBP' and buyccy != 'GBP'

		/*select @partiallyunwound_totalprofitline = partiallyclosed
		from [CurrencyBank].[MarketTrade]
		where bookname = @name and bookopendate = @marketopendate
		and sellccy = 'GBP' and buyccy = 'GBP'

		select @partiallyunwound_soldline = partiallyclosed
		from [CurrencyBank].[MarketTrade]
		where bookname = @name and bookopendate = @marketopendate
		and sellccy = 'GBP' and buyccy != 'GBP'*/

 
	if @soldtotal is null
		set @soldtotal = 0

	if @booksold is null
		set @booksold = 0

	IF @totalprofit IS NULL 
		SET @totalprofit = 0

	IF @booksold IS NULL
		SET @booksold = 0

---print @fullname + N',' + cast(@opendate as varchar) + N',' + cast(@profit as varchar) + N',' + cast(@totalprofit as varchar) + N',' + cast(@soldtotal as varchar) + ',' + cast(@booksold as varchar)

insert into TradeProfitOutput(Bookname,[Datestring],[TradingProfit],[TotalProfit],[Trade Sold],[Market Sold],PartiallyClosed_totalprofitline,PartiallyClosed_soldline,tradeid)
values						(@fullname , @opendate , @profit ,@totalprofit ,@soldtotal ,@booksold,@partiallyunwound_totalprofitline,@partiallyunwound_soldline,@tradeid)


	fetch next from book_cursor 
	into @fullname
end
close book_cursor
deallocate book_cursor
go
select * from  tradeprofitoutput
