/*
--exec sp_help 'valitor.pan'
--exec sp_help 'valitor.wallet'
*/
-->drop then create the test tables 
drop table [Valitor].[Pan_Test]
go


drop table [Valitor].[Wallet_Test]
go


CREATE TABLE [Valitor].[Wallet_Test](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ValitorWalletId] [bigint] NOT NULL,
	[AccountId] [nvarchar](256) NULL,
	[ParentId] [bigint] NULL,
	[CardGroupName] [nvarchar](max) NULL,
	[IsArchived] [bit] NOT NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_Valitor.Wallet_Test] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
ALTER TABLE [Valitor].[Wallet_Test] ADD  DEFAULT ((0)) FOR [IsArchived]
GO
--------------------------------------------------------------------------------------
CREATE TABLE [Valitor].[Pan_Test](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ValitorPanId] [bigint] NOT NULL,
	[MaskedPan] [nvarchar](max) NULL,
	[Expiry] [nvarchar](max) NULL,
	[WalletId] [bigint] NOT NULL,
	[PanStatusCode] [nvarchar](max) NULL,
	[CardStatusCode] [nvarchar](max) NULL,
	[DateCreated] [datetime] NOT NULL,
	[IsDirty] [bit] NOT NULL,
	[CardHolderName] [nvarchar](max) NULL,
	[CardHolderEmail] [nvarchar](max) NULL,
	[IsArchived] [bit] NOT NULL,
	[CardSequenceNumber] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_Valitor.Pan_Test] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
ALTER TABLE [Valitor].[Pan_Test] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [Valitor].[Pan_Test] ADD  DEFAULT ((0)) FOR [IsArchived]
GO
ALTER TABLE [Valitor].[Pan_Test]  WITH CHECK ADD  CONSTRAINT [FK_Valitor.Pan_Valitor.Wallet_WalletId_Test] FOREIGN KEY([WalletId])
REFERENCES	[Valitor].[Wallet_Test] ([Id])
GO
ALTER TABLE [Valitor].[Pan_Test] CHECK CONSTRAINT [FK_Valitor.Pan_Valitor.Wallet_WalletId_Test]
GO
--------------------------------------------------------------


SET IDENTITY_INSERT [FirebirdFx-prod].[Valitor].[Wallet_Test] ON 
GO
INSERT INTO [Valitor].[Wallet_Test]([Id],[ValitorWalletId],[AccountId],[ParentId],[CardGroupName],[IsArchived],[FB_DateCreated],[FB_DateUpdated])
SELECT * FROM [Valitor].[Wallet] (nolock)
GO
SET IDENTITY_INSERT [FirebirdFx-prod].[Valitor].[Wallet_Test] OFF 
GO

-------
SET IDENTITY_INSERT [FirebirdFx-prod].[Valitor].[Pan_Test] ON 
GO
insert into [Valitor].[Pan_Test]([Id],[ValitorPanId],[MaskedPan],[Expiry],[WalletId],[PanStatusCode],[CardStatusCode],[DateCreated],[IsDirty],[CardHolderName],[CardHolderEmail],[IsArchived],[CardSequenceNumber],[FB_DateCreated],[FB_DateUpdated])
select * from [Valitor].[Pan]
go
SET IDENTITY_INSERT [FirebirdFx-prod].[Valitor].[Pan_Test] OFF 
GO

