﻿CREATE VIEW [raw].[vwStatementAcquirerAltaPay]
	AS 
	WITH Statements (DataInstanceCode, [FileName], DateMax, DateMin)
	AS
	-- Define the CTE query.
	(
		SELECT DataInstanceCode, [FileName], MAX(Date) AS DateMax, MIN(Date) AS DateMin
		FROM [raw].[tblStatementAcquirerAltaPay]
		GROUP BY DataInstanceCode, [FileName]
	)

	SELECT DISTINCT x.DataInstanceCode, 
		RIGHT(x.FileName,CHARINDEX('\',REVERSE(x.FileName))-1) AS FileName,
		CONVERT(varchar, s.DateMin, 112) + 'MIN' + CONVERT(varchar, s.DateMin, 112) + 'MAX' AS GroupCode,  Date,Type,ID,ReconciliationIdentifier,ISNULL(Payment,'') AS Payment,[Order],Terminal,Shop,TransactionCurrency,TransactionAmount,ExchangeRate,SettlementCurrency,SettlementAmount,FixedFee,FixedFeeVAT,RateBasedFee,RateBasedFeeVAT
	FROM [raw].[tblStatementAcquirerAltaPay] AS x
	INNER JOIN Statements AS s
		ON s.[FileName] = x.FileName
		AND s.DataInstanceCode = x.DataInstanceCode

GO
