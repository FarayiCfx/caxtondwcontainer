﻿
	CREATE VIEW [raw].[vwStatementIssuerValitor]
	AS 
	WITH Statements (DataInstanceCode, [FileName], DateMax, DateMin)
	AS
	-- Define the CTE query.
	(
		SELECT DataInstanceCode, [FileName],
			MAX(TransactionDateTime) AS DateMax,
			MIN(TransactionDateTime) AS DateMin
		FROM [raw].[tblStatementIssuerValitor]
		WHERE TransactionKeyCategory IN ('Charge','Deposit')
		GROUP BY DataInstanceCode, [FileName]
	)

	SELECT DISTINCT 
	x.DataInstanceCode,
	RIGHT(x.FileName,CHARINDEX('\',REVERSE(x.FileName))-1) AS FileName,
	CONVERT(varchar, s.DateMin, 112) + 'MIN' + CONVERT(varchar, s.DateMin, 112) + 'MAX' AS GroupCode,  
	ServiceProviderId,ExtractDateTimeTxt,Type,Id,PanId,AccountId,ReferenceNumber,BillingAmount,BillingCurrency,
	RegistrationDate,SettlementId,
	TransactionDateTimeTxt,
	TransactionDateTime AS TransactionDateTime,
	TransactionKey,TransactionKeyCategory,ClearingCycleId,
	WalletId,CardTypeCode,Arn,AuthorizationNumber,AuthorizationTransactionId,ForeignExchangeFee,
	MerchantCity,MerchantCountryCode,MerchantMcc,MerchantId,MerchantName,TerminalCode,
	TransactionAmount,TransactionCurrency,ProcessingDateTxt,Region,TimestampId,Description,Label,ReferenceId,SourceId
	FROM [raw].[tblStatementIssuerValitor]AS x
	INNER JOIN Statements AS s
		ON s.[FileName] = x.FileName
		AND s.DataInstanceCode = x.DataInstanceCode
	WHERE x.TransactionKeyCategory IN ('Charge','Deposit')

