﻿CREATE TABLE [raw].[tblStatementBankRBS]
(
	[UID] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL,
	[DataInstanceCode] [varchar](50) NOT NULL,
	[FileName] [varchar](1000) NOT NULL,
	[SortCode] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountAlias] [varchar](50) NULL,
	[AccountShortName] [varchar](50) NULL,
	[Currency] [varchar](50) NULL,
	[Account Type] [varchar](50) NULL,
	[BIC] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[BranchName] [varchar](50) NULL,
	[Date] [datetime] NULL,
	[Narrative1] [varchar](200) NULL,
	[Narrative2] [varchar](200) NULL,
	[Narrative3] [varchar](200) NULL,
	[Narrative4] [varchar](200) NULL,
	[Narrative5] [varchar](200) NULL,
	[Type] [varchar](200) NULL,
	[Debit] [decimal](28, 2) NULL,
	[Credit] [decimal](28, 2) NULL,
	CONSTRAINT PK_UID PRIMARY KEY NONCLUSTERED (UID),
	INDEX idxDataInstanceCode CLUSTERED ([DataInstanceCode] )
)

