﻿CREATE TABLE [raw].[tblStatementIssuerValitorCcyLookup](
	[ISOCodeNumeric] [varchar](50) NOT NULL,
	[ISOCode] [char](3) NOT NULL,
	[Name] [varchar](1000) NOT NULL
) ON [PRIMARY]
