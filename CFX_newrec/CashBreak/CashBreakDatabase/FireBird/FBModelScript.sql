﻿/****** Object:  Schema [CurrencyBank]    Script Date: 09/10/2017 18:58:26 ******/
CREATE SCHEMA [CurrencyBank]
GO
/****** Object:  Schema [Firebird]    Script Date: 09/10/2017 18:58:26 ******/
CREATE SCHEMA [Firebird]
GO
/****** Object:  Schema [Payment]    Script Date: 09/10/2017 18:58:26 ******/
CREATE SCHEMA [Payment]
GO
/****** Object:  Schema [Settlement]    Script Date: 09/10/2017 18:58:26 ******/
CREATE SCHEMA [Settlement]
GO
/****** Object:  Table [CurrencyBank].[AccountSummary]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[AccountSummary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Balance] [decimal](18, 2) NOT NULL,
	[UserId] [nvarchar](256) NULL,
	[CurrencyId] [bigint] NOT NULL,
	[LastModifiedTimeStamp] [timestamp] NOT NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.AccountSummary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [CurrencyBank].[AccountTransaction]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[AccountTransaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionDateTime] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Balance] [decimal](18, 2) NOT NULL,
	[Cleared] [bit] NOT NULL,
	[AccountSummaryId] [bigint] NOT NULL,
	[LastModifiedTimeStamp] [timestamp] NOT NULL,
	[Status] [nvarchar](512) NULL,
	[ClearedAmount] [decimal](18, 2) NOT NULL,
	[ExternalTransactionId] [nvarchar](256) NULL,
	[ExternalTransRef] [nvarchar](256) NULL,
	[BC_TransId] [nvarchar](256) NULL,
	[BC_TransRef] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.AccountTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [CurrencyBank].[AccountTransactionDetail]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[AccountTransactionDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionType] [nvarchar](max) NULL,
	[AccountTransactionId] [bigint] NOT NULL,
	[TransactionReferenceKey] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.AccountTransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [CurrencyBank].[Currency]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[Currency](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BuyCcyCode] [nvarchar](100) NULL,
	[Balance] [decimal](18, 2) NOT NULL,
	[LastModifiedTimeStamp] [timestamp] NOT NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.Currency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [CurrencyBank].[MarketTrade]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[MarketTrade](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BookName] [nvarchar](max) NULL,
	[BankRate] [decimal](18, 2) NOT NULL,
	[CounterpartyRefNo] [nvarchar](max) NULL,
	[Comment] [nvarchar](max) NULL,
	[DateCreated] [datetime] NOT NULL,
	[LastModifiedTimeStamp] [timestamp] NOT NULL,
	[SellCcy] [nvarchar](max) NULL,
	[SellCcyAmount] [decimal](18, 2) NOT NULL,
	[BuyCcy] [nvarchar](max) NULL,
	[BuyCcyAmount] [decimal](18, 2) NOT NULL,
	[PartiallyClosed] [bit] NOT NULL,
	[BookOpenDate] [datetime] NOT NULL,
	[Transaction940Id] [bigint] NULL,
	[Transaction940Sell] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.MarketTrade] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [CurrencyBank].[Trade]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[Trade](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](256) NULL,
	[SellCcyCode] [nvarchar](100) NULL,
	[BuyCcyCode] [nvarchar](100) NULL,
	[SellAmount] [decimal](18, 2) NOT NULL,
	[BuyAmount] [decimal](18, 2) NOT NULL,
	[Settled] [bit] NOT NULL,
	[BuyCcyClientTradingBookId] [bigint] NOT NULL,
	[SellCcyClientTradingBookId] [bigint] NOT NULL,
	[WorkflowInstanceId] [uniqueidentifier] NULL,
	[TradeDate] [datetime] NULL,
	[IsAutomatic] [bit] NOT NULL,
	[TradeEndDate] [datetime] NOT NULL,
	[TradeProfit] [decimal](18, 2) NOT NULL,
	[TreasuryProfit] [decimal](18, 2) NOT NULL,
	[Reversal] [bit] NOT NULL,
	[IsReversed] [bit] NOT NULL,
	[IsUnWound] [bit] NOT NULL,
	[TradeReason] [nvarchar](50) NULL,
	[HelpdeskUserId] [nvarchar](256) NULL,
	[TransferId] [bigint] NOT NULL,
	[TradeNotes] [nvarchar](max) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.Trade] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [CurrencyBank].[TradeDetail]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [CurrencyBank].[TradeDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TradeId] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[PaidDate] [datetime] NULL,
	[AccountTransactionId] [bigint] NOT NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CurrencyBank.TradeDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Firebird].[CardsFee]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [Firebird].[CardsFee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AlphaCcy] [nvarchar](max) NULL,
	[NumeriCcy] [nvarchar](max) NULL,
	[CcyFee] [nvarchar](max) NULL,
 CONSTRAINT [PK_Firebird.CardsFee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Payment].[PaymentType]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [Payment].[PaymentType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PaymentTypeCode] [nvarchar](2) NULL,
	[Description] [nvarchar](50) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_Payment.PaymentType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Payment].[PendingTransfer]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [Payment].[PendingTransfer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BeneficiaryId] [bigint] NOT NULL,
	[UserId] [nvarchar](max) NULL,
	[BuyCurrency] [nvarchar](max) NULL,
	[BuyAmount] [decimal](18, 2) NOT NULL,
	[Complete] [bit] NOT NULL,
	[Automatic] [bit] NOT NULL,
	[IsValidated] [bit] NOT NULL,
	[IsTransferCreated] [bit] NOT NULL,
	[PendingTradeId] [bigint] NOT NULL,
	[PayAwayDate] [datetime] NOT NULL,
	[PayAwayReference] [nvarchar](max) NULL,
	[TransferId] [bigint] NOT NULL,
	[TransferTransactionId] [uniqueidentifier] NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[ChainMutationHash] [nvarchar](256) NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_Payment.PendingTransfer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Payment].[TradeType]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [Payment].[TradeType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TradeTypeCode] [nvarchar](3) NULL,
	[Description] [nvarchar](50) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_Payment.TradeType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [Payment].[Transfer]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [Payment].[Transfer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BeneficiaryId] [bigint] NOT NULL,
	[UserId] [nvarchar](max) NULL,
	[BuyCcyCode] [nvarchar](max) NULL,
	[BuyAmount] [decimal](18, 2) NOT NULL,
	[Complete] [bit] NOT NULL,
	[Automatic] [bit] NOT NULL,
	[ExecutedDate] [datetime] NULL,
	[PayAwayDate] [datetime] NULL,
	[PayStatus] [nvarchar](max) NULL,
	[WorkflowInstanceId] [uniqueidentifier] NULL,
	[ResubmitCount] [bigint] NOT NULL,
	[AccountTransactionId] [bigint] NOT NULL,
	[Authorisation] [nvarchar](max) NULL,
	[AuthorisationStatus] [tinyint] NULL,
	[FileAccepted] [tinyint] NULL,
	[PaymentFile] [nvarchar](max) NULL,
	[FileSent] [datetime] NULL,
	[PaymentAccepted] [tinyint] NULL,
	[CreatedDate] [datetime] NULL,
	[PaymentProcessing] [tinyint] NULL,
	[PaymentProcessingDate] [datetime] NULL,
	[UrgentIntl] [tinyint] NULL,
	[RejectionReason] [nvarchar](255) NULL,
	[PaymentReference] [nvarchar](max) NULL,
	[TransferTransactionId] [uniqueidentifier] NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[ChainMutationHash] [nvarchar](256) NULL,
	[FB_DateDeleted] [datetime] NULL,
	[PaymentProvider] [nvarchar](10) NULL,
 CONSTRAINT [PK_Payment.Transfer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [Settlement].[RbsStatement]    Script Date: 09/10/2017 18:58:26 ******/

CREATE TABLE [Settlement].[RbsStatement](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileImportId] [bigint] NOT NULL,
	[FileEntry] [nvarchar](max) NULL,
	[TransactionRefNo] [nvarchar](25) NULL,
	[SortCode] [nvarchar](6) NULL,
	[AccountNumber] [nvarchar](35) NULL,
	[AccountAlias] [nvarchar](35) NULL,
	[AccountName] [nvarchar](35) NULL,
	[AccountCurrency] [nvarchar](3) NULL,
	[AccountType] [nvarchar](20) NULL,
	[BankIdentifierCode] [nvarchar](10) NULL,
	[BankName] [nvarchar](35) NULL,
	[BankBranchName] [nvarchar](35) NULL,
	[Narrative1] [nvarchar](25) NULL,
	[Narrative2] [nvarchar](25) NULL,
	[Narrative3] [nvarchar](25) NULL,
	[Narrative4] [nvarchar](25) NULL,
	[Narrative5] [nvarchar](25) NULL,
	[TransactionType] [nvarchar](25) NULL,
	[AccountTransactionId] [bigint] NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[VoidReason] [nvarchar](100) NULL,
	[VoidedBy] [nvarchar](256) NULL,
	[TradeFundingHintList] [nvarchar](256) NULL,
	[MatchType] [nvarchar](20) NULL,
	[ValueDate] [datetime] NULL,
	[DateMatched] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[TransactionDate] [datetime] NULL,
	[DateVoided] [datetime] NULL,
	[Voided] [bit] NOT NULL,
	[CreditValue] [decimal](18, 2) NOT NULL,
	[DebitValue] [decimal](18, 2) NOT NULL,
	[SettlementType] [nvarchar](10) NULL,
	[FB_DateCreated] [datetime] NULL,
	[FB_DateUpdated] [datetime] NULL,
	[FB_DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_Settlement.RbsStatement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Index [IX_CurrencyBankAccountSummary_UserId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_CurrencyBankAccountSummary_UserId] ON [CurrencyBank].[AccountSummary]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CurrencyId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_CurrencyId] ON [CurrencyBank].[AccountSummary]
(
	[CurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountSummaryId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_AccountSummaryId] ON [CurrencyBank].[AccountTransaction]
(
	[AccountSummaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_AccountTransaction_ExternalTransactionId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_AccountTransaction_ExternalTransactionId] ON [CurrencyBank].[AccountTransaction]
(
	[ExternalTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_AccountTransaction_TransactionDateTime]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_AccountTransaction_TransactionDateTime] ON [CurrencyBank].[AccountTransaction]
(
	[TransactionDateTime] ASC
)
INCLUDE ( 	[Amount],
	[ExternalTransRef],
	[Status]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_CurrencyBankCurrency_IdBuyCcyCode]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_CurrencyBankCurrency_IdBuyCcyCode] ON [CurrencyBank].[Currency]
(
	[Id] ASC,
	[BuyCcyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ix_currencybanktrade_date]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [ix_currencybanktrade_date] ON [CurrencyBank].[Trade]
(
	[TradeDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ix_currencybanktrade_userid]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [ix_currencybanktrade_userid] ON [CurrencyBank].[Trade]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SellAmount_Settled]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_SellAmount_Settled] ON [CurrencyBank].[Trade]
(
	[SellAmount] ASC,
	[Settled] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_Trade_SellCcyClientTradingBookId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_Trade_SellCcyClientTradingBookId] ON [CurrencyBank].[Trade]
(
	[SellCcyClientTradingBookId] ASC
)
INCLUDE ( 	[BuyAmount],
	[BuyCcyClientTradingBookId],
	[BuyCcyCode],
	[Id],
	[IsAutomatic],
	[IsReversed],
	[IsUnWound],
	[Reversal],
	[SellAmount],
	[SellCcyCode],
	[Settled],
	[TradeDate],
	[TradeEndDate],
	[TradeProfit],
	[TreasuryProfit],
	[UserId],
	[WorkflowInstanceId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TradeId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_TradeId] ON [CurrencyBank].[TradeDetail]
(
	[TradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BeneficiaryId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_BeneficiaryId] ON [Payment].[PendingTransfer]
(
	[BeneficiaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_ChainMutationHash]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_ChainMutationHash] ON [Payment].[PendingTransfer]
(
	[ChainMutationHash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [nci_wi_PendingTransfer_7E14DD9CD0F5EBC0749A54E747CCAB44]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [nci_wi_PendingTransfer_7E14DD9CD0F5EBC0749A54E747CCAB44] ON [Payment].[PendingTransfer]
(
	[PendingTradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BeneficiaryId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_BeneficiaryId] ON [Payment].[Transfer]
(
	[BeneficiaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_ChainMutationHash]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_ChainMutationHash] ON [Payment].[Transfer]
(
	[ChainMutationHash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [nci_wi_Transfer_F5D8B41F1A0B73D7C7F36018C340F01C]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [nci_wi_Transfer_F5D8B41F1A0B73D7C7F36018C340F01C] ON [Payment].[Transfer]
(
	[WorkflowInstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FileImportId]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [IX_FileImportId] ON [Settlement].[RbsStatement]
(
	[FileImportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [nci_wi_RbsStatement_C29F7E9A12D4E245E10429369B08EF30]    Script Date: 09/10/2017 18:58:26 ******/
CREATE NONCLUSTERED INDEX [nci_wi_RbsStatement_C29F7E9A12D4E245E10429369B08EF30] ON [Settlement].[RbsStatement]
(
	[AccountTransactionId] ASC,
	[SettlementType] ASC,
	[DateCreated] ASC,
	[FB_DateDeleted] ASC
)
INCLUDE ( 	[AccountAlias],
	[AccountCurrency],
	[AccountName],
	[AccountNumber],
	[AccountType],
	[BankBranchName],
	[BankIdentifierCode],
	[BankName],
	[CreditValue],
	[DateMatched],
	[DateVoided],
	[DebitValue],
	[FB_DateCreated],
	[FB_DateUpdated],
	[FileEntry],
	[FileImportId],
	[MatchType],
	[Narrative1],
	[Narrative2],
	[Narrative3],
	[Narrative4],
	[Narrative5],
	[SortCode],
	[TradeFundingHintList],
	[TransactionDate],
	[TransactionRefNo],
	[TransactionType],
	[UserName],
	[ValueDate],
	[Voided],
	[VoidedBy],
	[VoidReason]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [CurrencyBank].[AccountTransaction] ADD  DEFAULT ((0)) FOR [ClearedAmount]
GO
ALTER TABLE [CurrencyBank].[MarketTrade] ADD  DEFAULT ((0)) FOR [SellCcyAmount]
GO
ALTER TABLE [CurrencyBank].[MarketTrade] ADD  DEFAULT ((0)) FOR [BuyCcyAmount]
GO
ALTER TABLE [CurrencyBank].[MarketTrade] ADD  DEFAULT ((0)) FOR [PartiallyClosed]
GO
ALTER TABLE [CurrencyBank].[MarketTrade] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [BookOpenDate]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT (getdate()) FOR [TradeDate]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [IsAutomatic]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [TradeEndDate]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [TradeProfit]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [TreasuryProfit]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [Reversal]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [IsReversed]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [IsUnWound]
GO
ALTER TABLE [CurrencyBank].[Trade] ADD  DEFAULT ((0)) FOR [TransferId]
GO
ALTER TABLE [CurrencyBank].[TradeDetail] ADD  DEFAULT (getdate()) FOR [PaidDate]
GO
ALTER TABLE [CurrencyBank].[TradeDetail] ADD  DEFAULT ((0)) FOR [AccountTransactionId]
GO
ALTER TABLE [Payment].[PendingTransfer] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [PayAwayDate]
GO
ALTER TABLE [Payment].[PendingTransfer] ADD  DEFAULT ((0)) FOR [TransferId]
GO
ALTER TABLE [Payment].[Transfer] ADD  DEFAULT ((0)) FOR [Automatic]
GO
ALTER TABLE [Payment].[Transfer] ADD  DEFAULT ((0)) FOR [ResubmitCount]
GO
ALTER TABLE [Payment].[Transfer] ADD  DEFAULT ((0)) FOR [AccountTransactionId]
GO
ALTER TABLE [Payment].[Transfer] ADD  CONSTRAINT [DF_Payment.Transfer_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [Settlement].[RbsStatement] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
