﻿/*
This section of comments is a reference section. 
/*
This is the table with the RECINSTANCE code, if you have 
more than one set of data in the inventory tables over different 
recs then this table needs to have one set marked accordingly 
*/
SELECT *	FROM [rec].[tblRecInstance]

UPDATE		[rec].[tblRecInstance]
SET			ExcelInclude = 0
WHERE		ExcelInclude = 1

INSERT		INTO [rec].[tblRecInstance](Code, ExcelInclude)
VALUES		('REC201710.V1',1);

UPDATE		[rec].[tblRecInstance]
SET			ExcelInclude = 1

only the uncommented scripts are needed 
*/

--Run stored procedures in this order 
--1 
EXEC [rec].[insInventoryEventCFX]
	@RecInstanceCode='REC201804.V1',
	@DateFrom='1 Mar 2018',
	@DateTo='31 Mar 2018',
	@IsDebugOn=1
	go

--2
EXEC [rec].[insMatchResult]
	@RecInstanceCode='REC201803.V1',
	@DateFrom='1 Mar 2018',
	@DateTo='31 Mar 2018',
	@LoadData = 0,
	@IsDebugOn=1
	go

--3
EXEC [rec].[insInventoryEventRecMatchResult]
	@RecInstanceCode='REC201803.V1',
	@DateFrom='1 Mar 2018',
	@DateTo='31 Mar 2018',
	@IsDebugOn=1
	go
/*
-->disable foreign key stopping completeness of break execution analysis , if you get an error indicating this was a problem
ALTER	TABLE [rec].[tblInventoryEvent] 
NOCHECK CONSTRAINT FK_tblEventTypeCode;  
GO 

select newid()

select	*
from	rec.tblInventoryEvent ie
left	outer join rec.tblEventType AS etcmd
on		et.Code = ie.EventTypeCode
where	et.Code is null

--EXCEL STATS
EXEC	[rec].[selMatchResultSummary] @RecInstanceCode='REC201710.V1'

--EXCEL DETAIL
EXEC	[rec].[selMatchResultBreakDetail] @RecInstanceCode='REC201710.V1'
--------------------------------------------------------------------------------

EXEC	[rec].[selMatchResultSummary] @RecInstanceCode='REC201711.V1'

--EXCEL DETAIL
EXEC	[rec].[selMatchResultBreakDetail] @RecInstanceCode='REC201711.V1'
*/


select * from rec.tblFXRate

update  rec.tblFXRate
set		date = '2018-03-31'