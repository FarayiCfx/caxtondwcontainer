﻿--BELOW CHECKES WILL CONFIRM THAT THE FILES WERE LOADED IN THE RAW FILES 
SELECT DISTINCT FileName
  FROM [Reconciliations2].[raw].[tblStatementIssuerValitor]
WHERE DataInstanceCode = 'REC201804.V1'

SELECT DISTINCT FileName
  FROM [Reconciliations2].[raw].tblStatementBankRBS
WHERE DataInstanceCode = 'REC201804.V1'

SELECT DISTINCT FileName
  FROM [Reconciliations2].[raw].tblStatementAcquirerAltaPay
WHERE DataInstanceCode = 'REC201804.V1'

/**************************************************************************/
  SELECT * FROM [rec].[tblFXRate] 
  --make sure that there are no missing dates 

  SELECT * FROM rbs.StatementFile 
  ORDER BY transactiondate 
  --check if any date is missing 
  
/**************************************************************************/
/*Check for duplicates post process */

SELECT *
  FROM [Reconciliations2].[rec].[tblInventoryEvent] WITH (NOLOCK) 
  WHERE RecInstanceCode = 'REC201804.V1'
  AND EventTypeCode = 'RECEIPT-ALTAPAY' AND NostroCodeValue = '10106021' 
  ORDER BY EventDate 

SELECT *
  FROM [Reconciliations2].[rec].[tblInventoryEvent] WITH (NOLOCK) 
  WHERE RecInstanceCode = 'REC201804.V1'
  AND EventTypeCode = 'PAYMENT-VALITOR' AND NostroCodeValue = '10106021' 
  ORDER BY EventDate 

  SELECT *
  FROM [Reconciliations2].[rec].[tblInventoryEvent] WITH (NOLOCK) 
  WHERE RecInstanceCode = 'REC201804.V1'
  AND EventTypeCode = 'RECEIPT-CLIENTMONEY' AND NostroCodeValue = 'CAXTONCLEURA' 
  ORDER BY EventDate 

  
/**************************************************************************/

SELECT EventDate,name, EventSubTypeCode, SUM(ValueReportCcy)  FROM rec.tblInventoryEvent
INNER JOIN rec.tblEventType ON tblEventType.Code = tblInventoryEvent.EventTypeCode
WHERE RecInstanceCode = 'REC201805.V1'  AND SourceCode = 'Firebird'
GROUP BY EventDate,name, EventSubTypeCode 
ORDER BY EventDate


  SELECT DataInstanceCode, [FileName],
			MAX(TransactionDateTime) AS DateMax,
			MIN(TransactionDateTime) AS DateMin
		FROM [raw].[tblStatementIssuerValitor]
		WHERE TransactionKeyCategory IN ('Charge','Deposit') AND DataInstanceCode = 'REC201805.V1'
		GROUP BY DataInstanceCode, [FileName]