﻿
/* 
This is the select statement for the bank, with the right account numbers

Run on datawarehouse server tcp:qwdf2wsfp5.database.windows.net,1433
last run 01/05 for all april data 
*/
select	* 
from	rbs.statementfile 
where	[date] between '2018-04-01' and '2018-04-30'
and		[Account Number] in ('10106021',
'10108865',
'10123821',
'CAXTONCLEURA',
'CAXTONCLUSDA',
'CAXTONCLSEKA',
'CAXTONCLCHFA',
'CAXTONCLTHBC',
'CAXTONCLCADA',
'CAXTONCLDKKA',
'CAXTONCLZARA',
'CAXTONCLNOKA',
'CAXTONCLAUDA',
'CAXFX   ILSC',
'CAXTFX  HUFC',
'CAXTFXL CZKC',
'CAXTONCLPLNC',
'CAXTONCLNZDA',
'CAXTONCLAEDC',
'CAXTONCLJPYA',
'CAXTONCLSGDA',
'CAXTONCLHKDA',
'CAXTONCLINRC',
'CAXLTD  MXNC',
'CAXTONCLSARC',
'CAXTFXL TRYC',
'CAFLTRD MADC')
order	by date asc