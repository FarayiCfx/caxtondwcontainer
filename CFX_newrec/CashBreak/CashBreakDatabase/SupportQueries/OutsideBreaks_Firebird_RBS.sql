﻿--QUERY D: Settlements in RBS (FB version) during the period where date settled in FB is post period end 
SELECT act.Id AS [FBAccountTransactionId],
       act.TransactionDateTime AS [FBTransactionDateTime],
       ccy.BuyCcyCode AS [FBTransactionCurrency],
       act.Amount AS [FBValue],
       CAST(act.Amount / fxfb.Rate AS DECIMAL(28,2)) AS FBValueReportCcy,
       rbs.AccountTransactionId AS [RBSAccountTransactionId],
       rbs.TransactionDate AS [RBSTransactionDate],
       rbs.AccountNumber AS [RBSAccountNumber],
       rbs.AccountCurrency AS [RBSTransactionCurrency],
       CASE WHEN rbs.DebitValue > 0 THEN -rbs.DebitValue ELSE rbs.CreditValue END AS [RBSValue],
       CAST(CASE WHEN rbs.DebitValue > 0 THEN - rbs.DebitValue ELSE rbs.CreditValue END / fxrbs.Rate AS DECIMAL(28,2))  AS [RBSValueReportCcy]
FROM	[Settlement].RbsStatement AS rbs
INNER	JOIN [CurrencyBank].[AccountTransaction] AS act
ON		act.Id = rbs.AccountTransactionId
INNER	JOIN [CurrencyBank].[AccountSummary] AS acts
ON		act.AccountSummaryId = acts.Id
INNER	JOIN [CurrencyBank].[Currency] AS ccy
ON		ccy.Id = acts.CurrencyId
LEFT	OUTER JOIN [rec].[tblFXRate] AS fxfb
ON		fxfb.RecInstanceCode = 'REC201802.V1'
		AND fxfb.FromCcyCode = 'GBP'
		AND fxfb.ToCcyCode = ccy.BuyCcyCode
LEFT	OUTER JOIN [rec].[tblFXRate] AS fxrbs
ON		fxrbs.RecInstanceCode = 'REC201802.V1'
		--AND fxrbs.FromCcyCode = 'GBP'
		AND fxrbs.ToCcyCode = rbs.AccountCurrency
WHERE	rbs.[TransactionDate] >= '1 Feb 2017'
AND		rbs.[TransactionDate] < '19 Feb 2018'
AND		NOT ISNULL(rbs.[AccountTransactionId],0) = 0
AND		(act.TransactionDateTime >= '19 Feb 2018' AND act.ExternalTransRef = 'RBS')
order by RBSTransactionDate