﻿SELECT act.Id AS [FBAccountTransactionId]
		,acts.UserId
       , ccy.BuyCcyCode
	   ,act.TransactionDateTime AS [FBTransactionDateTime],
       ccy.BuyCcyCode AS [FBTransactionCurrency],
       act.Amount AS [FBValue],
       CAST(act.Amount / fxfb.Rate AS DECIMAL(28,2)) AS FBValueReportCcy,
       rbs.AccountTransactionId AS [RBSAccountTransactionId],
       rbs.TransactionDate AS [RBSTransactionDate],
       rbs.AccountNumber AS [RBSAccountNumber],
       rbs.AccountCurrency AS [RBSTransactionCurrency],
       CASE WHEN rbs.DebitValue > 0 THEN -rbs.DebitValue ELSE rbs.CreditValue END AS [RBSValue],
       CAST(CASE WHEN rbs.DebitValue > 0 THEN - rbs.DebitValue ELSE rbs.CreditValue END / fxrbs.Rate AS DECIMAL(28,2))  AS [RBSValueReportCcy]
FROM	[Settlement].RbsStatement AS rbs
INNER	JOIN [CurrencyBank].[AccountTransaction] AS act
ON		act.Id = rbs.AccountTransactionId
INNER	JOIN [CurrencyBank].[AccountSummary] AS acts
ON		act.AccountSummaryId = acts.Id
INNER	JOIN [CurrencyBank].[Currency] AS ccy
ON		ccy.Id = acts.CurrencyId
LEFT	OUTER JOIN [rec].[tblFXRate] AS fxfb
ON		fxfb.RecInstanceCode = 'REC201802.V1'
		AND fxfb.FromCcyCode = 'GBP'
		AND fxfb.ToCcyCode = ccy.BuyCcyCode
LEFT	OUTER JOIN [rec].[tblFXRate] AS fxrbs
ON		fxrbs.RecInstanceCode = 'REC201802.V1'
		--AND fxrbs.FromCcyCode = 'GBP'
		AND fxrbs.ToCcyCode = rbs.AccountCurrency
where	(act.TransactionDateTime >= '1 Feb 2018'  and act.TransactionDateTime < '19 Feb 2018')
AND		act.ExternalTransRef = 'RBS'
and		rbs.[TransactionDate] >= '19 Feb 2018'
order by 7 asc

/*
And here is a line by line showing for some of the diffs
select	* 
from	CurrencyBank.AccountTransaction
where	Id =  10070160

select	* from Settlement.RbsStatement where Id = 412044
*/