﻿/*
Use this query to identify records that have come in 
from ALTAPAY but do not have an external transaction id 
thus causing the insert for inventory event to fail due to 
the code value not accepting NULL when running 
[rec].[insInventoryEventRecMatchResult]
*/


DECLARE @startofmonth datetime

--SELECT	@startofmonth = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)

select	@startofmonth = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)

PRINT	@startofmonth

select	bb.UserId,aa.* 
from	currencybank.accounttransaction aa  
join	CurrencyBank.AccountSummary bb
on		aa.AccountSummaryId = bb.Id
where	aa.TransactionDateTime >= @startofmonth
and		aa.ExternalTransRef ='ALTAPAY'
AND		aa.EXTERNALTRANSACTIONID IS null
;


update currencybank.accounttransaction   
set		externaltransactionid = 0 
where	ExternalTransRef ='ALTAPAY'
AND		EXTERNALTRANSACTIONID IS null
