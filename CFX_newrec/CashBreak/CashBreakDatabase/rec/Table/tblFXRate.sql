﻿CREATE TABLE [rec].[tblFXRate]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	RecInstanceCode VARCHAR(50) NOT NULL,
	[Date] Date NOT NULL,
	FromCcyCode VARCHAR(50) NOT NULL,
	ToCcyCode VARCHAR(50) NOT NULL,
	Rate Decimal(28,8) NOT NULL,
	SettlementDays SMALLINT NULL,
	INDEX idxToCcyCode CLUSTERED (ToCcyCode,[Date],FromCcyCode)
)