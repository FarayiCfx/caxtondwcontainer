﻿CREATE TABLE [rec].[tblInventoryEventRecMatchResult]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	RecInstanceCode VARCHAR(50) NOT NULL,
	RecId SMALLINT NOT NULL,
	InventoryEventId BIGINT NOT NULL,
	EventTypeCode VARCHAR(50) NULL, --DENORMALISED
	MatchResultId BIGINT NOT NULL,
	MatchTypeCode VARCHAR(50) NULL, --DENORMALISED
	MappingTypeCode VARCHAR(50) NULL, --DENORMALISED
	BreakReasonCode VARCHAR(50) NULL,
	BreakReasonDescription VARCHAR(1000) NULL,
	IsExplained BIT NOT NULL DEFAULT 0,
	[CDateTime]	 DATETIMEOFFSET(7)	NOT NULL DEFAULT(SYSDATETIMEOFFSET()),
	[CBy] NVARCHAR(255)	NOT NULL DEFAULT(SUSER_NAME()),
	[CHost]	 NVARCHAR(255) NOT NULL DEFAULT(HOST_NAME()),
	CONSTRAINT FK_tblInventoryEventRecMatchResult_RecId FOREIGN KEY ([RecId]) REFERENCES [rec].[tblRec](Id),
	CONSTRAINT FK_tblInventoryEventRecMatchResult_InventoryEventId FOREIGN KEY (InventoryEventId) REFERENCES [rec].[tblInventoryEvent](Id),
	CONSTRAINT FK_tblInventoryEventRecMatchResult_MatchResultId FOREIGN KEY (MatchResultId) REFERENCES [rec].[tblMatchResult](Id),
	--CONSTRAINT FK_tblBreakReason FOREIGN KEY ([BreakReasonCode]) REFERENCES [rec].[tblBreakReason](Code),
	INDEX idxInventoryEventId CLUSTERED (RecInstanceCode,InventoryEventId, RecId, MatchResultId),
	INDEX idxMatchResultId NONCLUSTERED (MatchResultId)
);