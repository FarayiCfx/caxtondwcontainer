﻿CREATE TABLE [rec].[tblBreakReason]
(
	[Id] SMALLINT NOT NULL IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	Code VARCHAR(50) NOT NULL,
	Name VARCHAR(500) NOT NULL,
	Description VARCHAR(1000) NOT NULL,
	CONSTRAINT UQ_tblBreakReason_Code UNIQUE CLUSTERED (Code)
)
