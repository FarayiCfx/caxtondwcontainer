﻿CREATE TABLE [rec].[tblInventoryEventMapping]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	RecInstanceCode VARCHAR(50) NOT NULL,
	MappingTypeCode VARCHAR(50) NOT NULL,
	CodeSchema VARCHAR(50) NOT NULL,
	CodeValue VARCHAR(500) NOT NULL,
	InventoryEventCodeSchema VARCHAR(50) NOT NULL,
	InventoryEventCodeValue VARCHAR(500) NOT NULL,
	[CDateTime]	 DATETIMEOFFSET(7)	NOT NULL DEFAULT(SYSDATETIMEOFFSET()),
	[CBy] NVARCHAR(255)	NOT NULL DEFAULT(SUSER_NAME()),
	[CHost]	 NVARCHAR(255) NOT NULL DEFAULT(HOST_NAME()),
	CONSTRAINT UQ_RecInstanceCode UNIQUE CLUSTERED (RecInstanceCode, CodeValue, MappingTypeCode, InventoryEventCodeValue, CodeSchema, InventoryEventCodeSchema),
	INDEX idxInventoryEvent NONCLUSTERED (InventoryEventCodeValue,InventoryEventCodeSchema)
);