﻿CREATE TABLE [rec].[tblMatchResult]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	RecInstanceCode VARCHAR(50) NOT NULL,
	[RecId] SMALLINT NOT NULL,
	[Side0SourceTypeCode] VARCHAR(50) NOT NULL,
	[Side1SourceTypeCode] VARCHAR(50) NOT NULL,
	EventTypeCode VARCHAR(50) NOT NULL,
	MatchTypeCode VARCHAR(50) NOT NULL,
	MappingTypeCode  VARCHAR(50) NOT NULL,
	MappingTypePriority  SMALLINT NOT NULL,
	MappingCodeSchema VARCHAR(50) NOT NULL,
	MappingCodeValue VARCHAR(1000) NOT NULL,
	InstrumentCode VARCHAR(50) NOT NULL,
	MaxSettlementDateSide0 DATE NULL,
	MaxSettlementDateSide1 DATE NULL,
	--Base Currency
	QuantitySide0 DECIMAL(28,2) NULL,
	QuantitySide1 DECIMAL(28,2) NULL,
	QuantityDelta AS ISNULL(QuantitySide1,0) - ISNULL(QuantitySide0,0),
	--Report Currency
	ReportCcyCode VARCHAR(50) NULL,
	ValueReportCcySide0 DECIMAL(28,2) NULL,
	ValueReportCcySide1 DECIMAL(28,2) NULL,
	ValueReportCcyDelta AS ISNULL(ValueReportCcySide1,0) - ISNULL(ValueReportCcySide0,0),
	FXRateReportCcy DECIMAL (28,8) NULL,
	--Audit
	[CDateTime]	 DATETIMEOFFSET(7)	NOT NULL DEFAULT(SYSDATETIMEOFFSET()),
	[CBy] NVARCHAR(255)	NOT NULL DEFAULT(SUSER_NAME()),
	[CHost]	 NVARCHAR(255) NOT NULL DEFAULT(HOST_NAME()),
	CONSTRAINT FK_tblMatchResult_RecId FOREIGN KEY ([RecId]) REFERENCES [rec].[tblRec](Id),
	INDEX idxMappingCode CLUSTERED (RecInstanceCode,MappingCodeValue,MappingCodeSchema)
);