﻿CREATE TABLE [rec].[tblRecScope]
(
	[Id] SMALLINT NOT NULL IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	[RecId] SMALLINT NOT NULL,
	[SourceCode] VARCHAR(50) NOT NULL,
	[SourceSetCode] VARCHAR(50) NULL,
	[EventTypeCode] VARCHAR(50) NULL,
	[MappingTypeCode] VARCHAR(50) NOT NULL,
	[MappingTypePriority] SMALLINT NOT NULL,
	[SourceTypeCode] VARCHAR(50) NOT NULL,
	CONSTRAINT FK_tblRecScope_RecId FOREIGN KEY ([RecId]) REFERENCES [rec].[tblRec](Id),
	CONSTRAINT CK_SourceTypeCode CHECK ([SourceTypeCode] IN ('TRANSACTION','SETTLEMENT','STATEMENT','CALCULATED')),
	INDEX idxCode CLUSTERED ([RecId],[SourceCode],[SourceSetCode])
)
