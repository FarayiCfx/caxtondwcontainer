﻿CREATE VIEW [rec].[vwRecExceptions]
AS 
	
SELECT	inv.RecInstanceCode,
		r.Code AS RecCode, r.Name AS RecName, 
		inv.SourceTypeCode ,inv.SourceCode,inv.SourceSetCode,
		inv.EventTypeCode, ISNULL(et.Name, inv.EventTypeCode) AS EventTypeName, ISNULL(et.Description, inv.EventTypeCode) AS EventTypeDescription,
		inv.EventSubTypeCode,
		inv.CodeSchema,inv.CodeValue,
		inv.NostroCodeSchema,inv.NostroCodeValue,inv.NostroName,inv.EventDateTime,inv.EventDate,inv.ClientId,
		inv.InstrumentCode,inv.Quantity,inv.ReportCcyCode,inv.ValueReportCcy,inv.FXRateReportCcy,invrmr.MatchResultId,
		invrmr.MatchTypeCode,invrmr.MappingTypeCode,
		invrmr.BreakReasonCode, ISNULL(br.Name, invrmr.BreakReasonCode) AS BreakReasonName, ISNULL(br.Description, invrmr.BreakReasonCode) AS BreakReasonDescription,
		invrmr.BreakReasonDescription AS Notes,
		invrmr.IsExplained,
		mr.QuantityDelta AS BreakQuantity,mr.ValueReportCcyDelta AS BreakValueReportCcy
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblRecInstance] AS ri
	ON ri.[Code] = inv.RecInstanceCode
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr 
	ON invrmr.RecInstanceCode = ri.[Code]
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblMatchResult] AS mr 
	ON mr.RecInstanceCode = ri.[Code]
	AND mr.Id = invrmr.MatchResultId
INNER JOIN [rec].[tblRec] AS r
	ON r.[Id] = invrmr.[RecId]
LEFT OUTER JOIN [rec].[tblEventType] AS et
	ON [et].[Code] = [inv].[EventTypeCode]
LEFT OUTER JOIN [rec].[tblBreakReason] AS br
	ON [br].[Code] = [invrmr].[BreakReasonCode]
WHERE invrmr.MatchTypeCode = 'NON-MATCHED'
AND ri.ExcelInclude = 1

GO