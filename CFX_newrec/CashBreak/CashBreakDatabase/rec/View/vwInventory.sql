﻿CREATE VIEW [rec].[vwInventory]
AS 
	
SELECT	x.RecInstanceCode,x.SourceCode,x.SourceTypeCode,x.SourceSetCode,
		x.EventTypeCode, ISNULL(et.Name, 'UNKNOWN') AS EventTypeName, ISNULL(et.Description, 'UNKNOWN') AS EventTypeDescription,
		x.EventSubTypeCode,
		x.GroupCodeSchema,x.GroupCodeValue,
		x.CodeSchema,x.CodeValue,
		x.NostroCodeSchema,x.NostroCodeValue,x.NostroName,
		x.EventDateTime,x.EventDate,x.SettlementDate,x.IsSettlementDateExpected,
		x.TransactionTypeCode,
		x.ClientId,x.TradeId,x.TradeTypeCode,x.TradeLegCode,
		x.InstrumentCode,
		x.Quantity,
		x.QuantityNegative,x.QuantityPositive,
		x.ReportCcyCode,
		x.ValueReportCcy,
		x.ValueReportCcyNegative,x.ValueReportCcyPositive,
		x.FXRateReportCcy,
		x.CDateTime,x.CBy,x.CHost,
		x.Notes
FROM [rec].[tblInventoryEvent] AS x
INNER JOIN [rec].[tblRecInstance] AS ri
	ON ri.[Code] = x.RecInstanceCode
LEFT OUTER JOIN [rec].[tblEventType] AS et
	ON [et].[Code] = [x].[EventTypeCode]
WHERE ri.ExcelInclude = 1
GO