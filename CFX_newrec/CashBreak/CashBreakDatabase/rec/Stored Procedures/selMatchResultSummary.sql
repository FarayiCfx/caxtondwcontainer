﻿CREATE PROCEDURE [rec].[selMatchResultSummary]
	@RecInstanceCode VARCHAR(50),
	@IsDebugOn BIT = 0
AS
SET NOCOUNT ON

DECLARE @Rowcount BIGINT;

--STATS
SELECT r.Code AS RecCode, r.Name AS RecName, inv.SourceCode, inv.SourceSetCode, invrmr.MatchTypeCode, invrmr.BreakReasonCode, mr.ReportCcyCode, COUNT(*) AS [Count], SUM(ValueReportCcyDelta) AS ValueReportCcy
FROM [rec].[tblInventoryEvent] AS inv
LEFT OUTER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr 
	ON invrmr.InventoryEventId = inv.Id
	AND invrmr.RecInstanceCode = @RecInstanceCode
LEFT OUTER JOIN [rec].[tblMatchResult] AS mr 
	ON mr.Id = invrmr.MatchResultId
	AND mr.RecInstanceCode = @RecInstanceCode
LEFT OUTER JOIN [rec].[tblRec] AS r
	ON r.[Id] = invrmr.[RecId]
WHERE inv.RecInstanceCode = @RecInstanceCode
GROUP BY r.Code, r.Name, inv.SourceCode, inv.SourceSetCode, invrmr.MatchTypeCode, invrmr.BreakReasonCode, mr.ReportCcyCode
ORDER BY r.Code, inv.SourceCode, inv.SourceSetCode, invrmr.MatchTypeCode



GO