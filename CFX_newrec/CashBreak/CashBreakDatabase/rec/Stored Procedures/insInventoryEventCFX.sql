﻿CREATE PROCEDURE [rec].[insInventoryEventCFX]
	@RecInstanceCode VARCHAR(50),
	@DateFrom DATE,
	@DateTo DATE,
	@IsDebugOn BIT = 0
AS
SET NOCOUNT ON

/*
EXEC [rec].[insMatchResultBatch]
	@RecInstanceCode='REC201708.V1',
	@DateFrom='1 Aug 2017',
	@DateTo='31 Aug 2017',
	@IsDebugOn=1
*/
DECLARE @Rowcount BIGINT;
DECLARE @DateToAdj DATE;
SET @DateToAdj = DATEADD(DAY,1, @DateTo);

DECLARE @RBSBANKFeeReportCcyDelta DECIMAL(28,2);
SET @RBSBANKFeeReportCcyDelta = 10.00;

DELETE FROM [rec].[tblInventoryEventRecMatchResult] WHERE RecInstanceCode = @RecInstanceCode;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Deleted records from [rec].[tblInventoryEventRecMatchResult]: ' + CAST(@Rowcount AS VARCHAR);
END

DELETE FROM [rec].[tblMatchResult] WHERE RecInstanceCode = @RecInstanceCode;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Deleted records from [rec].[tblMatchResult]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Build Inventory Event List for Rec Instance Code ' + @RecInstanceCode;
END

DELETE FROM [rec].[tblInventoryEvent] WHERE RecInstanceCode = @RecInstanceCode;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Delete records from [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

DELETE FROM [rec].[tblInventoryEventMapping] WHERE RecInstanceCode = @RecInstanceCode;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Delete records from [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Build TRADE List for Rec Instance Code ' + @RecInstanceCode;
END


--TRADES
INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD-TRADE' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'TRADE' AS SourceSetCode,
		 CASE 
			WHEN tr.TradeReason = 'Guranteed Buy-Back' AND NOT tr.UserId = 'val_monitor'THEN 'TRADE-GUARANTEEDBUYBACK'
			WHEN tr.TradeReason = 'Card load' AND NOT tr.UserId = 'val_monitor' THEN 'TRADE-CARDLOAD'
			WHEN tr.TradeReason = 'Buy and Hold' AND NOT tr.UserId = 'val_monitor' THEN 'TRADE-BUYHOLD'
			WHEN tr.TradeReason = 'Buy and Send' AND NOT tr.UserId = 'val_monitor' THEN 'TRADE-BUYSEND'
			WHEN tr.UserId = 'val_monitor' THEN 'TRADE-VALMONITOR'
			ELSE 'TRADE-OTHER'
		END AS EventTypeCode,
		CASE
			WHEN tr.Reversal = 1 THEN 'REVERSAL'
			WHEN tr.IsReversed = 1 THEN 'IS-REVERSED'
			WHEN tr.IsUnwound = 1 THEN 'IS-UNWOUND'
			WHEN trd.Amount = 0 THEN 'REVERSAL-OTHER'
			ELSE ''
		END AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'FB-TRADEID' AS CodeSchema,
		CAST(tr.Id AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		tr.TradeDate AS EventDateTime,
		CAST(tr.TradeDate AS DATE) AS EventDate,
		CAST(ISNULL(tr.TradeEndDate, tr.TradeEndDate) AS DATE) AS SettlementDate,
		CASE
			WHEN trd.TradeId IS NULL OR trd.Amount = tr.SellAmount THEN 1 
			ELSE 0
		END AS IsSettlementDateExpected,
		tr.UserId AS ClientId,
		NULL AS TransactionTypeCode,
		tr.Id AS TradeId,
		tr.TradeReason AS TradeTypeCode,
		'SELL' AS TradeLegCode, 
		tr.SellCcyCode AS InstrumentCode,
		tr.SellAmount AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		tr.SellAmount / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'UserId: ' + ISNULL(tr.UserId,'<NULL>') + ' Sell: ' + tr.SellCcyCode + ' ' + CAST(tr.SellAmount AS VARCHAR) + ' Buy: ' + tr.BuyCcyCode + ' ' + CAST(tr.BuyAmount AS VARCHAR) + ' Trade Notes: ' + ISNULL(tr.TradeNotes,'')   AS Notes
FROM [CurrencyBank].[Trade] AS tr
LEFT OUTER JOIN
		(SELECT TradeId, Sum(Amount) AS Amount
		 FROM [CurrencyBank].[TradeDetail] AS trd
		 GROUP BY TradeId) AS trd
		ON trd.TradeId = tr.Id
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = tr.SellCcyCode
WHERE tr.FB_DateDeleted IS NULL
AND tr.TradeDate BETWEEN @DateFrom AND @DateToAdj
UNION ALL
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD-TRADE' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'TRADE' AS SourceSetCode,
		 CASE 
			WHEN tr.TradeReason = 'Guranteed Buy-Back' AND NOT tr.UserId = 'val_monitor'THEN 'TRADE-GUARANTEEDBUYBACK'
			WHEN tr.TradeReason = 'Card load' AND NOT tr.UserId = 'val_monitor' THEN 'TRADE-CARDLOAD'
			WHEN tr.TradeReason = 'Buy and Hold' AND NOT tr.UserId = 'val_monitor' THEN 'TRADE-BUYHOLD'
			WHEN tr.TradeReason = 'Buy and Send' AND NOT tr.UserId = 'val_monitor' THEN 'TRADE-BUYSEND'
			WHEN tr.UserId = 'val_monitor' THEN 'TRADE-VALMONITOR'
			ELSE 'TRADE-OTHER'
		END AS EventTypeCode,
		CASE
			WHEN tr.Reversal = 1 THEN 'REVERSAL'
			WHEN tr.IsReversed = 1 THEN 'IS-REVERSED'
			WHEN tr.IsUnwound = 1 THEN 'IS-UNWOUND'
			WHEN trd.Amount = 0 THEN 'REVERSAL-OTHER'
			ELSE ''
		END AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'FB-TRADEID' AS CodeSchema,
		CAST(tr.Id AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		tr.TradeDate AS EventDateTime,
		CAST(tr.TradeDate AS DATE) AS EventDate,
		CAST(ISNULL(tr.TradeEndDate, tr.TradeEndDate) AS DATE) AS SettlementDate,
		CASE
			WHEN trd.TradeId IS NULL OR trd.Amount = tr.SellAmount THEN 1 
			ELSE 0
		END AS IsSettlementDateExpected,
		tr.UserId AS ClientId,
		NULL AS TransactionTypeCode,
		tr.Id AS TradeId,
		tr.TradeReason AS TradeTypeCode,
		'BUY' AS TradeLegCode, 
		tr.BuyCcyCode AS InstrumentCode,
		-tr.BuyAmount AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		-tr.BuyAmount / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'UserId: ' + ISNULL(tr.UserId,'<NULL>') + ' Sell: ' + tr.SellCcyCode + ' ' + CAST(tr.SellAmount AS VARCHAR) + ' Buy: ' + tr.BuyCcyCode + ' ' + CAST(tr.BuyAmount AS VARCHAR) + ' Trade Notes: ' + ISNULL(tr.TradeNotes,'')   AS Notes
FROM [CurrencyBank].[Trade] AS tr
LEFT OUTER JOIN
		(SELECT TradeId, Sum(Amount) AS Amount
		 FROM [CurrencyBank].[TradeDetail] AS trd
		 GROUP BY TradeId) AS trd
		ON trd.TradeId = tr.Id
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = tr.BuyCcyCode
WHERE tr.FB_DateDeleted IS NULL
AND tr.TradeDate BETWEEN @DateFrom AND @DateToAdj;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted FIREBIRD TRADE records into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Build TRADE List for Rec Instance Code ' + @RecInstanceCode;
END



IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Build TRANSACTION List for Rec Instance Code ' + @RecInstanceCode;
END


--TRADES & COMPLETED TRANSFERS
INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'RBSBANK' AS SourceSetCode,
		CASE
			WHEN x.Status = 'Credit for rejected transfer' THEN 'RECEIPT-CLIENTMONEY'
			WHEN x.Status = 'Currency deposited' THEN 'RECEIPT-CLIENTMONEY'
			WHEN x.Status LIKE 'Debit for payaway%' THEN 'PAYMENT-BENEFICIARY'
			WHEN x.Status LIKE '<HD>%' THEN 'ADJUSTMENT'
			ELSE 'UNCATEGORISED'
		END AS EventTypeCode,
		CASE
			WHEN rbs.AccountTransactionId IS NOT NULL THEN 'RBS-MATCHED'
			ELSE ''
		END AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'FB-ACCTTRANID' AS CodeSchema,
		CAST(x.Id AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		x.TransactionDateTime AS EventDateTime,
		CAST(x.TransactionDateTime AS DATE) AS EventDate,
		CAST(ISNULL(t.TradeEndDate, x.TransactionDateTime) AS DATE) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		acs.UserId AS ClientId,
		CASE
			WHEN ExternalTransRef = 'RBS' AND Status Like 'Debit for payaway%'
				THEN 'Transfer'
			ELSE x.ExternalTransRef
		END AS TransactionTypeCode,
		t.Id AS TradeId,
		t.TradeReason AS TradeTypeCode,
		CASE
			WHEN t.Id IS NOT NULL THEN 
				CASE WHEN x.Amount <= 0 THEN 'BUY' ELSE 'SELL' END
			ELSE NULL
		END AS TradeLegCode, 
		ccy.BuyCcyCode AS InstrumentCode,
		CASE WHEN x.ExternalTransRef = 'Trade' THEN -x.Amount ELSE x.Amount END AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		CASE WHEN x.ExternalTransRef = 'Trade' THEN -x.Amount ELSE x.Amount END / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		x.Status AS Notes
FROM [CurrencyBank].[AccountTransaction] AS x
INNER JOIN [CurrencyBank].[AccountSummary] AS acs
	ON acs.Id = x.AccountSummaryId
INNER JOIN [CurrencyBank].[Currency] AS ccy
	ON ccy.Id = acs.CurrencyId
LEFT OUTER JOIN (SELECT DISTINCT rbs.AccountTransactionId
				FROM [Settlement].[RbsStatement] AS rbs
				WHERE NOT rbs.AccountTransactionId = 0
				AND rbs.AccountTransactionId IS NOT NULL
				AND NOT rbs.TransactionType = 'CHG'
				AND rbs.TransactionDate >= @DateFrom AND rbs.TransactionDate < @DateToAdj) AS rbs
	ON rbs.AccountTransactionId = x.Id
LEFT OUTER JOIN [CurrencyBank].[Trade] AS t
	ON t.Id = CASE WHEN ISNUMERIC(x.ExternalTransactionId) = 1 AND x.ExternalTransRef = 'Trade' THEN x.ExternalTransactionId ELSE NULL END
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = ccy.BuyCcyCode
WHERE x.TransactionDateTime >= @DateFrom AND x.TransactionDateTime < @DateToAdj
AND x.ExternalTransRef = 'RBS'
AND x.FB_DateDeleted IS NULL;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted RBS TRANSACTION records into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END
	
--MARKET TRADES
INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'RBSMARKET' AS SourceSetCode,
		'PAYMENT-COUNTERPARTY' AS EventTypeCode,
		'' AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'FB-MKTTRANID' AS CodeSchema,
		CAST(x.Id AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		x.DateCreated AS EventDateTime,
		CAST(x.DateCreated AS DATE) AS EventDate,
		CAST(x.DateCreated AS DATE) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		'CFX' AS ClientId,
		'MARKET' AS TransactionTypeCode,
		x.Id AS TradeId,
		'MARKET' AS TradeTypeCode,
		'SELL' AS TradeLegCode, 
		x.SellCcy AS InstrumentCode,
		-x.SellCcyAmount AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		-x.SellCcyAmount / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'Counterparty Ref No.: ' + ISNULL(x.CounterpartyRefNo,'<NULL>') + ' Book: ' + ISNULL(x.BookName,'<NULL>')
FROM [CurrencyBank].[MarketTrade] AS x
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = x.SellCcy
WHERE x.DateCreated >= @DateFrom AND x.DateCreated < @DateToAdj
AND NOT x.BuyCcyAmount = 0
AND x.FB_DateDeleted IS NULL
UNION
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'RBSMARKET' AS SourceSetCode,
		'PAYMENT-COUNTERPARTY' AS EventTypeCode,
		'' AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'FB-MKTTRANID' AS CodeSchema,
		CAST(x.Id AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		x.DateCreated AS EventDateTime,
		CAST(x.DateCreated AS DATE) AS EventDate,
		CAST(x.DateCreated AS DATE) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		'CFX' AS ClientId,
		'MARKET' AS TransactionTypeCode,
		x.Id AS TradeId,
		'MARKET' AS TradeTypeCode,
		'BUY' AS TradeLegCode, 
		x.BuyCcy AS InstrumentCode,
		x.BuyCcyAmount AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		x.BuyCcyAmount / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'Counterparty Ref No.: ' + ISNULL(x.CounterpartyRefNo,'<NULL>') + ' Book: ' + ISNULL(x.BookName,'<NULL>')
FROM [CurrencyBank].[MarketTrade] AS x
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = x.BuyCcy
WHERE x.DateCreated >= @DateFrom AND x.DateCreated < @DateToAdj
AND NOT x.BuyCcyAmount = 0
AND x.FB_DateDeleted IS NULL;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted RBS Market TRANSACTION records into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

--ALTAPAY records need to be aggregated as there are multiple per Payment reference which is used to match
	
INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'ALTAPAY' AS SourceSetCode,
		'RECEIPT-ALTAPAY-TRANSACTION' AS EventTypeCode,
		CASE WHEN x.Amount < 0 THEN 'REFUND' ELSE 'PAYMENT' END AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'ALTAPAY-KEY' AS CodeSchema,
		ExternalTransactionId + '|' + ccy.BuyCcyCode + '|' + CASE WHEN x.Amount < 0 THEN 'REFUND' ELSE 'PAYMENT' END AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		MAX(CAST(x.TransactionDateTime AS DATE)) AS EventDateTime,
		MAX(CAST(x.TransactionDateTime AS DATE)) AS EventDate,
		MAX(CAST(x.TransactionDateTime AS DATE)) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		MAX(CASE WHEN acs.UserId IN ('cpp_monitor','gbb_monitor','val_monitor') THEN NULL ELSE acs.UserId END) AS ClientId,--Remove system users
		NULL AS TransactionTypeCode,
		NULL AS TradeId,
		NULL AS TradeTypeCode,
		NULL AS TradeLegCode, 
		ccy.BuyCcyCode AS InstrumentCode,
		SUM(x.Amount) AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		SUM(x.Amount) / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'' AS Notes
FROM [CurrencyBank].[AccountTransaction] AS x
INNER JOIN [CurrencyBank].[AccountSummary] AS acs
	ON acs.Id = x.AccountSummaryId
INNER JOIN [CurrencyBank].[Currency] AS ccy
	ON ccy.Id = acs.CurrencyId
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = ccy.BuyCcyCode
WHERE x.TransactionDateTime >= @DateFrom AND x.TransactionDateTime < @DateToAdj
AND ExternalTransRef = 'ALTAPAY'
AND x.FB_DateDeleted IS NULL
--AND NOT Status like '%\_load' ESCAPE '\' -- VALIDATE IF CORRECT
GROUP BY 
		CASE WHEN x.Amount < 0 THEN 'REFUND' ELSE 'PAYMENT' END,
		ExternalTransactionId + '|' + ccy.BuyCcyCode + '|' + CASE WHEN x.Amount < 0 THEN 'REFUND' ELSE 'PAYMENT' END,
		x.ExternalTransRef, ccy.BuyCcyCode, fx.FromCcyCode, fx.Rate;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted ALTAPAY TRANSACTION records into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

--VALITOR Transactions
INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'FIREBIRD' AS SourceCode,
		'TRANSACTION' AS SourceTypeCode,
		'VALITOR' AS SourceSetCode,
		'PAYMENT-DEPOSIT-DIRECT' AS EventTypeCode,
		CASE ExternalTransRef WHEN 'Unload' THEN 'UNLOAD' ELSE 'LOAD' END AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		CASE WHEN LEN(x.ExternalTransactionId) < 10 THEN 'VALITOR-ID' ELSE 'VALITOR-REFID' END AS CodeSchema, 
		CAST(x.ExternalTransactionId AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		MAX(x.TransactionDateTime) AS EventDateTime,
		CAST(MAX(x.TransactionDateTime) AS DATE) AS EventDate,
		CAST(MAX(x.TransactionDateTime) AS DATE) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		MAX(CASE WHEN acs.UserId IN ('cpp_monitor','gbb_monitor','val_monitor') THEN NULL ELSE acs.UserId END) AS ClientId, --Remove system users
		NULL AS TransactionTypeCode,
		NULL AS TradeId,
		NULL AS TradeTypeCode,
		NULL AS TradeLegCode, 
		ccy.BuyCcyCode AS InstrumentCode,
		SUM(x.Amount) AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		SUM(x.Amount) / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'' AS Notes
FROM [CurrencyBank].[AccountTransaction] AS x
INNER JOIN [CurrencyBank].[AccountSummary] AS acs
	ON acs.Id = x.AccountSummaryId
INNER JOIN [CurrencyBank].[Currency] AS ccy
	ON ccy.Id = acs.CurrencyId
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = ccy.BuyCcyCode
WHERE x.TransactionDateTime >= @DateFrom AND x.TransactionDateTime < @DateToAdj
AND x.FB_DateDeleted IS NULL
AND ExternalTransRef IN ('VALITOR','Unload')
GROUP BY 
	CASE WHEN LEN(x.ExternalTransactionId) < 10 THEN 'VALITOR-ID' ELSE 'VALITOR-REFID' END,
	CAST(x.ExternalTransactionId AS VARCHAR),
	CASE ExternalTransRef WHEN 'Unload' THEN 'UNLOAD' ELSE 'LOAD' END,
	x.ExternalTransRef, ccy.BuyCcyCode, fx.FromCcyCode, fx.Rate;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted VALITOR TRANSACTION records into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Build TRANSACTION List for Rec Instance Code ' + @RecInstanceCode;
END
	
IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Check for Duplicate TRANSACTIONS ' + @RecInstanceCode;
END

--CHECK FOR DUPLICATED
IF (SELECT COUNT(*) FROM (
	SELECT CodeSchema,CodeValue,TradeLegCode, COUNT(*) AS num
	FROM [rec].[tblInventoryEvent]
	WHERE RecInstanceCode = @RecInstanceCode
	AND SourceTypeCode = 'TRANSACTION'
	GROUP BY CodeSchema,CodeValue,TradeLegCode
	HAVING COUNT(*) > 1) AS x) > 0
BEGIN 
	PRINT 'DUPLICATES FOUND !!!!!!!!!!!! Stopping process';

	SELECT 'DUPLICATE', CodeSchema,CodeValue,TradeLegCode, COUNT(*) AS num
	FROM [rec].[tblInventoryEvent]
	WHERE RecInstanceCode = @RecInstanceCode
	AND SourceTypeCode = 'TRANSACTION'
	GROUP BY CodeSchema,CodeValue,TradeLegCode
	HAVING COUNT(*) > 1

	RETURN
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'No duplicate transactions found';
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Check for Duplicate TRANSACTIONS ' + @RecInstanceCode;
END
	
IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Insert TRANSACTION Mapping Data ' + @RecInstanceCode;
END
	
INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	DISTINCT
		@RecInstanceCode AS RecInstanceCode,
		CASE 
			WHEN t.CodeSchema = 'ALTAPAY-KEY' THEN 'STATEMENT-ID'
			WHEN t.CodeSchema = 'VALITOR-REFID' THEN 'STATEMENT-REFID'
			WHEN t.CodeSchema = 'VALITOR-ID' THEN 'STATEMENT-ID'
			ELSE 'TRANSACTION-ID'
		END AS MappingTypeCode,
		t.CodeSchema AS CodeSchema,
		t.CodeValue AS CodeValue,
		t.CodeSchema AS InventoryEventCodeSchema,
		t.CodeValue AS InventoryEventCodeValue
FROM [rec].[tblInventoryEvent] AS t
WHERE t.RecInstanceCode = @RecInstanceCode
AND t.SourceTypeCode = 'TRANSACTION'
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode =
		CASE 
			WHEN t.CodeSchema = 'ALTAPAY-KEY' THEN 'STATEMENT-ID'
			WHEN t.CodeSchema = 'VALITOR-REFID' THEN 'STATEMENT-REFID'
			WHEN t.CodeSchema = 'VALITOR-ID' THEN 'STATEMENT-ID'
			ELSE 'TRANSACTION-ID'
		END
	AND s.CodeSchema = t.CodeSchema
	AND s.CodeValue = t.CodeValue
	AND s.InventoryEventCodeSchema = t.CodeSchema
	AND s.InventoryEventCodeValue = t.CodeValue);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted TRANSACTION Mapping records (TransactionId) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END
	
INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	DISTINCT
		@RecInstanceCode AS RecInstanceCode,
		'SETTLEMENT-LREF' AS MappingTypeCode,
		'LREF' AS CodeSchema,
		x.CounterpartyRefNo AS CodeValue,
		t.CodeSchema AS InventoryEventCodeSchema,
		t.CodeValue AS InventoryEventCodeValue
FROM [rec].[tblInventoryEvent] AS t
INNER JOIN [CurrencyBank].[MarketTrade] AS x 
	ON x.Id = CASE WHEN ISNUMERIC(t.CodeValue) = 1 THEN CAST(t.CodeValue AS BIGINT) ELSE NULL END
	AND x.FB_DateDeleted IS NULL
WHERE t.RecInstanceCode = @RecInstanceCode
AND t.SourceTypeCode = 'TRANSACTION'
AND SourceCode = 'FIREBIRD'
AND SourceSetCode = 'RBSMARKET'
AND TradeLegCode = 'BUY'
AND (x.CounterpartyRefNo LIKE 'L1%'
	OR x.CounterpartyRefNo LIKE 'N1%'
	OR x.CounterpartyRefNo LIKE 'N2%'
	OR x.CounterpartyRefNo LIKE 'S1%')
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'SETTLEMENT-LREF'
	AND s.CodeSchema = 'LREF'
	AND s.CodeValue = x.CounterpartyRefNo
	AND s.InventoryEventCodeSchema = t.CodeSchema
	AND s.InventoryEventCodeValue = t.CodeValue);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted Market TRANSACTION Mapping records (Market Trade LREF) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END
	
INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	DISTINCT
		@RecInstanceCode AS RecInstanceCode,
		'CLIENT-ID' AS MappingTypeCode,
		'CLIENTID' AS CodeSchema,
		t.ClientId AS CodeValue,
		t.CodeSchema AS InventoryEventCodeSchema,
		t.CodeValue AS InventoryEventCodeValue
FROM [rec].[tblInventoryEvent] AS t
WHERE t.RecInstanceCode = @RecInstanceCode
AND t.SourceTypeCode = 'TRANSACTION'
AND t.ClientId IS NOT NULL
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'CLIENT-ID'
	AND s.CodeSchema = 'CLIENTID'
	AND s.CodeValue = t.ClientId
	AND s.InventoryEventCodeSchema = t.CodeSchema
	AND s.InventoryEventCodeValue = t.CodeValue);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted TRANSACTION Mapping records (CLIENTID) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END
	
IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Insert TRANSACTION Mapping Data ' + @RecInstanceCode;
END

/* SETTLEMENT */

IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Build SETTLEMENT List for Rec Instance Code ' + @RecInstanceCode;
END

INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'RBSBANK' AS SourceCode,
		'SETTLEMENT' AS SourceTypeCode,
		CASE
			--Market Trades
			WHEN LEFT([Narrative2],2) IN ('L1','N1','N2','S1') THEN 'RBSMARKET'
			ELSE 'RBSBANK'
		END AS SourceSetCode,
		CASE
			--Market Trades
			WHEN LEFT([Narrative2],2) IN ('L1','N1','N2','S1') THEN 'PAYMENT-COUNTERPARTY'
			--Beneficiary Payment
			WHEN LEFT([Narrative1],8) = 'TRANREF:' THEN 'PAYMENT-BENEFICIARY'
			--AltaPay (Valitor) (RECEIPT)
			WHEN (
				UPPER(LEFT([Narrative2],7)) = 'VALITOR' OR UPPER(LEFT([Narrative3],7)) = 'VALITOR')
				AND NOT (UPPER([Narrative1]) LIKE 'CAXTON MFA GBP%'
						OR UPPER([Narrative1]) = 'VALITOR BUFFER'
						OR UPPER([Narrative3]) = 'VALITOR BUFFER'
						OR (UPPER([Narrative3]) = 'VALITOR HF' AND UPPER([Narrative1]) IN ('REFUND','NONREF'))
						)
				THEN 'RECEIPT-ALTAPAY'
			--AltaPay (Credorax) (RECEIPT)
			WHEN UPPER(LEFT([Narrative3],8)) = 'CREDORAX' THEN 'RECEIPT-ALTAPAY'
			--Valitor (PAYMENT)
			WHEN UPPER(LEFT([Narrative1],8)) = 'VALITOR ' 
					OR (UPPER([Narrative1]) LIKE 'CAXTON MFA GBP%'
						OR UPPER([Narrative1]) = 'VALITOR BUFFER'
						OR UPPER([Narrative3]) = 'VALITOR BUFFER'
						OR (UPPER([Narrative3]) = 'VALITOR HF' AND UPPER([Narrative1]) IN ('REFUND','NONREF'))
					) THEN 'PAYMENT-VALITOR'
			--RBS Margin
			WHEN [Narrative1] = 'COLCFX' THEN 'TRANSFER-RBSCOLLATERAL'
			--Client Money Transfer
			WHEN UPPER([Narrative1]) IN
				('CAXTON FX LTD CA','CAXTON FX LTD CL','CAXTON FX CLIENT M','CAXTON FX LTD TR','CAXTON FX LTD CLIE','Client money','CM TFR') THEN 'TRANSFER-CLIENTMONEY'
			--Contra
			WHEN UPPER([Narrative1]) IN
				('CAXTON FX LIMITED','CAXTON FX LTD','CAXTON FX LTD TRAD','CAXTON FX LTD TR')
				AND [Currency] = 'GBP' AND [Type] = 'EBP' THEN 'TRANSFER-CONTRA'
			--Charges
			WHEN [Type] = 'BLN' THEN 'PAYMENT-CHARGES'
			--Interest (Charged)
			WHEN [Type] = 'CHG' AND [Debit] <0 THEN 'PAYMENT-INTERESTCHARGED'
			--Interest (Income)
			WHEN [Type] = 'CHG' AND [Credit] >=0 THEN 'PAYMENT-INTERESTINCOME'
			--Direct Debit
			WHEN [Type] = 'D/D' AND [Credit] >=0 THEN 'PAYMENT-DIRECTDEBIT'
			--Sundry Credit
			WHEN [Type] = 'SCR' AND [Credit] >=0 THEN 'PAYMENT-SUNDRY'
			--Cheque
			WHEN [Type] = 'BGC' AND [Credit] >=0 THEN 'PAYMENT-CHEQUE'
			--Reserves
			WHEN LEFT(UPPER([Narrative1]),6) = 'LLOYDS' OR RIGHT(UPPER([Narrative1]),6) = 'LLOYDS' OR LEFT(UPPER([Narrative2]),6) = 'LLOYDS' THEN 'TRANSFER-RESERVES'
			--Card refunds
			WHEN ([Narrative2] = 'CARD REFUND' AND [Type] = 'EBP') OR [Narrative1] = 'CARD REFUND' THEN 'PAYMENT-CARDREFUND'
			--Reverse Fees
			WHEN ([Narrative4] IN ('INTER A/C TFR','CCY A/C TFR') AND [Narrative3] = 'RBS OUTWARD PAYM') OR ([Narrative4] = 'ROYWORLD EXPRESS' AND LEFT([Narrative2],6) = 'RBSCHQ') THEN 'PAYMENT-AMMENDMENTFEE'
			--ASSUMES REMAINDER IS CLIENT MONEY
			ELSE 'RECEIPT-CLIENTMONEY'
		END AS EventTypeCode,
		CASE  
			--AltaPay (Valitor) (RECEIPT)
			WHEN (UPPER(LEFT([Narrative2],7)) = 'VALITOR' OR UPPER(LEFT([Narrative3],7)) = 'VALITOR')
					AND NOT (UPPER([Narrative1]) LIKE 'CAXTON MFA GBP%'
							OR UPPER([Narrative1]) = 'VALITOR BUFFER'
							OR UPPER([Narrative3]) = 'VALITOR BUFFER'
							OR (UPPER([Narrative3]) = 'VALITOR HF' AND UPPER([Narrative1]) IN ('REFUND','NONREF'))
							)
				THEN 'VALITOR'
			--AltaPay (Credorax) (RECEIPT)
			WHEN UPPER(LEFT([Narrative3],8)) = 'CREDORAX' THEN 'CREDORAX'
			--Valitor (PAYMENT)
			WHEN UPPER(LEFT([Narrative1],8)) = 'VALITOR ' 
						AND NOT ((UPPER([Narrative1]) LIKE 'CAXTON MFA GBP%'
								OR UPPER([Narrative1]) = 'VALITOR BUFFER'
								OR UPPER([Narrative3]) = 'VALITOR BUFFER'
								OR (UPPER([Narrative3]) = 'VALITOR HF' AND UPPER([Narrative1]) IN ('REFUND','NONREF') )
								)
					) THEN 'NON-BUFFER'
			--Valitor (PAYMENT) - BUFFER
			WHEN (UPPER([Narrative1]) LIKE 'CAXTON MFA GBP%'
					OR UPPER([Narrative1]) = 'VALITOR BUFFER')
					OR UPPER([Narrative3]) = 'VALITOR BUFFER'
					OR (UPPER([Narrative3]) = 'VALITOR HF' AND UPPER([Narrative1]) IN ('REFUND','NONREF') )
					 THEN 'BUFFER'
		END
		AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'SYSTEM' AS CodeSchema,
		CAST(x.UID AS CHAR(36)) AS CodeValue,
		'ACCNO' AS NostroCodeSchema,
		x.AccountNumber AS NostroCodeValue,
		x.AccountShortName AS NostroName,
		x.[Date] AS EventDateTime,
		CAST(x.[Date] AS Date) AS EventDate,
		CAST(x.[Date] AS Date) AS SettlementDate,
		0 AS IsSettlementDateExpected,
		NULL AS ClientId,
		'SETTLEMENT' AS TransactionTypeCode,
		NULL AS TradeId,NULL AS TradeTypeCode,NULL AS TradeLegCode,
		x.Currency AS InstrumentCode,
		CASE WHEN Credit = 0 THEN x.Debit ELSE x.Credit END AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		CASE WHEN Credit = 0 THEN x.Debit ELSE x.Credit END / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'Type: ' + ISNULL(x.Type, '<NULL>') +
		' | Narrative1: ' + ISNULL(x.Narrative1, '<NULL>') +
		' | Narrative2: '+ ISNULL(x.Narrative2, '<NULL>') +
		' | Narrative3: ' + ISNULL(x.Narrative3, '<NULL>') +
		' | Narrative4: ' + ISNULL(x.Narrative4, '<NULL>') +
		' | Narrative5: ' + ISNULL(x.Narrative5, '<NULL>') AS Notes
FROM [raw].[tblStatementBankRBS] AS x
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = x.Currency
WHERE x.[Date] >= @DateFrom AND x.[Date] < @DateToAdj

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted SETTLEMENT Event records (RBS) into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

--STEP 2: Build matching reference list for RBS flows

INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'CLIENT-ID' AS MappingTypeCode,
		'CLIENTID' AS CodeSchema,
		x.ClientId AS CodeValue,
		x.CodeSchema,
		x.CodeValue
FROM [rec].[tblInventoryEvent] AS x
WHERE x.RecInstanceCode = @RecInstanceCode
AND x.ClientId IS NOT NULL
AND x.SourceTypeCode = 'SETTLEMENT'
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'CLIENT-ID'
	AND s.CodeSchema = 'CLIENTID'
	AND s.CodeValue = x.ClientId
	AND s.InventoryEventCodeSchema = x.CodeSchema
	AND s.InventoryEventCodeValue = x.CodeValue);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted SETTLEMENT Event Mapping records (ClientId) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END


INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'SETTLEMENT-LREF' AS MappingTypeCode,
		'LREF' AS CodeSchema,
		x.[Narrative2] AS CodeValue,
		'SYSTEM' AS InventoryEventCodeSchema,
		CAST(x.UID AS CHAR(36)) AS InventoryEventCodeValue
FROM [raw].[tblStatementBankRBS] AS x
WHERE x.[Date] >= @DateFrom AND x.[Date] < @DateToAdj
AND LEFT([Narrative2],2) IN ('L1','N1','N2','S1')
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'SETTLEMENT-LREF'
	AND s.CodeSchema = 'LREF'
	AND s.CodeValue = x.[Narrative2]
	AND s.InventoryEventCodeSchema = 'RBSBANK'
	AND s.InventoryEventCodeValue = CAST(x.UID AS CHAR(36)));

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted Inventory Event Mapping records (RBS - Narrative 1) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END
		
SELECT SourceCode, SourceSetCode, Quantity
INTO #InventoryEventQuantityUnique
FROM [rec].[tblInventoryEvent]
WHERE RecInstanceCode = @RecInstanceCode
GROUP BY SourceCode, SourceSetCode, Quantity 
HAVING COUNT(*) = 1
	
SET @Rowcount = @@ROWCOUNT;
	
CREATE CLUSTERED INDEX idx1 ON #InventoryEventQuantityUnique(Quantity, SourceCode, SourceSetCode)
	
IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted unique quantity records (RBSBANK) into #InventoryEventQuantityUnique: ' + CAST(@Rowcount AS VARCHAR);
END

INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'QUANTITYUNIQUE' AS MappingTypeCode,
		'QTYUNIQUE' AS CodeSchema,
		inv.InstrumentCode + '|' + CAST(inv.Quantity AS VARCHAR) AS CodeValue,
		inv.CodeSchema AS InventoryEventCodeSchema,
		inv.CodeValue AS InventoryEventCodeValue
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN #InventoryEventQuantityUnique AS q
	ON inv.Quantity = q.Quantity
	AND inv.SourceCode = q.SourceCode
	AND inv.SourceSetCode = q.SourceSetCode
WHERE inv.RecInstanceCode = @RecInstanceCode
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'QUANTITYUNIQUE'
	AND s.CodeSchema = 'QTYUNIQUE'
	AND s.CodeValue = inv.InstrumentCode + '|' + CAST(inv.Quantity AS VARCHAR)
	AND s.InventoryEventCodeSchema = inv.CodeSchema
	AND s.InventoryEventCodeValue = inv.CodeValue);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted SETTLEMENT Event Mapping records (Unique Quantity for RBSBANK) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Build SETTLEMENT List for Rec Instance Code ' + @RecInstanceCode;
END

/* STATEMENT */

IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Build STATEMENT List for Rec Instance Code ' + @RecInstanceCode;
END

SELECT x.Payment
INTO #PaymentIgnore
FROM [raw].[vwStatementAcquirerAltaPay] AS x
LEFT OUTER JOIN (SELECT DISTINCT SUBSTRING(t.CodeValue,0,CHARINDEX('|',t.CodeValue)) AS CodeValue
					FROM [rec].[tblInventoryEvent] AS t
					WHERE t.RecInstanceCode = @RecInstanceCode
					AND t.SourceCode = 'FIREBIRD'
					AND t.SourceSetCode = 'ALTAPAY') AS y
			ON y.CodeValue = x.Payment
WHERE Date >= @DateFrom AND Date < @DateToAdj
AND y.CodeValue IS NULL
GROUP BY Payment,Date 
HAVING SUM(SettlementAmount) = 0
	
SET @Rowcount = @@ROWCOUNT;

CREATE CLUSTERED INDEX idx1 ON #PaymentIgnore(Payment)
	
IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted zero balance records (ALTAPAY) into #PaymentIgnore: ' + CAST(@Rowcount AS VARCHAR);
END

INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'ALTAPAY' AS SourceCode,
		'STATEMENT' AS SourceTypeCode,
		'ALTAPAY' AS SourceSetCode,
		'RECEIPT-ALTAPAY-TRANSACTION' AS EventTypeCode,
		CASE WHEN SUM(SettlementAmount) < 0 THEN 'REFUND' ELSE 'PAYMENT' END AS EventSubTypeCode,
		'FILENAME' AS GroupCodeSchema,
		x.FileName AS GroupCodeValue,
		'ALTAPAY-KEY' AS CodeSchema,
		x.Payment + '|' + x.SettlementCurrency + '|' + UPPER(x.Type) AS CodeValue,
		NULL AS NostroCodeSchema,
		NULL AS NostroCodeValue,
		NULL AS NostroName,
		MAX(CAST(x.Date AS Date)) AS EventDateTime,
		MAX(CAST(x.Date AS Date)) AS EventDate,
		DATEADD(DAY,
			CASE DATENAME(WEEKDAY,DATEADD(DAY, 1, MAX(x.Date)))
				WHEN 'Saturday' THEN 2
				WHEN 'Sunday' THEN 1
				ELSE 0
			END,
			DATEADD(DAY, 1, MAX(x.Date))) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		NULL AS ClientId,
		'STATEMENT' AS TransactionTypeCode,
		NULL AS TradeId,NULL AS TradeTypeCode,NULL AS TradeLegCode,
		SettlementCurrency AS InstrumentCode,
		SUM(SettlementAmount) AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		SUM(SettlementAmount) / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'' AS Notes
FROM [raw].[vwStatementAcquirerAltaPay] AS x
LEFT OUTER JOIN #PaymentIgnore AS payi
	ON payi.Payment = x.Payment
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = x.SettlementCurrency
WHERE x.Date >= @DateFrom AND x.Date < @DateToAdj
AND payi.Payment IS NULL
GROUP BY x.Payment + '|' + x.SettlementCurrency + '|' + UPPER(x.Type), SettlementCurrency, fx.FromCcyCode, fx.Rate, x.FileName
UNION ALL
SELECT	@RecInstanceCode AS RecInstanceCode,
		'ALTAPAY' AS SourceCode,
		'STATEMENT' AS SourceTypeCode,
		'ALTAPAY' AS SourceSetCode,
		'RECEIPT-ALTAPAY-FEES' AS EventTypeCode,
		CASE WHEN SUM(SettlementAmount) < 0 THEN 'REFUND' ELSE 'PAYMENT' END AS EventSubTypeCode,
		'FILENAME' AS GroupCodeSchema,
		x.FileName AS GroupCodeValue,
		'ALTAPAY-KEY' AS CodeSchema,
		x.Payment + '|' + x.SettlementCurrency + '|' + UPPER(x.Type) AS CodeValue,
		NULL AS NostroCodeSchema,
		NULL AS NostroCodeValue,
		NULL AS NostroName,
		MAX(CAST(x.Date AS Date)) AS EventDateTime,
		MAX(CAST(x.Date AS Date)) AS EventDate,
		DATEADD(DAY,
			CASE DATENAME(WEEKDAY,DATEADD(DAY, 1, MAX(x.Date)))
				WHEN 'Saturday' THEN 2
				WHEN 'Sunday' THEN 1
				ELSE 0
			END,
			DATEADD(DAY, 1, MAX(x.Date))) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		NULL AS ClientId,
		'STATEMENT' AS TransactionTypeCode,
		NULL AS TradeId,NULL AS TradeTypeCode,NULL AS TradeLegCode,
		SettlementCurrency AS InstrumentCode,
		SUM(RateBasedFee) AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		SUM(RateBasedFee) / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'' AS Notes
FROM [raw].[vwStatementAcquirerAltaPay] AS x
LEFT OUTER JOIN #PaymentIgnore AS payi
	ON payi.Payment = x.Payment
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = x.SettlementCurrency
WHERE x.Date >= @DateFrom AND x.Date < @DateToAdj
AND payi.Payment IS NULL
GROUP BY x.Payment + '|' + x.SettlementCurrency + '|' + UPPER(x.Type), SettlementCurrency, fx.FromCcyCode, fx.Rate, x.FileName;


SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted STETEMENT Event records (ALTAPAY) into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'VALITOR' AS SourceCode,
		'STATEMENT' AS SourceTypeCode,
		'VALITOR' AS SourceSetCode,
		CASE
			WHEN	x.TransactionKeyCategory = 'Charge'
					--AND x.TransactionKey IN ('MC6011','DPOSTR','BALINQ','GJALDF','MC6010','GJALWA','MC6011','GJALDF','GJALOU') 
					THEN 'RECEIPT-CHARGES-INCOME'
			--WHEN	x.TransactionKeyCategory = 'Charge' THEN 'RECEIPT-CHARGES-OTHER'
			WHEN x.TransactionKeyCategory = 'Deposit' AND x.Label IN ('ALIGN','S') THEN 'PAYMENT-DEPOSIT-INDIRECT'
			ELSE 'PAYMENT-DEPOSIT-DIRECT'
		END AS EventTypeCode,
		CASE Label WHEN 'Unload' THEN 'UNLOAD' ELSE 'LOAD' END AS EventSubTypeCode,
		'FILENAME' AS GroupCodeSchema,
		x.FileName AS GroupCodeValue,
		'VALITOR-ID' AS CodeSchema,
		x.Id AS CodeValue,
		NULL AS NostroCodeSchema,
		NULL AS NostroCodeValue,
		NULL AS NostroName,
		TransactionDateTime AS EventDateTime,
		CAST(TransactionDateTime AS DATE) AS EventDate,
		DATEADD(DAY, 
			CASE DATENAME(WEEKDAY,DATEADD(DAY, ISNULL(fx.SettlementDays,1), CAST(x.TransactionDateTime AS DATE)))
				WHEN 'Saturday' THEN 2
				WHEN 'Sunday' THEN 1
				ELSE 0
			END,
			DATEADD(DAY, ISNULL(fx.SettlementDays,1), CAST(x.TransactionDateTime AS DATE))) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		NULL AS ClientId,
		'STATEMENT' AS TransactionTypeCode,
		NULL AS TradeId,NULL AS TradeTypeCode,NULL AS TradeLegCode,
		ccy.ISOCode AS InstrumentCode, x.BillingAmount AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		x.BillingAmount / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		'' AS Notes
FROM [raw].[vwStatementIssuerValitor] AS x
LEFT OUTER JOIN [raw].[tblStatementIssuerValitorCcyLookup] AS ccy
	ON ccy.ISOCodeNumeric = x.BillingCurrency
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = ccy.ISOCode
WHERE x.TransactionDateTime >= @DateFrom AND x.TransactionDateTime < @DateToAdj
AND x.TransactionKeyCategory IN ('Charge','Deposit')

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted STATEMENT Event records (VALITOR) into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Build STATEMENT List for Rec Instance Code ' + @RecInstanceCode;
END

	
IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Insert STATEMENT Event Mapping Data ' + @RecInstanceCode;
END

--STEP 2: Build matching reference list for RBS flows

INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	DISTINCT
		@RecInstanceCode AS RecInstanceCode,
		'STATEMENT-ID' AS MappingTypeCode,
		x.CodeSchema AS CodeSchema,
		x.CodeValue AS CodeValue,
		x.CodeSchema AS InventoryEventCodeSchema,
		x.CodeValue AS InventoryEventCodeValue
FROM [rec].[tblInventoryEvent] AS x
WHERE x.RecInstanceCode = @RecInstanceCode
AND x.SourceTypeCode = 'STATEMENT'
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'STATEMENT-ID'
	AND s.CodeSchema = x.CodeSchema
	AND s.CodeValue = x.CodeValue
	AND s.InventoryEventCodeSchema = x.CodeSchema
	AND s.InventoryEventCodeValue = x.CodeValue);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted STATEMENT Event Mapping records (InventoryEventCode) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END

--INSERT INTO [rec].[tblInventoryEventMapping]
--	(RecInstanceCode,
--	MappingTypeCode,
--	CodeSchema,CodeValue,
--	InventoryEventCodeSchema,InventoryEventCodeValue)
--SELECT	DISTINCT
--		@RecInstanceCode AS RecInstanceCode,
--		'STATEMENT-PAYMENTGROUP' AS MappingTypeCode,
--		'PAYMENTGROUP' AS CodeSchema,
--		x.GroupCode + '|' + x.SettlementCurrency AS CodeValue,
--		'ALTAPAY-KEY' AS InventoryEventCodeSchema,
--		x.Payment + '|' + x.SettlementCurrency + '|' + UPPER(x.Type) AS InventoryEventCodeValue
--FROM [raw].[vwStatementAcquirerAltaPay] AS x
--LEFT OUTER JOIN #PaymentIgnore AS payi
--	ON payi.Payment = x.Payment
--LEFT OUTER JOIN [rec].[tblFXRate] AS fx
--	ON fx.RecInstanceCode = @RecInstanceCode
--	AND fx.[Date] = @DateTo
--	AND fx.ToCcyCode = x.SettlementCurrency
--WHERE x.Date >= @DateFrom AND x.Date < @DateToAdj
--AND payi.Payment IS NULL

--SET @Rowcount = @@ROWCOUNT;

--IF @IsDebugOn = 1
--BEGIN
--	PRINT 'Inserted STATEMENT Event Mapping records for PAYMENT GROUP (InventoryEventCode) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
--END
	
INSERT INTO [rec].[tblInventoryEventMapping]
	(RecInstanceCode,
	MappingTypeCode,
	CodeSchema,CodeValue,
	InventoryEventCodeSchema,InventoryEventCodeValue)
SELECT	DISTINCT
		@RecInstanceCode AS RecInstanceCode,
		'STATEMENT-REFID' AS MappingTypeCode,
		'VALITOR-REFID' AS CodeSchema,
		x.ReferenceId AS CodeValue,
		'VALITOR-ID' AS InventoryEventCodeSchema,
		x.Id AS InventoryEventCodeValue
FROM [raw].[vwStatementIssuerValitor] AS x
LEFT OUTER JOIN [raw].[tblStatementIssuerValitorCcyLookup] AS ccy
	ON ccy.ISOCodeNumeric = x.BillingCurrency
WHERE x.TransactionDateTime >= @DateFrom AND x.TransactionDateTime < @DateToAdj
AND x.TransactionKeyCategory IN ('Charge','Deposit')
AND x.ReferenceId IS NOT NULL
AND NOT EXISTS (
	SELECT *
	FROM [rec].[tblInventoryEventMapping] AS s
	WHERE s.RecInstanceCode = @RecInstanceCode
	AND s.MappingTypeCode = 'STATEMENT-REFID'
	AND s.CodeSchema = 'VALITOR-REFID'
	AND s.CodeValue = x.ReferenceId
	AND s.InventoryEventCodeSchema = 'VALITOR-ID'
	AND s.InventoryEventCodeValue = x.Id);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted STATEMENT Event Mapping records Valitor ID specific (InventoryEventCode) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
END

--INSERT INTO [rec].[tblInventoryEventMapping]
--	(RecInstanceCode,
--	MappingTypeCode,
--	CodeSchema,CodeValue,
--	InventoryEventCodeSchema,InventoryEventCodeValue)
--SELECT	DISTINCT
--		@RecInstanceCode AS RecInstanceCode,
--		'CLIENT-ID' AS MappingTypeCode,
--		'CLIENTID' AS CodeSchema,
--		x.ClientId AS CodeValue,
--		x.CodeSchema,
--		x.CodeValue
--FROM [rec].[tblInventoryEvent] AS x
--WHERE x.RecInstanceCode = @RecInstanceCode
--AND x.ClientId IS NOT NULL
--AND x.SourceTypeCode = 'STATEMENT'
--AND NOT EXISTS (
--	SELECT *
--	FROM [rec].[tblInventoryEventMapping] AS s
--	WHERE s.RecInstanceCode = @RecInstanceCode
--	AND s.MappingTypeCode = 'CLIENT-ID'
--	AND s.CodeSchema = 'CLIENTID'
--	AND s.CodeValue = x.ClientId
--	AND s.InventoryEventCodeSchema = x.CodeSchema
--	AND s.InventoryEventCodeValue = x.CodeValue);

--SET @Rowcount = @@ROWCOUNT;

--IF @IsDebugOn = 1
--BEGIN
--	PRINT 'Inserted STATEMENT Event Mapping records (ClientId) into [rec].[tblInventoryEventMapping]: ' + CAST(@Rowcount AS VARCHAR);
--END


	


IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Insert Inventory Event Mapping Data ' + @RecInstanceCode;
END
	
/* CALCULATED */

IF @IsDebugOn = 1
BEGIN
	PRINT 'START: Build CALCULATED List for Rec Instance Code ' + @RecInstanceCode;
END

--TRADES & COMPLETED TRANSFERS
INSERT INTO [rec].[tblInventoryEvent]
	(
	RecInstanceCode,
	SourceCode,SourceTypeCode,SourceSetCode,EventTypeCode,EventSubTypeCode,
	GroupCodeSchema,GroupCodeValue,
	CodeSchema,CodeValue,
	NostroCodeSchema,NostroCodeValue,NostroName,
	EventDateTime,EventDate,SettlementDate,IsSettlementDateExpected,
	ClientId,
	TransactionTypeCode,
	TradeId,TradeTypeCode,TradeLegCode,
	InstrumentCode,
	Quantity,
	ReportCcyCode,ValueReportCcy,FXRateReportCcy,
	Notes
	)
SELECT	@RecInstanceCode AS RecInstanceCode,
		'CALCULATED' AS SourceCode,
		'CALCULATED' AS SourceTypeCode,
		'RBSBANK' AS SourceSetCode,
		'PAYMENT-SETTLEMENT-FEE' AS EventTypeCode,
		'RBS' AS EventSubTypeCode,
		'' AS GroupCodeSchema,
		'' AS GroupCodeValue,
		'FB-ACCTTRANID' AS CodeSchema,
		CAST(x.Id AS VARCHAR) AS CodeValue,
		NULL AS NostroCodeSchema,NULL AS NostroCodeValue,NULL AS NostroName,
		x.TransactionDateTime AS EventDateTime,
		CAST(x.TransactionDateTime AS DATE) AS EventDate,
		CAST(ISNULL(t.TradeEndDate, x.TransactionDateTime) AS DATE) AS SettlementDate,
		1 AS IsSettlementDateExpected,
		acs.UserId AS ClientId,
		CASE
			WHEN ExternalTransRef = 'RBS' AND Status Like 'Debit for payaway%'
				THEN 'Transfer'
			ELSE x.ExternalTransRef
		END AS TransactionTypeCode,
		t.Id AS TradeId,
		t.TradeReason AS TradeTypeCode,
		CASE
			WHEN t.Id IS NOT NULL THEN 
				CASE WHEN x.Amount <= 0 THEN 'BUY' ELSE 'SELL' END
			ELSE NULL
		END AS TradeLegCode, 
		ccy.BuyCcyCode AS InstrumentCode,
		(rbs.Value - x.Amount) AS Quantity,
		fx.FromCcyCode AS ReportCcyCode,
		(rbs.Value - x.Amount) / fx.Rate AS ValueReportCcy,
		fx.Rate AS FXRateReportCcy,
		x.Status AS Notes
FROM [CurrencyBank].[AccountTransaction] AS x
INNER JOIN [CurrencyBank].[AccountSummary] AS acs
	ON acs.Id = x.AccountSummaryId
INNER JOIN [CurrencyBank].[Currency] AS ccy
	ON ccy.Id = acs.CurrencyId
INNER JOIN (SELECT rbs.AccountTransactionId, rbs.AccountCurrency AS Currency ,
							CASE WHEN DebitValue = 0 THEN CreditValue ELSE -DebitValue END AS [Value],
							CASE WHEN DebitValue = 0 THEN CreditValue ELSE -DebitValue END * fx.Rate AS [ValueReportCcy]
							FROM [Settlement].[RbsStatement] AS rbs
							LEFT OUTER JOIN [rec].[tblFXRate] AS fx
								ON fx.RecInstanceCode = @RecInstanceCode
								AND fx.[Date] = @DateTo
								AND fx.ToCcyCode = rbs.AccountCurrency
							WHERE NOT rbs.AccountTransactionId = 0
							AND rbs.AccountTransactionId IS NOT NULL
							AND NOT rbs.TransactionType = 'CHG'
							AND rbs.TransactionDate >= @DateFrom AND rbs.TransactionDate < @DateToAdj) AS rbs
	ON rbs.AccountTransactionId = x.Id
	AND rbs.Currency = ccy.BuyCcyCode
LEFT OUTER JOIN [CurrencyBank].[Trade] AS t
	ON t.Id = CASE WHEN ISNUMERIC(x.ExternalTransactionId) = 1 AND x.ExternalTransRef = 'Trade' THEN x.ExternalTransactionId ELSE NULL END
LEFT OUTER JOIN [rec].[tblFXRate] AS fx
	ON fx.RecInstanceCode = @RecInstanceCode
	AND fx.[Date] = @DateTo
	AND fx.ToCcyCode = ccy.BuyCcyCode
WHERE x.TransactionDateTime >= @DateFrom AND x.TransactionDateTime < @DateToAdj
AND x.ExternalTransRef = 'RBS'
AND x.FB_DateDeleted IS NULL
AND NOT ABS(rbs.[Value] - x.Amount) = 0
AND ABS((rbs.Value - x.Amount) / fx.Rate) < @RBSBANKFeeReportCcyDelta

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Inserted CALCULATED (PAYMENT-SETTLEMENT-FEE) Inventory Event records into [rec].[tblInventoryEvent]: ' + CAST(@Rowcount AS VARCHAR);
END

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: Build CALCULATED List for Rec Instance Code ' + @RecInstanceCode;
END


GO