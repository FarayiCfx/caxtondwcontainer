﻿CREATE PROCEDURE [rec].[insInventoryEventRecMatchResult]
	@RecInstanceCode VARCHAR(50),
	@DateFrom DATE,
	@DateTo DATE,
	@RecCode VARCHAR(50) = NULL,
	@IsDebugOn BIT = 0
AS
SET NOCOUNT ON

/*
EXEC [rec].[insInventoryEventRecMatchResult]
	@RecInstanceCode='REC201708.V1',
	@DateFrom='1 Aug 2017',
	@DateTo='31 Aug 2017',
	@IsDebugOn=1
*/
DECLARE @Rowcount BIGINT;
DECLARE @DateToAdj DATE;
SET @DateToAdj = DATEADD(DAY,1, @DateTo);

DECLARE @RecId SMALLINT;
DECLARE @RecName VARCHAR(100);


DECLARE @RBSBANKFeeReportCcyDelta DECIMAL(28,2);
SET @RBSBANKFeeReportCcyDelta = 10.00;

DECLARE @ValueMatchDayFilter AS SMALLINT;
SET @ValueMatchDayFilter = 60

DECLARE @MatchTypeCode varchar(50)
DECLARE @MappingTypeCode varchar(50)
DECLARE @MatchResultMappingTypePriority SMALLINT


IF @IsDebugOn = 1
BEGIN
	PRINT 'START: MATCH CATEGORISATION ' + @RecInstanceCode;
END


/***************************************************
ALTAPAY BREAK ANALYSIS
****************************************************/

/**** TRANSACTION SIDE ****/
--FB - ALTAPAY: Accrued cash i.e. Traded in period but not settled in period (but actual cash flow exists)
UPDATE invrmr
SET	BreakReasonCode = 'TIMING-EXPECTEDCASH',
	BreakReasonDescription = 'Trade date in FB ' + CONVERT(varchar,CAST(inv.EventDateTime AS DATE),103) + ' Actual settlement date from AltaPay Statement outside period ' + CONVERT(varchar,CAST(x.Date AS DATE),103),
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [raw].[tblStatementAcquirerAltaPay] AS x
	ON inv.CodeValue= x.Payment + '|' + x.SettlementCurrency + '|' + UPPER(x.Type)
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'ALTAPAY-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'FIREBIRD'
AND inv.SourceSetCode = 'ALTAPAY'
AND x.Date >= @DateToAdj

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = ALTAPAY AND BreakReasonCode = TIMING-EXPECTEDCASH: ' + CAST(@Rowcount AS VARCHAR);
END

--FB - ALTAPAY: Different Currency between FB and ALTAPAY
UPDATE invrmr
SET	BreakReasonCode = 'CURRENCY-DIFF',
	BreakReasonDescription = 'Instrument in FB ' + CAST(inv.InstrumentCode AS VARCHAR) + ' Instrument from AltaPay Statement ' + CAST(x.SettlementCurrency AS VARCHAR),
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [raw].[tblStatementAcquirerAltaPay] AS x
	ON x.Payment = SUBSTRING(inv.CodeValue,0,CHARINDEX('|',inv.CodeValue))
	AND x.Date >= @DateFrom AND x.Date < @DateToAdj
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'ALTAPAY-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'FIREBIRD'
AND inv.SourceSetCode = 'ALTAPAY'
AND NOT inv.InstrumentCode = x.SettlementCurrency

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = ALTAPAY AND BreakReasonCode = QUANTITY-DIFFERENCE: ' + CAST(@Rowcount AS VARCHAR);
END

/***************************************************
VALITOR BREAK ANALYSIS
****************************************************/

/**** TRANSACTION SIDE ****/

--FB - VALITOR: Accrued cash i.e. Traded in period but not settled in period (but actual cash flow exists)
UPDATE invrmr
SET	BreakReasonCode = 'TIMING-EXPECTEDCASH',
	BreakReasonDescription = 'Trade date in FB ' + CONVERT(varchar,CAST(inv.EventDateTime AS DATE),103) + ' Assumption all trades after '
		+ CAST(DATEADD(day,CASE DATENAME(dw,@DateTo) WHEN 'Sunday' THEN -2 WHEN 'Saturday' THEN -1 ELSE 0 END,@DateTo) AS VARCHAR) + ' are accruals',
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'VALITOR-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.BreakReasonCode = 'NO-IDENTIFIED-STATEMENT-ENTRY'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'FIREBIRD'
AND inv.SourceSetCode = 'VALITOR'
--Assumes any transaction from 30mins prior to last working day of month end and hasn't been mapped is an accrual
AND inv.EventDateTime >= DATEADD(day,CASE DATENAME(dw,@DateTo) WHEN 'Sunday' THEN -2 WHEN 'Saturday' THEN -1 ELSE 0 END,@DateTo)

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = VALITOR AND BreakReasonCode = TIMING-EXPECTEDCASH: ' + CAST(@Rowcount AS VARCHAR);
END

--FB - VALITOR: Unloading card fee
UPDATE invrmr
SET	BreakReasonCode = 'NO-IDENTIFIED-SETTLEMENT-BALANCEUNLOAD-FEE',
	BreakReasonDescription = 'Balance unload fee (no corresponding FB transaction)',
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [rec].[tblMatchResult] AS mr
	ON mr.Id = invrmr.MatchResultId
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'VALITOR-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.BreakReasonCode = 'QUANTITY-DIFF'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'FIREBIRD'
AND inv.SourceSetCode = 'VALITOR'
--Assumes 1.5 GBP difference is a fee
AND inv.InstrumentCode = 'GBP'
AND mr.QuantityDelta = 1.5

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = VALITOR AND BreakReasonCode = QUANTITY-DIFF AND VALUE = 1.50: ' + CAST(@Rowcount AS VARCHAR);
END

--FB - VALITOR: Zero value trades
UPDATE invrmr
SET	BreakReasonCode = 'ZERO-VALUE',
	BreakReasonDescription = 'Zero value transaction in FB',
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'VALITOR-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'FIREBIRD'
AND inv.SourceSetCode = 'VALITOR'
--Assumes any transaction from 30mins prior to last working day of month end and hasn't been mapped is an accrual
AND inv.Quantity = 0

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = VALITOR AND BreakReasonCode = ACCRUAL-EXPECTEDCASH: ' + CAST(@Rowcount AS VARCHAR);
END


/**** STATEMENT SIDE ****/

--VALITOR: Cards Fee
UPDATE invrmr
SET	BreakReasonCode = 'NO-IDENTIFIED-TRANSACTION-ENTRY-CARDS-FEE',
	BreakReasonDescription = 'Settled Fee for Caxton Card not held in Firebird',
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [Firebird].[CardsFee] AS cf
	ON cf.AlphaCcy = inv.InstrumentCode
	AND cf.CcyFee = inv.Quantity
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'VALITOR-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'VALITOR'
AND inv.SourceSetCode = 'VALITOR'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.BreakReasonCode = 'NO-IDENTIFIED-TRANSACTION-ENTRY'

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = VALITOR AND SourceSetCode = VALITOR AND BreakReasonCode = NO-IDENTIFIED-TRANSACTION AND Quantity = Card Fee: ' + CAST(@Rowcount AS VARCHAR);
END

--FB - VALITOR: Unloading card fee
UPDATE invrmr
SET	BreakReasonCode = 'NO-IDENTIFIED-TRANSACTION-ENTRY-BALANCEUNLOAD-FEE',
	BreakReasonDescription = 'Balance unload fee GBP 1.50 (no corresponding FB transaction)',
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [rec].[tblMatchResult] AS mr
	ON mr.Id = invrmr.MatchResultId
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'VALITOR-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.BreakReasonCode = 'QUANTITY-DIFF'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'VALITOR'
AND inv.SourceSetCode = 'VALITOR'
--Assumes 1.5 GBP difference is a fee
AND inv.InstrumentCode = 'GBP'
AND mr.QuantityDelta = 1.5

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = VALITOR AND BreakReasonCode = QUANTITY-DIFF AND VALUE = 1.50: ' + CAST(@Rowcount AS VARCHAR);
END

/***************************************************
RBS BREAK ANALYSIS
****************************************************/

/**** TRANSACTION SIDE ****/

--FB - RBS: Incorrect settlement date in FB
UPDATE invrmr
SET	BreakReasonCode = 'INCORRECT-FB-SETTELEMENTDATE',
	BreakReasonDescription = 'Settlement date in FB is ' + CONVERT(varchar,CAST(inv.EventDateTime AS DATE),103) + ' Actual settlement date from RBS Statement ' + CONVERT(varchar,CAST(x.TransactionDate AS DATE),103),
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [Settlement].[RbsStatement] AS x 
	ON x.AccountTransactionId = inv.CodeValue
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'RBSBANK-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND inv.SourceCode = 'FIREBIRD'
AND inv.SourceSetCode = 'RBSBANK'
AND NOT CAST(inv.EventDateTime AS DATE) = x.TransactionDate
AND x.TransactionDate < @DateFrom

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = RBSBANK AND BreakReasonCode = INCORRECT-FB-SETTELEMENTDATE: ' + CAST(@Rowcount AS VARCHAR);
END

--FB - RBS: Accrued cash i.e. Traded in period but not settled in period (but actual cash flow exists)
UPDATE invrmr
SET	BreakReasonCode = 'ACCRUAL-EXPECTEDCASH',
	BreakReasonDescription = 'Trade date in FB ' + CONVERT(varchar,CAST(inv.EventDateTime AS DATE),103) + ' Actual settlement date from RBS Statement ' + CONVERT(varchar,CAST(x.TransactionDate AS DATE),103),
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN 
		(SELECT x.AccountTransactionId, x.AccountCurrency, MAX(x.TransactionDate) AS TransactionDate
		 FROM [Settlement].[RbsStatement] AS x
		 WHERE NOT x.AccountTransactionId = 0
			AND x.AccountTransactionId IS NOT NULL
			AND NOT x.TransactionType = 'CHG'
		 GROUP BY x.AccountTransactionId, x.AccountCurrency) AS x
	ON x.AccountTransactionId = CASE WHEN ISNUMERIC(inv.CodeValue) =1 THEN inv.CodeValue ELSE NULL END
	AND x.AccountCurrency = inv.InstrumentCode
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'RBSBANK-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND SourceCode = 'FIREBIRD'
AND SourceSetCode = 'RBSBANK'
AND x.TransactionDate >= @DateToAdj

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = RBSBANK AND BreakReasonCode = ACCRUAL-EXPECTEDCASH: ' + CAST(@Rowcount AS VARCHAR);
END

--FB - RBS: FB HelpDesk Adjustments
UPDATE invrmr
SET	BreakReasonCode = 'NO-IDENTIFIED-SETTLEMENT-HELPDESK-ADJ',
	BreakReasonDescription = 'Missing/non-matched settlement against Help Desk adjustment [FB Status: ' + act.Status + ']',
	IsExplained = 1
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventRecMatchResult] AS invrmr
	ON invrmr.RecInstanceCode = @RecInstanceCode
	AND invrmr.InventoryEventId = inv.Id
INNER JOIN [rec].[tblRec] AS r
	ON r.Id = invrmr.RecId
INNER JOIN [rec].[tblMatchResult] AS x 
	ON x.Id = invrmr.MatchResultId
INNER JOIN [CurrencyBank].[AccountTransaction] AS act
	ON act.ID = CASE WHEN ISNUMERIC(inv.CodeValue) = 1 THEN CAST(inv.CodeValue AS BIGINT) ELSE NULL END
	AND act.FB_DateDeleted IS NULL
WHERE inv.RecInstanceCode = @RecInstanceCode
AND r.Code = 'RBSBANK-FB'
AND invrmr.MatchTypeCode = 'NON-MATCHED'
AND invrmr.IsExplained = 0
AND SourceCode = 'FIREBIRD'
AND SourceSetCode = 'RBSBANK'
--AND invrmr.BreakReasonCode ='NO-IDENTIFIED-SETTLEMENT-ENTRY'
AND act.Status Like '<HD>%'

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Update unmatched records: in [rec].[tblInventoryEvent] for SourceCode = FIREBIRD AND SourceSetCode = RBSBANK AND BreakReasonCode = ACCRUAL-EXPECTEDCASH: ' + CAST(@Rowcount AS VARCHAR);
END

GO