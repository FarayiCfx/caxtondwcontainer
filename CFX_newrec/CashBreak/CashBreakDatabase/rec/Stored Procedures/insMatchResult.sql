﻿CREATE PROCEDURE [rec].[insMatchResult]
	@RecInstanceCode VARCHAR(50),
	@DateFrom DATE,
	@DateTo DATE,
	@RecCode VARCHAR(50) = NULL,
	@LoadData BIT = 1,
	@IsDebugOn BIT = 0
AS
SET NOCOUNT ON

/*
EXEC [rec].[insMatchResult]
	@RecInstanceCode='REC201708.V1',
	@DateFrom='1 Aug 2017',
	@DateTo='31 Aug 2017',
	@LoadData = 1,
	@IsDebugOn=1
*/
DECLARE @Rowcount BIGINT;
DECLARE @DateToAdj DATE;
SET @DateToAdj = DATEADD(DAY,1, @DateTo);

DECLARE @RecId SMALLINT;
DECLARE @RecName VARCHAR(100);


DECLARE @RBSBANKFeeReportCcyDelta DECIMAL(28,2);
SET @RBSBANKFeeReportCcyDelta = 10.00;

DECLARE @ValueMatchDayFilter AS SMALLINT;
SET @ValueMatchDayFilter = 60

DECLARE @MatchTypeCode varchar(50)
DECLARE @MappingTypeCode varchar(50)
DECLARE @MatchResultMappingTypePriority SMALLINT

IF @LoadData = 1
BEGIN
	EXEC [rec].[insInventoryEventCFX]
		@RecInstanceCode=@RecInstanceCode,
		@DateFrom=@DateFrom,
		@DateTo=@DateTo,
		@IsDebugOn=1
END

--MATCHING PROCESS

IF @IsDebugOn = 1
BEGIN
	PRINT 'START: RECONCILIATION ' + @RecInstanceCode;
END

IF @RecCode IS NOT NULL SET @RecID = (SELECT Id FROM [rec].[tblRec] WHERE Code = @RecCode)


DELETE FROM [rec].[tblInventoryEventRecMatchResult] WHERE RecInstanceCode = @RecInstanceCode AND (RecId = @RecId OR @RecId IS NULL);;

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Deleted records from [rec].[tblInventoryEventRecMatchResult]: ' + CAST(@Rowcount AS VARCHAR);
END

DELETE FROM [rec].[tblMatchResult] WHERE RecInstanceCode = @RecInstanceCode AND (RecId = @RecId OR @RecId IS NULL);

SET @Rowcount = @@ROWCOUNT;

IF @IsDebugOn = 1
BEGIN
	PRINT 'Deleted records from [rec].[tblMatchResult]: ' + CAST(@Rowcount AS VARCHAR);
END

SELECT	inv.SourceTypeCode, inv.SourceCode, inv.SourceSetCode, inv.EventTypeCode,
		invm.MappingTypeCode, invm.CodeSchema AS MappingCodeSchema, invm.CodeValue AS MappingCodeValue, inv.InstrumentCode, 
		MAX(SettlementDate) AS MaxSettlementDate,
		SUM(inv.Quantity) AS Quantity,
		inv.ReportCcyCode,
		SUM(inv.ValueReportCcy) AS ValueReportCcy,
		inv.FXRateReportCcy
INTO #InventoryEventAgg
FROM [rec].[tblInventoryEvent] AS inv
INNER JOIN [rec].[tblInventoryEventMapping] AS invm
	ON invm.RecInstanceCode = @RecInstanceCode
	AND invm.InventoryEventCodeSchema = inv.CodeSchema
	AND invm.InventoryEventCodeValue = inv.CodeValue
INNER JOIN 
		( SELECT DISTINCT SourceCode, SourceSetCode, EventTypeCode, MappingTypeCode
		  FROM [rec].[tblRecScope]
		  WHERE (RecId = @RecId OR @RecId IS NULL)) AS rstran
	ON rstran.SourceCode = inv.SourceCode
	AND rstran.SourceSetCode = inv.SourceSetCode
	AND rstran.EventTypeCode = inv.EventTypeCode
	AND rstran.MappingTypeCode = invm.MappingTypeCode
WHERE inv.RecInstanceCode = @RecInstanceCode
GROUP BY inv.SourceTypeCode, inv.SourceCode, inv.SourceSetCode, inv.EventTypeCode,
		invm.MappingTypeCode,invm.CodeSchema, invm.CodeValue, inv.InstrumentCode, inv.ReportCcyCode,inv.FXRateReportCcy

SET @Rowcount = @@ROWCOUNT;

CREATE UNIQUE CLUSTERED INDEX idx1 ON #InventoryEventAgg(EventTypeCode, MappingCodeValue, InstrumentCode, MappingTypeCode, SourceTypeCode, SourceCode, SourceSetCode, MappingCodeSchema);

IF @IsDebugOn = 1
BEGIN
	PRINT 'Created/populated worktable #InventoryEventAgg: ' + CAST(@Rowcount AS VARCHAR);
END


IF @IsDebugOn = 1
BEGIN
	PRINT 'START: MATCHING ' + @RecInstanceCode;
END

DECLARE curRec CURSOR FOR   
SELECT Id, Code, Name  
FROM [rec].[tblRec] AS r

OPEN curRec  

WHILE 1=1
BEGIN  
	
	FETCH NEXT FROM curRec INTO @RecId, @RecCode, @RecName

	IF NOT @@FETCH_STATUS = 0 BREAK;

	IF @IsDebugOn = 1
	BEGIN
		PRINT 'START: Reconciliation [' + @RecCode + ']: '  + @RecName;
	END
	
	INSERT INTO [rec].[tblMatchResult](
			RecInstanceCode,
			RecId,
			Side0SourceTypeCode,
			Side1SourceTypeCode,
			EventTypeCode,
			MatchTypeCode,
			MappingTypeCode,
			MappingTypePriority,
			MappingCodeSchema,MappingCodeValue,
			InstrumentCode,
			MaxSettlementDateSide0,MaxSettlementDateSide1,
			--Base Currency
			QuantitySide0,QuantitySide1,
			--Report Currency
			ReportCcyCode,
			ValueReportCcySide0,ValueReportCcySide1,
			FXRateReportCcy)
	SELECT	@RecInstanceCode,
			@RecId,
			r.[Side0SourceTypeCode],
			r.[Side1SourceTypeCode],
			rs.EventTypeCode,
			CASE WHEN SUM(inv.Quantity * CASE WHEN r.[Side0SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE -1 END) = 0 THEN 'MATCHED' ELSE 'NON-MATCHED' END AS MatchTypeCode,
			rs.MappingTypeCode ,
			rs.MappingTypePriority + CASE WHEN SUM(inv.Quantity * CASE WHEN r.[Side0SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE NULL END) IS NULL OR SUM(inv.Quantity * CASE WHEN r.[Side1SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE NULL END) IS NULL THEN 100 ELSE 0 END AS MappingTypePriority, -- Don't overide matching priority when there is a missing record on either side
			inv.MappingCodeSchema,
			inv.MappingCodeValue,
			inv.InstrumentCode,
			MAX(CASE WHEN r.[Side0SourceTypeCode] = rs.SourceTypeCode THEN inv.MaxSettlementDate ELSE NULL END) AS MaxSettlementDateSide0,
			MAX(CASE WHEN r.[Side1SourceTypeCode] = rs.SourceTypeCode THEN inv.MaxSettlementDate ELSE NULL END) AS MaxSettlementDateSide1,
			SUM(inv.Quantity * CASE WHEN r.[Side0SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE NULL END) AS QuantitySide0,
			SUM(inv.Quantity * CASE WHEN r.[Side1SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE NULL END) AS QuantitySide1,
			inv.ReportCcyCode,
			SUM(inv.ValueReportCcy * CASE WHEN r.[Side0SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE NULL END) AS ValueReportCcyTransacted,
			SUM(inv.ValueReportCcy * CASE WHEN r.[Side1SourceTypeCode] = rs.SourceTypeCode THEN 1 ELSE NULL END) AS ValueReportCcyInventoryEvent,
			inv.FXRateReportCcy
	FROM [rec].[tblRec] AS r
	INNER JOIN [rec].[tblRecScope] AS rs
		ON rs.RecId = r.Id
	INNER JOIN #InventoryEventAgg AS inv
		ON rs.MappingTypeCode = inv.MappingTypeCode
		AND rs.EventTypeCode = inv.EventTypeCode
		AND rs.SourceCode = inv.SourceCode
		AND rs.SourceSetCode = inv.SourceSetCode
		AND rs.SourceTypeCode = inv.SourceTypeCode
	WHERE r.Id = @RecID
	GROUP BY 
			r.[Side0SourceTypeCode],
			r.[Side1SourceTypeCode],
			rs.EventTypeCode,
			rs.MappingTypeCode ,
			rs.MappingTypePriority,
			inv.MappingCodeSchema,
			inv.MappingCodeValue,
			inv.InstrumentCode,
			inv.ReportCcyCode,
			inv.FXRateReportCcy
			
	SET @Rowcount = @@ROWCOUNT;

	IF @IsDebugOn = 1
	BEGIN
		PRINT 'Inserted records [' + @RecCode + '] into [rec].[tblMatchResult]: ' + CAST(@Rowcount AS VARCHAR);
	END

	--LOOP THROUGH PRIORITIES
	
	DECLARE curMatchResultPriority CURSOR FOR   
	SELECT DISTINCT MappingTypePriority 
	FROM [rec].[tblMatchResult] AS mr
	WHERE mr.RecInstanceCode = @RecInstanceCode
		AND mr.RecId = @RecID
	ORDER BY 1 ASC

	OPEN curMatchResultPriority  

	WHILE 1=1  
	BEGIN  
	
		FETCH NEXT FROM curMatchResultPriority  INTO @MatchResultMappingTypePriority

		IF NOT @@FETCH_STATUS = 0 BREAK;

		IF @IsDebugOn = 1
		BEGIN
			PRINT 'START: Update Transaction & Inventory with Matches [' + @RecCode + '] Priority: '  + CAST(@MatchResultMappingTypePriority AS VARCHAR);
		END
		
		INSERT INTO [rec].[tblInventoryEventRecMatchResult](
			RecInstanceCode,RecId,InventoryEventId,
			EventTypeCode,
			MatchResultId,MatchTypeCode,MappingTypeCode,
			BreakReasonCode,BreakReasonDescription,IsExplained)
		SELECT 
			@RecInstanceCode AS RecInstanceCode,@RecID AS RecId,
			inv.Id AS InventoryEventId,
			inv.EventTypeCode,
			mr.Id AS MatchResultId,mr.MatchTypeCode,mr.MappingTypeCode,
			CASE mr.MatchTypeCode
				WHEN 'MATCHED' THEN 'MATCHED'
				ELSE
					CASE
						WHEN mr.QuantitySide0 IS NULL THEN 'NO-IDENTIFIED-' + mr.[Side0SourceTypeCode] + '-ENTRY'
						WHEN mr.QuantitySide1 IS NULL THEN 'NO-IDENTIFIED-' + mr.[Side1SourceTypeCode] + '-ENTRY'
						ELSE 'QUANTITY-DIFF'
					END
			END AS BreakReasonCode,	
			CASE mr.MatchTypeCode
					WHEN 'NON-MATCHED' THEN mr.[Side0SourceTypeCode] + ' (Side 0) Quantity ' + ISNULL(CAST(mr.QuantitySide0 AS VARCHAR),'<MISSING>') + ' ' + mr.[Side1SourceTypeCode] + ' (Side 1) Quantity ' + ISNULL(CAST(mr.QuantitySide1 AS VARCHAR),'<MISSING>')
					ELSE 'Matched'
			END AS BreakReasonDescription,
			0 AS IsExplained
		FROM [rec].[tblMatchResult] AS mr
		INNER JOIN [rec].[tblInventoryEventMapping] AS invm
			ON invm.RecInstanceCode = @RecInstanceCode
			AND invm.MappingTypeCode = mr.MappingTypeCode
			AND invm.CodeSchema = mr.MappingCodeSchema
			AND invm.CodeValue = mr.MappingCodeValue
		INNER JOIN [rec].[tblInventoryEvent] AS inv
			ON inv.RecInstanceCode = @RecInstanceCode
			AND inv.EventTypeCode = mr.EventTypeCode
			AND inv.CodeSchema = invm.InventoryEventCodeSchema
			AND inv.CodeValue = invm.InventoryEventCodeValue
			AND inv.InstrumentCode = mr.InstrumentCode
		WHERE mr.RecInstanceCode = @RecInstanceCode
		AND mr.RecId = @RecID
		AND mr.MappingTypePriority = @MatchResultMappingTypePriority
		AND NOT EXISTS (SELECT * 
						FROM  [rec].[tblInventoryEventRecMatchResult] AS invrmr
						WHERE invrmr.RecInstanceCode = @RecInstanceCode
						AND invrmr.RecId = @RecID
						AND invrmr.InventoryEventId = inv.Id) 

		SET @Rowcount = @@ROWCOUNT;

		IF @IsDebugOn = 1
		BEGIN
			PRINT 'Inserted tblInventoryEvent rec matched records [' + @RecCode + ']: in [rec].[tblInventoryEventRecMatchResult]: ' + CAST(@Rowcount AS VARCHAR);
		END
	END

	CLOSE curMatchResultPriority
	DEALLOCATE curMatchResultPriority

	
	IF @IsDebugOn = 1
	BEGIN
		PRINT 'END: Reconciliation [' + @RecCode + ']: '  + @RecName;
	END
END

CLOSE curRec
DEALLOCATE curRec

IF @IsDebugOn = 1
BEGIN
	PRINT 'END: MATCHING ' + @RecInstanceCode;
END

GO