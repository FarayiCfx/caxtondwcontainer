﻿CREATE PROCEDURE [rec].[selMatchResultBreakDetail]
	@RecInstanceCode VARCHAR(50),
	@IsDebugOn BIT = 0
AS
SET NOCOUNT ON

DECLARE @Rowcount BIGINT;

SELECT	RecInstanceCode,RecCode,RecName,
		SourceTypeCode,SourceCode,SourceSetCode,
		EventTypeCode,EventTypeName,EventTypeDescription,EventSubTypeCode,
		CodeSchema,CodeValue,NostroCodeSchema,NostroCodeValue,NostroName,
		EventDateTime,EventDate,
		ClientId,InstrumentCode,
		Quantity,ReportCcyCode,ValueReportCcy,FXRateReportCcy,
		MatchResultId,MatchTypeCode,
		MappingTypeCode,BreakReasonCode,BreakReasonName,BreakReasonDescription,IsExplained,
		BreakQuantity,BreakValueReportCcy,
		Notes
FROM [rec].[vwRecExceptions] AS ex
WHERE ex.RecInstanceCode = @RecInstanceCode


GO