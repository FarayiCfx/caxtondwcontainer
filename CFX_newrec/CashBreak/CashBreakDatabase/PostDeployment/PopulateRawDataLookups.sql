﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r .\Data\raw\PopulateValitorCcyLookup.sql
:r .\Data\Rec\PopulateEventType.sql
:r .\Data\Rec\PopulateBreakReason.sql
:r .\Data\Rec\PopulateRec.sql