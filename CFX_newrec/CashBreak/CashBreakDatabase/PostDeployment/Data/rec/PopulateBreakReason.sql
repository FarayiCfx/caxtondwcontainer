﻿/*
ALTER TABLE [rec].[tblInventoryEventRecMatchResult] DROP CONSTRAINT FK_tblBreakReason
*/

SET NOCOUNT ON
GO
DELETE FROM [rec].[tblBreakReason];
GO
PRINT 'START: Populate tblBreakReason'
GO
INSERT INTO [rec].[tblBreakReason]
	(Code,Name,Description)
VALUES
('MATCHED','Matched','Matched with equal quantity'),
('QUANTITY-DIFF','Quantity Difference','Matched records but different quantities'),
('CURRENCY-DIFF','Instrument/Currency Difference','Matched records but different instruments/currencies'),
('ACCRUAL-EXPECTEDCASH','Accrual - Expected Cash','Expected cash in next period'),
('NO-IDENTIFIED-SETTLEMENT-ENTRY','Missing Settlement Record','Missing settlement record'),
('NO-IDENTIFIED-STATEMENT-ENTRY','Missing Statement Record','Missing statement record'),
('NO-IDENTIFIED-TRANSACTION-ENTRY','Missing Transaction Record','Missing transaction record'),
('NO-IDENTIFIED-SETTLEMENT-BALANCEUNLOAD-FEE','Missing Settlement - Balance Unload Fee','Missing Settlement - Balance Unload Fee'),
('NO-IDENTIFIED-TRANSACTION-ENTRY-BALANCEUNLOAD-FEE','Missing Transaction - Balance Unload Fee','Missing Transaction - Balance Unload Fee'),
('NO-IDENTIFIED-TRANSACTION-ENTRY-CARDS-FEE','Missing Transaction - Card Fee','Missing Transaction - Card Fee'),
('INCORRECT-FB-SETTELEMENTDATE','Incorrect FB Settlement Date - Next Period','Incorrect FB Settlement Date - Next Period'),
('TIMING-EXPECTEDCASH','Missing Statement Record - Expected Cash','Missing Statement Record - Settlement is beyond end date'),
('ZERO-VALUE','Zero Value in FB','Zero Value in FB')

PRINT 'END: Populate tblBreakReason [Records: ' + CAST(@@ROWCOUNT AS VARCHAR) + ']';
GO
/*
ALTER TABLE [rec].[tblInventoryEventRecMatchResult] ADD CONSTRAINT FK_tblBreakReason FOREIGN KEY ([BreakReasonCode]) REFERENCES [rec].[tblBreakReason](Code)
*/


