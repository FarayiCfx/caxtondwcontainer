﻿/*
ALTER TABLE [rec].[tblInventoryEvent] DROP CONSTRAINT FK_tblEventTypeCode
*/

SET NOCOUNT ON
GO
DELETE FROM [rec].[tblEventType];
GO
PRINT 'START: Populate tblEventType'
GO
INSERT INTO [rec].[tblEventType]
	(Code,Name,Description)
VALUES
('TRADE-GUARANTEEDBUYBACK','Client Trade - Guaranteed  Buy-Back','Client Trade - Guaranteed  Buy-Back'),
('TRADE-CARDLOAD','Client Trade - Card Load','Client Trade - Card Load'),
('TRADE-BUYHOLD','Client Trade - Buy and Hold','Client Trade - Buy and Hold'),
('TRADE-BUYSEND','Client Trade - Buy and Send','Client Trade - Buy and Send'),
('TRADE-VALMONITOR','Client Trade - UserId ValMonitor','Client Trade - UserId ValMonitor'),
('TRADE-OTHER','Client Trade - Other','Client Trade - Other'),
('ADJUSTMENT','Firebird Adjustment','Firebird Adjustment (Refunds, fraud, etc.)'),
('PAYMENT-AMMENDMENTFEE','Bank Fee for Ammends','Fee from Bank on ammending payment'),
('PAYMENT-SETTLEMENT-FEE','Bank Fee for Settlements','Fee from Bank for settlements'),
('PAYMENT-BENEFICIARY','Beneficiary Payment','Beneficiary payment'),
('PAYMENT-CARDREFUND','Card Refund','Card Refund'),
('PAYMENT-CHARGES','Bank Charges','Bank payment charges'),
('PAYMENT-CHEQUE','Cheques Paid','Cheques paid in/out of Bank'),
('PAYMENT-COUNTERPARTY','Counterparty Trades','Counterparty Trades'),
('PAYMENT-DEPOSIT-DIRECT','Valitor Deposit','Direct deposits (recorded in Firebird)'),
('PAYMENT-DEPOSIT-INDIRECT','Valitor Wallet Alignment','Indirect Deposits (due to client transactions in alternate ccy - not recorded in Firebird)'),
('PAYMENT-DIRECTDEBIT','Direct Debit','Direct Debits in/out of Bank'),
('PAYMENT-INTERESTCHARGED','Interest Charged','Bank interest charged'),
('PAYMENT-INTERESTINCOME','Interest Income','Bank interest income'),
('PAYMENT-VALITOR','Valitor Payment','Payment (less refunds) to Valitor'),
('RECEIPT-ALTAPAY','AltaPay Receipts','Receipts from AltaPay (Transaction less Fees)'),
('RECEIPT-ALTAPAY-FEES','AltaPay Fees','AltaPay Fees'),
('RECEIPT-ALTAPAY-TRANSACTION','AltaPay Transaction','AltaPay Transaction'),
('RECEIPT-CHARGES-INCOME','Valitor Income from Card Charges (CFX Idenfified)','Valitor Receipts/Income from Card Charges (ATM withdrawals, etc.)'),
('RECEIPT-CHARGES-OTHER','Valitor Income Charges (non CFX IDentified)','Valitor Income Charges (non CFX IDentified)'),
('RECEIPT-CLIENTMONEY','Client Money Deposited/Returned','Client Money Deposited/Returned'),
('TRANSFER-CLIENTMONEY','Client Money Transfer','Client Money Transfer'),
('TRANSFER-CONTRA','Contra Transfer','Contra Transfer'),
('TRANSFER-RBSCOLLATERAL','RBS Collateral Transfer','Transferred collateral'),
('TRANSFER-RESERVES','Transfer of Reserves (Lloyds Bank)','Transfer of Reserves (Lloyds Bank)')

PRINT 'END: Populate tblEventType [Records: ' + CAST(@@ROWCOUNT AS VARCHAR) + ']';
GO
/*
ALTER TABLE [rec].[tblInventoryEvent] ADD CONSTRAINT FK_tblEventTypeCode FOREIGN KEY ([EventTypeCode]) REFERENCES [rec].[tblEventType](Code)
*/
