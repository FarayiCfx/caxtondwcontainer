﻿SET NOCOUNT ON
GO
TRUNCATE TABLE [raw].[tblStatementIssuerValitorCcyLookup];
GO
PRINT 'START: Populate tblStatementIssuerValitorCcyLookup'
GO
INSERT INTO [raw].[tblStatementIssuerValitorCcyLookup]
	(ISOCodeNumeric,ISOCode,Name)
VALUES('124','CAD','Canadian dollar'),
('208','DKK','Danish krone'),
('344','HKD','Hong Kong dollar'),
('348','HUF','Hungarian forint'),
('36','AUD','Australian dollar'),
('392','JPY','Japanese yen'),
('554','NZD','New Zealand dollar'),
('578','NOK','Norwegian krone'),
('710','ZAR','South African rand'),
('752','SEK','Swedish krona/kronor'),
('756','CHF','Swiss franc'),
('826','GBP','Pound sterling'),
('840','USD','United States dollar'),
('978','EUR','Euro'),
('985','PLN','Polish zloty');
PRINT 'END: Populate tblStatementIssuerValitorCcyLookup [Records: ' + CAST(@@ROWCOUNT AS VARCHAR) + ']';
GO